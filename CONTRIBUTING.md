# Introduction

Any contribution to or working with the eELib is very welcome. In case you are in for any of the
two, this guide will provide some things to point out.
In case you want to contribute to the eELib, you either want to simply give a hint to us or want to
implement things on your own. The first one is fairly easy, as you can just create an issue with an
explanation of the problem (like a bug) or enhancement (like a new feature) and we will discuss the
topic afterwards. Please check for a similar issue before opening a new one.

Do not hesitate to get in touch if you a questions regarding the use or implementations, a request
or are just interested in a general discussion. Most of the current contributors can be reached via
their contact on the [elenia Hompage](https://www.tu-braunschweig.de/en/elenia).


__Author: elenia@TUBS__

Copyright 2024 elenia

The eELib is free software under the terms of the GNU GPL Version 3.


## Project Structure

The project structure is explained in detail in the
[folder structure part](https://eelib.readthedocs.io/en/main/about_eelib.html#folder-structure) of
our ReadtheDocs. The eELib mainly consists of:

- the documentation
- the main part including all models, data implementations, utility functions, and testing
- and examples for the models with the use of mosaik.


### Coding conventions

We also provide coding conventions for programming close to the
[PEP 8](https://peps.python.org/pep-0008) style guide for python code. It contains e.g. code layout
and naming conventions.

Our convention deviates only in a few aspects:
- Line lenghts is limited to 100 characters instead of 79.
- Tabs are the preferred indentation method (not spaces, whereby most IDEs easily recognizes 4
spaces as one tab)
- The preferred quotation mark is ".

Some adittional conventions are listed in the "_Coding Conventions_" of the git wiki.

Formatting should follow
[black style](https://black.readthedocs.io/en/stable/the_black_code_style/index.html).

API documentation is done with docstrings, following
[google conventions](https://github.com/NilsJPWerner/autoDocstring/blob/HEAD/docs/google.md).


## Testing

We are using unittests for the models and functionalities of the eELib, you can find the
[here](https://gitlab.com/elenia1/elenia-energy-library/-/tree/main/eelib/testing/unit?ref_type=heads).
Additionally, the test scenarios provided in the example folder (e.g. for a building case) represent
a good opportunity to test the interfunctionality of the models. Operability is shown if these test
scenarios can be executed, and a look at the results can at least in some cases reveal
disfunctionalities, if present. The execution of the set of tests is also described in the README of
the eELib testing folder.
As this testing strategy is far from perfect, we are open to any feedback or hints.


## Development Workflow

If you want to implement code on your own, you can either fork the project repository or just create
a new branch in this eELib repository - both options are fine and the preference just depends
on your style. When you fork the project, you have some more possibilities to keep the adjustments
private until they are finished.
In both cases you can then make adjustments, integrate new code and try things. Once you have
finished and validated as well as tested everything, you should create a merge request (the target
branch should then be the main branch in our eELib repository for both cases) and we will discuss
its implementation before merging it into the main branch of the repository.

Releases with new eELib version from the main crew of the elenia Institute are done from time to
time with different stuff resulting from dissertations, publications, research project work,
studies, or service projects. Until new stuff is released, it may take a while.
