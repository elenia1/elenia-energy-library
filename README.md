# elenia Energy Library - eELib

![eELib Logo](/docs/source/_static/eelib_logo.png "eELib Logo")

__Author: elenia@TUBS__

Copyright 2024 elenia

The eELib is free software under the terms of the GNU GPL Version 3.


## General Information

The eELib (elenia Energy Library) is the software tool for simulations concerning future power
systems for prosumers. The library with its functionalities and models can be used for various
simulative investigations regarding research or current challenges in the field of a distributed
electrical power system.
The goal of the eELib is creating a model library that is suitable for solving energy-related
questions around prosumers (consumers that are now also producing energy). This includes, among
other things, the ...

- ... creation and consideration of different energy supply scenarios (on building, district and grid
level, among others with different penetration levels of distributed facilities like PV).
- ... comparison of different operating strategies for energy management systems, including e.g.
variable tariffs, multi-use concepts, operator models or schedule-based flexibility.
- ... investigation of the impacts and interactions of prosumer households (e.g., sector coupling and
electrification) with the power grid to identify violations of grid limits.
- ... calculating the economic values of different use cases and strategies for devices and systems.
- ... investigation of innovative marketing strategies of market players in the spot and balancing
power markets.

The [documentation](https://eelib.readthedocs.io/en/) of the eELib will help you to get started, to work
with and to contribute to the eELib.


## Contribute

In case you are interested in contributing to the eELib, have a look at the __CONTRIBUTING__ file.


## How to cite eELib

If you want to cite the eELib in a work that you have done using the repository, you can use this:

Carsten Wegkamp, Henrik Wagner, Eike Niehs, Julien Essers, Marcel L�decke, Mattias Hadlak, Bernd Engel:
"eELib: Open-Source Model Library for Prosumer Power Systems and Energy Management Strategies",
Open Source Modelling and Simulation of Energy Systems (OSMSES) 2024, Vienna, Austria, 2024

## Contact

Do not hesitate to get in touch if you a questions regarding the use or implementations, a request
or are just interested in a general discussion. Most of the current contributors can be reached via
their contact on the
[elenia Hompage](https://www.tu-braunschweig.de/en/elenia/institute/team/research-staff).
