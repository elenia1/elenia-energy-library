`./docs` houses config and data for the public documentation of the eElib.

__Author: elenia@TUBS__

Copyright 2024 elenia

The eELib is free software under the terms of the GNU GPL Version 3.


# General
The API Reference is automatically created by pipelines on change in main. **Other** docs content may be added manually by creating an `.rst` file -  the source file representing an individual page - and including it in the toctree of an `index.rst` being the parent page for navigation. On merge in main, the pipeline will build an `.html` from each `.rst` file and deploy the page.

# Workflow
To view your changes, you can't upload them to the public docs, so you need a way to locally create `.html` files from your `.rst` files.

## with extensions
A convenient way is to use the extension **[reStructuredText](https://marketplace.visualstudio.com/items?itemName=lextudio.restructuredtext)** with the **[Esbonio Language Server](https://marketplace.visualstudio.com/items?itemName=swyddfa.esbonio)**. They provide an "open to side" live preview of the `.rst` file you're currently working on. Install the above extension in your VSC.

## without extensions
If the extension does not do what you want or for double-checking, you can create a local version of the docs with clickable navigation. To do this, open a terminal and execute the following commands.

**First Run (updates the API Reference)**

```
sphinx-apidoc -f --no-toc --templatedir=./docs/templates -d=2 -M -o ./docs/source/api_reference ./eelib
./docs/make.bat clean
./docs/make.bat html
```

**After Every Change**

```
./docs/make.bat clean
./docs/make.bat html
```
If an error occurs because "rst/autoapi files could not be found", they probably result due to
shifted or deleted files and the autoapi package still has the listed. Simply go to the
`source/autoapi/eelib` folder and delete the respective parts, this section will nonetheless be
newly generated with each new creation of the docs pages.

The html files are then located in `_build/html`. Open them in your favourite browser or html viewer and you should look at an exact version of the docs.

# Create a page
## 1. Create a new `.rst` in the correct location
For convenience, the `.rst` files should be saved in the hierarchy corresponding to the navigation levels in the final docs. E.g. tutorial articles are saved in `docs/source/tutorials` and referenced in the toctree of `docs/source/tutorials/index.rst`. The `index.rst` of the tutorials folder represents the landing page for the tutorial section and houses the navigation for all it's subpages.

## 2. Give a page title
Every page needs a title. It usually is the first thing in an `.rst` file and is formatted like this:
```
################################
Guide for Installation and Setup
################################
```

## 3. Add page to navigation
Add `<filename>` (filename without extension) to the toctree directive in the `index.rst` of **the same folder**. A toctree is a list of hyperlinks to docpages automatically using the **page title** as preview.

# Cheatsheet: Edit a page
To fill your newly created page or edit an existing one, you will need to use the [reStructuredText](https://www.sphinx-doc.org/en/master/usage/restructuredtext/index.html) language. On this link, you find an extensive reference, but following is a list of the most frequently used formatting options.
The extension **[reStructuredText](https://marketplace.visualstudio.com/items?itemName=lextudio.restructuredtext)** also provides snippet insertion to save time on understanding the syntax. [List of available snippets](https://docs.restructuredtext.net/articles/snippets)

## plain text
is just written in the `.rst` files without any additional format.

## bold, italics and code
`**bold**`, `*italics*` and ```Inline Code``` (using two backticks):

## underline section headers
```
Sections
========

Subsections
-----------

Subsubsections
^^^^^^^^^^^^^^
```

## lists
```
- This is a bulleted list.
- It has two items, the second
  item uses two lines.

#. This is an autonumbered list.
#. It has two items too.
```
Nested lists must be seperated with blank lines from their parent.
Plus, it is important to use exactly 3x space before the bullet character.
```
- this is
- a list

   - with a nested list
   - and some subitems

- and here the parent list continues
```
Even definition lists are natural.
```
term (up to a line of text)
   Definition of the term, which must be indented

   and can even consist of multiple paragraphs

next term
   Description.
```

## directives
reST uses directives to achieve more powerful formatting.

### toctree in an ``index.rst``
```
.. toctree::
   :maxdepth: 1
   :caption: Articles

   installation_setup
   git_workflow
```
Has the options `:maxdepth:` to define the shown navigation depth and the common option `:caption:` that is available for many directives. It is followed by the filenames (with ".rst") to be included in the navigation in order of their intended appearance.

### code with syntax color coding
```
.. code-block:: python
   :lineno-start: 23          # Show line numbers starting on line 23
   :emphasize-lines: 2,4,12   # highlight specific lines in the following code snippet

   <code snippet>
```

### note boxes
Content in these boxes is visibly accentuated.

```
.. note::

   This project is under active development.

   If note text runs over a line, make sure the lines wrap and are indented to
   the same level as the note tag.
```

```
.. warning::

    This is warning text. Use a warning for information the user must
    understand to avoid negative consequences.
```

Other available boxes are
- attention
- error
- hint
- important
- caution
- danger
- tip
- admonition

For more options, brwose through the existing pages and adapt the used options in your file or visit a reST reference.
