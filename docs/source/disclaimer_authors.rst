Disclaimer / Authors
####################

Author: `elenia@TUBS <https://www.tu-braunschweig.de/en/elenia>`_

Copyright 2024 elenia

The eELib is free software under the terms of the GNU GPL Version 3.

Despite careful control elenia assumes no liability for the content of external links. The operators
of such a website are solely responsible for its content. At the time of linking the concerned sites
were checked for possible violations of law. Illegal contents were not identifiable at that time. A
permanent control of the linked pages is not reasonable without specific indications of a violation.
Upon notification of violations, elenia will remove such links immediately.
