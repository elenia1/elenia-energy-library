.. Author: elenia@TUBS
   Copyright 2024 elenia
   This file is part of eELib, which is free software under the terms of the GNU GPL Version 3.

#########
Impressum
#########

| **Anschrift**
| Technische Universitaet Braunschweig
| elenia Institut fuer Hochspannungstechnik und Energiesysteme
| Schleinitzstr. 23
| 38106 Braunschweig
| Deutschland

| **Kontakt**
| Telefon +49 531 391-0
| E-Mail: elenia@tu-braunschweig.de
| Internet: https://www.tu-braunschweig.de/elenia

| **Institutsleitung**
| Prof. Dr.-Ing. Bernd Engel
| Prof. Dr.-Ing. Michael Kurrat
