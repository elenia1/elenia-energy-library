.. eELib documentation master file, created by
   sphinx-quickstart on Fri Feb 10 16:19:56 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.
   Author: elenia@TUBS
   Copyright 2024 elenia
   This file is part of eELib, which is free software under the terms of the GNU GPL Version 3.

Welcome to eELib's documentation!
=================================

The **eELib** (**e**\lenia **E**\nergy **Lib**\rary) is the software tool for simulating
**future power systems for prosumers**. The library, with its functionalities and models,
can be used for various simulative investigations regarding research or current challenges in the field of a
distributed electrical power systems.

The goal of the eELib is providing a model library that is suitable for solving energy-related questions
around prosumers (consumers that are now also producing energy). This includes, among
other things, the

- creation and consideration of different **energy supply scenarios** (on building, district and grid
  level, among others with different penetration levels of distributed facilities like PV).
- comparison of different **operating strategies for energy management systems**, including, e.g.,
  variable tariffs, multi-use concepts, operator models or schedule-based flexibility.
- investigation of the **impacts and interactions** of prosumer households, e.g., sector coupling and
  electrification **with the power grid** to identify violations of grid limits.
- calculating the **economic values of different use cases and strategies** for components and systems.
- investigation of innovative marketing strategies of different market players in the spot and balancing **power
  markets**.


Let's Get Started   :octicon:`rocket;2em`
=========================================

.. button-ref:: quick_start_guide
    :color: primary
    :expand:
    :shadow:
    :outline:

    Quick Start Guide

..

    *Information on how to get started, how to set up the repository with an installation guide,
    how to learn about the eELib structure and tips for helpful information*


.. button-ref:: tutorials/index
    :color: primary
    :expand:
    :shadow:
    :outline:
    :tooltip: Gives some easy tutorials to learn how to work with the eELib.

    Tutorials

..

    *Examples of different advancements of the eELib, like how to configure and run a scenario, how
    to implement a new model, and how to add a new operating strategy for an energy management
    system*


.. button-ref:: reference_manual/index
    :color: primary
    :expand:
    :shadow:
    :outline:
    :tooltip: Provides an overview of the setup of the eELib and its models and gives explanations
       to the classes and their methods, which are provided by the docstrings in the source code.

    Reference Manual

..

    *Information on the general structure and setup of the eELib, as well as the documentation
    of all classes, including their methods*


.. button-link:: https://gitlab.com/elenia1/elenia-energy-library
    :color: primary
    :expand:
    :outline:
    :shadow:

    View eELib in GitLab Repository :octicon:`git-branch;2em`


.. toctree::
   :maxdepth: 1
   :caption: Contents
   :hidden:

   Start Page <self>
   quick_start_guide
   tutorials/index
   reference_manual/index
   publications
   legals
   impressum
   disclaimer_authors
