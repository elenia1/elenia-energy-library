.. Author: elenia@TUBS
   Copyright 2024 elenia
   This file is part of eELib, which is free software under the terms of the GNU GPL Version 3.

######
Legals
######

**Address**

Technische Universitaet Braunschweig

elenia Institut fuer Hochspannungstechnik und Energiesysteme

Schleinitzstr. 23

38106 Braunschweig

Germany

Phone +49 531 391-0

Email: elenia@tu-braunschweig.de

Internet: https://www.tu-braunschweig.de/en/elenia

**Institute Management**

Prof. Dr.-Ing. Bernd Engel

Prof. Dr.-Ing. Michael Kurrat

**Disclaimer**

Despite careful control elenia assumes no liability for the content of external links. The operators
of such a website are solely responsible for its content. At the time of linking the concerned sites
were checked for possible violations of law. Illegal contents were not identifiable at that time. A
permanent control of the linked pages is not reasonable without specific indications of a violation.
Upon notification of violations, elenia will remove such links immediately.
