.. Author: elenia@TUBS
   Copyright 2024 elenia
   This file is part of eELib, which is free software under the terms of the GNU GPL Version 3.

############
Publications
############

The eELib is used within the following publications:

.. list-table::
   :widths: 1, 27, 1, 1, 1, 69
   :header-rows: 1

   * - Year
     - Title
     - DOI
     - Gitlab Tag
     - Data / Results
     - Description
   * - 2024
     - eELib: Open-Source Model Library for Prosumer Power Systems and Energy Management Strategies
     - | `10.1109/OSMSES62085 <https://ieeexplore.ieee.org/document/10668964>`__
       | `.2024.10668964 <https://ieeexplore.ieee.org/document/10668964>`__
     - 1.1.0
     - | `10.5281/zenodo <https://zenodo.org/records/12608544>`__
       | `.12608544 <https://zenodo.org/records/12608544>`__
     - Initial publication of the eELib. Illustrating its requirements and corresponding research
       questions to be answered. Providing information about the simulation process, implemented
       models and some exemplary simulation results.
   * - 2024
     - Empowering Collective Self-Consumption in Multi-Family Houses: User-Based Multi-Use of
       Residential Battery Storage Systems
     - tba
     - WSIS24_user-based_mu
     - | `10.5281/zenodo <https://doi.org/10.5281/zenodo.13821117>`__
       | `.13821117 <https://doi.org/10.5281/zenodo.13821117>`__
     - This research examines the user-based multi-use of residential BSS in multi-family houses
       as form of collective self-consumption. Hence, a new strategy for the user-based
       multi-use is implemented in eELib's HEMS model. Static allocation of the PVS and BSS
       capabilities among the MFH's households (HH), mapping the past German legal framework,
       is compared with forms of dynamic allocation mapping the adapted legal framework due to
       German 'Solarpaket I'.

If your publication using eELib is not listed, please contact us.

Cite eELib
----------

If you want to cite eELib, e.g. in a publication/work in which you use eELib, you can use this
publication:

-----------------------------------

*C. Wegkamp et al., "eELib: Open-Source Model Library for Prosumer Power Systems and Energy
Management Strategies," 2024 Open Source Modelling and Simulation of Energy Systems (OSMSES),
Vienna, Austria, 2024, pp. 1-6, doi: 10.1109/OSMSES62085.2024.10668964.*
