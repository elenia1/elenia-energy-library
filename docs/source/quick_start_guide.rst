.. Author: elenia@TUBS
   Copyright 2024 elenia
   This file is part of eELib, which is free software under the terms of the GNU GPL Version 3.

#################
Quick Start Guide
#################

Welcome to the eELib! We are here to help you get started!


**Basic steps to get started with the eELib**

First, clone the repository and set up the environment using the instructions in the
:doc:`Installation Guide <reference_manual/installation_setup>`.

.. note::
   The eELib will soon be available to install via `PyPI - the Python Package Index <https://pypi.org/>`_.

Next, become familiar with the structure of the eELib in the
:doc:`Reference Manual <reference_manual/index>`.
Last, try one of the test scenarios or :doc:`set up a scenario <tutorials/setup_run_sim>` to run
your first simulation.


**Learn how to use the eELib with some short tutorials**

Try implementing a :doc:`new EMS strategy <tutorials/implement_strategy>`.
:doc:`Add new implementations like models <tutorials/config_model>`.
You can generally use the :doc:`Tutorials <tutorials/index>` section for further detailed
instructions and information on the setup of existing implementations.


**Some additional helpful examples**

:doc:`Working with Git for version control <reference_manual/git_workflow>`


**Frequently asked questions**

Have a look at the :doc:`FAQs <reference_manual/faq_glossary>`.
