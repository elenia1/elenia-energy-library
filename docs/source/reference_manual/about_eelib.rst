.. Author: elenia@TUBS
   Copyright 2024 elenia
   This file is part of eELib, which is free software under the terms of the GNU GPL Version 3.

###########
About eELib
###########

The implementations of the eELib hold some characteristics that are explained in the following. The
focus is on the architecture of simulations (we use the co-simulation framework mosaik to run
simulations with eELib models) and the design of simulations (event-based such that we take
interdependence between models into account).


Coupling with mosaik and Architecture of a Simulation
=====================================================

One major thing to know for working with and using the eELib is that we are **using a co-simulation
tool** (`mosaik <https://mosaik.offis.de/>`_) to run simulations, as the eELib itself just provides
the implementation of models and no configuration to run a simulation, respectively, no central
simulator. To make the above mentioned investigation cases possible, one needs to couple the
different models of the library in scenarios to be created, e.g., a **PV** system with the energy
management system (**EMS**). For this purpose, an :term:`Orchestrator` is to be used, which performs
the exchange of data sets between models, calls the calculation of the individual models and controls
the general model flow as well as the coupling with a database. `mosaik <https://mosaik.offis.de/>`_
is intended for these tasks. Certain input values are assumed, data sets are calculated internally,
and output data sets are issued. See the :doc:`eELib docomentation part on mosaik <mosaik>` for
more information. The eELib should definetily be usable with other orchestrators of a simulation,
but the explanations in this documentation are done for mosaik and its simulation orchestration.
Additionally, the eELib provides simulators to its models that serve the prupose of APIs for a
simulation with mosaik.

The following graphic illustrates the architecture of a simulation and the usage of mosaik in it.
To explain the terms used for the parties, look at the
:doc:`glossary <faq_glossary>`. While the eELib provides implementations for the models and
their simulators, mosaik is used for a simulation. Additionally, a scenario script and data for the
scenario (like parameterization of the model entities and linking of entities) have to be provided.
Examples for these last two are provided within the eELib in an *examples* part.

.. figure:: ../_static/architecture_overview.*
   :width: 50%
   :align: center
   :alt: A flow chart of the parties within a simulation.

   The architecture of a simulation using mosaik with the eELib.

For a setup of a simulation, have a look at the following illustration. It depicts the physical and
data connections between the different parts of the eELib.

.. figure:: ../_static/simulation_setup.*
   :width: 55%
   :align: center

   The setup of a (grid) simulation with the eELib.


Event-Based Simulation
======================
The computations within the eELib - using mosaik - are executed in an **event-based** manner. This implies the
simulation process to depend on the triggering of events. Independent of a :term:`step size <STEP_SIZE_IN_SECONDS>` -
the length of a simulation step in seconds - the process within one simulation step is executed by
event triggering, as depicted in the following figure. To add to the `explanation of mosaik
<https://mosaik.readthedocs.io/en/latest/tutorials/sametimeloops.html>`_, the execution of events
depends on triggering such events. E.g. the simulation of a BSS is triggered by an EMS
sending a power set value to the BSS. For this, mosaik knows about ...

* ... when each model has to be called for calculation.
* ... which outputs for the models are send to which inputs for other models.

.. figure:: ../_static/execution_process_eventbased.*
   :width: 100%
   :align: center
   :alt: an illustration of mosaik's simulation process

   The simulation process of event-based simulation.

#. mosaik calls the calculation of the entities via the simulator
#. models calculate output with the already given input
#. simulator returns the timestep, when the models have to be calculated next
#. mosaik sends the output to a connected entity (e.g. the power generation of a PV system to the
   HEMS)
#. mosaik then calls the calculation of the next entity for which all inputs have been collected
   (this is done by means of the word "triggering", so the finalized calculation of one entity triggers
   the calculation of another entity...)

This is done within a single timestep as long as data is sent between the entities so that the
renewed calculation of a model entity is triggered. mosaik calls the execution of the models in the
way they are connected to each other and send values each way. If everything is finished for this
timestep, mosaik advances to the next timestep and the simulation process carries on.

The following figure better shows the implementation of the event-based process within one
simulation step. It shows that when a model is called, the inputs for the models are provided by
mosaik and set by the simulator. Afterward, the models are stepped, and ultimately, the
simulators are called to extract the (by mosaik requested) result outputs from the models.

.. figure:: ../_static/simulator_stepping_process.*
   :width: 70%
   :align: center
   :alt: an illustration of mosaik's stepping process for one simulator of model entities

Handling endless same-time-loops
--------------------------------

A same-time loop occurs when models are sending outputs back and forth, e.g., if a HEMS sends a set
value to the BSS, the BSS sends back new power values (and new limits), and the HEMS may send a new
set value. Typically, these outputs converge in a few loops to a final value that does not change
anymore. In case of very small changes to the value sent before (smaller than a defined adoption
tolerance), the simulators that give back data to mosaik do not send the newly calculated output,
see :ref:`the get-data explanation for the simulators <Handling of output data in get-data>` .
If so, the sending of new output values with the ``get_data()`` method of the simulators is exited
and the simulation for this step is finished, going on to the next simulation step.

This behaviour can cause problems when the output values are continuously changing. This can
typically have two causes:

* Two (or more) outputs commute between two values, e.g. the HEMS set value changes from a to b,
  back to a and so on. Maybe the BSS always adjusts the efficiency based on the power value.
* An optimization comes to different solutions because the solvers "search for corners" in the
  room of solutions and may come to different "corners" by going in different directions for the
  same problem formulation. For example, for a household with static tariffs charging the EV at 6pm
  or 7pm does not make a difference, resulting in the same objective function value. This though
  should only impose a probleme if the actually sent output values (e.g. the set value that result
  from the optimization) are changing between the optimizations, which could be handled by not
  allowing the optimization to be run for every calculation (in the step function), but to limit it
  to a few calls or checking if optimization results, e.g. schedules have already been calculated.
