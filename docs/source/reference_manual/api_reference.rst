API Reference
=============

The API reference provides detailed descriptions of eElib's classes and methods.
This is taken from the implementations of the models, which can be taken from the
`public Gitlab-Repository <https://gitlab.com/elenia1/elenia-energy-library>`_.

.. toctree::
   :titlesonly:
   :maxdepth: 2

   ../autoapi/eelib/index
