.. Author: elenia@TUBS
   Copyright 2024 elenia
   This file is part of eELib, which is free software under the terms of the GNU GPL Version 3.

##############
FAQ & Glossary
##############


Glossary
========

.. glossary::
   Model
      Representation of real processes (assuming simplifications), using the general procedure of "*Input* - *internal calculation* - *output*".

   Orchestrator
      Coordination of the whole simulation procedure and coupling of the models (**mosaik**).

   Simulator
      API/interface for communication between the orchestrator mosaik and the entities of the specific model.

   Entity
      Created instance of a model type. In "ename", "etype", "eid" etc. the "e" stands for entity.

   PSC
      We use the **passive sign convention** (german: **Verbraucherzaehlpfeilsystem**), therefore loads are positive while generation is negative.

   Forecast
      Prediction of a behaviour for a defined time horizon in the future, e.g. power values for the upcoming 24h steps for a household base load from a csv reader.

   Schedule
      Calculated set values for a defined time horizon in the future, e.g. target power values for the upcoming 24h steps for a battery storage system.

   time-based
      Used for time-discrete simulation of models, e.g. if just some data is collected to be transmitted to other models.

   event-based/hybrid
      Used for triggered simulation of models, e.g. if model calculation can be triggered by inputs like the receiving of a power set value.

   weak and strong connections for mosaik simulations
      As you may be tasked with adding a new model or directly adding new connections between
      existing models, you may come across connections between models in multiple directions -
      either directly or within a circle of multiple models. When that happens, you have to add the
      order of the connections (into ``model_connections/connect_directions_config.json``) for
      mosaik to know the order in which the models have to be called. Weak connections are first
      replaced by None-values such that their recieving model is first called and afterwards the
      strong connection is taken.


Parameters used in test scenarios
=================================

.. glossary::

    ``SIM_CONFIG``
        information about used models/simulators, their paths and what data to store for each model

    ``START``
        simulation starting time (e.g. 2023-01-01 00:00:00)

    ``END``
        simulation ending time (e.g. 2023-01-02 00:00:00 for a full day calculation)

    ``N_STEPS``
        number of steps that should be simulated, calculated from start and end time (e.g. 96 steps for a day with 15 min timesteps)

    ``STEP_SIZE_IN_SECONDS``
        length of one (pre-defined) timestep in seconds (e.g. 15 min steps: 15*60 sec = 900 sec)

    ``USE_FORECAST``
        True-False value whether to consider forecasts in the simulation or not


Devices Glossary
================

.. glossary::

    EMS
        Energy Management System: Unit for the automated collection, controlling and
        (optimised) coordination of energy flows in a closed system, considering a specified goal
        (technical, economical, etc.) for the operational coordination within a system

    BSS
        Battery Storage System: Device to store energy in electro-chemical way

    PV
        Photovoltaic (system): Device to convert solar radiation into electrical power, combination
        of solar module and AC-DC inverter

    EV
        Electric Vehicle: Mobility device, alternative to conventional combustion-fueled vehicles

    CS
        Charging Station: Device to charge the EV Battery when connected

    HP
        Heat Pump: Device to convert electrical into thermal power using ambient energy.


FAQ
===

What is the process of building Model-Factories?
    ``examplesim = world.start("ExampleSim", eidprefix="Model")``

    The resulting ``examplesim`` is an entity of the class ``mosaik.scenario.ModelFactory``.
    It can create and store the entities of an example model.

What can one do in case of an ImportError when running the example scenarios?
    ``pip install -e .``

What about an error like ``Could not import module: No module named 'XXX' --> No module named 'XXX'``
    when running the example scenarios (after merging main into your branch)?

    Maybe some new package was added to the requirements, try to install them again by
    ``pip install -r requirements.txt`` and see wether any packages were newly installed.

What attributes does a model have?
    For that you should have a look at the ``META`` of the simulator or optionally within the model
    class itself.

    .. code-block::
        :lineno-start: 13
        :caption: e.g. EMS_simulator.py (01/24)

        # SIMULATION META DATA
        META = {
            "type": "hybrid",
            "models": {
                "HEMS": {
                    "public": True,
                    "params": ["init_vals"],
                    "attrs": [
                        "q",
                        "p",
                        "p_max",
                        "p_min",
        ...

What inputs does a model have?
    Have a look at the ``VALID_PARAMETERS`` stored in each model class:

    .. code-block::
        :lineno-start: 20
        :caption: e.g. EMS_model.py (01/24)

        # Valid values and types for each parameter that apply for all subclasses
        _VALID_PARAMETERS = {
            "cs_strategy": {
                "types": [str],
                "values": ["max_p", "balanced", "night_charging", "solar_charging"],
            },
            "bss_strategy": {
                "types": [None, str],
                "values": [None, "reduce_curtailment"],
            },
        }

    Exemplarily the ``model_data`` of the test scenarios set (at least some of) these parameters and
    their initial values for the models:

    .. code-block::
        :lineno-start: 1
        :caption: e.g. model_data_building.json (01/24)

        {
            "ems": [
                {
                    "cs_strategy": "balanced"
                }
            ],

What connections does a model have?
    For that the eELib provides the ``model_connections/model_connect_config.json`` file, where all
    of the ``FROM-TO-CONNECTIONS`` are given for each model type implemented in the eELib:

    .. code-block::
        :lineno-start: 10
        :caption: e.g. model_connect_config.json (01/24)
        :emphasize-lines: 5, 6

        "HEMS_default": {
            "PVLib": [["p_set_pv", "p_set"]],
            "BSS": [
                [
                    "p_set_storage",
                    "p_set"
                ]
            ],
            "ChargingStation": [
                [
                    "p_set_charging_station",
                    "p_set"
                ]
            ],
            "Heatpump": [
                [
                    "p_th_set_heatpump",
                    "p_th_set"
                ]
            ],
            "grid_load": [["p_balance", "p_w"], ["q_balance", "q_var"]]
        },

    .. code-block::
        :lineno-start: 111
        :caption: e.g. model_connect_config.json (01/24)
        :emphasize-lines: 4,5, 8, 9, 12, 13

        "BSS": {
            "HEMS_default": [
                [
                    "p_discharge_max",
                    "p_min"
                ],
                [
                    "p_charge_max",
                    "p_max"
                ],
                [
                    "p",
                    "p"
                ]
            ],
        },

    Extracted from these two examples above can be the sending of values for attributes ``p``,
    ``p_min`` and ``p_max`` from ``BSS`` to ``HEMS`` and the return of a value for ``p_set``.

What can I do in case of simulations that raise an error because the maximum amount of iterations
    for a simulation step was exceeded?

    See the :ref:`explanation <Handling endless same-time-loops>` in the section about mosaik's
    event-based simulation behaviour.


Units
=====

Make use of the `SI-units <https://en.wikipedia.org/wiki/International_System_of_Units>`_!!

- **Power**: W
- **Time (e.g. simulation time)**: s
- **Energy**: Wh
- **Temperature**: Degree celsius
- ...
