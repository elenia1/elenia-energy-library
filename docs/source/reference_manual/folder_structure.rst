.. Author: elenia@TUBS
   Copyright 2024 elenia
   This file is part of eELib, which is free software under the terms of the GNU GPL Version 3.

################
Folder Structure
################

This is to give an overview of where different parts of the eELib are stored:

- :mod:`docs` : Files for the documentation with AutodocSphinx into the GitLab Pages style.
  Documentation is stored in ``.rst`` files within the :mod:`source` subfolder.

- :mod:`eelib` : This stores the main part of the elenia Energy Library, as it contains the models and
  all other functionalities.

  - :mod:`core <eelib.core>` : Here all of the models are stored. This is divided into the devices (like PV
    system or electric vehicle), control models (like energy management system), grid models
    (for power flow calculation), forecasting, and market models (like intraday market).

  - :mod:`data <eelib.data>` : This contains all models that are "just" retrieving input data (like a simple
    csv-reader). It also includes the functionalities for simulation data to be collected and
    assessed.

  - :mod:`model_connections <eelib.model_connections>` : Exchange values between all models of the eELib and functionalities to
    extract them.

  - :mod:`testing <eelib.testing>` : Contains all of the testing for the models and functionalities of the library.

  - :mod:`utils <eelib.utils>` : Contains helper functions and classes, exemplarily for running a simulation or
    evaluating and plotting/presenting outputs of simulations.

- :mod:`examples` : This folder provides data and scripts for some test scenarios and provides an
  overview of how the eELib can be used. The data subfolder contains some exemplary data and also
  contains results after running simulations.

The highest folder also contains various files that are used for setup of the environment, gitlab
communication, a ``README`` for information etc.

.. seealso::
  You can familiarize with the folder structure of the :mod:`eElib package <eelib>` by exploring
  the :doc:`API Reference <api_reference>` and browsing through the source code documentation in
  there.
