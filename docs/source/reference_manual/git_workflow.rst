.. Author: elenia@TUBS
   Copyright 2024 elenia
   This file is part of eELib, which is free software under the terms of the GNU GPL Version 3.

############
Git Workflow
############

This wiki page will give detailed instruction on how to work with the commands in Git, especially
via Visual Studio Code (VSC)! It will not provide a full overview of how the git process works but
informs about the necessary commands.


**Create Personal Access Token**
===================================

#. In Gitlab, via 'Edit profile', you go to 'Access Tokens' and create a Token.

   - The name is irrelevant and the expiration date can be set to a year.
   - You can select all scopes.

#. The Token has to be saved somewhere, because you have to give it in order to establish a
   connection from your local repository to the online repository.


**Cloning**
==============

The first thing that has to be done to collaborate in the eELib is cloning the online Git repository
in a local copy onto your local computer.

#. You can do this via Visual Studio Code by typing in ``gitcl`` in the command line and following
   the processes.

   - Use either the URL or SSH-key from the repository.
   - When asked for username and password you have to give your **GitLab username** and the **Personal Access
     Token**.
   - This process can also be done via GitBash, when going to the target folder and typing in
     ``git clone <REPO-URL>``.

#. It is recommended to end this process by saving the Personal Access Token in this Git project
   by running ``git remote set-url origin https://oauth2:[PersonalAccessToken]@<GIT-REPO>``
   via GitBash, then your Git program will not ask for the Token every time.
#. You should also set your username and mail adress for when using Git by running
   ``git config --global user.name "Your Name"`` resp.
   ``git config --global user.email "youremail@yourdomain.de"``.


**Visual Studio Code**
=========================

- Visual Studio Code allows easy version management via Git.
- You should use the window "Source Control" (left side) to always see the current changes you
  did on the code.
- When using the Extension `Git Graph <https://marketplace.visualstudio.com/items?itemName=mhutchie.git-graph>`_ ,
  you can also have an overview of all the changes that have been done in the repository
  (possibly by others too!).


**Change the Branch**
========================

- Before any adaption to the code, you should always check your currently selected branch.
- The default is always the ``main`` branch, in which you should **not change anything!**
- Changing the selected branch can be done by using the command "Source Control -> Branches ->
  Switch to Another Branch" and then selecting that specific branch. Or you can use the command
  ``check-out to`` to switch to another branch.

.. hint:: You can only change the selected branch when you currently have no changes in the files.


**Saving Changes (Commit)**
==============================

The next thing is saving changes, that were coded locally - this is done by 'committing' the code,
which is still a local process but kind of saves the stage at the moment.

#. You have to "stage" the relevant code files by pressing the "plus" (+) sign.
#. Then type in a fitting commit message (What have you done? Short!).
#. Last thing is to hit the commit button ("check" sign).


**Getting Changes from Online Repository (Pull)**
====================================================

When others make relevant code changes, which you might need, you can get their changes from
the online repository by "pulling" them.

#. For that, you must have everything committed - so do that first.
#. To pull the current state of the branch from the online repository, simply click on the 3
   points and select "Pull".
#. In case any merge conflicts occur, see step :ref:`**Merge Changes Into Your Local Branch**`


**Sending Changes to Online Repository (Push)**
==================================================

- When having implemented relevant code changes, which other programmers/users might need, AFTER
  VALIDATING THEM you can (and should regularly) push your saved commits.
- This should only be done after pulling the online repository first.
- Also, all current changes have to be staged.
- To push, simply click on the 3 points and select "Push".


**Merge Changes Into Your Local Branch**
========================================

- Merging Changes is generally needed, when two programmers changed parts of the same code and
  Git does not know, which "solutions" it should select.
- This process should be done **carefully**, as nobody wants to discard changes, that have been done
  by others.
- Merging can be needed in two cases: when you merge another branch into your own local branch,
  or when you pull the online repository status into your local state.
- In the process, click on the files with "*Merge Conflicts*", go through the problems and try to
  find a solution that approriately takes both solutions into account.
- If you have concerns, ask for help!
- Merges with merge conflicts always have to be committed after the conflicts have been solved
  and the file is saved and staged.


**Create a New Branch for each Topic**
=========================================

The *work flow* is supposed to include the creation of a new branch for each problem. So, if you
want to create a new model, you create a branch named ``model/[model_name]`` and work
on this problem only in this branch. Or if you want to fix something existing, create a branch
named ``bugfix/[problem_name]`` etc.

This is a way to structurize the current work and additions to the joint use of the library
in the ``main`` branch.


**Compare the State of two Branches**
=====================================

When working on a topic in a specific branch, it can be beneficial before a merge request to review
the changes that one made. So after merging the current state of the ``main`` branch into your own
branch, you can compare these two branches (without creating a merge request first) by going to your
branch in the Gitlab repository and clicking ``Compare``. This can also help to check whether the
merge of the ``main`` branch into your branch worked.


**Merge Your Changes into the main Branch**
===============================================

When you

#. completed a task in a specific branch (don't use the ``main`` branch for that!)
#. and tested your stuff,

you can make this accessible for others via the ``main`` branch.

For that you should ...

#. ... merge the current state of the main branch into your (feature) branch.
#. ... push your changes into the remote branch.
#. ... go to the GitLab Repository and create a merge request for your branch into ``main``. There
   you should also assign a *developer* to shortly check your updates and may even assign a
   *reviewer*.

The merge will then be completed and afterwards your changes are also part of the ``main`` branch.
