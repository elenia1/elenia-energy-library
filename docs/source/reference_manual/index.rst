.. Author: elenia@TUBS
   Copyright 2024 elenia
   This file is part of eELib, which is free software under the terms of the GNU GPL Version 3.

################
Reference Manual
################

The reference manual provides the documentation of the eELib regarding its architecture and the
source code reference.


.. button-ref:: about_eelib
    :color: primary
    :expand:
    :shadow:
    :outline:

    General Information about the eELib

..

    *Describing how eELib's architecture is set up, why and how we use mosaik for simulations, and how
    the simulations are working for event-based triggering of the models*


.. button-ref:: faq_glossary
    :color: primary
    :expand:
    :shadow:
    :outline:

    Glossary and FAQ

..

    *Glossary of some frequently used terms, devices, and units as well as parameters for
    scenario configuration and frequently asked questions (FAQ)*


.. button-ref:: forecast_schedule
    :color: primary
    :expand:
    :shadow:
    :outline:

    Forecasts and Schedules

..

    *How forecasts can be created, how to integrate them into simulations, and how to use them (to
    create schedules)*


.. button-ref:: git_workflow
    :color: primary
    :expand:
    :shadow:
    :outline:

    Helpful Tips for the Usage of Git Program

..

    *Instruction on how to work with various commands in Git (focused on Visual Studio Code)*


.. button-ref:: installation_setup
    :color: primary
    :expand:
    :shadow:
    :outline:

    Installation and Setup Guide

..

    *Full Guide for the installation of Python, Git, Visual Studio Code, your local working copy of
    the meELib, etc. and how to setup your working environment*


.. button-ref:: mosaik
    :color: primary
    :expand:
    :shadow:
    :outline:
    :ref-type: doc

    mosaik

..

    *Describing the co-simulation framework mosaik and providing information on helpful mosaik tutorials*


.. button-ref:: folder_structure
    :color: primary
    :expand:
    :shadow:
    :outline:

    Folder Structure

..

    *Structure of the eELib's models and functionalities based on folders and sub-folders*


.. button-ref:: model_overview
    :color: primary
    :expand:
    :shadow:
    :outline:

    Overview of implemented Models

..

    *Listing of all implemented models in the eELib with their folder path and a tabular listing of
    all parameters for each model, including possible input types and values to initialize the model
    entities*


.. button-ref:: api_reference
    :color: primary
    :expand:
    :shadow:
    :outline:

    API Reference

..

    *Explanations and documentation to the classes resp. modules and their methods, which are
    provided by the docstrings in the source code*


**Indices and Tables**

.. button-ref:: genindex
    :color: primary
    :expand:
    :shadow:
    :outline:

    Alphabetical ordering of all classes, methods, modules, and parameters

.. button-ref:: modindex
    :color: primary
    :expand:
    :shadow:
    :outline:

    Alphabetical ordering of all eELib modules


.. toctree::
   :maxdepth: 1
   :caption: Articles
   :hidden:

   about_eelib
   faq_glossary
   forecast_schedule
   git_workflow
   installation_setup
   mosaik
   folder_structure
   model_overview
   api_reference
