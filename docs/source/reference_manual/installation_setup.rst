.. Author: elenia@TUBS
   Copyright 2024 elenia
   This file is part of eELib, which is free software under the terms of the GNU GPL Version 3.

######################
Installation and Setup
######################

Installation of Python
======================

#. Download Python version 3.10.X from `Python Downloads <https://www.python.org/downloads/>`_
#. When the download is finished, double-click the installer.
#. Select *Install for all users* and click *Next*.
#. The default installation path is okay. Click *Next*.
#. In the *Customize Python* page, click on the *Python* node and select *Entire feature will be installed on local hard
   drive*. Make sure that *Add python.exe to Path* is enabled. Click *Next*.

   - If not, the Python Path has to be added to system variables by hand afterwards

#. When Windows asks you to allow the installation, do so. Wait for it to happen. Click
   *Finish*

.. note::

  This will also install the Python package manager pip. For checking and if not, see
  `Pip getting started <https://pip.pypa.io/en/stable/getting-started/>`_
  (can also be used generally for working with pip).


Installation of Git
===================

#. In order to work with version control management, you need to install Git
#. Visit the `Git Getting Started Page <https://git-scm.com/book/en/v2/Getting-Started-Installing-Git>`_
   and ...
#. Install Git by downloading the `Installer <https://git-scm.com/downloads>`_ and clicking through
   the setup
#. For Git steps/processes afterwards, have a look at the :doc:`Git Workflow <git_workflow>` page


Installation and Setup of Python IDE (VSC)
==========================================

Easier than using command window or PowerShell is the use of an IDE (integrated development
environment) for Python, especially when working with the code.

#. Decide for an IDE. There are several good options: PyCharm, Visual Studio Code, Jupyter Notebook,
   IDLE, Spyder, Pydev

   - We recommend Visual Studio Code (VSC) for eELib, so this tutorial will be based on VSC

#. If needed, download VSC from the `VSC Homepage <https://code.visualstudio.com/>`_ and install it
#. Configuration of User Settings

   #. Install some Extensions (:kbd:`Ctrl` + :kbd:`Shift` + :kbd:`X`)

      - Python (for working with python programming language)
      - autoDocstring (for simple creation of docstrings for classes and methods)
      - H5Web (for a quick and easy view of the HDF5 simulation output)
      - Other possibly helpful extensions: Black Formatter, Git Graph, GitLens, Rainbow CSV

   #. Settings -> Editor: Default Formatter -> set to "Black Formatter"
   #. Settings -> Text Editor -> Formatting -> Format on Save should be true
   #. Settings -> Features -> Notebook -> Format on Save should be true
   #. For max line lenght of 100 set: Settings -> Text Editor -> Rulers -> click
      "Edit in settings.json" and then set "'editor.rulers': [100]" (you can instantly see the
      vertical line shift to the right when saving the file)


Cloning eELib Repository to your Local Workspace
================================================

#. Clone the Git Repository with VSC: When all folders are closed, select *Clone (Git) Repository*

   - https address: "https://gitlab.com/elenia1/elenia-energy-library"

#. You can also use GitBash (or other Git programs) for cloning (personal choice, up to you)

   .. code-block:: bash

     $ git clone https://gitlab.com/elenia1/elenia-energy-library

#. Afterwards follow the steps you will be guided through, as you probably need to give your
   username and password for Gitlab

.. note:: The path to the project folder will now be noted as *<Project Folder>*.


Setup Working in VSC with eELib
===============================

#. Open VSC and navigate to *<Project Folder>*
#. Open new Terminal: PowerShell is recommended (GitBash or Command Window are possible too, but
   not as mighty)
#. Create a virtual environment in the directory of your repository:

   #. Run ``python -m venv <_VENV-PATH_>``

      - For virtual environment path ``<_VENV-PATH_>``, we typically use ``.venv`` (which for this
        tutorial we will now exemplarily do so)
      - Accept VSC for acknowledging new environment, if it is detected

   #. Run the activation script: ``.venv\scripts\activate``

      - In case Scripts can't be executed, you have to adjust the Execution Policy by running
        ``Set-ExecutionPolicy Bypass -Scope CurrentUser -Force`` and try again
      - Check: If successful, the prompt should now start with ``(.venv)``
      - Check whether the correct python interpreter is selected: ``python --version`` (Output: Python 3.10.X)

#. Install requirements into the virtual environment

   - If VSC explorer isn't already in the repository folder, you have to navigate there
   - Run ``pip install -r requirements.txt``

#. If a new release of pip is available, you can update it via ``python.exe -m pip install --upgrade pip``
#. Check: If you open a Python file, the selected virtual environment is listed in the lower right corner in the blue row
   *('.venv')*
#. Install Configurations for Processes, that are executed before each commit

   - Run ``pre-commit install``


Test successfull installation
=============================
1. You can test the functionality and correct installation by running a testcase

   - Open the file ``test_scenario_building.py`` in the examples folder and click on the *Run* sign
     in the upper right corner (Quick start typicall possible with :kbd:`F5`)
   - (Or run ``python test_scenario_building.py`` in the terminal)

2. If you are not able to run the ``test_scenario`` and get the error ``no module named 'eelib'`` ...

   - If your are in your *<Project Folder>*: Execute ``pip install -e.``
   - Otherwise you have to insert the path: Execute ``pip install -e <Project Folder>``
