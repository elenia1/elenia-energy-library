Model Overview with Parameters
==============================
(State: 2024-05-29)


GridEMS
-------

path: :mod:`eelib.core.control.grid`

============================ ===================== =================================================================
INPUT PARAMETER              ALLOWED TYPES         POSSIBLE VALUES
============================ ===================== =================================================================
use_14a_enwg                 ['bool']              [True, False]
grid_tariff_model            ['str']               ['flat-rate', 'percentage', 'time-variable']
strategy                     ['str']               ['optimal']
energy_price_static          ['float', 'int']      (0, inf)
capacity_fee_dem             ['float', 'int']      (0, inf)
capacity_fee_gen             ['float', 'int']      (0, inf)
capacity_fee_horizon_sec     ['int']               (1, inf)
grid_model_config            ['dict']              None
============================ ===================== =================================================================


HEMS
----

path: :mod:`eelib.core.control.hems`

============================ ===================== =================================================================
INPUT PARAMETER              ALLOWED TYPES         POSSIBLE VALUES
============================ ===================== =================================================================
cs_strategy                  ['str']               ['max_p', 'balanced', 'night_charging', 'solar_charging']
bss_strategy                 ['str']               ['surplus', 'reduce_curtailment']
============================ ===================== =================================================================


HEMS_default
------------

path: :mod:`eelib.core.control.hems`

============================ ===================== =================================================================
INPUT PARAMETER              ALLOWED TYPES         POSSIBLE VALUES
============================ ===================== =================================================================
cs_strategy                  ['str']               ['max_p', 'balanced', 'night_charging', 'solar_charging']
bss_strategy                 ['str']               ['surplus', 'reduce_curtailment']
============================ ===================== =================================================================


GCP_Aggregator_HEMS
-------------------

path: :mod:`eelib.core.control.hems`

============================ ===================== =================================================================
INPUT PARAMETER              ALLOWED TYPES         POSSIBLE VALUES
============================ ===================== =================================================================
cs_strategy                  ['str']               ['max_p', 'balanced', 'night_charging', 'solar_charging']
bss_strategy                 ['str']               ['surplus', 'reduce_curtailment']
============================ ===================== =================================================================


HEMS_forecast_base
------------------

path: :mod:`eelib.core.control.hems`

============================ ===================== =================================================================
INPUT PARAMETER              ALLOWED TYPES         POSSIBLE VALUES
============================ ===================== =================================================================
cs_strategy                  ['str']               ['max_p', 'balanced', 'night_charging', 'solar_charging']
bss_strategy                 ['str']               ['surplus', 'reduce_curtailment']
forecast_horizon_hours       ['int']               (0, inf)
forecast_frequency_hours     ['int']               (0, inf)
use_forecast                 ['bool']              None
forecast_type                ['str']               ['household_only']
============================ ===================== =================================================================


HEMS_forecast_default
---------------------

path: :mod:`eelib.core.control.hems`

============================ ===================== =================================================================
INPUT PARAMETER              ALLOWED TYPES         POSSIBLE VALUES
============================ ===================== =================================================================
cs_strategy                  ['str']               ['max_p', 'balanced', 'night_charging', 'solar_charging']
bss_strategy                 ['str']               ['surplus', 'reduce_curtailment']
forecast_horizon_hours       ['int']               (0, inf)
forecast_frequency_hours     ['int']               (0, inf)
use_forecast                 ['bool']              None
forecast_type                ['str']               ['household_only']
============================ ===================== =================================================================


HEMS_forecast_opt
-----------------

path: :mod:`eelib.core.control.hems`

============================ ===================== =================================================================
INPUT PARAMETER              ALLOWED TYPES         POSSIBLE VALUES
============================ ===================== =================================================================
cs_strategy                  ['str']               ['max_p', 'balanced', 'night_charging', 'solar_charging']
bss_strategy                 ['str']               ['surplus', 'reduce_curtailment']
forecast_horizon_hours       ['int']               (0, inf)
forecast_frequency_hours     ['int']               (0, inf)
use_forecast                 ['bool']              None
forecast_type                ['str']               ['household_only']
============================ ===================== =================================================================


BSS
---

path: :mod:`eelib.core.devices.bss`

============================ ===================== =================================================================
INPUT PARAMETER              ALLOWED TYPES         POSSIBLE VALUES
============================ ===================== =================================================================
soc_init                     ['float']             (0, 1)
e_bat_rated                  ['int']               (0, inf)
p_rated_discharge_max        ['int']               (-inf, 0)
p_rated_charge_max           ['int']               (0, inf)
discharge_efficiency_init    ['float']             (0, 1)
charge_efficiency_init       ['float']             (0, 1)
status_curve                 ['bool']              None
loss_rate                    ['float']             (0, 1)
dod_max                      ['float']             (0, 1)
status_aging                 ['bool']              None
soh_init                     ['float']             (0, 1)
soh_cycles_max               ['float']             (0, 1)
bat_cycles_max               ['int']               (0, inf)
bat_cycles_init              ['int']               (0, inf)
step_size                    ['int']               (0, inf)
bat2ac_efficiency            ['list', 'NoneType']  None
ac2bat_efficiency            ['list', 'NoneType']  None
============================ ===================== =================================================================


ChargingStation
---------------

path: :mod:`eelib.core.devices.charging_station`

============================ ===================== =================================================================
INPUT PARAMETER              ALLOWED TYPES         POSSIBLE VALUES
============================ ===================== =================================================================
p_rated                      ['float', 'int']      (0, inf)
output_type                  ['str']               ['AC', 'DC']
charge_efficiency            ['float', 'int']      (0, 1)
discharge_efficiency         ['float', 'int']      (0, 1)
cos_phi                      ['float', 'int']      (0, 1)
============================ ===================== =================================================================


EV
--

path: :mod:`eelib.core.devices.ev`

============================ ===================== =================================================================
INPUT PARAMETER              ALLOWED TYPES         POSSIBLE VALUES
============================ ===================== =================================================================
start_time                   ['str']               None
file_emobpy                  ['str']               None
step_size                    ['int']               None
soc_init                     ['float']             (0, 1)
set_emobpy_val               ['bool']              None
soc_min                      ['float']             (0, 1)
e_max                        ['int']               (0, inf)
p_nom_discharge_max          ['int']               (-inf, 0)
p_nom_charge_max             ['int']               (0, inf)
dcharge_efficiency           ['float']             (0, 1)
charge_efficiency            ['float']             (0, 1)
n_steps                      ['int']               (0, inf)
============================ ===================== =================================================================


Heatpump
--------

path: :mod:`eelib.core.devices.heatpump`

============================ ===================== =================================================================
INPUT PARAMETER              ALLOWED TYPES         POSSIBLE VALUES
============================ ===================== =================================================================
p_rated_th                   ['float', 'int']      (0, inf)
p_min_th_rel                 ['float']             (0, 1)
time_min                     ['int']               (0, inf)
cop                          ['float']             (0, inf)
modulation                   ['int']               [0, 1]
cos_phi                      ['float', 'int']      (0, 1)
============================ ===================== =================================================================


PVLib
-----

path: :mod:`eelib.core.devices.pv`

============================ ===================== =================================================================
INPUT PARAMETER              ALLOWED TYPES         POSSIBLE VALUES
============================ ===================== =================================================================
latitude                     ['float', 'int']      (-180, 180)
longitude                    ['float', 'int']      (-180, 180)
azimuth                      ['float', 'int']      (0, 360)
tilt                         ['float', 'int']      (0, 90)
cos_phi                      ['float', 'int']      (0, 1)
start_time                   ['str']               None
timezone                     ['str']               None
step_size                    ['int']               (0, inf)
p_rated                      ['float', 'int']      (0, inf)
inverter_efficiency          ['float', 'int']      (0, 1)
losses_standby               ['float', 'int']      (0, inf)
min_power                    ['float', 'int']      (0, inf)
gamma_pdc                    ['float', 'int']      (-inf, 0)
============================ ===================== =================================================================


PVLibExact
----------

path: :mod:`eelib.core.devices.pv`

============================ ===================== =================================================================
INPUT PARAMETER              ALLOWED TYPES         POSSIBLE VALUES
============================ ===================== =================================================================
latitude                     ['float', 'int']      (-180, 180)
longitude                    ['float', 'int']      (-180, 180)
azimuth                      ['float', 'int']      (0, 360)
tilt                         ['float', 'int']      (0, 90)
cos_phi                      ['float', 'int']      (0, 1)
start_time                   ['str']               None
timezone                     ['str']               None
step_size                    ['int']               (0, inf)
altitude_m                   ['float', 'int']      (0, 10000)
module_name                  ['str']               None
num_modules_per_string       ['int']               (0, inf)
num_strings                  ['int']               (0, inf)
inverter_name                ['str']               None
num_inverters                ['int']               (0, inf)
============================ ===================== =================================================================


Forecast
--------

path: :mod:`eelib.core.forecast`

============================ ===================== =================================================================
INPUT PARAMETER              ALLOWED TYPES         POSSIBLE VALUES
============================ ===================== =================================================================
============================ ===================== =================================================================


Pandapower
----------

path: :mod:`eelib.core.grid.pandapower`

============================ ===================== =================================================================
INPUT PARAMETER              ALLOWED TYPES         POSSIBLE VALUES
============================ ===================== =================================================================
gridfile                     ['str']               None
sim_start                    ['str', 'NoneType']   None
============================ ===================== =================================================================


RetailElectricityProvider
-------------------------

path: :mod:`eelib.core.market.retail_electricity_provider`

============================ ===================== =================================================================
INPUT PARAMETER              ALLOWED TYPES         POSSIBLE VALUES
============================ ===================== =================================================================
step_size                    ['int']               None
n_steps                      ['int']               (0, inf)
tariff_type                  ['str']               ['static', 'variable', 'dynamic']
elec_price                   ['int', 'float']      (0, inf)
feedin_tariff                ['int', 'float']      (0, inf)
============================ ===================== =================================================================


HouseholdCSV
------------

path: :mod:`eelib.data.csv_reader`

============================ ===================== =================================================================
INPUT PARAMETER              ALLOWED TYPES         POSSIBLE VALUES
============================ ===================== =================================================================
datafile                     ['str']               None
header_rows                  ['int']               (0, inf)
start_time                   ['str']               None
date_format                  ['str']               None
delimiter                    ['str']               None
step_size                    ['int']               (1, inf)
p_rated                      ['int']               (1, inf)
p_rated_profile              ['int']               (1, inf)
cos_phi                      ['float']             (0, 1)
============================ ===================== =================================================================


PvCSV
-----

path: :mod:`eelib.data.csv_reader`

============================ ===================== =================================================================
INPUT PARAMETER              ALLOWED TYPES         POSSIBLE VALUES
============================ ===================== =================================================================
datafile                     ['str']               None
header_rows                  ['int']               (0, inf)
start_time                   ['str']               None
date_format                  ['str']               None
delimiter                    ['str']               None
step_size                    ['int']               (1, inf)
p_rated                      ['int']               (1, inf)
p_rated_profile              ['int']               (1, inf)
cos_phi                      ['float']             (0, 1)
============================ ===================== =================================================================


HeatpumpCSV
-----------

path: :mod:`eelib.data.csv_reader`

============================ ===================== =================================================================
INPUT PARAMETER              ALLOWED TYPES         POSSIBLE VALUES
============================ ===================== =================================================================
datafile                     ['str']               None
header_rows                  ['int']               (0, inf)
start_time                   ['str']               None
date_format                  ['str']               None
delimiter                    ['str']               None
step_size                    ['int']               (1, inf)
p_rated                      ['int']               (1, inf)
p_rated_profile              ['int']               (1, inf)
cos_phi                      ['float']             (0, 1)
============================ ===================== =================================================================


ChargingStationCSV
------------------

path: :mod:`eelib.data.csv_reader`

============================ ===================== =================================================================
INPUT PARAMETER              ALLOWED TYPES         POSSIBLE VALUES
============================ ===================== =================================================================
datafile                     ['str']               None
header_rows                  ['int']               (0, inf)
start_time                   ['str']               None
date_format                  ['str']               None
delimiter                    ['str']               None
step_size                    ['int']               (1, inf)
p_rated                      ['int']               (1, inf)
p_rated_profile              ['int']               (1, inf)
cos_phi                      ['float']             (0, 1)
============================ ===================== =================================================================


HouseholdThermalCSV
-------------------

path: :mod:`eelib.data.csv_reader`

============================ ===================== =================================================================
INPUT PARAMETER              ALLOWED TYPES         POSSIBLE VALUES
============================ ===================== =================================================================
datafile                     ['str']               None
header_rows                  ['int']               (0, inf)
start_time                   ['str']               None
date_format                  ['str']               None
delimiter                    ['str']               None
step_size                    ['int']               (1, inf)
p_rated                      ['int']               (1, inf)
p_rated_profile              ['int']               (1, inf)
============================ ===================== =================================================================


MarketIntradayContinuousCSV
---------------------------

path: :mod:`eelib.data.csv_reader`

============================ ===================== =================================================================
INPUT PARAMETER              ALLOWED TYPES         POSSIBLE VALUES
============================ ===================== =================================================================
datafile                     ['str']               None
header_rows                  ['int']               (0, inf)
start_time                   ['str']               None
date_format                  ['str']               None
delimiter                    ['str']               None
step_size                    ['int']               (1, inf)
============================ ===================== =================================================================


GenericInflux
-------------

path: :mod:`eelib.data.influx_reader`

============================ ===================== =================================================================
INPUT PARAMETER              ALLOWED TYPES         POSSIBLE VALUES
============================ ===================== =================================================================
measurement_name             ['str']               None
tags                         ['dict']              None
fields                       ['list']              None
start_time                   ['str']               None
end_time                     ['str']               None
step_size                    ['int']               None
influx_url                   ['str']               None
influx_token                 ['str']               None
influx_org                   ['str']               None
influx_bucket                ['str']               None
============================ ===================== =================================================================


HouseholdInflux
---------------

path: :mod:`eelib.data.influx_reader`

============================ ===================== =================================================================
INPUT PARAMETER              ALLOWED TYPES         POSSIBLE VALUES
============================ ===================== =================================================================
measurement_name             ['str']               None
fields                       ['list']              None
start_time                   ['str']               None
end_time                     ['str']               None
step_size                    ['int']               None
influx_url                   ['str']               None
influx_token                 ['str']               None
influx_org                   ['str']               None
influx_bucket                ['str']               None
p_rated                      ['int']               (1, inf)
p_rated_profile              ['int']               (1, inf)
cos_phi                      ['float']             (0, 1)
============================ ===================== =================================================================


PvInflux
--------

path: :mod:`eelib.data.influx_reader`

============================ ===================== =================================================================
INPUT PARAMETER              ALLOWED TYPES         POSSIBLE VALUES
============================ ===================== =================================================================
measurement_name             ['str']               None
fields                       ['list']              None
start_time                   ['str']               None
end_time                     ['str']               None
step_size                    ['int']               None
influx_url                   ['str']               None
influx_token                 ['str']               None
influx_org                   ['str']               None
influx_bucket                ['str']               None
p_rated                      ['int']               (1, inf)
p_rated_profile              ['int']               (1, inf)
cos_phi                      ['float']             (0, 1)
============================ ===================== =================================================================
