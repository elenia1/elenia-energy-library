.. Author: elenia@TUBS
   Copyright 2024 elenia
   This file is part of eELib, which is free software under the terms of the GNU GPL Version 3.

######
mosaik
######

mosaik, according to the `mosaik documentation
<https://mosaik.readthedocs.io/en/latest/>`_ is a "flexible Smart Grid
co-simulation framework". It can combine various existing models to run large-scale scenarios - and
this is what we intend to use it for. mosaik can combine our prosumer models, energy management
systems, grid calculations, and all the others.

For an introduction to mosaik and its behaviour you can have a look at its `tutorial
<https://mosaik.readthedocs.io/en/latest/tutorials/index.html>`_. Recommended articles of this
tutorial for use within the eELib are:

   #. Integrating a simulation model into the mosaik ecosystem
   #. Creating and running simple simulation scenarios
   #. (optionally helpful) Adding a control mechanism to a scenario
   #. (optionally helpful) Integrating a control mechanism
