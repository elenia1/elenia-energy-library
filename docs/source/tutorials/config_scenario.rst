.. Author: elenia@TUBS
   Copyright 2024 elenia
   This file is part of eELib, which is free software under the terms of the GNU GPL Version 3.

###############################
Configure a Simulation Scenario
###############################

.. caution:: Still **WIP** and might not work.

Excel-file setup
================

#. Open the Excel-file ``(eelib\utils\simulation_setup\sim_config_data.xlsx)``. This one is part of the :mod:`utils package <eelib.utils>`.
#. The sheets **bus, load, ext_grid, trafo, line, sgen, storage** are used to configure the grid.

   .. image:: ../_static/sheets.*
      :width: 100%
      :align: center
      :alt: The sheets in a scenario setup excel.

#. To **add a new entity** to the grid, you create a new row and include the element's index name
   and init_vals. More information about the characteristics of the grid elements:
   https://pandapower.readthedocs.io/en/v2.13.1/elements.html

   (Make sure you have at least **one transformer**, **one external grid** and **buses are connected through lines**)

   .. image:: ../_static/sheet_bus.*
      :width: 100%
      :align: center
      :alt: Example sheet for buses in a scenario setup excel.

#. After configurating the grid, you can add the model entities.

   #. Open the sheet ems, define the number of ems and connect them to a bus
   #. Households, charging stations, or pv can be connected to a bus too (but only one per bus)
   #. Adding a new entitiy of a model type is similar to adding a new grid element ( important is to set the connection to an ems or a bus and differentiate between csv_reader model or exact mode type
   #. Now set the init_vals for every entity of the model type

   .. image:: ../_static/cs_sheet.*
      :width: 100%
      :align: center
      :alt: Example sheet for buses in a scenario setup excel.


Create .json files for scenario
===============================

#. Open jupyter notebook ``eelib\\utils\\simulation_setup\\script_sim_setup.ipynb``
#. Run the corresponding cells to import the packages and set the input and output paths:

   To create a grid
      #. Read the sheets from the excel file
      #. Run the "create grid data file" cell
      #. An image of the grid is shown underneath and the ``.json``-file is safed at ``C:/Users/Puplic/Documents``

   To create a model data file
      #. Read the model type sheets frome the excel file
      #. Run "create model data file" cell
      #. ``.json``-file is safed at ``C:/Users/Public/Documents``

   To create a connection file
      #. Run the cell "Create model_grid_config file"
      #. File is safed at ``C:/Users/Public/Documents``

#. Now run your scenario file (from the examples folder), see the ``test_scenarios``


Add a completely new model type
===============================

1. Create a new sheet for the model type in the excel file
2. Create entities and set the init_vals and the connection to a bus or ems for the new model type
3. Open the jupyter notebook ``eelib\\utils\\simulation_setup\\script_sim_setup.ipynb``

   1. Read the new excel sheet

      .. code-block:: python

         df_new_model_type = pd.read_excel(FILE_SCENARIO_INPUT, sheet_name="new_model_type", index_col=0)


   2. Add new model entities to model_data file (if necessery differentiate between exact- and csv-model)

      .. code-block:: python

         #create an empty list for the new model type
         new_model_type = []
         for i in df_new_model_type.index:
         # Add all init_vals for model type
            new_model_type_data = {
               "datafile": df_new_model_type.at[i, "datafile"],
               "start_time": df_new_model_type.at[i, "start_time"],
               "date_format": df_new_model_type.at[i, "date_format"],
               "header_rows": int(df_new_model_type.at[i, "header_rows"]),
            }
            new_model_type.append(new_model_type_data)
         # add new model type list to dictionary
         model_data= { ...
                     new_model_type : new_model_type
         }

   3. Add new model type to ``model_grid_config`` file

      .. code-block:: python

         # create an empty list for the new model type
         new_model_types = []
         # loop over all entities
         for j in df_new_model_type.index:
               # if the entity is connected with bus or ems the name of the entity is added to the list new_model_types
               if df_loads.at[i, "bus"] == df_new_model_type.at[j, "bus"] or ems_idx == df_new_model_type.at[j, "ems"]:
                     new_model_type = df_new_model_type.at[j, "name"]
                     new_model_types.append(new_model_type)
         # add new model type list to dictionary
         elements = {...
               new_model_type: new_model_types
         }

4. Now run the cells to create new ``.json`` files
