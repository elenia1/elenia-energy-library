.. Author: elenia@TUBS
   Copyright 2024 elenia
   This file is part of eELib, which is free software under the terms of the GNU GPL Version 3.

##################################
Implementing an Operating Strategy
##################################

Energy Management Strategies are handled by the Energy Management Systems (EMS).
To implement a new strategy, additions in multiple places are necessary.


1. Adapt the :mod:`EMS_model.py <eelib.core.control.EMS.EMS_model>` file and implement the operating strategy
-------------------------------------------------------------------------------------------------------------

Create a new class that is inherited from the general :class:`~eelib.core.control.EMS.EMS_model.HEMS`
class. Don't forget to add the strategy in the valid parameter of the HEMS ABC class. You can also
inherit from subclasses of the ``HEMS`` class like ``HEMS_default`` to also be able to inherit their methods and
properties.

.. code-block::
   :lineno-start: 290
   :caption: eelib/core/control/EMS/EMS_model.py (01/24)

    class HEMS_default(HEMS):
        """Default strategy for Energy Management System.
        Should be copied and adapted for the use of a specific EMS concept.
        """

        @classmethod
        def get_valid_parameters(cls):
            """Returns dictionary containing valid parameter types and values.

            Returns:
                dict: valid parameters for this model
            """

            # use parent's parameter list and modify them for this class
            result = HEMS.get_valid_parameters().copy()
            result.update({})
            return result

        def __init__(self, eid: str, step_size: int = 900, **kwargs):
            """Initializes the eELib HEMS default model.

            Args:
                eid (str): name of the entity to create
                step_size (int): length of a simulation step in seconds
                **kwargs: initial values for the HEMS entity

            Raises:
                ValueError: Error if selected strategy does not comply with model type.
            """

            # check given strategy
            if "strategy" in kwargs.keys() and kwargs["strategy"] != "HEMS_default":
                raise ValueError("Created a HEMS_default entity with strategy not 'HEMS_default'!")
            else:
                kwargs["strategy"] = "HEMS_default"  # set strategy if not already given

            # call init function of super HEMS class
            super().__init__(eid=eid, step_size=step_size, **kwargs)

Add a ``step()`` function that first calls the HEMS :meth:`~eelib.core.control.EMS.EMS_model.HEMS.step()`
and afterward implement the functionalities of your operating strategy.

.. code-block::
   :lineno-start: 329
   :caption: eelib/core/control/EMS/EMS_model.py (01/24)
   :emphasize-lines: 9

    def step(self, timestep):
        """Calculates power set values for each connected component according to the strategy.

        Args:
            timestep (int): Current simulation time
        """

        # execute general processes (aggregation of power values etc.)
        super().step(timestep)

        # from here on: execute strategy-specific processes
        ...

-------------------------

2. Add the strategy and its input to the ``model_data`` of the scenarios
------------------------------------------------------------------------

.. code-block::
   :lineno-start: 1
   :caption: examples/data/model_data_scenario/model_data_building.json (01/24)

   {
       "ems": [
           {
               "strategy": "HEMS_default",
               "cs_strategy": "balanced"
           }
       ],
       ...

-------------------------

3. Add your EMS class to the ``model_connections/model_connect_config.json`` file
---------------------------------------------------------------------------------

Add the data that is **sent out**...

.. code-block::
    :lineno-start: 10
    :caption: eelib/model_connections/model_connect_config.json (01/24)

    "HEMS_default": {
        "PVLib": [["p_set_pv", "p_set"]],
        "BSS": [
            [
                "p_set_storage",
                "p_set"
            ]
        ],
        "ChargingStation": [
            [
                "p_set_charging_station",
                "p_set"
            ]
        ],
        "Heatpump": [
            [
                "p_th_set_heatpump",
                "p_th_set"
            ]
        ],
        "grid_load": [["p_balance", "p_w"], ["q_balance", "q_var"]]
    },

... but also to every model, which **data is sent to your HEMS!**

.. code-block::
    :lineno-start: 111
    :caption: e.g. but not only the BSS
    :emphasize-lines: 2-15

    "BSS": {
        "HEMS_default": [
            [
                "p_discharge_max",
                "p_min"
            ],
            [
                "p_charge_max",
                "p_max"
            ],
            [
                "p",
                "p"
            ]
        ],
        ...
    },

-------------------------

4. Add the model with its name and (input/output) attributes to the ``META`` of the ``EMS_simulator``
-----------------------------------------------------------------------------------------------------

.. code-block::
   :lineno-start: 14
   :caption: eelib/core/control/EMS/EMS_simulator.py (01/24)

   META = {
       "type": "hybrid",
       "models": {

.. code-block::
   :lineno-start: 50

           "HEMS_default": {
               "public": True,
               "params": ["init_vals"],
               "attrs": [
                   "q",
                   "p",
                   "p_max",
                   "p_min",
                   "p_set_storage",
                   "p_set_charging_station",
                   "p_set_pv",
                   "p_balance",
                   "q_balance",
                   "bss_data",
                   "pv_data",
                   "cs_data",
                   "p_th",
               ],
