.. Author: elenia@TUBS
   Copyright 2024 elenia
   This file is part of eELib, which is free software under the terms of the GNU GPL Version 3.

#########
Tutorials
#########

The tutorial section provides a walkthrough for different tasks and self-aid in case of
common questions.

One thing to know when using the eELib is the **how to set up a simulation** and how to make it run.
This can be learned with the :doc:`example of a simulation <setup_run_sim>`.

Additionally, when you want to run a specific simulation scenario with the eELib, you should know
how to **configure a scenario**. :doc:`This tutorial <config_scenario>` guides you through which
files are needed for that, how to set them up and hints at an easy way how to create all simulation
files.

If you want to **implement a new model**, you should look at the tutorials for
:doc:`a new model <config_model>` and :doc:`a new simulator <config_simulator>`, as both are needed
to use the model in simulations with the eELib. You can have a look at the
:doc:`About eELib <../reference_manual/about_eelib>` introduction and the :ref:`Glossary` to learn
more about the setup of models and simulators.

Lastly, if you are just interested in **implementing a new operation strategy** for an energy
management system, you can use the :doc:`Strategy Implementation Tutorial <implement_strategy>`.
This allows you to use this operational behaviour in your own simulation scenarios.


.. toctree::
   :maxdepth: 1
   :caption: Tutorial List

   setup_run_sim
   config_scenario
   config_model
   config_simulator
   implement_strategy
