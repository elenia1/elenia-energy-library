.. Author: elenia@TUBS
   Copyright 2024 elenia
   This file is part of eELib, which is free software under the terms of the GNU GPL Version 3.

###########################
Set Up and Run a Simulation
###########################

A good thing when working with the eELib would be the familiarization with the three test scenarios
of the eELib: two for a single building (one with just a few devices and explained setup, one with
the usage of helper functions) and one for a small 8-bus low-voltage grid. With the following
explanations of the setup of simulations, try to understand the given examples. Afterwards, you
should know what is needed for a simulation and how you could set it up.


What files are needed for a simulation?
=======================================

To run a simulation, we need four files: A **scenario script** and the scenario data, which is
divided into a **model data** file, the **model connections** and a **grid file**. Additionally,
corresponding input data such as files for the input of pv generation or a household baseload, must
be provided.

Scenario script
---------------

Start the simulators, build the models, create the connections and start mosaik.
Exemplary scripts for building, grid etc. can be found in the ``examples`` folder.
The setup of these scripts is explained below in the :ref:`Scenario Script Configuration
<Configuration of a Scenario Script>` section.

.. figure:: ../_static/example_scenario_script.*
   :align: center

   examples/test_scenario_building.py (01/24)


Model data file
---------------

Information on the number of models and their parameterization.

This is stored as a dict with fields for each model type, named by the class of the model. E.g. the
field *"HouseholdCSV"* contains a list of all household baseloads that read from a csv file. In the
example below there is just one baseload, and if more of these should be integrated, we could add a
second dict with initialization parameters for this model inside of the given list.

One exception is the Energy Management System (EMS), as they are all sorted within the "ems" section.
Their differentiation comes from the field *"strategy"*, which is referring to the chosen
operating strategy of the EMS.

.. code-block::
   :lineno-start: 1
   :caption: examples/data/model_data_scenario/model_data_building.json (01/24)

   {
      "ems": [
         {
               "strategy": "HEMS_default",
               "cs_strategy": "balanced"
         }
      ],
      "HouseholdCSV": [
         {
               "p_rated": 4500,
               "p_rated_profile": 4000,
               "cos_phi": 1.0,
               "datafile": "examples/data/load/4_persons_profile/load_34.csv",
               "date_format": "YYYY-MM-DD HH:mm:ss",
               "header_rows": 2,
               "start_time": "2014-01-01 00:00:00"
         }
      ],
   ...


Model connections
-----------------

The model connections provide information on linking the model entities in the simulation.
This includes the connections between grid buses, ems models, and the prosumer devices. It states
which devices are located at which grid connection point (by naming the corresponding load bus)
and whether an EMS is used. If no EMS is given at a loadbus (see *"0-load_1_2"* below), the
devices are directly connection to the grid, but this should only be possible for some of the
devices, as e.g. a single heat pump makes no sense.


.. code-block::
   :lineno-start: 1
   :caption: examples/data/grid/grid_model_config.json (01/24)

   {
      "0-load_1_1": {
         "ems": "HEMS_default_0",
         "load": [
               "HouseholdCSV_0"
         ],
         "household_thermal": [],
         "pv": [],
         "bss": [],
         "hp": [],
         "cs": [
               "ChargingStation_0"
         ],
         "ev": [
               "EV_0"
         ]
      },
      "0-load_1_2": {
         "ems": "",
         "load": [
               "HouseholdCSV_1"
         ],
   ...

Grid file
---------

In ``.json`` format (possibly created via pandapower). Provides details on the given buses and their
connections via grid lines (plus their parameterization).

.. code-block::
   :lineno-start: 1
   :caption: examples/data/grid/example_grid_kerber.json (01/24)

   {
     "_module": "pandapower.auxiliary",
     "_class": "pandapowerNet",
     "_object": {
       "bus": {
         "_module": "pandas.core.frame",
         "_class": "DataFrame",
         "_object": "{\"columns\":[\"name\",\"vn_kv\",\"type\",\"zone\",\"in_service\"],\"index\":[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17],\"data\":[[\"Trafostation_OS\",10.0,\"b\",null,true],[\"main_busbar\",0.4,\"b\",null,true],[\"MUF_1_1\",0.4,\"n\",null,true],[\"loadbus_1_1\",0.4,\"b\",null,true],[\"KV_1_2\",0.4,\"b\",null,true],[\"loadbus_1_2\",0.4,\"b\",null,true],[\"MUF_1_3\",0.4,\"n\",null,true],[\"loadbus_1_3\",0.4,\"b\",null,true],[\"KV_1_4\",0.4,\"b\",null,true],[\"loadbus_1_4\",0.4,\"b\",null,true],[\"MUF_1_5\",0.4,\"n\",null,true],[\"loadbus_1_5\",0.4,\"b\",null,true],[\"KV_1_6\",0.4,\"b\",null,true],[\"loadbus_1_6\",0.4,\"b\",null,true],[\"MUF_2_1\",0.4,\"n\",null,true],[\"loadbus_2_1\",0.4,\"b\",null,true],[\"KV_2_2\",0.4,\"b\",null,true],[\"loadbus_2_2\",0.4,\"b\",null,true]]}",
         "orient": "split",
         "dtype": {
           "name": "object",
           "vn_kv": "float64",
           "type": "object",
           "zone": "object",
           "in_service": "bool"
         }
       },
   ...

---------------------------------------------------------

.. tip::
  All files can be created (more easily) with a :ref:`Scenario Configurator <Configure a Simulation Scenario>`
  (``.ipynb``). You can also use existing files and adapt the configuration and parameterization.


Configuration of a Scenario Script
==================================

.. note:: All of these code-blocks derive from ``examples/test_scenario_building.py`` as of (01/24)
          if not stated otherwise.

Setup
-----

.. code-block::
   :lineno-start: 8
   :caption: ``import`` of used packages (examples, like mosaik or logging during the simulation)

   import os
   import mosaik
   import eelib.utils.simulation_helper as sim_help
   from eelib.model_connections.connections import get_default_connections
   import logging

.. code-block::
   :lineno-start: 27
   :caption: Setting of paths for simulation data and used model simulators

   # define paths and filenames
   DIR = sim_help.get_default_dirs(
       os.path.realpath(os.path.dirname(__file__)), scenario="building", grid=None, format_db="hdf5"
   )

.. code-block::
   :lineno-start: 37
   :caption: Define simulators and models for the simulation

   # Sim config.: Simulators and their used model types with the properties to store into DB
   SIM_CONFIG = {
      # used database, will be left out for model creation and connections
      "DBSim": {"python": "eelib.data.database.hdf5:Hdf5Database"},
      # all the used simulators and their models for this simulation
      "EMSSim": {
         "python": "eelib.core.control.EMS.EMS_simulator:Sim",
         "models": {"ems": ["p_balance", "q_balance", "p_th_balance", "p_th_dem"]},
      },
      "CSVSim": {
         "python": "eelib.data.csv_reader.csv_reader_simulator:Sim",
         "models": {
               "HouseholdCSV": ["p", "q"],
               "PvCSV": ["p", "q"],
               "ChargingStationCSV": ["p", "q"],
               "HeatpumpCSV": ["p_el", "q_el"],
               "HouseholdThermalCSV": ["p_th_room", "p_th_water"],
         },
      },
      ...

This is done in a format that fits both mosaik for orchestrating the simulation and the
handling of data and the simulation setup. So in the *"SIM_CONFIG"* dict we first need to give all
used simulators as the keys (e.g., *"CSVSim"* for all used csv readers). With the sub-key *"python"*
, we then provide the path to this simulator in our library (e.g.,
*"eelib.data.csv_reader.csv_reader_simulator:Sim"*). Under *"models"* we list all of the used model
types within the simulation for this simulator (e.g., *"HouseholdCSV", "PvCSV"*) and the attributes
of them that should be stored within the database (e.g., *["p", "q"]* for active and reactive power
of the PV system).

.. code-block::
   :lineno-start: 82
   :caption: Configure start and end time of the simulation, step length, and whether to use
      forecasts. Model data and connections are read and ``SIM_CONFIG`` is handed to mosaik.

   # Configuration of scenario: time and granularity
   START = "2020-01-01 00:00:00"
   END = "2020-01-04 00:00:00"
   USE_FORECAST = False
   STEP_SIZE_IN_SECONDS = 900  # 1=sec-steps, 3600=hour-steps, 900=15min-steps, 600=10min-steps
   N_SECONDS = int(
       (
           arrow.get(END, "YYYY-MM-DD HH:mm:ss") - arrow.get(START, "YYYY-MM-DD HH:mm:ss")
       ).total_seconds()
   )
   N_STEPS = int(N_SECONDS / STEP_SIZE_IN_SECONDS)
   scenario_config = {
       "start": START,  # time of beginning for simulation
       "end": END,  # time of ending
       "step_size": STEP_SIZE_IN_SECONDS,
       "n_steps": N_STEPS,
       "use_forcast": USE_FORECAST,
       "bool_plot": False,
   }

   # Read Scenario file with data for model entities
   with open(DIR["MODEL_DATA"]) as f:
       model_data = json.load(f)

   # Read configuration file with data for connections between prosumer devices
   model_connect_config = get_default_connections()

   # Create world
   world = mosaik.World(SIM_CONFIG, debug=True)


With *mosaik.World*, we start the orchestrator mosaik and afterward set up the simulation using
mosaik. For this, we mostly use mosaik functionalities within the helper functions of the
*eelib.utils.simulation_helper* folder. Regarding the order, we mostly first handle the exceptions
of the models, like the output database, and afterward use the helper functions for all
other models. Fore a deeper understanding of what is done with these steps, you should look
at the implementation of these helper functions and possibly even the setup of mosaik for creating
the simulation.

---------------------------------------------------------


Start Simulators for all used Models
------------------------------------

.. code-block::
   :lineno-start: 120

   # start all simulators/model factories with mosaik for data given in SIM_CONFIG
   dict_simulators = sim_help.start_simulators(
       sim_config=SIM_CONFIG, world=world, scenario_config=scenario_config
   )

---------------------------------------------------------


Initiate Model Entities using the created Simulators for each Device Type
-------------------------------------------------------------------------

.. code-block::
   :lineno-start: 133

   # create all models based on given SIM_CONFIG
   dict_entities = sim_help.create_entities(
       sim_config=SIM_CONFIG, model_data=model_data, dict_simulators=dict_simulators
   )

---------------------------------------------------------


Connect Entities
----------------

The connections for each entity are listed in the ``model_connect_config`` data. Now tell mosaik
about them and let mosaik create graphs for how the calculation procedure of the simulation is to be
executed.
Additionally, the entities are connected to the database to store the desired resulting property
values.

.. code-block::
   :lineno-start: 143

   # connect all models to each other
   sim_help.connect_entities(
       world=world,
       dict_entities=dict_entities,
       model_connect_config=model_connect_config,
       dict_simulators=dict_simulators,
   )

---------------------------------------------------------


Run Simulation
--------------

.. code-block::
   :lineno-start: 161

   world.run(until=scenario_config["n_steps"], print_progress=True)


Just go Ahead and Run a Simulation
==================================

- You can run one of the ``test_scenarios`` in the ``examples`` folder
   ``building``: Just one single building with a bunch of different devices to see the operation of
   devices inside the household.

   ``grid``: Simple low voltage grid (2 feeders with six resp. two household connection points)
   to estimate of the impact of different operating strategies on the local grid.

- While running the simulation, some stuff is logged like creation of simulators, entities, and
  connections. Additionally, mosaik plots the progress of the whole simulation (like how many time
  steps are finished calculating).
- Adapting the parameterization in the :ref:`simulation files
  <What files are needed for a simulation?>` can yield quite different results.
- Running one of the simulations will create a ``.hdf5`` results data file in the folder
  ``/examples/results``.
- You can view the information of this file via the
  `H5Web Extension <https://marketplace.visualstudio.com/items?itemName=h5web.vscode-h5web>`_ in
  Microsoft VSC and plot the profiles (stored under ``Series``) of the used devices.
- If you want, you can set the parameter *bool_plot* in the *scenario_config* dict to true (
  only do this for small simulations!). mosaik will then present four plots for the connections, the
  simulation process and the timely duration during the simulation.


Create your own simulation
==========================

#. Copy one of the :ref:`test scenarios <Scenario script>` and delete all of the redundant
   simulators/devices/connections.
#. Set up corresponding :ref:`model <Model data file>` (and :ref:`grid <Grid file>`) data and
   a :ref:`model connection file <Model connections>`.
#. ALTERNATIVE: Use the :ref:`Scenario Configurator <Configure a Simulation Scenario>`
