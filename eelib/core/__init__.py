"""
Here, all of the models are stored. This is divided into the devices (like PV
system or electric vehicle), control models (like energy management system), grid models
(like grid control) and market models (like electricity providers).
"""
