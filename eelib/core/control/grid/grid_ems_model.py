"""
The eElib grid EMS model is some sort of an implementation for the grid operator, as it is the grid
energy management system. The control unit gathers the power flow results of the grid and can send
out different kind of control signals, like set values or prices.

| Author: elenia@TUBS
| Copyright 2024 elenia
| This file is part of eELib, which is free software under the terms of the GNU GPL Version 3.
"""

from math import inf
from eelib.data import GridTariff, GridTariffSignal, ControlSignalEMS


class GridEMS(object):
    """Models a grid energy managament system."""

    # Valid values and types for each parameter that apply for all subclasses
    _VALID_PARAMETERS = {
        "use_14a_enwg": {"types": [bool], "values": [True, False]},
        "grid_tariff_model": {
            "types": [str],
            "values": ["flat-rate", "percentage", "time-variable"],
        },
        "strategy": {"types": [str], "values": ["optimal"]},
        "energy_price_static": {"types": [float, int], "values": (0, inf)},
        "capacity_fee_dem": {"types": [float, int], "values": (0, inf)},
        "capacity_fee_gen": {"types": [float, int], "values": (0, inf)},
        "capacity_fee_horizon_sec": {"types": [int], "values": (1, inf)},
        "grid_model_config": {
            "types": [dict],
            "values": None,
        },
    }

    @classmethod
    def get_valid_parameters(cls):
        """Returns dictionary containing valid parameter types and values.

        Returns:
            dict: valid parameters for this model
        """
        return cls._VALID_PARAMETERS

    def __init__(
        self,
        eid: str,
        strategy: str,
        grid_model_config: dict,
        use_14a_enwg: bool = True,
        grid_tariff_model: str = "flat-rate",
        energy_price_static: float = 0.08,
        capacity_fee_dem: float = 0.0,
        capacity_fee_gen: float = 0.0,
        capacity_fee_horizon_sec: int = 3600 * 24,
        step_size: int = 900,
    ):
        """Initializes the eELib grid EMS model.

        Args:
            eid (str): name of the model entity
            strategy (str): Strategy for the HEMS - type of the created energy management entity
                (e.g. HEMS_default, the default home energy management system)
            grid_model_config (dict): contains assignment of the devices to loads in the grid
            use_14a_enwg (bool): whether to use Par.14a EnWG control on households. Defaults to true
            grid_tariff_model (str): which control model is selected for the households. Defaults to
                "flat-rate"
            energy_price_static (float): static grid fee for energy [EUR/kWh]. Defaults to 0.08.
            capacity_fee_dem (float): capacity grid fee - demand [EUR/kW]. Defaults to 0.0.
            capacity_fee_gen (float): capacity grid fee - generation [EUR/kW]. Defaults to 0.0.
            capacity_fee_horizon_sec (int): time horizon capacity grid fee in seconds. Defaults to
                3600*24.
            step_size (int): length of a simulation step in seconds. Defaults to 900
        """

        # Set attributes of init_vals to static properties
        self.eid = eid
        self.strategy = strategy
        self.grid_model_config = grid_model_config
        self.use_14a_enwg = use_14a_enwg
        self.grid_tariff = GridTariff(
            grid_tariff_model=grid_tariff_model,
            energy_price=energy_price_static,
            capacity_fee_dem=capacity_fee_dem,
            capacity_fee_gen=capacity_fee_gen,
            capacity_fee_horizon_sec=capacity_fee_horizon_sec,
        )

        # initialize input values from grid (calculation)
        self.grid_status = {}
        self.ptdf_mat = []
        self.vpif_mat = []

        # initialize input values from energy management systems
        self.p_ems = {}  # active power values at grid connection points [W]
        self.q_ems = {}  # reactive power values at grid connection points [var]

        # initialize output values
        self.congested = 0
        # control signals for energy management systems of prosumers
        self.control_signal = ControlSignalEMS()
        # initialize signals for grid (whether to calculate power flow with values from EMS)
        self.calc_grid = False

        # initialize signal for grid tariff values
        self.grid_tariff_signal = None

        # save time step length and current time step
        self.step_size = step_size
        self.timestep = 0

        self.forecast_request = {}
        self.forecast = {}

    def step(self, timestep):
        """Gathers current grid status, handles it and calculates output values.

        Args:
            timestep (int): Current simulation time
        """

        # handle current (newly arrived) time step
        if not self.timestep == timestep:
            self.timestep = timestep

        # calculate grid tariff signal with regard to chosen tariff model
        self._create_grid_tariff_signal()

        # set congested status of grid EMS
        self.congested = 0
        if self.grid_status is not None and self.grid_status["congested"]:
            self.congested = 1

    def _create_grid_tariff_signal(self):
        """Create the grid tariff signal to send to retail electricity providers and households.

        Raises:
            ValueError: In case grid tariff model is not implemented
        """

        if self.grid_tariff.grid_tariff_model == "flat-rate":
            self.grid_tariff_signal = GridTariffSignal(
                bool_is_list=False,
                energy_price=self.grid_tariff.energy_price,
                capacity_fee_gen=self.grid_tariff.capacity_fee_gen,
                capacity_fee_dem=self.grid_tariff.capacity_fee_dem,
                capacity_fee_horizon_sec=self.grid_tariff.capacity_fee_horizon_sec,
            )
        elif self.grid_tariff.grid_tariff_model == "percentage":
            self.grid_tariff_signal = GridTariffSignal()
        elif self.grid_tariff.grid_tariff_model == "time-variable":
            self.grid_tariff_signal = GridTariffSignal()
        else:
            raise ValueError(
                f"Chosen grid tariff model '{self.grid_tariff.grid_tariff_model}' for GridEMS is "
                "not implemented."
            )
