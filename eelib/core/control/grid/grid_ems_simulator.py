"""
Mosaik interface for the eELib grid energy management system model.
Simulator for communication between orchestrator (mosaik) and grid EMS entities.

Within this interface mosaik functionalities are used.
Copyright (c) LGPL

| Author: elenia@TUBS
| Copyright 2024 elenia
| This file is part of eELib, which is free software under the terms of the GNU GPL Version 3.
"""

import mosaik_api_v3
from eelib.core.control.grid import grid_ems_model
import dataclasses
import eelib.utils.validation as vld
from copy import deepcopy

# SIMULATION META DATA
META = {
    "type": "hybrid",
    "models": {
        "GridEMS": {
            "public": True,
            "params": ["init_vals"],
            "attrs": [
                "use_14a_enwg",
                "grid_tariff",
                "congested",
                "grid_status",
                "ptdf_mat",
                "vpif_mat",
                "forecast_request",
                "forecast",
                "grid_tariff_signal",
                "control_signal",
                "p_ems",
                "q_ems",
                "calc_grid",
            ],
            "trigger": [
                "grid_status",
                "ptdf_mat",
                "vpif_mat",
                "forecast",
                "p_ems",
                "q_ems",
            ],  # input attributes
            "non-persistent": [
                "forecast_request",
                "grid_tariff_signal",
                "control_signal",
                "calc_grid",
            ],  # output attributes
        },
    },
    "extra_methods": ["get_entity_by_id"],
}

# accuracy for cached output values to avoid triggering of event-based simulators
ADAPTION_TOLERANCE = 10 ** (-5)


class Sim(mosaik_api_v3.Simulator):
    """Simulator class for eELib grid EMS model.

    Args:
        mosaik_api_v3 (module): defines communication between mosaik and simulator


    Yields:
        object: Initializes a mosaik event to return ``set_data``.
    """

    def __init__(self):
        """Constructs an object of the EMS:Sim class."""

        super(Sim, self).__init__(META)

        # storing of event-based output info for current same-time loop
        self.output_cache = {}

        # initiate empty dict for model entities
        self.entities = {}

    def init(self, sid, scenario_config, time_resolution=1.0):
        """Initializes parameters for an object of the grid_ems:Sim class.

        Args:
            sid (str): ID of the created entity of the simulator (e.g. GridEMSSim-0)
            scenario_config (dict): scenario configuration data, like resolution or step size
            time_resolution (float): fitting of the step size to the simulation scenario step size.

        Returns:
            meta: description of the simulator
        """

        # assign properties for simulator
        self.sid = sid
        self.scenario_config = scenario_config

        return self.meta

    def create(self, num, model_type, init_vals=None):
        """Creates entities of the eELib grid EMS model.
        Core function of mosaik.

        Args:
            num (int): number of models to be created
            model_type (str): type of created entity (e.g. "grid_ems")
            init_vals (list): list with initial values for each grid EMS entity, defaults to None

        Raises:
            ValueError: If entity ID is already existing

        Returns:
            dict: created entities
        """

        # create empty list for created entities
        entities_orchestrator = []

        for eid, entity_vals in init_vals.items():
            # check if entity ID is already existing
            if eid in self.entities:
                raise ValueError(f"Entity ID {eid} for model type {model_type} already existing.")

            # create entity by specified full ID
            full_id = self.sid + "." + eid

            # get class of specific model and create entity with init values after validation
            entity_cls = getattr(grid_ems_model, model_type)
            vld.validate_init_parameters(entity_cls, entity_vals)
            entity = entity_cls(eid=eid, **entity_vals, step_size=self.scenario_config["step_size"])

            # add info to the simulators entity-list and current entities
            self.entities[eid] = {
                "eid": eid,
                "etype": model_type,
                "model": entity,
                "full_id": full_id,
            }
            entities_orchestrator.append({"eid": eid, "type": model_type})

        return entities_orchestrator

    def get_entity_by_id(self, entity_id: str):
        """Searches for a requested entity id and gives back the entity model.

        Args:
            entity_id (str): id of the entity to be searched for

        Returns:
            object: entity model if found, None otherwise
        """

        if entity_id in self.entities.keys():
            return self.entities[entity_id]["model"]
        else:
            return None

    def step(self, time, inputs, max_advance):
        """Performs simulation step calling the eELib grid EMS model.
        Core function of mosaik.

        Args:
            time (int): current simulation time (given by mosaik)
            inputs (dict): allocation of set values to specific models
            max_advance (int, optional): simulation time until the simulator can safely advance
                it's internal time without causing any causality errors.

        Returns:
            int: next timestep (when orchestrator calls again)
        """

        self.timestep = time

        # assign property values for each entity and attribute with entity ID
        # process input signals: for the entities (eid), attr is a dict for attributes to be set
        for eid, attrs in inputs.items():
            # for the attributes (attr), setter is a dict for entities with corresponding set values
            for attr, setter in attrs.items():
                # simply set the values from the grid and connected entities
                setting_value_dict = deepcopy(getattr(self.entities[eid]["model"], attr))

                # go by each input for this attribute separately
                bool_val_from_grid = False  # to check whether input from grid
                for eid_setter, value_dict in setter.items():
                    # check if value is incoming from grid -> only write this value
                    if "grid" in eid_setter.lower():
                        setting_value_dict = value_dict
                        bool_val_from_grid = True
                if not bool_val_from_grid:  # value not from grid -> write directly
                    setting_value_dict = setter

                # set the collected value for this attribute
                setattr(self.entities[eid]["model"], attr, setting_value_dict)

        # call step function for each entity in the list
        for eid, entity_dict in self.entities.items():
            entity_dict["model"].step(time)

        # next timestamp for simulation
        return None

    def get_data(self, outputs):
        """Gets the data for the next concatenated model.
        Core function of mosaik.

        Args:
            outputs (dict): dictionary with data outputs from each control model

        Raises:
            ValueError: error if attribute not in model metadata

        Returns:
            dict: dictionary with simulation outputs
        """

        # create flag for checking if any new data is send out
        flag_output_changed = False

        # create empty output data dict
        data = {}

        # check current timestep (whether a new one is reached)
        if not self.output_cache == {} and self.output_cache["time"] != self.timestep:
            self.output_cache = {}
        data["time"] = self.timestep
        self.output_cache["time"] = self.timestep

        for transmitter_ename, attrs in outputs.items():
            # get name for current entity and create dict field
            entry = self.entities[transmitter_ename]
            if transmitter_ename not in self.output_cache:
                self.output_cache[transmitter_ename] = {}

            # loop over all targeted attributes and check if info is available
            for attr in attrs:
                if attr not in self.meta["models"][type(entry["model"]).__name__]["attrs"]:
                    raise ValueError("Unknown output attribute: %s" % attr)

                # create empty field for cache and output data
                if attr not in self.output_cache[transmitter_ename]:
                    self.output_cache[transmitter_ename][attr] = {}

                output_data_to_save = getattr(entry["model"], attr)
                if dataclasses.is_dataclass(output_data_to_save):  # attribute is dataclass object
                    # check if data empty or values outside ofadaption tolerance
                    if self.output_cache[transmitter_ename][
                        attr
                    ] == {} or output_data_to_save.check_adaption_tolerance(
                        ADAPTION_TOLERANCE,
                        self.output_cache[transmitter_ename][attr],
                    ):
                        # convert dataclass to dict
                        self.output_cache[transmitter_ename][attr] = dataclasses.asdict(
                            output_data_to_save
                        )
                        flag_output_changed = True
                    continue  # go on with next attribute

                # check if output is a dict or just a single value
                if isinstance(output_data_to_save, dict):
                    for emitter_ename, value in output_data_to_save.items():
                        if (
                            emitter_ename not in self.output_cache[transmitter_ename][attr]
                        ):  # value currently not stored in output cache
                            self.output_cache[transmitter_ename][attr] = output_data_to_save
                            flag_output_changed = True
                        elif isinstance(value, (float, int)):  # value existent and comparable
                            if (
                                abs(
                                    self.output_cache[transmitter_ename][attr][emitter_ename]
                                    - value
                                )
                                > ADAPTION_TOLERANCE
                            ):
                                self.output_cache[transmitter_ename][attr] = output_data_to_save
                                flag_output_changed = True
                        else:  # value existent and not comparable
                            if self.output_cache[transmitter_ename][attr][emitter_ename] != value:
                                self.output_cache[transmitter_ename][attr] = output_data_to_save
                                flag_output_changed = True
                    if (
                        output_data_to_save == {}
                        and self.output_cache[transmitter_ename][attr] != {}
                    ):
                        self.output_cache[transmitter_ename][attr] = output_data_to_save
                        flag_output_changed = True
                else:  # output_data_to_save is no dict, but a single value to be stored
                    if self.output_cache[transmitter_ename][attr] == {}:  # no value existent
                        self.output_cache[transmitter_ename][attr] = output_data_to_save
                        flag_output_changed = True
                    elif isinstance(output_data_to_save, (float, int)):  # value exist. and compar.
                        if (
                            abs(self.output_cache[transmitter_ename][attr] - output_data_to_save)
                            > ADAPTION_TOLERANCE
                        ):
                            self.output_cache[transmitter_ename][attr] = output_data_to_save
                            flag_output_changed = True
                    else:  # value existent and not comparable
                        if self.output_cache[transmitter_ename][attr] != output_data_to_save:
                            self.output_cache[transmitter_ename][attr] = output_data_to_save
                            flag_output_changed = True

            # set whole output data for this entity
            data[transmitter_ename] = self.output_cache[transmitter_ename]

        # check if nothing is to be send out - send output 1 step later to avoid waiting for data
        if not flag_output_changed:
            if self.timestep == self.scenario_config["n_steps"] - 1:  # is last time step?
                data["time"] = self.timestep + 1
            else:
                data = {"time": self.timestep}

        # return data to mosaik
        return data

    def add_controlled_entity(self, control_entity_eid: str, entity_dict: dict):
        """Adds entities (e.g. pv systems) to the specific control unit entity.

        Args:
            control_entity_eid (str): entity id of control unit entity
            entity_dict (dict): Dictionary of created models to be added to the control unit entity
        """

        self.entities[control_entity_eid]["model"].add_controlled_entity(entity_dict)
