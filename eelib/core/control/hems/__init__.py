"""
The eElib HEMS model was developed as an energy management system on the building level and is
therefore connected to all of the building's components. The control gathers all of the component's
power flows, calculates the energy balance at the building node and the component's flexibility,
and then sends out power set values in each step. Hereby, available solar power is favored to grid
consumption, as well as charging / discharging limits of the charging stations, and batteries are
considered.

Regarding charging stations, different charging strategies are implemented: Default / Maximum power
charging, balanced charging, solar charging and night charging.

| Author: elenia@TUBS
| Copyright 2024 elenia
| This file is part of eELib, which is free software under the terms of the GNU GPL Version 3.
"""
