"""
Helper functions for handling of battery storage systems in EMS.

| Author: elenia@TUBS
| Copyright 2024 elenia
| This file is part of eELib, which is free software under the terms of the GNU GPL Version 3.
"""

from eelib.data import BSSData


def bss_calc_balance(p_balance: float, p_max: float, p_min: float) -> float:
    """Calculates the set power values of storage based on given p_balance.
    Resulting in charging and discharging values.

    Args:
        p_balance (float): Input p_balance which storage tries to level
        p_min (float): Discharge rate of storage
        p_max (float): Charge rate of storage

    Returns:
        float: calculated power set value of bss to level ``p_balance``

    .. note::
        This function was created based on the function ``_set_power_within_limit()`` in
        ``storage_model``.
    """
    if p_balance > 0:
        # discharging: negative minimum of residual load and max. discharge power
        p_set_bss = -min(p_balance, -p_min)
    elif p_balance < 0:
        # charging: minimum of residual surplus and max. charging power
        p_set_bss = min(-p_balance, p_max)
    else:  # power is balanced
        p_set_bss = 0

    return p_set_bss


def bss_strategy_reduce_curtailment(
    p_balance: float, p_max: float, p_min: float, bss_charging_limit: float = 0.6
):
    """This strategy reduces curtailment of the solar power system
    by reducing the bss' charging power. This decreases self-sufficiency but also the
    curtailment due to EEG2021 (e.g. feed-in is only allowed at max. 70% of
    installed power for uncontrolled solar power systems). This operation strategy
    should not be used in combination with electric vehicle charging. Suitable limits
    are 30%-60%.

    Args:
        p_balance (float): input balance which bss needs to level
        p_max (float): maximum charging power of bss
        p_min (float): maximum discharging power of bss
        bss_charging_limit (float): Charging limit of batter storage. Defaults to 0.6.

    Returns:
        int: reduced charging power for battery storage
    """

    p_curtailment = 0

    if p_balance > 0:
        # discharging: negative minimum of residual load and max. discharge power
        p_curtailment = -min(p_balance, -p_min)

    elif p_balance < 0:
        # charging: minimum of residual surplus and max. charging power reduced by curtailment
        p_curtailment = min(-p_balance, p_max * bss_charging_limit)

    else:  # power is balanced
        p_curtailment = 0

    return p_curtailment


def bss_set_energy_within_limit(bss_data: BSSData) -> float:
    """Check that battery energy limits are not surpassed.
    Similiar to function in storage_model.py.

    Args:
        bss_data (BSSData): extract from bss dataclass

    Returns:
        float: energy level of the bss after scheduling step

    .. note::
        This function was created based on the function ``_set_energy_within_limit()`` in
        ``storage_model``.
    """

    if bss_data.e_bat > bss_data.e_bat_usable:
        e_bat = bss_data.e_bat_usable
    elif bss_data.e_bat < bss_data.soc_min * bss_data.e_bat_usable:
        e_bat = bss_data.soc_min * bss_data.e_bat_usable
    else:
        e_bat = bss_data.e_bat

    return e_bat


def bss_calc_e_bat(step_size: int, bss_data: BSSData) -> float:
    """Calculates the energy level of the forecasted battery storage system.

    Args:
        step_size (int): simulation step_size
        bss_data (BSSData): extract of dataclass for currently scheduled bss

    Returns:
        float: energy level of forecasted bss

    .. note::
        This function was created based on the function ``step()`` in ``storage_model``.
    """
    e_bat_init_save = bss_data.e_bat

    # adapt energy content from last timestep ( + self-discharge)
    e_bat_forecast_self_discharge = -(bss_data.e_bat * bss_data.self_discharge_step)

    if bss_data.p >= 0:  # charging
        e_bat_forecast_step_volume = bss_data.p * bss_data.charge_efficiency * (step_size / 3600)
    else:  # discharging
        e_bat_forecast_step_volume = bss_data.p / bss_data.discharge_efficiency * (step_size / 3600)

    e_bat = bss_data.e_bat + e_bat_forecast_step_volume + e_bat_forecast_self_discharge

    # check for over or undercharging
    bss_data.e_bat = e_bat
    e_bat = bss_set_energy_within_limit(bss_data=bss_data)

    # return to initial energy in BSS
    bss_data.e_bat = e_bat_init_save

    return e_bat


def bss_calc_p_limits(step_size: int, bss_data: BSSData) -> tuple[float, float]:
    """Calculates the bss active power limits for the forecast using input ''bss_e_bat_usable''.

    Args:
        step_size (int): simulation step_size
        bss_data (BSSData): extract of dataclass for currently scheduled bss

    Returns:
        float: maximum charging power of bss
        float: minimum charging (resp. max. discharging) power of bss

    .. note::
        This function was created based on the function ``_calc_power_limits()`` in
        ``storage_model``.
    """

    # calc max. DISCHARGING active power
    p_min = max(
        -(bss_data.e_bat - bss_data.soc_min * bss_data.e_bat_usable)
        * bss_data.discharge_efficiency
        * (1 - bss_data.self_discharge_step)
        / (step_size / 3600),
        bss_data.p_rated_discharge_max,
    )

    # calc max. CHARGING active power
    p_max = min(
        (bss_data.e_bat_usable - bss_data.e_bat * (1 - bss_data.self_discharge_step))
        / bss_data.charge_efficiency
        / (step_size / 3600),
        bss_data.p_rated_charge_max,
    )

    return p_max, p_min
