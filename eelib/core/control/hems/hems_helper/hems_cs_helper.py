"""
Helper functions for handling of charging stations in EMS.

| Author: elenia@TUBS
| Copyright 2024 elenia
| This file is part of eELib, which is free software under the terms of the GNU GPL Version 3.
"""

from eelib.data import CSData


def cs_distribute_charging_power(forecast: dict, t: int, cs_data: CSData) -> dict:
    """Distributes the charging power  across all connected cars.
    For this, the distribution is done evenly unless the power limits of the cars are exceeded.

    Args:
        forecast (dict): collected forecast from EMS
        t (int): control variable in range of current forecast
        cs_data (CSData): contains all information of charging station

    Raises:
        ValueError: If Vehicles charging power does not match the power of the charging station
        ValueError: If set charging power does not fit the power limits of the connected evs
        ValueError: If charging station has power value although no car is connected
        ValueError: If sum of vehicle power does not match power

    Returns:
        dict: individual charging power for all connected cars

    .. note::
        This function was created based on the function ``_distribute_charging_power()``
        in ``charging_station_model``.
    """

    # current efficiency depending on the direction of power flow
    cs_efficiency = cs_calc_current_efficiency(cs_data)

    # check if a distribution within the limits is possible
    p_set = cs_data.p * cs_efficiency

    if p_set > sum(
        [
            ev_data.p_max
            for ev_id, ev_data in cs_data.ev_data.items()
            if forecast[ev_id]["appearance"][t]
        ]
    ):
        raise ValueError("Set power for cs to create schedule too big for limits of the cars")
    elif p_set < sum(
        [
            ev_data.p_min
            for ev_id, ev_data in cs_data.ev_data.items()
            if forecast[ev_id]["appearance"][t]
        ]
    ):
        raise ValueError("Set power for cs to create schedule too small for limits of the cars")

    else:  # distribution within the limits is possible
        p_device = {ev_full_id: 0 for ev_full_id in cs_data.ev_data.keys()}

        # first simply divide by number of connected cars for a balanced distribution
        num_considered_cars = sum([1 for ev_data in cs_data.ev_data.values() if ev_data.appearance])
        sum_power = cs_data.p * cs_efficiency
        average_power = sum_power / num_considered_cars if num_considered_cars != 0 else 0

        # create dict to save whether a power value for a device has been fixed
        dict_power_fixed = {car_key: False for car_key in cs_data.ev_data.keys()}

        # distribute until all car power values are fixed
        while not all(dict_power_fixed.values()):  # return 0 power for all if no car avail. at cs
            average_power_loop = average_power  # set av. power in this loop for verification

            # loop over all entities in forecast
            for ev_full_id in cs_data.ev_data.keys():
                # check if car not already fixed or not available
                if dict_power_fixed[ev_full_id] or not cs_data.ev_data[ev_full_id].appearance:
                    continue

                # check if the average power is within the limits of the car
                if (
                    average_power <= cs_data.ev_data[ev_full_id].p_max
                    and average_power >= cs_data.ev_data[ev_full_id].p_min
                ):
                    p_device[ev_full_id] = average_power
                else:
                    # average power exceeds maximum power -> set maximum power and adapt for average
                    if average_power > cs_data.ev_data[ev_full_id].p_max:
                        p_device[ev_full_id] = cs_data.ev_data[ev_full_id].p_max
                        sum_power -= cs_data.ev_data[ev_full_id].p_max
                    # avrg. power short of min. power -> set min. power and adapt for average
                    else:
                        p_device[ev_full_id] = cs_data.ev_data[ev_full_id].p_min
                        sum_power -= cs_data.ev_data[ev_full_id].p_min
                    # set this car to fix
                    num_considered_cars -= 1
                    dict_power_fixed[ev_full_id] = True

            # check if all cars are at their limit - otherwise adapt average
            if num_considered_cars == 0:
                break
            else:
                average_power = sum_power / num_considered_cars
                # check if this distribution works and no power value changed in last iteration
                if average_power == average_power_loop:
                    break

    # return power distribution dict
    return p_device


def cs_calc_power_limits(forecast: dict, t: int, cs_data: CSData) -> tuple[float, float]:
    """Calculate the power limits for the charging station with the input thats coming from the
    electric vehicles.

    Args:
        forecast (dict): collected forecast from EMS
        t (int): control variable in range of current forecast
        cs_data (CSData): contains all information of charging stations

    Raises:
        ValueError: If the power limits of at least one connected ev do not work together.

    Returns:
        float: maximum charging power of charging station
        float: maximum discharging (resp. minimum charging) power of charging station

    .. note::
        This function was created based on the function ``_calc_power_limits()`` in
        ``charging_station_model``.
    """

    # in case no ev is connected to cs - no active power flexibility
    p_min = 0
    p_max = 0

    for car_full_id in cs_data.ev_data.keys():
        # check for each ev if connected - consider their limits, efficiency and nominal power
        if forecast[car_full_id]["appearance"][t]:
            # check if min. and max. power are correct
            if cs_data.ev_data[car_full_id].p_min > cs_data.ev_data[car_full_id].p_max:
                raise ValueError(
                    f"FORECAST: Min. and max. power of ev {car_full_id} do not comply."
                )
            # handle the power limits
            if cs_data.ev_data[car_full_id].p_min <= 0:
                p_min_ev = cs_data.ev_data[car_full_id].p_min * cs_data.discharge_efficiency
            else:
                raise ValueError(
                    "FORECAST: Maximum discharging power of EV, p_min, cannot be positive!"
                )

            if cs_data.ev_data[car_full_id].p_max >= 0:
                p_max_ev = cs_data.ev_data[car_full_id].p_max / cs_data.charge_efficiency
            else:
                raise ValueError(
                    "FORECAST: Maximum charging power of EV, p_max, cannot be negative!"
                )

            p_min = max(p_min + p_min_ev, -cs_data.p_rated)
            p_max = min(p_max + p_max_ev, cs_data.p_rated)

    return p_max, p_min


def cs_calc_current_efficiency(cs_data: CSData) -> float:
    """Calculates the charging efficiency for charging station from static properties.
    Based on the active power.

    Args:
        cs_data (CSData): contains all information of charging station

    Returns:
        float: efficiency for dis/charging process of cs

    .. note::
        This function was created based on the function ``_calc_current_efficiency()`` in
        ``charging_station_model``.
    """

    if cs_data.p > 0:  # charging process: powerflow from grid into direction of cars
        cs_efficiency = cs_data.charge_efficiency
    elif cs_data.p < 0:  # discharging process: powerflow from cars into direction of grid
        cs_efficiency = 1 / cs_data.discharge_efficiency
    else:  # no (dis)charging process, no power "losses"
        cs_efficiency = 1

    return cs_efficiency
