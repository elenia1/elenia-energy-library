"""
Helper functions for handling of cars in EMS.

| Author: elenia@TUBS
| Copyright 2024 elenia
| This file is part of eELib, which is free software under the terms of the GNU GPL Version 3.
"""

from eelib.data import EVData


def ev_calc_e_bat(step_size: int, ev_data: EVData) -> float:
    """Calculates the energy amount of electric car after power influence (from last step).

    Args:
        step_size (int): simulation step_size
        ev_data (EVData): contains all information of electric vehicles

    Returns:
        float: new energy level of electric vehicle (for next step)

    NOTE: This function was created based on the function step() in car_model
    """
    e_bat_init_save = ev_data.e_bat

    # energy content adaption (not including efficiency)
    e_adapt_ac_side = ev_data.p * (step_size / 3600)

    # now include efficiency when adapting the current energy value
    if ev_data.p > 0:
        e_bat_ev_new = ev_data.e_bat + (e_adapt_ac_side * ev_data.charge_efficiency)
    else:
        e_bat_ev_new = ev_data.e_bat + e_adapt_ac_side / ev_data.dcharge_efficiency

    # set battery energy within limit of car
    ev_data.e_bat = e_bat_ev_new
    e_bat_ev_new = ev_set_energy_within_limit(ev_data=ev_data)

    # return to initial energy in BSS
    ev_data.e_bat = e_bat_init_save

    return e_bat_ev_new


def ev_set_energy_within_limit(ev_data: EVData) -> float:
    """Check that battery energy limits are not surpassed.

    Args:
        ev_data (EVData): contains all information of electric vehicles

    Returns:
        float: updated energy level of electric vehicle

    NOTE: This function was created based on the function set_energy_within_limit() in
        car_model.
    """

    # avoid overcharging with min and undercharging with max
    e_bat_ev = min(ev_data.e_max, max(ev_data.e_bat, ev_data.e_max * ev_data.soc_min))

    return e_bat_ev


def ev_calc_power_limits(step_size: int, ev_data: EVData) -> tuple[float, float]:
    """Calculate the maximum (dis)charging power for the car battery.
    depending on the power limits and the current stored energy.

    Args:
        step_size (int): simulation step_size
        ev_data (EVData): contains all information of electric vehicles

    Returns:
        float: minimum charging (resp. max. discharging) power of EV
        float: maximum (charging) power of EV

    NOTE: This function was created based on the function _calc_power_limits() in car_model.
    """

    # Calc max. discharging power (NOTE: should be <0 due to passive sign convention)
    if not ev_data.dcharge_efficiency == 0:
        p_min = max(
            -ev_data.e_bat * ev_data.dcharge_efficiency / (step_size / 3600),
            ev_data.p_nom_discharge_max,
        )
    else:
        p_min = 0

    # Calc max. charging power (NOTE: should be >0 due to passive sign convention)
    if not ev_data.charge_efficiency == 0:
        p_max = min(
            (ev_data.e_max - ev_data.e_bat) / ev_data.charge_efficiency / (step_size / 3600),
            ev_data.p_nom_charge_max,
        )
    else:
        p_max = 0

    return p_min, p_max


def ev_calc_power(step_size: int, consumption_step: float, p_set: float, ev_data: EVData) -> float:
    """Calculate the power at the current timestep within the (dis)charging limits.

    Args:
        step_size (int): simulation step_size
        consumption_step (float): consumption at the current timestep
        p_set (float): set value for charging power of car
        ev_data (EVData): contains all information of electric vehicles

    Returns:
        float: electric power of the vehicle

    .. note::
        This function was created based on the function ``_calc_power()`` in ``car_model``.
    """

    p_car = 0

    if consumption_step > 0:  # during a trip
        p_car = -consumption_step * 1000 / (step_size / 3600)
    else:  # when connected to charging station
        # check for set value
        if p_set is None:
            p_car = 0
        else:
            p_car = p_set

        # consider power limits
        p_car = max(min(p_car, ev_data.p_max), ev_data.p_min)

    return p_car
