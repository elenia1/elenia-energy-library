"""
Helper functions for handling of heatpumps in EMS.

| Author: elenia@TUBS
| Copyright 2024 elenia
| This file is part of eELib, which is free software under the terms of the GNU GPL Version 3.
"""


def hp_set_power_with_limits(
    hp_p_th_max: float, hp_p_th_min_on: float, hp_p_th_min: float, hp_p_th_target: float
) -> float:
    """Set the thermal power of a heatpump with regard to its three limits (max, min, min_on).

    Args:
        hp_p_th_max (float): maximum thermal power
        hp_p_th_min_on (float): minimum thermal power in on-state
        hp_p_th_min (float): minimum thermal power overall
        hp_p_th_target (float): desired power value

    Returns:
        float: thermal power set value according to power limits

    .. note::
        This function was created based on the function ``step()`` in ``heatpump_model``.
    """
    if hp_p_th_target < hp_p_th_max:  # above max power
        p_th_set_hp = hp_p_th_max
    elif hp_p_th_target <= hp_p_th_min_on and hp_p_th_target >= hp_p_th_max:  # max - min_on
        p_th_set_hp = hp_p_th_target
    elif hp_p_th_target > hp_p_th_min_on and hp_p_th_target < hp_p_th_min:  # min_on - min
        p_th_set_hp = hp_p_th_min_on
    else:  # at or below min power
        p_th_set_hp = hp_p_th_min

    return p_th_set_hp


def set_hp_time(step_size: int, p_th_set: float, hp_time_on: int, hp_time_off: int) -> int:
    """Update on-/off-time of heatpump with regard to its current power output.

    Args:
        step_size (int): current simulation step size
        p_th_set (float): current thermal power output
        hp_time_on (int): current time in on-state [s]
        hp_time_off (int): current time in off-state [s]

    Returns:
        int, int:
        (1) updated time in on-state [s]
        (2) updated time in off-state [s]

    .. note::
        This function was created based on the function ``step()`` in ``heatpump_model``.
    """
    if p_th_set < 0:  # heatpump ON if th. gen. > 0
        hp_time_on += step_size
        hp_time_off = 0
    else:
        hp_time_off += step_size
        hp_time_on = 0

    return hp_time_on, hp_time_off


def calc_hp_power_limits(hp_p_rated: float, hp_p_min_th_rel: float, hp_state: str):
    """Set thermal power limits of heatpump with regard to state.

    Args:
        hp_p_rated (float): rated thermal power of heatpump
        hp_p_min_th_rel (float): relative minimum thermal power in on-state (regarding rated)
        hp_state (str): current state of the heatpump

    Returns:
        float, float, float:
        (1) maximum thermal power
        (2) minimum thermal power in on-state
        (3) minimum thermal power

    .. note::
        This function was created based on the function ``__calc_thermal_limits()``
        in ``heatpump_model``.
    """

    # set maximum thermal power and minimum on-power of heatpump with regard to state
    if hp_state != "must_off":  # Check whether heatpump can be turned off
        hp_p_th_max = -hp_p_rated
        hp_p_th_min_on = -hp_p_rated * hp_p_min_th_rel
    else:
        hp_p_th_max = 0
        hp_p_th_min_on = 0
    # set minimum power of heatpump with regard to state
    if hp_state != "must_on":  # Check whether heatpump can be turned off
        hp_p_th_min = 0
    else:
        hp_p_th_min = -hp_p_rated * hp_p_min_th_rel

    return hp_p_th_max, hp_p_th_min_on, hp_p_th_min


def set_hp_state(p_th_set: float, hp_time_on: int, hp_time_off: int, hp_time_min: int) -> str:
    """Update state of heatpump with regard to its on- or off-time and minimum state time.

    Args:
        p_th_set (float): current thermal power output
        hp_time_on (int): current time in on-state [s]
        hp_time_off (int): current time in off-state [s]
        hp_time_min (int): minimum time for one state [s]

    Returns:
        str: updated state of the heatpump

    .. note::
        This function was created based on the function ``step()``
        in ``heatpump_model``.
    """
    if p_th_set < 0:  # heatpump is ON if the thermal generation is larger than zero
        if hp_time_on >= hp_time_min:
            hp_state = "on"
        else:
            hp_state = "must_on"
    else:
        if hp_time_off >= hp_time_min:
            hp_state = "off"
        else:
            hp_state = "must_off"

    return hp_state
