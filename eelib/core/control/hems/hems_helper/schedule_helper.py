"""
Helper functions for the creation of schedules in EMS.

Pyomo released under 3-clause BSD license

| Author: elenia@TUBS
| Copyright 2024 elenia
| This file is part of eELib, which is free software under the terms of the GNU GPL Version 3.
"""

import numpy as np
import pyomo.environ as pyo
from pyomo.environ import Var, ConstraintList, Objective
from eelib.core.control.hems.hems_helper import hems_hp_helper
from eelib.core.control.hems.hems_helper import hems_bss_helper, hems_cs_helper, hems_ev_helper
from eelib.data import ControlSignalEMS, TariffSignal, OptimOptions
import logging

_logger = logging.getLogger(__name__)


def calc_forecast_residual(forecast: dict, forecast_horizon: int) -> np.ndarray:
    """Calculates the residual forecast for the "p"-values of all devices' forecasts.

    Args:
        forecast (dict): input forecasts to calculate schedule
        forecast_horizon (int): timesteps in which forecast is created

    Returns:
        np.ndarray: residual electrical power values from forecasts for attribute "p" for devices
    """

    residual_load = np.zeros(forecast_horizon)

    # go by all uncontrolled models and check if electrical power forecast exists
    for forecast_devices_id, forecasts_for_attrs in forecast.items():
        # check if current forecast is already a collected value
        if isinstance(forecasts_for_attrs, np.ndarray) and forecast_devices_id in [
            "p_res",
            "p_th_dem",
        ]:
            continue
        if "p" in forecasts_for_attrs.keys() and "p_el" not in forecasts_for_attrs.keys():
            # for thermal davices, the electrical power value should be taken
            residual_load += np.array(forecast[forecast_devices_id]["p"])
        elif "p_el" in forecasts_for_attrs.keys():
            residual_load += np.array(forecast[forecast_devices_id]["p_el"])

    return residual_load


def calc_forecast_thermal_residual(forecast: dict, forecast_horizon: int) -> np.ndarray:
    """Calculates the residual forecast for the thermal demand ("p_th"-values).

    Args:
        forecast (dict): input forecasts to calculate schedule
        forecast_horizon (int): timesteps in which forecast is created

    Returns:
        np.ndarray: residual thermal demand values from forecast
    """

    th_residual_load_uncontrolled = np.zeros(forecast_horizon)

    # go by all forecasts of uncontrolled models and check if thermal power forecast exists
    for forecasts_for_attrs in forecast.values():
        if not isinstance(forecasts_for_attrs, dict):
            # if this is not a dict, this is no forecast of a device or read-only model - next
            continue
        for attr, forecast_values in forecasts_for_attrs.items():
            if attr in ["p_th_water", "p_th_room"]:
                th_residual_load_uncontrolled += np.array(forecast_values)

    return th_residual_load_uncontrolled


def bss_calc_schedule(
    step_size: int,
    schedule_p_res: list,
    bss_data: dict,
) -> dict:
    """Calculates the schedule for the p_set values of the bss.

    Args:
        step_size (int): simulation step_size
        schedule_p_res (list): Input schedule of residual load.
        bss_data (dict): battery information at start of forecast with structure {id: BSSData}

    Returns:
        dict: Schedule with ``p_set`` values of the bss in the forecast horizon.
    """

    # save initial values
    p_bss_init = {}
    e_bss_init = {}
    for bss_full_id in bss_data.keys():
        p_bss_init[bss_full_id] = bss_data[bss_full_id].p
        e_bss_init[bss_full_id] = bss_data[bss_full_id].e_bat

    # initialize power values for bss schedule
    p_set_bss_schedule = {}
    e_bat_bss_schedule = {}

    # initialize result arrays with zero values
    for bss_full_id in bss_data.keys():
        p_set_bss_schedule[bss_full_id] = {}
        p_set_bss_schedule[bss_full_id]["p"] = np.zeros(len(schedule_p_res))
        e_bat_bss_schedule[bss_full_id] = np.zeros(len(schedule_p_res) + 1)
        e_bat_bss_schedule[bss_full_id][0] = bss_data[bss_full_id].e_bat

    # iterate over forecast horizon
    for t in range(len(schedule_p_res)):
        # set balance of current step according to schedule
        p_balance = schedule_p_res[t]

        # iterate over all controlled bss
        for bss_full_id in bss_data.keys():
            # calculate bss parameters
            p_max_bss_forecast, p_min_bss_forecast = hems_bss_helper.bss_calc_p_limits(
                step_size=step_size, bss_data=bss_data[bss_full_id]
            )

            # calculate p_set_bss according to p_balance[t]
            p_set_bss_schedule[bss_full_id]["p"][t] = hems_bss_helper.bss_calc_balance(
                p_balance=p_balance,
                p_max=p_max_bss_forecast,
                p_min=p_min_bss_forecast,
            )

            # calculate final power balance for bss forecast timestep
            p_balance += p_set_bss_schedule[bss_full_id]["p"][t]

            # Update bss_e_bat with calculated set value
            bss_data[bss_full_id].p = p_set_bss_schedule[bss_full_id]["p"][t]
            e_bat_bss_schedule[bss_full_id][t + 1] = hems_bss_helper.bss_calc_e_bat(
                step_size=step_size, bss_data=bss_data[bss_full_id]
            )
            bss_data[bss_full_id].e_bat = e_bat_bss_schedule[bss_full_id][t + 1]

    # reset to initial values
    for bss_full_id in bss_data.keys():
        bss_data[bss_full_id].p = p_bss_init[bss_full_id]
        bss_data[bss_full_id].e_bat = e_bss_init[bss_full_id]

    return p_set_bss_schedule


def pv_calc_schedule(forecast: dict, pv_data: dict) -> dict:
    """Calculates the schedule for all pv systems using the forecast (maximum power).

    Args:
        forecast (dict): collected forecast from EMS
        pv_data (dict): contains all information of pv systems with structure {id: PVData}

    Returns:
        dict: Schedule with power set values of the pv systems in the forecast horizon.
    """
    p_set_pv_schedule = {}

    for pv_full_id in pv_data.keys():
        p_set_pv_schedule[pv_full_id] = {}
        p_set_pv_schedule[pv_full_id]["p"] = np.array(forecast[pv_full_id]["p"])

        # remove forecast from residual forecast (is now handled in the schedule)
        forecast["p_res"] -= forecast[pv_full_id]["p"]

    return p_set_pv_schedule


def hp_calc_schedule(
    step_size: int,
    th_demand_profile: list,
    energy_demand_th: float,
    hp_data: dict,
) -> dict:
    """Calculates the schedule for the thermal power set values of the heatpump.

    Args:
        step_size (int): simulation step_size
        th_demand_profile (list): input profile of the thermal power demand.
        energy_demand_th (float): current thermic energy demand
        hp_data (dict): contains all information of heatpumps with structure {id: HPData}

    Returns:
        dict: Schedule with thermal and electrical set values of the hps in the forecast horizon.
    """

    # save initial values
    p_th_init = {}
    p_el_init = {}
    state_init = {}
    cop_init = {}
    time_on_init = {}
    time_off_init = {}
    for hp_full_id in hp_data.keys():
        p_th_init[hp_full_id] = hp_data[hp_full_id].p_th
        p_el_init[hp_full_id] = hp_data[hp_full_id].p
        state_init[hp_full_id] = hp_data[hp_full_id].state
        cop_init[hp_full_id] = hp_data[hp_full_id].cop
        time_on_init[hp_full_id] = hp_data[hp_full_id].time_on
        time_off_init[hp_full_id] = hp_data[hp_full_id].time_off

    # initialize set and balance dict/list and copy hp-related values to adapt them in schedule
    p_set_hp_schedule = {}

    for hp_full_id in hp_data.keys():
        p_set_hp_schedule[hp_full_id] = {}
        p_set_hp_schedule[hp_full_id]["p_th"] = np.zeros(len(th_demand_profile))
        p_set_hp_schedule[hp_full_id]["p_el"] = np.zeros(len(th_demand_profile))

    for t in range(len(th_demand_profile)):
        # first calculate thermal (residual) demand at current timestep
        p_th_balance = th_demand_profile[t]
        p_th_need_step = -p_th_balance - energy_demand_th * 3600 / step_size

        for hp_full_id in hp_data.keys():
            # calc HP limits with state, time_on and time_off
            hp_p_th_max, hp_p_th_min_on, hp_p_th_min = hems_hp_helper.calc_hp_power_limits(
                hp_p_rated=hp_data[hp_full_id].p_rated_th,
                hp_p_min_th_rel=hp_data[hp_full_id].p_min_th_rel,
                hp_state=hp_data[hp_full_id].state,
            )

            # set HP power according to the limits
            p_set_hp_schedule[hp_full_id]["p_th"][t] = hems_hp_helper.hp_set_power_with_limits(
                hp_p_th_max=hp_p_th_max,
                hp_p_th_min_on=hp_p_th_min_on,
                hp_p_th_min=hp_p_th_min,
                hp_p_th_target=p_th_need_step,
            )
            # set electrical power with "fixed" cop (from start of forecasting period)
            p_set_hp_schedule[hp_full_id]["p_el"][t] = -(
                p_set_hp_schedule[hp_full_id]["p_th"][t] / hp_data[hp_full_id].cop
            )

            # update th power balance
            p_th_need_step -= p_set_hp_schedule[hp_full_id]["p_th"][t]
            p_th_balance += p_set_hp_schedule[hp_full_id]["p_th"][t]

            # adjust time that hp is on or off
            hp_data[hp_full_id].time_on, hp_data[hp_full_id].time_off = hems_hp_helper.set_hp_time(
                step_size=step_size,
                p_th_set=p_set_hp_schedule[hp_full_id]["p_th"][t],
                hp_time_on=hp_data[hp_full_id].time_on,
                hp_time_off=hp_data[hp_full_id].time_off,
            )

            # update state of HP
            hp_data[hp_full_id].state = hems_hp_helper.set_hp_state(
                p_th_set=p_set_hp_schedule[hp_full_id]["p_th"][t],
                hp_time_on=hp_data[hp_full_id].time_on,
                hp_time_off=hp_data[hp_full_id].time_off,
                hp_time_min=hp_data[hp_full_id].time_min,
            )

        # update thermal energy balance from this step
        energy_demand_th += p_th_balance * (step_size / 3600)

    # reset to initial values
    for hp_full_id in hp_data.keys():
        hp_data[hp_full_id].p_th = p_th_init[hp_full_id]
        hp_data[hp_full_id].p = p_el_init[hp_full_id]
        hp_data[hp_full_id].state = state_init[hp_full_id]
        hp_data[hp_full_id].cop = cop_init[hp_full_id]
        hp_data[hp_full_id].time_on = time_on_init[hp_full_id]
        hp_data[hp_full_id].time_off = time_off_init[hp_full_id]

    return p_set_hp_schedule


def cs_calc_schedule_uncontrolled(
    step_size: int,
    forecast: dict,
    forecast_horizon: int,
    cs_data: dict,
):
    """Calculates the schedule for all connected charging stations and its connected cars.
    Uses the forecast of the appereance and the cars consumption.
    NOTE: For this schedule the car is always charged with max. power.

    Args:
        step_size (int): simulation step size in seconds
        forecast (dict): collected forecast from EMS
        forecast_horizon (int): length of the forecast period
        cs_data (dict): contains all information of charging stations with structure {id: CSData}

    Returns:
        dict: Schedule with power set values of the charging stations in the forecast horizon.
    """

    # save initial values
    p_cs_init = {}
    p_ev_init = {}
    e_ev_init = {}
    app_ev_init = {}
    p_min_ev_init = {}
    p_max_ev_init = {}
    for cs_full_id in cs_data.keys():
        p_cs_init[cs_full_id] = cs_data[cs_full_id].p
        for ev_full_id in cs_data[cs_full_id].ev_data.keys():
            p_ev_init[ev_full_id] = cs_data[cs_full_id].ev_data[ev_full_id].p
            e_ev_init[ev_full_id] = cs_data[cs_full_id].ev_data[ev_full_id].e_bat
            app_ev_init[ev_full_id] = cs_data[cs_full_id].ev_data[ev_full_id].appearance
            p_min_ev_init[ev_full_id] = cs_data[cs_full_id].ev_data[ev_full_id].p_min
            p_max_ev_init[ev_full_id] = cs_data[cs_full_id].ev_data[ev_full_id].p_max

    # initialize active power schedule
    p_set_cs_schedule = {}

    for cs_full_id in cs_data.keys():
        p_set_cs_schedule[cs_full_id] = {}
        p_set_cs_schedule[cs_full_id]["p"] = np.zeros(forecast_horizon)

    # iterate over forecast horizon
    for t in range(forecast_horizon):
        # iterate over forecasted charging stations
        for cs_full_id in cs_data.keys():
            ev_data_cs = cs_data[cs_full_id].ev_data

            # calculate power limits of connected cars
            for ev_id, ev_data in ev_data_cs.items():
                ev_data.appearance = forecast[ev_id]["appearance"][t]
                ev_data.p_min, ev_data.p_max = hems_ev_helper.ev_calc_power_limits(
                    step_size=step_size, ev_data=ev_data
                )

            # calculate power limits for the charging station in current forecast step
            cs_p_max, cs_p_min = hems_cs_helper.cs_calc_power_limits(
                forecast=forecast, t=t, cs_data=cs_data[cs_full_id]
            )

            # set charging power of cs - consider maximum possible power
            p_set_cs_schedule[cs_full_id]["p"][t] = max(
                min(cs_p_max, cs_data[cs_full_id].p_rated), cs_p_min
            )

            # calculate the distribution of the charging power to the cars
            cs_data[cs_full_id].p = p_set_cs_schedule[cs_full_id]["p"][t]
            p_device = hems_cs_helper.cs_distribute_charging_power(
                forecast=forecast, t=t, cs_data=cs_data[cs_full_id]
            )

            for ev_id, ev_data in ev_data_cs.items():
                # calculate the dis/charging power for cars
                ev_data.p = hems_ev_helper.ev_calc_power(
                    step_size=step_size,
                    consumption_step=forecast[ev_id]["consumption"][t],
                    p_set=p_device[ev_id],
                    ev_data=ev_data,
                )

                # calculate the energy within the cars batteries
                ev_data.e_bat = hems_ev_helper.ev_calc_e_bat(
                    step_size=step_size,
                    ev_data=ev_data,
                )

    # reset to initial values
    for cs_full_id in cs_data.keys():
        cs_data[cs_full_id].p = p_cs_init[cs_full_id]
        for ev_full_id in cs_data[cs_full_id].ev_data.keys():
            cs_data[cs_full_id].ev_data[ev_full_id].p = p_ev_init[ev_full_id]
            cs_data[cs_full_id].ev_data[ev_full_id].e_bat = e_ev_init[ev_full_id]
            cs_data[cs_full_id].ev_data[ev_full_id].appearance = app_ev_init[ev_full_id]
            cs_data[cs_full_id].ev_data[ev_full_id].p_min = p_min_ev_init[ev_full_id]
            cs_data[cs_full_id].ev_data[ev_full_id].p_max = p_max_ev_init[ev_full_id]

    return p_set_cs_schedule


def calc_schedule_opt(
    step_size: int,
    forecast: dict,
    forecast_horizon: int,
    opt: OptimOptions = OptimOptions(),
    tariff: TariffSignal = TariffSignal(),
    cs_data: dict = {},
    bss_data: dict = {},
    pv_data: dict = {},
    hp_data: dict = {},
    control_signals: ControlSignalEMS = ControlSignalEMS(),
) -> dict:
    """Calculates an optimal schedule for all connected devices.

    Args:
        step_size (int): simulation step_size
        forecast (dict): collected forecasts from EMS
        forecast_horizon (int): timesteps for which forecast should be calculated
        opt (OptimOptions): information about optimization setup (whats used and what not)
        tariff (TariffSignal): electricity price signal. Defaults to TariffSignal().
        cs_data (dict): info of charging stations, like {id: CSData}. Defaults to {}.
        bss_data (dict): info of battery storage systems, like {id: BSSData}. Defaults to {}.
        pv_data (dict): info of pv systems, like {id: PVData}. Defaults to {}.
        hp_data (dict): info of heatpumps, like {id: HPData}. Defaults to {}.
        control_signals (ControlSignalEMS): Control signals sent to EMS with lists for power limits.

    Raises:
        ValueError: constructed optimization problem could not be solved by solver
        UserWarning: If a given capacity fee horizon does not fit to the forecast/schedule horizon.

    Returns:
        dict: Schedule with power set values of the connected devices in the forecast horizon.
    """

    # initialize active power schedule
    schedules = {}
    for dev_id in (
        list(cs_data.keys()) + list(bss_data.keys()) + list(pv_data.keys()) + list(hp_data.keys())
    ):
        schedules[dev_id] = {}

    # initialize pyomo model for optimization handling
    m = pyo.ConcreteModel()
    steps = range(forecast_horizon)
    steps_p_1 = range(forecast_horizon + 1)
    granularity = step_size / 3600  # step size in seconds, we want hourly resolution

    # ---------- VARIABLES ----------
    # add general optimization variables
    m.grid_load = Var(steps, domain=pyo.NonNegativeReals)
    m.grid_feedin = Var(steps, domain=pyo.NonNegativeReals)
    # variable for thermal energy demand
    m.th_e_demand = Var(steps_p_1, domain=pyo.Reals)
    m.th_e_pen_pos = Var(steps, domain=pyo.NonNegativeReals)
    m.th_e_pen_neg = Var(steps, domain=pyo.NonNegativeReals)

    # peakshaving variables
    m.p_peak = pyo.Var(range(1), domain=pyo.NonNegativeReals)
    m.p_valley = pyo.Var(range(1), domain=pyo.NonNegativeReals)

    # add bss optimization variables
    m.bss_p_cha = Var(range(len(bss_data)), steps, domain=pyo.NonNegativeReals)
    m.bss_p_discha = Var(range(len(bss_data)), steps, domain=pyo.NonNegativeReals)
    m.bss_e = Var(range(len(bss_data)), steps_p_1, domain=pyo.NonNegativeReals)
    for idx_bss, bss in enumerate(bss_data.values()):
        m.bss_p_cha[idx_bss, :].setub(bss.p_rated_charge_max)
        m.bss_p_discha[idx_bss, :].setub(-bss.p_rated_discharge_max)
        m.bss_e[idx_bss, :].setlb(bss.e_bat_usable * bss.soc_min)
        m.bss_e[idx_bss, :].setub(bss.e_bat_usable)
    # add cs and car optimization variables
    m.cs_p_cha = Var(range(len(cs_data)), steps, domain=pyo.NonNegativeReals)
    m.cs_p_discha = Var(range(len(cs_data)), steps, domain=pyo.NonNegativeReals)
    num_ev = sum([len(cs.ev_data) for cs in cs_data.values()])
    count_ev = 0  # to count all evs and go by each one
    ev_data = {}  # to save all the electric vehicle information
    m.ev_e = Var(range(num_ev), steps_p_1, domain=pyo.NonNegativeReals)
    m.ev_p_cha = Var(range(num_ev), steps, domain=pyo.NonNegativeReals)
    m.ev_p_discha = Var(range(num_ev), steps, domain=pyo.NonNegativeReals)
    m.ev_e_pen = Var(range(num_ev), domain=pyo.NonNegativeReals)  # penalize non-filled ev (at end)
    for idx_cs, cs in enumerate(cs_data.values()):
        m.cs_p_cha[idx_cs, :].setub(cs.p_rated)
        m.cs_p_discha[idx_cs, :].setub(cs.p_rated)
        for ev_id, ev in cs.ev_data.items():
            m.ev_e[count_ev, :].setlb(ev.e_max * ev.soc_min)
            m.ev_e[count_ev, :].setub(ev.e_max)
            m.ev_p_cha[count_ev, :].setub(ev.p_nom_charge_max)
            m.ev_p_discha[count_ev, :].setub(-ev.p_nom_discharge_max)
            count_ev += 1
            ev_data[ev_id] = ev
    # add hp optimization variables
    m.hp_p_th = Var(range(len(hp_data)), steps, domain=pyo.NonNegativeReals)
    m.hp_state = Var(range(len(hp_data)), steps, domain=pyo.Binary)
    for idx_hp, hp in enumerate(hp_data.values()):
        m.hp_p_th[idx_hp, :].setub(hp.p_rated_th)
    # add pv optimization variables
    m.pv_p = Var(range(len(pv_data)), steps, domain=pyo.Reals)
    for pv_id in pv_data.keys():
        # remove pv forecast from residual forecast (is now handled with variables in a schedule)
        forecast["p_res"] -= forecast[pv_id]["p"]

    # variables for control signals regarding residual power (for each step)
    m.control_pen_max = Var(steps, domain=pyo.NonNegativeReals)
    m.control_pen_min = Var(steps, domain=pyo.NonNegativeReals)

    # ---------- OBJECTIVE FUNCTION ----------
    # add price info for demand/feedin from tariff signal - initialize with zero values
    elec_price_list = [0 for _ in steps]
    feedin_tariff_list = [0 for _ in steps]
    if isinstance(tariff, TariffSignal):
        if not tariff.bool_is_list:  # tariff values are static
            elec_price_list = [tariff.elec_price for _ in steps]
            feedin_tariff_list = [tariff.feedin_tariff for _ in steps]
        else:  # tariff values are lists itself
            elec_price_list = tariff.elec_price
            feedin_tariff_list = tariff.feedin_tariff
    # add price info for capacity fees
    capacity_fee_sum = 0
    if isinstance(tariff, TariffSignal):
        opt_horizon_h = len(steps) * granularity
        cap_fee_horizon_h = tariff.capacity_fee_horizon_sec / 3600
        if opt_horizon_h == cap_fee_horizon_h:  # capacity fee horizon fits optimization horizon
            capacity_fee_sum = (
                tariff.capacity_fee_dem * m.p_peak[0] + tariff.capacity_fee_gen * m.p_valley[0]
            )
        else:  # horizons do not fit - print warning of there are capacity values
            if tariff.capacity_fee_dem != 0 or tariff.capacity_fee_gen != 0:
                _logger.warning(
                    f"Capacity fee horizon ({cap_fee_horizon_h} h) does not fit schedule horizon "
                    f"({cap_fee_horizon_h} h) - capacity fees will hence not be considered."
                )
                raise UserWarning("Capacity fees will not be considered due to unequal horizon.")

    # penalize non-compliance with control signals
    obj_signals = sum(  # simply add penalty costs to objective function if limits are surpassed
        control_signals.penalty_cost * (m.control_pen_max[t] + m.control_pen_min[t]) for t in steps
    )
    # penalize non-filled ev at the schedule end
    obj_ev_penalty_end = 0
    if opt.ev_end_energy_penalty is not None:
        obj_ev_penalty_end = sum(
            (opt.ev_end_energy_penalty * m.ev_e_pen[idx_ev]) for idx_ev in range(num_ev)
        )
    # penalize later charging to incentivize direct charging with maximum power
    obj_ev_direct_charging = 0
    if opt.ev_direct_cha:
        obj_ev_direct_charging = sum(
            (t * opt.dir_cha_desc_factor * m.cs_p_cha[idx_cs, t])
            for t in range(len(steps))
            for idx_cs in range(len(cs_data))
        )
    obj_bss_direct_charging = 0
    if opt.bss_direct_cha:
        obj_bss_direct_charging = sum(
            (t * opt.dir_cha_desc_factor * (m.bss_p_cha[idx_bss, t] + m.bss_p_discha[idx_bss, t]))
            for t in range(len(steps))
            for idx_bss in range(len(bss_data))
        )
    c_bss_energy = (feedin_tariff_list[-1] + elec_price_list[-1]) / 2
    # incentive bss energy at the end to not randomly feed into grid but store for next day
    obj_bss_incent_end = 0
    if opt.bss_end_energy_incentive:
        obj_bss_incent_end = sum(
            (-c_bss_energy * m.bss_e[idx_bss, steps_p_1[-1]] / 1000)
            for idx_bss in range(len(bss_data))
        )
    # penalize thermal energy demand outside of range
    obj_th_e_penalty = sum(
        (opt.th_e_penalty * (m.th_e_pen_pos[t] + m.th_e_pen_neg[t])) for t in steps
    )
    # set objective Function
    m.OBJ = Objective(
        expr=sum(
            [
                elec_price_list[t] * m.grid_load[t] / 1000 * granularity
                - feedin_tariff_list[t] * m.grid_feedin[t] / 1000 * granularity
                for t in steps
            ]
        )
        + obj_signals
        + obj_th_e_penalty
        + obj_ev_penalty_end
        + obj_bss_incent_end
        + obj_ev_direct_charging
        + obj_bss_direct_charging
        + capacity_fee_sum
    )

    # ---------- CONSTRAINTS ----------
    # balance constraint for electrical power side
    m.C_bal = ConstraintList()
    for t in steps:
        m.C_bal.add(
            expr=forecast["p_res"][t]
            + m.grid_feedin[t]
            + sum(m.bss_p_cha[idx_bss, t] for idx_bss in range(len(bss_data)))
            + sum(m.cs_p_cha[idx_cs, t] for idx_cs in range(len(cs_data)))
            + sum(m.hp_p_th[idx_hp, t] / hp.cop for idx_hp, hp in enumerate(hp_data.values()))
            == m.grid_load[t]
            + sum(m.pv_p[idx_pv, t] for idx_pv in range(len(pv_data)))
            + sum(m.bss_p_discha[idx_bss, t] for idx_bss in range(len(bss_data)))
        )
    # balance constraint for thermal power side
    if opt.consider_thermal:
        m.C_bal_th = ConstraintList()
        m.C_restr_th = ConstraintList()
        for t in steps:
            m.C_bal_th.add(
                expr=m.th_e_demand[t + 1]
                == m.th_e_demand[t]
                + forecast["p_th_dem"][t] * granularity
                - sum(m.hp_p_th[idx_hp, t] * granularity for idx_hp in range(len(hp_data)))
            )
            # set restrictions for maximum demand in both direction (with penalty variables)
            m.C_restr_th.add(
                expr=m.th_e_demand[t + 1] <= opt.thermal_energy_restriction + m.th_e_pen_pos[t]
            )
            m.C_restr_th.add(
                expr=m.th_e_demand[t + 1] >= -opt.thermal_energy_restriction - m.th_e_pen_neg[t]
            )
        # starting thermal energy level
        m.C_e_th_start = pyo.Constraint(expr=m.th_e_demand[0] == opt.thermal_energy_start)

    # constraints for control signals: go by each step and possibly apply constraints
    m.C_control = ConstraintList()
    if control_signals.steps != [] and control_signals.steps is not None:
        for t in steps:
            # find index in list
            indices = [i for i, x in enumerate(control_signals.steps) if x == t]
            for idx in indices:
                # check for a valid value and apply the restriction (with penalty variable)
                if (
                    control_signals.p_max is not None
                    and control_signals.p_max != []
                    and control_signals.p_max[idx]
                    not in (
                        None,
                        np.inf,
                        -np.inf,
                    )
                ):
                    m.C_control.add(
                        expr=m.grid_load[t] - m.grid_feedin[t]
                        <= control_signals.p_max[idx] + m.control_pen_max[t]
                    )
                if (
                    control_signals.p_min is not None
                    and control_signals.p_min != []
                    and control_signals.p_min[idx]
                    not in (
                        None,
                        np.inf,
                        -np.inf,
                    )
                ):
                    m.C_control.add(
                        expr=m.grid_load[t] - m.grid_feedin[t]
                        >= control_signals.p_min[idx] - m.control_pen_min[t]
                    )

    # constraints for peak and valley power
    m.C_peak = ConstraintList()
    for t in steps:
        m.C_peak.add(expr=m.p_peak[0] >= m.grid_load[t])
    m.C_val = ConstraintList()
    for t in steps:
        m.C_val.add(expr=m.p_valley[0] >= m.grid_feedin[t])

    # pv constraint: max generation (and minimum power)
    m.C_pv_power = ConstraintList()
    for idx_pv, pv_id in enumerate(pv_data.keys()):
        for t in steps:
            m.C_pv_power.add(expr=m.pv_p[idx_pv, t] <= -forecast[pv_id]["p"][t])
            # on lower side either limit to zero or minimum value (if losses are included)
            m.C_pv_power.add(expr=m.pv_p[idx_pv, t] >= -max(0, max(forecast[pv_id]["p"])))

    # ev constraints: starting energy level
    m.C_ev_start = ConstraintList()
    for idx_ev, ev in enumerate(ev_data.values()):
        m.C_ev_start.add(expr=m.ev_e[idx_ev, 0] == ev.e_bat)
    # ev constraints: time coupling & charging power equals power of evs
    m.C_ev_coupl = ConstraintList()
    m.C_ev_cs_eq = ConstraintList()
    count_ev = 0
    for idx_cs, cs in enumerate(cs_data.values()):
        for t in steps:
            # power of charging station (ev-side) equals power of evs
            cs_p = (
                m.cs_p_cha[idx_cs, t] * cs.charge_efficiency
                - m.cs_p_discha[idx_cs, t] / cs.discharge_efficiency
            )
            ev_p = sum(
                m.ev_p_cha[idx_ev, t] - m.ev_p_discha[idx_ev, t]
                for idx_ev in range(count_ev, count_ev + len(cs.ev_data))
            )
            m.C_ev_cs_eq.add(expr=cs_p == ev_p)
            # time coupling of ev energy level (between the timesteps)
            for idx_ev, ev_id in enumerate(cs.ev_data.keys()):
                m.C_ev_coupl.add(
                    expr=m.ev_e[idx_ev, t + 1]
                    == m.ev_e[idx_ev, t]
                    + m.ev_p_cha[idx_ev, t] * cs.ev_data[ev_id].charge_efficiency * granularity
                    - m.ev_p_discha[idx_ev, t] / cs.ev_data[ev_id].dcharge_efficiency * granularity
                    - forecast[ev_id]["consumption"][t]
                    * 1000  # emobpy ev profile values are in kWh, we need to transform to Wh
                    / cs.ev_data[ev_id].dcharge_efficiency
                )
        count_ev += len(cs.ev_data)
    # ev constraints: only charge while home & full at end of schedule horizon
    m.C_ev_home = ConstraintList()
    for idx_ev, ev_id in enumerate(ev_data.keys()):
        for t in steps:
            if not forecast[ev_id]["appearance"][t]:
                m.C_ev_home.add(expr=m.ev_p_cha[idx_ev, t] == 0)
                m.C_ev_home.add(expr=m.ev_p_discha[idx_ev, t] == 0)
    # ev constraints: full at end of schedule horizon
    m.C_ev_end = ConstraintList()
    for idx_ev, ev in enumerate(ev_data.values()):
        m.C_ev_end.add(expr=m.ev_e[idx_ev, steps_p_1[-1]] + m.ev_e_pen[idx_ev] == ev.e_max)

    # hp constraints: couple state and thermal output
    m.C_hp_state = ConstraintList()
    for idx_hp, hp in enumerate(hp_data.values()):
        for t in steps:
            m.C_hp_state.add(expr=m.hp_state[idx_hp, t] * hp.p_rated_th >= m.hp_p_th[idx_hp, t])
            m.C_hp_state.add(
                expr=m.hp_state[idx_hp, t] * hp.p_rated_th * hp.p_min_th_rel <= m.hp_p_th[idx_hp, t]
            )
    # hp constraints: time limits for state (should at least be "time_min" consecutively on or off)
    m.C_hp_time_state = ConstraintList()
    for idx_hp, hp in enumerate(hp_data.values()):
        steps_min = int(np.ceil(hp.time_min / step_size))
        for t in range(1, steps[-steps_min + 1]):
            # turn off (left = steps_min) only possible when staying off for min. time (sum = 0)
            m.C_hp_time_state.add(
                (m.hp_state[idx_hp, t - 1] - m.hp_state[idx_hp, t]) * steps_min
                <= steps_min - sum(m.hp_state[idx_hp, x] for x in range(t, t + steps_min))
            )
        for t in range(1, steps[-steps_min + 1]):
            # turn on (left = steps_min) only when staying on for min. time (sum = steps_min)
            m.C_hp_time_state.add(
                (m.hp_state[idx_hp, t] - m.hp_state[idx_hp, t - 1]) * steps_min
                <= sum(m.hp_state[idx_hp, x] for x in range(t, t + steps_min))
            )
        # for the last steps, maximum one state change possible
        for t in range(steps[-steps_min + 1], steps.stop):
            # for turning off
            m.C_hp_time_state.add(
                (m.hp_state[idx_hp, t - 1] - m.hp_state[idx_hp, t]) * (steps.stop - t)
                <= (steps.stop - t) - sum(m.hp_state[idx_hp, x] for x in range(t, steps.stop))
            )
            # for turning on
            m.C_hp_time_state.add(
                (m.hp_state[idx_hp, t] - m.hp_state[idx_hp, t - 1]) * (steps.stop - t)
                <= sum(m.hp_state[idx_hp, x] for x in range(t, steps.stop))
            )
        # for first timestep consider the time that the hp is already on or off
        if hp.time_off > 0:
            if hp.time_off < hp.time_min:  # next steps the hp cannot be turned on
                for t in range(0, steps_min - int(hp.time_off / step_size)):
                    m.C_hp_time_state.add(m.hp_state[idx_hp, t] == 0)
            else:  # limit ongoing steps in case hp is turned on now
                m.C_hp_time_state.add(
                    (m.hp_state[idx_hp, 0] - 0) * steps_min
                    <= sum(m.hp_state[idx_hp, x] for x in range(steps_min))
                )
        else:  # heatpump is on, so time on is larger than zero
            if hp.time_on < hp.time_min:  # next steps the hp cannot be turned off
                for t in range(0, steps_min - int(hp.time_on / step_size)):
                    m.C_hp_time_state.add(m.hp_state[idx_hp, t] == 1)
            else:  # limit ongoing steps in case hp is turned off now
                m.C_hp_time_state.add(
                    (1 - m.hp_state[idx_hp, 0]) * steps_min
                    <= steps_min - sum(m.hp_state[idx_hp, x] for x in range(steps_min))
                )

    # bss constraints: starting energy level
    m.C_bss_start = ConstraintList()
    for idx_bss, bss in enumerate(bss_data.values()):
        m.C_bss_start.add(expr=m.bss_e[idx_bss, 0] == bss.e_bat)
    # bss constraints: time coupling of bss energy level (between the time steps)
    m.C_bss_coupl = ConstraintList()
    for idx_bss, bss in enumerate(bss_data.values()):
        # check (dis)charging efficiencies
        if bss.charge_efficiency is None or bss.charge_efficiency == 0.0:
            bss.charge_efficiency = 1
            _logger.warning(f"Charging eff. for {list(bss_data.keys())[idx_bss]} set to 1.")
        if bss.discharge_efficiency is None or bss.discharge_efficiency == 0.0:
            bss.discharge_efficiency = 1
            _logger.warning(f"Discharging eff. for {list(bss_data.keys())[idx_bss]} set to 1.")
        for t in steps:
            m.C_bss_coupl.add(
                expr=m.bss_e[idx_bss, t + 1]
                == m.bss_e[idx_bss, t]
                * (1 if bss.self_discharge_step is None else 1 - bss.self_discharge_step)
                + m.bss_p_cha[idx_bss, t] * bss.charge_efficiency * granularity
                - m.bss_p_discha[idx_bss, t] / bss.discharge_efficiency * granularity
            )

    # ---------- SOLVE OPTIMIZATION ----------
    solver = pyo.SolverFactory("gurobi")  # set solver as default to gurobi
    # check whether problem can be solved with gurobi
    if not solver.available() or not solver.license_is_valid():  # pragma: no cover
        solver = pyo.SolverFactory("appsi_highs")  # otherwise take OS solver HiGHS APPSI
    # run the solver on the optimization problem
    result = solver.solve(m, load_solutions=False)

    # ---------- RESULTS ----------
    if result.solver.termination_condition == pyo.TerminationCondition.optimal:
        # load the optimal results into the model
        m.solutions.load_from(result)
        _logger.info("EMS Schedule opt. solved and optimal solution found.")

        schedules["p_res"] = np.round(
            np.array(m.grid_load[:]()) - np.array(m.grid_feedin[:]()), opt.round_int
        )
        for idx_pv, pv_id in enumerate(pv_data.keys()):
            # - because generation
            schedules[pv_id]["p"] = -np.round(m.pv_p[idx_pv, :](), opt.round_int)
        for idx_cs, cs_id in enumerate(cs_data.keys()):
            schedules[cs_id]["p"] = np.round(m.cs_p_cha[idx_cs, :](), opt.round_int) - np.round(
                m.cs_p_discha[idx_cs, :](), opt.round_int
            )
        for idx_bss, bss_id in enumerate(bss_data.keys()):
            schedules[bss_id]["p"] = np.round(m.bss_p_cha[idx_bss, :](), opt.round_int) - np.round(
                m.bss_p_discha[idx_bss, :](), opt.round_int
            )
        for idx_hp, hp_id in enumerate(hp_data.keys()):
            # - because generation
            schedules[hp_id]["p_th"] = -np.round(m.hp_p_th[idx_hp, :](), opt.round_int)
            schedules[hp_id]["state"] = np.round(m.hp_state[idx_hp, :](), opt.round_int)

        if num_ev > 0 and any(
            [
                forecast[ev_id]["appearance"][-1] and m.ev_e_pen[idx_ev]() > 10 ** (-opt.round_int)
                for idx_ev, ev_id in enumerate(ev_data.keys())
            ]
        ):
            _logger.warning(
                "EMS Schedule opt. was solved but not all EVs could be entirely filled at the end. "
                f"Penalty variables for the EVs: {sum(m.ev_e_pen[:]())}"
            )  # pragma: no cover
        if opt.consider_thermal and any(np.array(m.th_e_pen_pos[:]()) > 10 ** (-opt.round_int)):
            _logger.warning(
                "EMS schedule opt. was solved but pos. energy demand restriction was surpassed. "
                f"Penalty vars for surp. upper th. energy limit: {sum(m.th_e_pen_pos[:]())}"
            )  # pragma: no cover
        if opt.consider_thermal and any(np.array(m.th_e_pen_neg[:]()) > 10 ** (-opt.round_int)):
            _logger.warning(
                "EMS schedule opt. was solved but neg. energy demand restriction was surpassed. "
                f"Penalty vars for surp. lower th. energy limit: {sum(m.th_e_pen_neg[:]())}"
            )  # pragma: no cover
        if control_signals.steps is not None and any(
            np.array(m.control_pen_max[:]()) > 10 ** (-opt.round_int)
        ):
            _logger.warning(
                "EMS schedule opt. solved but not all control signals (p_max) could be applied. "
                f"Penalty variables for omitting p_max: {sum(m.control_pen_max[:]())}"
            )  # pragma: no cover
        if control_signals.steps is not None and any(
            np.array(m.control_pen_min[:]()) > 10 ** (-opt.round_int)
        ):
            _logger.warning(
                "EMS schedule opt. solved but not all control signals (p_min) could be applied. "
                f"Penalty variables for omitting p_max: {sum(m.control_pen_max[:]())}"
            )  # pragma: no cover

    else:
        raise ValueError("Schedule Optimization unsuccessful: " + result.solver.termination_message)

    return schedules
