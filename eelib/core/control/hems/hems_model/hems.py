"""
Base class for HEMS models.

| Author: elenia@TUBS
| Copyright 2024 elenia
| This file is part of eELib, which is free software under the terms of the GNU GPL Version 3.
"""

from abc import ABC, abstractmethod
from eelib.core.control.hems.hems_helper import hems_bss_helper
from eelib.core.control.hems.hems_helper import hems_hp_helper
from eelib.data import BSSData, CSData, HPData, PVData
from eelib.data import Tariff, TariffSignal


class HEMS(ABC):
    """Models an home energy managament system on building level."""

    # Valid values and types for each parameter that apply for all subclasses
    _VALID_PARAMETERS = {
        "cs_strategy": {
            "types": [str],
            "values": ["max_p", "balanced", "night_charging", "solar_charging"],
        },
        "bss_strategy": {
            "types": [str],
            "values": ["surplus", "reduce_curtailment"],
        },
    }

    @classmethod
    def get_valid_parameters(cls):
        """Returns dictionary containing valid parameter types and values.

        Returns:
            dict: valid parameters for this model
        """
        return cls._VALID_PARAMETERS

    def __init__(
        self,
        eid: str,
        step_size: int,
        cs_strategy: str = "max_p",
        bss_strategy: str = "surplus",
    ):
        """Initializes the eELib HEMS model.

        Args:
            eid (str): name of the model entity
            step_size (int): length of a simulation step in seconds
            cs_strategy (str): selected charging strategy. Defaults to "max_p"
            bss_strategy (str): selected BSS strategy. Default to "surplus".
        """

        # Set attributes of init_vals to static properties
        self.eid = eid
        self.step_size = step_size
        self.cs_strategy = cs_strategy
        self.bss_strategy = bss_strategy

        # create empty list of controlled components and type-specific dict
        self.contr_entities_by_eid = {}
        self.contr_fullid_list_by_type = {}

        # initialize input from potential devices
        self.cs_data: dict[str:CSData] = {}  # dataclass for charging station parameters
        self.bss_data: dict[str:BSSData] = {}  # dataclass for battery storage system parameters
        self.hp_data: dict[str:HPData] = {}  # dataclass for heatpump parameters
        self.pv_data: dict[str:PVData] = {}  # dataclass for photovoltaic system parameters

        # initialize input electrical values
        self.p = {}  # active electrical power of "read-only" devices [W]
        self.q = {}  # active electrical power of "read-only" devices [W]
        self.p_min = None  # minimum active power [W]
        self.p_max = None  # maximum active power [W]

        # initialize output electrical values
        self.p_demand = None  # demand/consumption of active power [W]
        self.p_generation = None  # generation of active power [W]
        self.p_balance = None  # balance for active power [W]
        self.q_balance = None  # balance for reactive power [VAr]
        self.p_set_storage = {}  # active power set value for storage systems [W]
        self.p_set_pv = {}  # active power set value for PV systems [W]
        self.p_set_charging_station = {}  # active power set value for charging stations [W]
        self.p_th_set_heatpump = {}  # thermal power set value for heatpumps [W]

        # initialize input thermal values
        self.p_th_water = {}  # thermal power water [W]
        self.p_th_room = {}  # thermal power room [W]
        self.p_th_dem = {}  # thermal power demand [W]
        self.p_th_gen = {}  # thermal power generation [W]

        # initialize incoming control values
        self.control_signal = None

        # initialize output thermal values
        self.p_th_balance = 0  # thermal power balance in current step
        self.e_th = 0  # thermal energy need at current step
        self.p_th_need_step = 0  # thermal power need at current step

        # save step length and current timestep
        self.step_size = step_size
        self.timestep = 0

        # initialize financial values
        self.tariff = None  # incoming tariff signal
        self.tariff_step = None  # tariff structure for current timestep
        self.cache_capacity_used = []  # to store power values of past timesteps
        self.cost = 0  # payment for the current timestep
        self.revenue = 0  # created revenue for the current timestep
        self.profit = 0  # profit (=revenue-cost) for the current timestep

        # initialize collection of IDs of all PV systems
        self.pv_ids = []

    def add_controlled_entity(self, entities_dict: dict):
        """Adds a controlled entity to the corresponding HEMS's lists (controlled components and
        type-specific).

        Args:
            entities_dict (dict): Input list with all models of a type (e.g. charging_station) which
                            will be connected to the controller.
        """

        # append each entity to control list and update type-specific entity dict
        for entity in entities_dict.values():
            # append entity to control list (overwrite if already present)
            self.contr_entities_by_eid[entity["eid"]] = {
                "full_id": entity["full_id"],
                "etype": entity["type"],
            }

            # update type-specific entity dict
            if entity["type"] not in self.contr_fullid_list_by_type:
                # create field for type if not existing
                self.contr_fullid_list_by_type[entity["type"]] = []
            # use full ID to add entity - no element will be added if already present
            self.contr_fullid_list_by_type[entity["type"]].append(entity["full_id"])

        # collect the IDs of all PV systems
        self.pv_ids = []
        if "PVLib" in self.contr_fullid_list_by_type:
            for pv_full_id in self.contr_fullid_list_by_type["PVLib"]:
                self.pv_ids.append(pv_full_id)
        if "PVLibExact" in self.contr_fullid_list_by_type:
            for pv_full_id in self.contr_fullid_list_by_type["PVLibExact"]:
                self.pv_ids.append(pv_full_id)

    def _get_component_by_type(self, type: str):
        """Gets all controlled components of passed type.

        Args:
            type (str): Query to determine all controlled components of this type.

        Returns:
            dict: Returns all entities of queried type.
        """

        components_of_type = {}

        # check the controlled entities for queried type
        for eid, entity in self.contr_entities_by_eid.items():
            if entity["etype"] == type:
                components_of_type[eid] = entity

        return components_of_type

    @abstractmethod
    def step(self, timestep: int):
        """Handles stepping of energy managament systems by checking power values.

        Args:
            timestep (int): Current simulation time
        """

        # reset financial values at newly arrived timestep
        if not self.timestep == timestep:
            self._reset_financial_values()

        # handle tariff and control signal
        self._handle_incoming_signals()

        # update power, limits, and energy balances
        self._calc_power_energy(timestep=timestep)

        # calculate financial output values
        self._calc_financial_output()

        # update timestep
        if not self.timestep == timestep:
            self.timestep = timestep

    def _calc_power_energy(self, timestep: int):
        """Gathers current power flows, aggregates limits, and calculates energy balances.

        Args:
            timestep (int): Current simulation time
        """

        # handle current (newly arrived) timestep
        if not self.timestep == timestep:
            # set the thermal energy demand of the past (p_th_balance is from last step)
            self.e_th += self.p_th_balance * (self.step_size / 3600)

        # resetting set values and balance for current timestep
        self.p_balance = 0
        self.p_demand = 0
        self.p_generation = 0
        self.p_min = 0
        self.p_max = 0
        self.p_set_storage = {}
        self.p_set_pv = {}
        self.p_set_charging_station = {}
        self.q_balance = 0
        self.p_th_dem = 0
        self.p_th_balance = 0
        self.p_th_set_heatpump = {}

        # NOTE: Demand values are positive, generation values (or BSS discharging) are negative
        # NOTE: Positive values are inductive generation, negative ones are capacitive gen.

        # aggregate power values and limtis for all connected entities: modeled devices
        dev_data_list = []
        if self.bss_data is not None and self.bss_data != {}:
            dev_data_list.append(self.bss_data.values())
        if self.pv_data is not None and self.pv_data != {}:
            dev_data_list.append(self.pv_data.values())
        if self.hp_data is not None and self.hp_data != {}:
            dev_data_list.append(self.hp_data.values())
        if self.cs_data is not None and self.cs_data != {}:
            dev_data_list.append(self.cs_data.values())
        for dev_type_data_list in dev_data_list:
            for dev_data in dev_type_data_list:
                # NOTE: Demand values are positive, generation values (or BSS discharging) are neg.
                if dev_data.p is not None:
                    self.p_balance += dev_data.p
                    self.p_demand += max(dev_data.p, 0)
                    self.p_generation += min(dev_data.p, 0)

                # NOTE: Minimum power is high generation resp. low demand limit, maximum analogue
                if hasattr(dev_data, "p_min") and dev_data.p_min is not None:
                    self.p_min += dev_data.p_min
                if hasattr(dev_data, "p_max") and dev_data.p_max is not None:
                    self.p_max += dev_data.p_max

                # NOTE: Positive values are thermal demand, negative ones are thermal generation
                if hasattr(dev_data, "p_th") and dev_data.p_th is not None:
                    self.p_th_balance += dev_data.p_th

                # NOTE: Positive values are inductive generation, negative ones are capacitive gen.
                if hasattr(dev_data, "q") and dev_data.q is not None:
                    self.q_balance += dev_data.q

        # aggregate power values for all connected entities: read-only devices (electrical)
        if self.p is not None and self.p != {}:
            self.p_balance += sum([p for p in self.p.values() if p is not None])
            self.p_demand += sum([p for p in self.p.values() if p > 0])
            self.p_generation += sum([p for p in self.p.values() if p < 0])
            # power limits also have to account for the fixed power values
            self.p_min += sum([p for p in self.p.values() if p is not None])
            self.p_max += sum([p for p in self.p.values() if p is not None])
        if self.q is not None and self.q != {}:
            self.q_balance += sum([q for q in self.q.values() if q is not None])

        # aggregate power values for all connected entities: read-only devices (thermal)
        if self.p_th_water is not None:
            self.p_th_dem += sum(self.p_th_water.values())
        if self.p_th_room is not None:
            self.p_th_dem += sum(self.p_th_room.values())
        self.p_th_balance += self.p_th_dem

        # calc needed thermal power for this step (<0 for needed generation)
        self.p_th_need_step = -self.p_th_balance - self.e_th * 3600 / self.step_size

    def _set_pv_max(self):
        """Calculate PV system setpoints for all devices by using the maximum power."""

        # set new PV power setpoint if PV sent values
        for pv_full_id in self.pv_ids:
            if pv_full_id in self.pv_data.keys():
                # remove power from balance for target value calculation
                if self.pv_data[pv_full_id].p is not None:
                    self.p_balance -= self.pv_data[pv_full_id].p

                # set target value to max. possible power (NOTE: negative value for generation)
                if self.pv_data[pv_full_id].p_max is not None:
                    self.p_set_pv[pv_full_id] = self.pv_data[pv_full_id].p_max
                else:
                    self.p_set_pv[pv_full_id] = 0

                # calc new p balance
                self.p_balance += self.p_set_pv[pv_full_id]

    def _set_cs_p(self):
        """Calculate charging station setpoints using the chosen charging strategy.

        Raises:
            ValueError: If chosen strategy is not implemented
        """
        # set power setpoint if CS sent data
        if "ChargingStation" in self.contr_fullid_list_by_type:
            for cs_full_id in self.contr_fullid_list_by_type["ChargingStation"]:
                if cs_full_id in self.cs_data.keys():
                    # remove power from balance for target value calculation
                    if self.cs_data[cs_full_id].p is not None:
                        self.p_balance -= self.cs_data[cs_full_id].p

                    # set target value according to cs strategy
                    if self.cs_strategy == "max_p":
                        # charging the EV with the maximal power of the cs as soon as it arrives
                        p_cs = self.cs_data[cs_full_id].p_max
                    else:
                        raise ValueError(f"CS-Strategy {self.cs_strategy} not implemented for HEMS")

                    self.p_set_charging_station[cs_full_id] = p_cs

                    # calc new p balance
                    self.p_balance += self.p_set_charging_station[cs_full_id]

    def _set_hp_p_th(self):
        """Calculate heatpump thermal power setpoints by reducing needed power at this step."""
        if "Heatpump" in self.contr_fullid_list_by_type:
            for hp_full_id in self.contr_fullid_list_by_type["Heatpump"]:
                # adjust needed thermal power with the value from the heatpump
                if hp_full_id in self.hp_data.keys():
                    self.p_th_need_step += self.hp_data[hp_full_id].p_th
                    self.p_th_balance -= self.hp_data[hp_full_id].p_th

                    # set thermal power value according to the thermal limits
                    self.p_th_set_heatpump[hp_full_id] = hems_hp_helper.hp_set_power_with_limits(
                        hp_p_th_max=self.hp_data[hp_full_id].p_th_max,
                        hp_p_th_min_on=self.hp_data[hp_full_id].p_th_min_on,
                        hp_p_th_min=self.hp_data[hp_full_id].p_th_min,
                        hp_p_th_target=self.p_th_need_step,
                    )

                    # adapt thermal power need and balance
                    self.p_th_need_step -= self.p_th_set_heatpump[hp_full_id]
                    self.p_th_balance += self.p_th_set_heatpump[hp_full_id]

    def _set_bss_p(self):
        """Calculate battery storage system setpoints for all devices using the chosen strategy.

        Raises:
            ValueError: If chosen strategy is not implemented
        """
        if "BSS" in self.contr_fullid_list_by_type:
            for bss_full_id in self.contr_fullid_list_by_type["BSS"]:
                # check if this storage device sent power limits, otherwise do not consider
                if bss_full_id not in self.bss_data.keys():
                    continue

                # remove power from balance for target value calculation
                if self.bss_data[bss_full_id].p is not None:
                    self.p_balance -= self.bss_data[bss_full_id].p

                # set target value for BSS according to sent limits
                if self.bss_strategy == "reduce_curtailment":
                    self.p_set_storage[bss_full_id] = (
                        hems_bss_helper.bss_strategy_reduce_curtailment(
                            p_balance=self.p_balance,
                            p_max=self.bss_data[bss_full_id].p_max,
                            p_min=self.bss_data[bss_full_id].p_min,
                        )
                    )

                elif self.bss_strategy == "surplus":
                    self.p_set_storage[bss_full_id] = hems_bss_helper.bss_calc_balance(
                        p_balance=self.p_balance,
                        p_max=self.bss_data[bss_full_id].p_max,
                        p_min=self.bss_data[bss_full_id].p_min,
                    )

                else:
                    raise ValueError(f"BSS-Strategy {self.cs_strategy} not implemented for HEMS")

                # calculate resulting and final power balance for current timestep
                self.p_balance += self.p_set_storage[bss_full_id]

    def _reset_financial_values(self):
        """Reset all financial values (at the beginning of a new timestep)."""
        self.tariff_step = None
        self.cost = 0
        self.revenue = 0
        self.profit = 0

    def _handle_incoming_signals(self):
        """Retrieve tariff settings for this timestep from an incoming tariff signal."""
        # check if there is a tariff signal collected
        if self.tariff is not None and isinstance(self.tariff, TariffSignal):
            if self.tariff.bool_is_list:  # if the signal is of list-form
                self.tariff_step = None
            else:  # if the signal is of single-value-form
                self.tariff_step = Tariff(
                    elec_price=self.tariff.elec_price,
                    feedin_tariff=self.tariff.feedin_tariff,
                    capacity_fee_dem=self.tariff.capacity_fee_dem,
                    capacity_fee_gen=self.tariff.capacity_fee_gen,
                    capacity_fee_horizon_sec=self.tariff.capacity_fee_horizon_sec,
                )
        else:
            self.tariff_step = None

    def _calc_financial_output(self):
        """Calculate financial values like cost, profit and revenue for this timestep."""
        if self.tariff_step is not None:  # check if tariff is currently available
            self.cost = max(self.p_balance, 0) * self.tariff_step.elec_price
            self.revenue = -min(self.p_balance, 0) * self.tariff_step.feedin_tariff
        else:
            self.cost = 0
            self.revenue = 0
        self.profit = self.revenue - self.cost
