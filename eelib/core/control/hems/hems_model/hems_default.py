"""
Default strategy for Energy Management System.
Should be copied and adapted for the use of a specific HEMS concept.

| Author: elenia@TUBS
| Copyright 2024 elenia
| This file is part of eELib, which is free software under the terms of the GNU GPL Version 3.
"""

from .hems import HEMS


class HEMS_default(HEMS):
    """Default strategy for Energy Management System.
    Should be copied and adapted for the use of a specific HEMS concept.
    """

    @classmethod
    def get_valid_parameters(cls):
        """Returns dictionary containing valid parameter types and values.

        Returns:
            dict: valid parameters for this model
        """

        # use parent's parameter list and modify them for this class
        result = HEMS.get_valid_parameters().copy()
        result.update({})
        return result

    def __init__(self, eid: str, step_size: int = 900, **kwargs):
        """Initializes the eELib HEMS default model.

        Args:
            eid (str): name of the entity to create
            step_size (int): length of a simulation step in seconds
            **kwargs: initial values for the HEMS entity
        """

        # call init function of super HEMS class
        super().__init__(eid=eid, step_size=step_size, **kwargs)

    def step(self, timestep: int):
        """Calculates power set values for each connected component according to the strategy.

        Args:
            timestep (int): Current simulation time
        """

        # execute general processes (aggregation of power values etc.)
        super().step(timestep)

        # from here on: execute strategy-specific processes

        # PVs from PVLib: If possible, set PV power setpoint to maximum
        self._set_pv_max()

        # CHARGING STATIONs
        self._set_cs_p()

        # HEATPUMPs AND THERMAL DEMAND
        self._set_hp_p_th()

        # BATTERY STORAGEs
        self._set_bss_p()
