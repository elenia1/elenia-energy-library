"""
Baseclass for HEMS strategies using forecasts (and optimizations).

| Author: elenia@TUBS
| Copyright 2024 elenia
| This file is part of eELib, which is free software under the terms of the GNU GPL Version 3.
"""

from .hems import HEMS
from eelib.core.control.hems.hems_helper import hems_hp_helper
from abc import abstractmethod
from math import inf


class HEMS_forecast(HEMS):
    """Baseclass for HEMS strategies using forecasts (and optimizations)."""

    # define the different attrs based on requested forecast type
    forecast_types = {
        "household_only": {  # NOTE: Only request forecasts from primary models (like csv reader)
            "HouseholdCSV": ["p"],
            "PvCSV": ["p"],
            "HeatpumpCSV": ["p_el"],
            "ChargingStationCSV": ["p"],
            "HouseholdThermalCSV": ["p_th_water", "p_th_room"],
            "EV": ["appearance", "consumption"],
            "PVLib": ["p"],
            "PVLibExact": ["p"],
        }
    }

    @classmethod
    def get_valid_parameters(cls):
        """Returns dictionary containing valid parameter types and values.

        Returns:
            dict: valid parameters for this model
        """

        # use parent's parameter list and modify them for this class
        result = super().get_valid_parameters().copy()
        result.update(
            {
                "forecast_horizon_hours": {"types": [int], "values": (0, inf)},
                "forecast_frequency_hours": {"types": [int], "values": (0, inf)},
                "use_forecast": {"types": [bool], "values": None},
                "forecast_type": {"types": [str], "values": ["household_only"]},
            }
        )
        return result

    def __init__(
        self,
        eid: str,
        step_size: int,
        forecast_horizon_hours: int = 24,
        forecast_frequency_hours: int = 24,
        forecast_type: str = "household_only",
        cs_strategy: str = "max_p",
        bss_strategy: str = "surplus",
    ):
        """Initializes the eELib HEMS model.

        Args:
            eid (str): name of the model entity
            step_size (int): length of a simulation step in seconds
            forecast_horizon_hours (int): timespan of forecast in hours
            forecast_frequency_hours (int): regularity of forecast calculation in hours (time
                between two forecasts)
            forecast_type (str): Types of forecast to use. Defaults to "household_only"
            cs_strategy (str): selected charging strategy. Defaults to "max_p"
            bss_strategy (str): selected BSS strategy. Default to "surplus".

        Raises:
            ValueError: If the forecast frequency is higher than the forecast horizon.
        """

        # call init function of super HEMS class
        super().__init__(
            eid=eid,
            step_size=step_size,
            cs_strategy=cs_strategy,
            bss_strategy=bss_strategy,
        )

        # Set forecast attributes
        self.forecast_type = forecast_type
        self.bool_forecast_now = False  # calculate a forecast at this timestep
        self.bool_schedule_now = False  # calculate schedules at this timestep
        self.forecast_request = dict()  # request for forecasts to calculate
        self.forecast_start = 0  # step to start forecast for
        self.forecast_end = 0  # step to end forecast with
        self.forecast = dict()  # resulting forecasts
        self.schedule = dict()  # schedules for the devices to calculate

        # convert input forecast from hours into timesteps
        self.forecast_horizon = int(forecast_horizon_hours * (3600 / self.step_size))
        self.forecast_frequency = int(forecast_frequency_hours * (3600 / self.step_size))

        if self.forecast_frequency > self.forecast_horizon:
            raise ValueError(f"For HEMS ({self.eid}) the forecast frequency cannot be > horizon.")

    def _request_forecast(self):
        """Set models with their attributes and time horizon for forecast request."""
        # clear request for forecasts
        self.forecast_request = {}

        # check whether there is a forecast needed (or already present)
        if (
            self.timestep == self.forecast_start
            and (self.timestep + self.forecast_horizon) == self.forecast_end
        ):  # forecasting period was already set to this forecast horizon
            if self.forecast != {}:  # there is already a forecast given
                return

        # only request forecast if currently desired
        if self.bool_forecast_now:
            # set starting and end timesteps for forecasts
            self.forecast_start = self.timestep
            self.forecast_end = self.timestep + self.forecast_horizon

            # go by each connected device and check for the need of a forecast
            for model_type, e_fullid_list in self.contr_fullid_list_by_type.items():
                if model_type in self.forecast_types[self.forecast_type].keys():
                    # add forecast request for every entity of this model type
                    for e_full_id in e_fullid_list:
                        self.forecast_request[e_full_id] = {
                            "attr": self.forecast_types[self.forecast_type][model_type],
                            "t": range(self.forecast_start, self.forecast_end),
                        }

    def _set_schedule_values(self):
        """Take schedules of the devices and convert them into set values."""
        self._set_schedule_values_pv()
        self._set_schedule_values_cs()
        self._set_schedule_values_hp()
        self._set_schedule_values_bss()

    def _set_power_in_limits(self, power: float, p_min: float, p_max: float) -> float:
        """Set a power value with regard to a maximum and minimum value, if given.

        Args:
            power (float): current power value to start with
            p_min (float): minimum power limit
            p_max (float): maximum power limit

        Returns:
            float: power value within the limiting bounds
        """
        if p_max is None and p_min is None:  # check if power limits are given
            return power
        elif p_max is None:
            return max(power, p_min)
        elif p_min is None:
            return min(power, p_max)
        else:
            return max(min(power, p_max), p_min)

    def _set_schedule_values_pv(self):
        """Take schedules of PV systems and convert them to set values by using the power limits."""
        if (
            "PVLib" in self.contr_fullid_list_by_type
            or "PVLibExact" in self.contr_fullid_list_by_type
        ):
            for pv_full_id in self.pv_data.keys():
                # switch direction of min/max, because device is GENERATING power
                self.p_set_pv[pv_full_id] = self._set_power_in_limits(
                    power=self.schedule[pv_full_id]["p"][self.timestep % self.forecast_horizon],
                    p_min=self.pv_data[pv_full_id].p_max,
                    p_max=self.pv_data[pv_full_id].p_min,
                )

    def _set_schedule_values_cs(self):
        """Take schedules of charging stations and convert to set values by using the limits."""
        if "ChargingStation" in self.contr_fullid_list_by_type:
            for cs_full_id in self.contr_fullid_list_by_type["ChargingStation"]:
                self.p_set_charging_station[cs_full_id] = self._set_power_in_limits(
                    power=self.schedule[cs_full_id]["p"][self.timestep % self.forecast_horizon],
                    p_min=self.cs_data[cs_full_id].p_min,
                    p_max=self.cs_data[cs_full_id].p_max,
                )

    def _set_schedule_values_hp(self):
        """Take schedules of heatpumps and convert them to set values by using the power limits."""
        if "Heatpump" in self.contr_fullid_list_by_type:
            for hp_full_id in self.contr_fullid_list_by_type["Heatpump"]:
                self.p_th_set_heatpump[hp_full_id] = hems_hp_helper.hp_set_power_with_limits(
                    hp_p_th_max=self.hp_data[hp_full_id].p_th_max,
                    hp_p_th_min_on=self.hp_data[hp_full_id].p_th_min_on,
                    hp_p_th_min=self.hp_data[hp_full_id].p_th_min,
                    hp_p_th_target=self.schedule[hp_full_id]["p_th"][
                        self.timestep % self.forecast_horizon
                    ],
                )

    def _set_schedule_values_bss(self):
        """Take schedules of battery storage systems and convert to set values using the limits."""
        if "BSS" in self.contr_fullid_list_by_type:
            for bss_full_id in self.contr_fullid_list_by_type["BSS"]:
                self.p_set_storage[bss_full_id] = self._set_power_in_limits(
                    power=self.schedule[bss_full_id]["p"][self.timestep % self.forecast_horizon],
                    p_min=self.bss_data[bss_full_id].p_min,
                    p_max=self.bss_data[bss_full_id].p_max,
                )

    @abstractmethod
    def step(self, timestep: int):
        """Gathers current energy flows and calculates energy balances.

        Args:
            timestep (int): Current simulation time
        """

        # set information regarding forecast calculation, if new forecast needs to be requested
        if timestep % self.forecast_frequency == 0:
            self.bool_forecast_now = True
            self.bool_schedule_now = True
        else:
            self.bool_forecast_now = False
            self.bool_schedule_now = False
            self.forecast_request = {}

        # execute general processes (aggregation of power values etc.)
        super().step(timestep)
