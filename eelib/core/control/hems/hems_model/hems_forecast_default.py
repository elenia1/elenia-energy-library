"""
Default forecast strategy for Energy Management System.
Should be copied and adapted for the use of a specific HEMS concept.

| Author: elenia@TUBS
| Copyright 2024 elenia
| This file is part of eELib, which is free software under the terms of the GNU GPL Version 3.
"""

from .hems_forecast import HEMS_forecast
import eelib.core.control.hems.hems_helper.schedule_helper as schedule_help


class HEMS_forecast_default(HEMS_forecast):
    """Default forecast strategy for Energy Management System.
    Should be copied and adapted for the use of a specific HEMS concept.
    """

    @classmethod
    def get_valid_parameters(cls):
        """Returns dictionary containing valid parameter types and values.

        Returns:
            dict: valid parameters for this model
        """

        # use parent's parameter list and modify them for this class
        result = super().get_valid_parameters().copy()
        result.update({})
        return result

    def __init__(self, eid: str, step_size: int = 900, **kwargs):
        """Initializes the eELib HEMS default forecast model.

        Args:
            eid (str): name of the entity to create
            step_size (int): length of a simulation step in seconds
            **kwargs: initial values for the HEMS entity
        """

        # call init function of super HEMS class
        super().__init__(eid=eid, step_size=step_size, **kwargs)

    def step(self, timestep: int):
        """Calculates power set values for each connected component according to the strategy.

        Args:
            timestep (int): Current simulation time
        """

        # execute general processes (aggregation of power values etc.)
        super().step(timestep)

        # request forecasts if needed
        super()._request_forecast()

        # CALC SCHEDULE WITH UNCONTROLLED (CSV) DEVICES
        if self.forecast != {} and self.bool_schedule_now is True:
            # calculate the residual load schedule including all not controllable devices
            self.forecast["p_res"] = schedule_help.calc_forecast_residual(
                forecast=self.forecast,
                forecast_horizon=self.forecast_horizon,
            )
            self.forecast["p_th_dem"] = schedule_help.calc_forecast_thermal_residual(
                forecast=self.forecast, forecast_horizon=self.forecast_horizon
            )

            self.schedule = {}

            # calc schedules for charging station
            if "ChargingStation" in self.contr_fullid_list_by_type:
                self.schedule.update(
                    schedule_help.cs_calc_schedule_uncontrolled(
                        step_size=self.step_size,
                        forecast=self.forecast,
                        forecast_horizon=self.forecast_horizon,
                        cs_data=self.cs_data,
                    )
                )

            # calc schedules for heatpump
            if "Heatpump" in self.contr_fullid_list_by_type:
                self.schedule.update(
                    schedule_help.hp_calc_schedule(
                        step_size=self.step_size,
                        th_demand_profile=self.forecast["p_th_dem"],
                        energy_demand_th=self.e_th,
                        hp_data=self.hp_data,
                    )
                )

            self.schedule["p_res"] = self.forecast["p_res"] + schedule_help.calc_forecast_residual(
                forecast=self.schedule, forecast_horizon=self.forecast_horizon
            )

            # calc schedules for PV systems
            if (
                "PVLib" in self.contr_fullid_list_by_type
                or "PVLibExact" in self.contr_fullid_list_by_type
            ):
                self.schedule.update(
                    schedule_help.pv_calc_schedule(forecast=self.forecast, pv_data=self.pv_data)
                )

            # get schedule from battery storage based on residual load schedule
            if "BSS" in self.contr_fullid_list_by_type:
                self.schedule.update(
                    schedule_help.bss_calc_schedule(
                        step_size=self.step_size,
                        schedule_p_res=self.schedule["p_res"],
                        bss_data=self.bss_data,
                    )
                )

                for bss_eid in self.contr_fullid_list_by_type["BSS"]:
                    self.schedule["p_res"] += self.schedule[bss_eid]["p"]

        # take the schedule values and set them as target values for the devices
        if not self.schedule:
            return

        self._set_schedule_values()
