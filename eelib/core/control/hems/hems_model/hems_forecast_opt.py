"""
Optimization-based forecast/schedule strategy for Energy Management System.

| Author: elenia@TUBS
| Copyright 2024 elenia
| This file is part of eELib, which is free software under the terms of the GNU GPL Version 3.
"""
from .hems_forecast import HEMS_forecast
import eelib.core.control.hems.hems_helper.schedule_helper as schedule_help
from eelib.data import OptimOptions


class HEMS_forecast_opt(HEMS_forecast):
    """Optimization-based forecast/schedule strategy for Energy Management System."""

    @classmethod
    def get_valid_parameters(cls):
        """Returns dictionary containing valid parameter types and values.

        Returns:
            dict: valid parameters for this model
        """

        # use parent's parameter list and modify them for this class
        result = super().get_valid_parameters().copy()
        result.update({})
        return result

    def __init__(self, eid: str, step_size: int = 900, **kwargs):
        """Initializes the eELib HEMS default forecast model.

        Args:
            eid (str): name of the entity to create
            step_size (int): length of a simulation step in seconds
            **kwargs: initial values for the HEMS entity
        """

        # call init function of super HEMS class
        super().__init__(eid=eid, step_size=step_size, **kwargs)

    def step(self, timestep: int):
        """Calculates power set values for each connected component according to the strategy.

        Args:
            timestep (int): Current simulation time
        """

        # execute general processes (aggregation of power values etc.)
        super().step(timestep)

        # request forecasts if needed
        self._request_forecast()

        # CALC SCHEDULE WITH UNCONTROLLED (CSV) DEVICES
        if self.forecast != {} and self.bool_schedule_now is True:
            # calculate the residual load schedule including all not controllable devices
            self.forecast["p_res"] = schedule_help.calc_forecast_residual(
                forecast=self.forecast,
                forecast_horizon=self.forecast_horizon,
            )
            self.forecast["p_th_dem"] = schedule_help.calc_forecast_thermal_residual(
                forecast=self.forecast, forecast_horizon=self.forecast_horizon
            )

            # calc schedules with optimization
            optim_options = OptimOptions(
                consider_thermal=True,
                thermal_energy_start=self.e_th,
                thermal_energy_restriction=2501,
                ev_direct_cha=True,
                ev_end_energy_penalty=3.5,
                bss_direct_cha=True,
                bss_end_energy_incentive=True,
                dir_cha_desc_factor=1 / 1e9,
            )
            self.schedule = schedule_help.calc_schedule_opt(
                step_size=self.step_size,
                forecast=self.forecast,
                forecast_horizon=self.forecast_horizon,
                opt=optim_options,
                tariff=self.tariff,
                cs_data=self.cs_data,
                bss_data=self.bss_data,
                pv_data=self.pv_data,
                hp_data=self.hp_data,
            )

        # take the schedule values and set them as target values for the devices
        if not self.schedule:
            return

        self._set_schedule_values()
