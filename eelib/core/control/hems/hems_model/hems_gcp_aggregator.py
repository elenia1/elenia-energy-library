"""
Aggregation of power flows for depiction of uncontrolled devices in a building.

| Author: elenia@TUBS
| Copyright 2024 elenia
| This file is part of eELib, which is free software under the terms of the GNU GPL Version 3.
"""

from .hems import HEMS


class GCP_Aggregator_HEMS(HEMS):
    """Aggregation of power flows for depiction of uncontrolled devices in a building."""

    @classmethod
    def get_valid_parameters(cls):
        """Returns dictionary containing valid parameter types and values.

        Returns:
            dict: valid parameters for this model
        """

        # use parent's parameter list and modify them for this class
        result = HEMS.get_valid_parameters().copy()
        result.update({})
        return result

    def __init__(self, eid: str, step_size: int = 900, **kwargs):
        """Initializes the eELib GCP_Aggregator_HEMS model.

        Args:
            eid (str): name of the entity to create
            step_size (int): length of a simulation step in seconds
            **kwargs: initial values for the HEMS entity
        """

        # call init function of super HEMS class
        super().__init__(eid=eid, step_size=step_size, **kwargs)

    def step(self, timestep):
        """Aggregation of power values.

        Args:
            timestep (int): Current simulation time
        """

        # execute only general processes (aggregation of power values etc.)
        super().step(timestep)
