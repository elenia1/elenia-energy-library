"""
HEMS for the user-based multi-use of PVS and BSS in MFH. As part of the publication:
Henrik Wagner, Constantin von Luetzow, Marcel Luedecke, Michel Meinert, Bernd Engel
"Empowering Collective Self-Consumption in Multi-Family Houses: User-Based Multi-Use of Residential
Battery Storage Systems", 23rd Wind & Solar Integration Workshop 2024, Helsinki, Finland, DOI: tba.

The simulations were computed using eELib's version from the "WSIS24_user-based_mu" tag.
The used model configurations, simulation data and results can be found on Zenodo,
DOI: 10.5281/zenodo.13821117.

| Author: elenia@TUBS
| Copyright 2024 elenia
| This file is part of eELib, which is free software under the terms of the GNU GPL Version 3.
"""

from .hems import HEMS
from eelib.core.control.hems.hems_helper import hems_bss_helper
from math import isclose, floor, ceil
import warnings
import re


class HEMS_mu_user(HEMS):
    """HEMS for multi household use of energy components. It covers different operating strategies.

    static_eq:
        PV, initial 'e_bss' and capacity allocated evenly per household, based on German 42b EnWG
        p.5 "zu gleichen Anteilen". Works without knowing the annual consumption of households
        ("hh").

    static:
        PV, initial e_bss and capacity allocated based on annual energy demand.
        This might lead to inefficent allocations, e.g. surplus of solar energy and hence grid
        feed-in from one hh, if its virtual storage partition ("vsp") is full,
        while the vsp of another hh could potentially still have capacity and store the energy.

    dynamic_charging:
        PV and initial e_bss allocated based on annual energy demand.
        Capacity and size of vsp not allocated, vsp are flexible and vary in size.
        A hh can feed surplus into the BSS until the total capacity is reached, while the
        available stored energy per hh only is this own feed-in.
        This leads to more efficient usage of the BSS capacity. Still, a surplus grid feed-in
        from one hh (if the BSS is full) might occur simultaneously to a hh drawing from the
        grid because it's vsp is empty.

    dynamic_full:
        PV allocated based on power demand, energy and capacity are not allocated.
        Every hh receives a dynamic proportion of PV power. The energy in the BSS is available
        to every hh based on their demand.
        This optimizes the MFH by avoiding grid feed-in and storing PV energy when possible, but
        a hh might empty the BSS on it's own, after which the others are forced to draw from the
        grid.

    NOTE: Only configured to work with CSV models. At least the e_demand_annual needed for
        static allocation is not integrated for other models yet. '_check_models()' enforces a list
        of supported types.
    """

    @classmethod
    def get_valid_parameters(cls):
        """Returns dictionary containing valid parameter types and values.

        Returns:
            dict: containing valid parameter types and values
        """

        # use parent's parameter list and modify them for this class
        result = HEMS.get_valid_parameters().copy()
        result.update(
            {
                # set in model_data, decides type of allocation of energy components
                # further explanation in class docstring
                "mu_user_op_strategy": {
                    "types": [str],
                    "values": ["static_eq", "static", "dynamic_charging", "dynamic_full"],
                },
            }
        )
        return result

    def __init__(
        self,
        eid: str,
        step_size: int = 900,
        cs_strategy: str = "max_p",
        bss_strategy: str = "surplus",
        mu_user_op_strategy: str = None,
    ):
        """Initializes the eelib HEMS_mu_user model.

        Args:
            eid (str): name of the entity to create
            step_size (int): length of a simulation step in seconds
            cs_strategy (str): selected charging strategy. Defaults to "max_p"
            bss_strategy (str): selected BSS strategy. Default to "surplus".
            mu_user_op_strategy (str): sharing strategy for PV and BSS
                "static_eq": PV and BSS allocated evenly per household
                "static": PV and BSS allocated based on annual demand
                "dynamic_charging": like "static", but HHs may charge until BSS is full
                "dynamic_full": PV allocated based on power demand, BSS shared completely
        """

        # call init function of super HEMS class
        super().__init__(
            eid=eid,
            step_size=step_size,
            cs_strategy=cs_strategy,
            bss_strategy=bss_strategy,
        )

        if bss_strategy != "surplus":
            warnings.warn(
                f"The bss_strategy was chosen to be '{self.bss_strategy}', but this HEMS is set for"
                "a user-based multi-use operation and does not use other bss_strategies. Therefor "
                "'bss_strategy' was set to 'surplus'.",
                UserWarning,
            )
            self.bss_strategy = "surplus"

        # store hems arguments
        self.mu_user_op_strategy = mu_user_op_strategy

        # init input from potential HouseholdCSVs
        self.e_demand_annual = {}

        # init cache timestep to feedback energy to households only on actual step
        self.cache_timestep = -1

        # init cache e_bss to feedback SoH, self-discharge, ... to hhs only once the energy changes
        self.cache_e_bss = 0

        # init data storage and output to db
        self.mu_data = {}
        self.mu_output = {}

        # unforecasted deviation in e_bss that is distributed on all hhs each step
        self.e_deviation_abs = 0  # [Wh]
        self.e_deviation_rel = 0  # (abs_deviation / e_bss) error relative to charging level

        ## efficiency ##

        # init demand devices, needed for dynamic allocation keys
        self.demand_full_ids = set()  # includes "HouseholdCSV", "ChargingStationCSV", "HeatpumpCSV"
        self.step_size_h = self.step_size / 3600  # [h] step size in hours, needed frequently

    def _check_models(self):
        """
        The HEMS model is only configured to work with CSV models. At least the 'e_demand_annual'
        needed for static allocation is not integrated for other models yet.

        Known incompatibilities are:
            "EV": doesn't return an 'e_demand_annual' and mapping of HHs EVs to multiple CSs is not
            supported
            "ChargingStation": mapping of HHs EVs to multiple CSs is not supported and controlling
            of an HP is not integrated
            "HouseholdThermalCSV": thermal values are received, but are input values for "Heatpump"
            which is not supported yet. Use "HeatpumpCSV" instead.
            "Heatpump": power value is not yet allocated to HHs. Use "HeatpumpCSV" instead.

        """

        supported_types = (
            "HouseholdCSV",
            "PvCSV",
            "PVLib",
            "PVLibExact",
            "ChargingStationCSV",
            "HeatpumpCSV",
            "BSS",
            "RetailElectricityProvider",
        )

        for type in self.contr_fullid_list_by_type:
            assert type in supported_types, (
                f"{type} is connected to this HEMS, but might not be supported. Check if the type "
                "is correctly handled in all methods and add it to list of 'supported_types'. "
                "See docstring of this method for a list of known incompatibilities."
            )

    def _init_mu_data(self):
        """Inits the dict 'mu_data', which contains the relevant data per household."""

        # init mu_data to store data per household
        for hh_full_id in self.contr_fullid_list_by_type["HouseholdCSV"]:
            # used in all operating strategies
            self.mu_data[hh_full_id] = {
                "cs_full_ids": [],  # assigned charging stations for this hh
                "num_ppl": None,  # extracted from full_id to allocate heatpump
                "all_key": 0.0,  # (op strat dependent) static or dynamic proportion of components
                "all_key_hp": (
                    0.0
                ),  # allocation key for allocation of electric demand of MFH's central heatpump
                "p_balance": 0.0,  # [W] balance of all power allocated to this hh in current step
                "p_demand": 0.0,  # [W] total demand (p) of hh, i.e. sum of load, cs and hp
                "p_load": 0.0,  # [W] load of hh in current timestep
                "p_cs": 0.0,  # [W] power of assigned charging station(s) in current timestep
                "p_pv": 0.0,  # [W] allocated pv power in current timestep
                "p_hp": 0.0,  # [W] allocated hp power in current timestep
                "p_bss": 0.0,  # [W] allocated bss power in current timestep
                "p_set_bss": 0.0,  # [W] the set value based on partition and component's limit
                "p_trade_req": 0.0,  # [W] power requested from MFH in 'self._bss_calc_transfer()'
                "p_trade": 0.0,  # [W] accepted power trade with the MFH
            }

            # operating strategy specific
            if self.mu_user_op_strategy in ("static_eq", "static", "dynamic_charging"):
                self.mu_data[hh_full_id].update(
                    {
                        "e_bss": 0.0,  # [Wh] allocated available e_bss
                        "e_bss_delta": 0.0,  # [Wh] calculated delta till next step on all paths
                    }
                )
            if self.mu_user_op_strategy in ("static_eq", "static"):
                self.mu_data[hh_full_id]["e_bss_usable"] = 0.0  # [Wh] max usable e_bss
            if self.mu_user_op_strategy in ("dynamic_full",):
                # locate demand_full_ids, to avoid repition in each self._update_allocation_keys()
                for type in ("HouseholdCSV", "ChargingStationCSV", "HeatpumpCSV"):
                    if type in self.contr_fullid_list_by_type:
                        self.demand_full_ids.update(self.contr_fullid_list_by_type[type])

    def _assign_charging_stations(self):
        """Assigns the full_id of each charging station to the first HHs in model_data."""

        if "ChargingStationCSV" in self.contr_fullid_list_by_type:
            cs_list = self.contr_fullid_list_by_type["ChargingStationCSV"].copy()
            while len(cs_list) > 0:
                for hh in self.mu_data.values():
                    # allocate a cs per household until all cs are allocated
                    try:
                        hh["cs_full_ids"].append(cs_list.pop(0))
                    except IndexError:
                        break
        else:
            warnings.warn(
                "There are no charging stations controlled by this HEMS.",
                UserWarning,
            )

    def _calc_all_key_hp(self):
        """Calculates the allocation key for the heatpump to distribute the electricity need
        of the central heatpump of a MFH based on the amount of persons per household.

        Raises:
            ValueError: If number of residents of household is 0, as no information is provided in
                hh_full_id or set to 0.
        """
        if "HeatpumpCSV" in self.contr_fullid_list_by_type:
            # extract amount of people in each househould
            for hh_full_id, hh in self.mu_data.items():
                match = re.search(r"hh_(\d+)p", hh_full_id)
                if match:
                    hh["num_ppl"] = int(match.group(1))

            if any(hh["num_ppl"] == 0 or hh["num_ppl"] is None for hh in self.mu_data.values()):
                raise ValueError(
                    "The number of residents of a household is 0. Check if the hh_full_id          "
                    "                     provides information about the number of residents"
                )
            else:
                # calculate allocation key for heatpump load
                ppl_sum = int(sum(hh["num_ppl"] for hh in self.mu_data.values()))

            for hh_full_id, hh in self.mu_data.items():
                hh["all_key_hp"] = hh["num_ppl"] / ppl_sum

    def _allocate_hh_loads(self):
        """Allocates the input loads from household and cs and allocates the hp load to the
        households. This is independent from the multi-use operating strategy.

        Raises:
            ValueError: If no allocation key for heatpump exists, i.e., allocating the electric load
                of the MFH's central heatpump.
        """

        # HOUSEHOLD LOADs
        for hh_full_id in self.contr_fullid_list_by_type["HouseholdCSV"]:
            # store hh load
            self.mu_data[hh_full_id]["p_load"] = p_load = self.p[hh_full_id]

            # initialize demand (sum of loads) for hh for current step
            self.mu_data[hh_full_id]["p_demand"] = p_load

            # initialize balance for hh for current step
            self.mu_data[hh_full_id]["p_balance"] = p_load

        # CHARGING STATIONs
        if "ChargingStationCSV" in self.contr_fullid_list_by_type:
            for hh_full_id, hh in self.mu_data.items():
                for cs_full_id in hh["cs_full_ids"]:
                    # store cs power
                    hh["p_cs"] = p_cs = self.p[cs_full_id]

                    # store cs load from hh in p_demand
                    hh["p_demand"] += p_cs

                    # and add to hh balance
                    hh["p_balance"] += p_cs

        # HEAT PUMPs
        if "HeatpumpCSV" in self.contr_fullid_list_by_type:
            for hp_full_id in self.contr_fullid_list_by_type["HeatpumpCSV"]:
                for hh_full_id, hh in self.mu_data.items():
                    if hh["all_key_hp"] != 0.0:
                        # store hp power, i.e., hp load of each household
                        hh["p_hp"] = p_hp = self.p[hp_full_id] * hh["all_key_hp"]

                        # store hp load from hh in p_demand
                        hh["p_demand"] += p_hp

                        # and add to hh balance
                        hh["p_balance"] += p_hp

                    else:
                        raise ValueError(
                            "Heat pump connected but electric load is not allocated to households!"
                        )

    def _update_allocation_keys(self):
        """
        Initially calculates or updates an allocation key for every household while
        differentiating operating strategies. They are stored in 'mfh_data[hh_full_id]["all_key"]'.

        static_eq:
            all_key calculated in timestep==0, evenly distributing PV, initial 'e_bss' and capacity
            among hhs. Works without knowing the annual energy demand.

        static:
            all_key calculated in timestep==0 based annual energy demand.
            Used for PV, initial e_bss and capacity

        dynamic_charging:
            like static, all_key calculated in timestep==0 based annual energy demand,
            but used only for PV and inital e_bss.

        dynamic_full:
            all_key calculated every timestep based on p_demand per hh.
            Used for PV.

        Raises:
            ValueError: If the selected operating strategy '{self.mu_user_op_strategy}' is unknown.
        """
        assert "HouseholdCSV" in self.contr_fullid_list_by_type, (
            "It was tried to calculate allocation keys, but there are no HouseholdCSVs"
            " connected to this HEMS. Check model_data.json['HouseholdCSV']."
        )

        match self.mu_user_op_strategy:
            case "static_eq":
                if self.timestep == 0:
                    all_key = 1 / len(self.mu_data)
                    for hh in self.mu_data.values():
                        hh["all_key"] = all_key

            case "static" | "dynamic_charging":
                if self.timestep == 0:
                    e_demand_annual_bldg = sum(self.e_demand_annual.values())

                    for hh_full_id, hh in self.mu_data.items():
                        assert all(
                            item in self.e_demand_annual
                            for item in ([hh_full_id] + hh["cs_full_ids"])
                        ), (  # debug
                            "It was tried to calculate 'static' allocation keys, but the HEMS"
                            " didn't receive an 'e_demand_annual' for every model associated with"
                            " this household. Check connected HouseholdCSVs, ChargingStationCSVs"
                            " and calculation in CSV Reader class."
                        )

                        e_demand_annual_hh = sum(
                            self.e_demand_annual[id] for id in ([hh_full_id] + hh["cs_full_ids"])
                        )
                        hh["all_key"] = e_demand_annual_hh / e_demand_annual_bldg

            case "dynamic_full":
                # MFH demand includes of all connected HHs, CSs and HPs
                # NOTE: similar to self.p_demand, but it includes BSS. So we have to rebuild it

                p_demand_mfh = sum(self.p[full_id] for full_id in self.demand_full_ids)

                # debug
                assert isclose(p_demand_mfh, sum(hh["p_demand"] for hh in self.mu_data.values())), (
                    f"logged demand ({sum(hh['p_demand'] for hh in self.mu_data.values())}) and "
                    f"actual demand ({p_demand_mfh}) diverge"
                )

                # demand of a hh is it's load, assigned CSs and allocated HPs power
                for hh in self.mu_data.values():
                    hh["all_key"] = hh["p_demand"] / p_demand_mfh

            case _:
                raise ValueError(f"The operating strategy '{self.mu_user_op_strategy}' is unknown.")

        # round down all_keys at a decimal to prevent allocating >100%
        magnitude = 10**9
        for hh in self.mu_data.values():
            hh["all_key"] = floor(hh["all_key"] * magnitude) / magnitude

        # debug
        all_sum = sum([hh["all_key"] for hh in self.mu_data.values()])
        assert 0.999 < all_sum <= 1.0, (
            f"Sum of allocation keys with operating strategy '{self.mu_user_op_strategy}' is"
            f" {all_sum} but should be 1.0. Allocation will be inaccurate."
        )

    def _allocate_pv(self):
        """
        Allocates the PV power to households based on the allocation key and sets the PV contol
        value to max.

        Raises:
            KeyError: If a PVLib(Exact) is connected via 'self.pv_ids', but no data / power value
                is received for it.
        """
        # PV
        p_pv_total = 0

        # PvCSVs
        if "PvCSV" in self.contr_fullid_list_by_type:
            p_pv_total += sum(
                self.p[pv_full_id] for pv_full_id in self.contr_fullid_list_by_type["PvCSV"]
            )

        # PVLibs and PVLibExacts
        for pv_full_id in self.pv_ids:
            if pv_full_id in self.pv_data and self.pv_data[pv_full_id].p is not None:
                p_pv_total += self.pv_data[pv_full_id].p
            else:
                raise KeyError(f"{pv_full_id} is connected, but no data is received for it.")

        for hh in self.mu_data.values():
            # the hh receives the "all_key" portion of the PV
            hh["p_pv"] = p_pv_total * hh["all_key"]

            # Update balance
            hh["p_balance"] += p_pv_total * hh["all_key"]

        # set control value
        self._set_pv_max()

    def _bss_log_energy(self):
        """
        If tracked individually (based on op strategy), allocates init 'e_bss' and 'e_bss_usable'.

        These are stored in a HEMS wide dict. Once time moves on (timestep changes):
        1. recalculates the e_bss_usable, taking degrading SoH, self-discharge, ... into account
        2. reads latest p_bss per household and feedbacks the energy flow back into the same dict
        """

        # CAPACITY
        if self.mu_user_op_strategy in ("static_eq", "static"):
            e_bss_usable = sum(BSS.e_bat_usable for BSS in self.bss_data.values())
            for hh in self.mu_data.values():
                # allocate static BSS capacity, but update every step because of SoH
                hh["e_bss_usable"] = e_bss_usable * hh["all_key"]

        # ENERGY
        if self.mu_user_op_strategy in ("static_eq", "static", "dynamic_charging"):
            e_bss_sum = sum(BSS.e_bat for BSS in self.bss_data.values())

            # INIT ENERGY
            if self.timestep == 0:
                for hh in self.mu_data.values():
                    hh["e_bss"] = e_bss_sum * hh["all_key"]

            # ENERGY DRAW in last timestep
            if self.cache_timestep != self.timestep:
                self.cache_timestep = self.timestep
                for hh in self.mu_data.values():
                    hh["e_bss"] += hh["e_bss_delta"]

                # deviation handling: ensure (0 <= e_bss <= e_bss_usable)

                # e_bss < 0
                assert all(hh["e_bss"] >= 0 for hh in self.mu_data.values()), (  # debug
                    "Logged e_bss < 0 Wh for at least one household. This should have been avoided "
                    "by formulas in _bss_calc_vsp_exchange() and _bss_calc_p_set()."
                )

                # e_bss_usable < e_bss
                if self.mu_user_op_strategy in ("static_eq", "static"):
                    # static considers PERSONAL capacity
                    for hh_full_id, hh in self.mu_data.items():
                        e_bss = hh["e_bss"]
                        hh["e_bss"] = min(hh["e_bss"], hh["e_bss_usable"])
                        rel_diff = (hh["e_bss"] - e_bss) / e_bss
                        assert -0.01 < rel_diff <= 0, (  # debug
                            f"e_bss of hh ({hh_full_id}) had to be corrected by"
                            f" {rel_diff*100}%. This might be too large, as it should only"
                            " account for sinking SoH."
                        )
                else:
                    # dynamic_charging considers SHARED capacity
                    e_bss = sum(hh["e_bss"] for hh in self.mu_data.values())
                    e_bss_usable = sum(BSS.e_bat_usable for BSS in self.bss_data.values())
                    f = e_bss_usable / e_bss
                    if f < 1:
                        for hh_full_id, hh in self.mu_data.items():
                            hh["e_bss"] = floor(hh["e_bss"] * f)
                        e_bss_2 = sum(hh["e_bss"] for hh in self.mu_data.values())
                        rel_diff = (e_bss_2 - e_bss) / e_bss
                        assert -0.01 < rel_diff <= 0, (  # debug
                            f"e_bss of MFH had to be corrected by {rel_diff*100}%. This might be"
                            " too large, as it should only account for sinking SoH."
                        )

            # consider other deviations (self-discharge, floating-point errors)
            if self.cache_e_bss != e_bss_sum:
                self.cache_e_bss = e_bss_sum
                e_bss_mfh = sum(hh["e_bss"] for hh in self.mu_data.values())

                # locate deviation between BSS and self.mu_data
                self.e_deviation_abs = deviation = e_bss_sum - e_bss_mfh
                self.e_deviation_rel = deviation / sum(BSS.e_bat for BSS in self.bss_data.values())

                # distribute deviations
                while not isclose(e_bss_sum, e_bss_mfh):
                    deviation = e_bss_sum - e_bss_mfh

                    # excess of energy: physical energy of bss > sum of virtual bss partitions
                    if deviation > 0:
                        if self.mu_user_op_strategy != "dynamic_charging":
                            e_bss_usable_mfh = sum(
                                hh["e_bss_usable"] for hh in self.mu_data.values()
                            )
                            e_cap_mfh = e_bss_usable_mfh - e_bss_mfh
                            if e_cap_mfh <= 0:
                                # happens if deviation is very small ~1e-3
                                break
                            # distribute excess energy based on free PERSONAL capacity
                            for hh in self.mu_data.values():
                                hh["e_bss"] = min(
                                    hh["e_bss"]
                                    + (deviation * (hh["e_bss_usable"] - hh["e_bss"]) / e_cap_mfh),
                                    hh["e_bss_usable"],
                                )  # prevent overflow
                        else:
                            # distribute excess energy proportionally
                            for hh in self.mu_data.values():
                                hh["e_bss"] += deviation * hh["e_bss"] / e_bss_mfh

                    # lack of energy: physical energy of bss < sum of virtual bss partitions
                    else:
                        # cut missing energy proportionally
                        for hh in self.mu_data.values():
                            hh["e_bss"] = max(
                                hh["e_bss"] + (deviation * hh["e_bss"] / e_bss_mfh), 0
                            )

                    e_bss_mfh = sum(hh["e_bss"] for hh in self.mu_data.values())

        # floating-point errors lead to min()/max() not being sufficient to
        # ensure (0 <= e_bss <= e_bss_usable). We'll pull in the limits and accept the deviation.
        if self.mu_user_op_strategy in ("static_eq", "static"):
            for hh in self.mu_data.values():
                hh["e_bss"] = min(hh["e_bss"], hh["e_bss_usable"] - 1)

    def _bss_calc_vsp_exchange(self):
        """
        From all balances, locate transfer energy/power to account with 100% efficiency, as the
        BSS is not physically involved. Given sufficient energy/capacity in VSP, this can already be
        stored. The resulting p_balance can be handled with the actual BSS.

        .. note::
            This function bases on the eelib v1.1.0 bss_model. It only considers self_discharge_step
            a relevant effect to predict when calculating a limit for transfer. Additional effects
            of a future version bss_model might have influence on this prediction!
        """

        assert (
            len(self.contr_fullid_list_by_type["BSS"]) <= 1
        ), "This method is not yet implemented for more than one controlled storage."

        # relevant effects for limits
        bss = list(self.bss_data.values())[0]
        self_discharge_step = bss.self_discharge_step

        for hh in self.mu_data.values():
            if self.mu_user_op_strategy in ("static_eq", "static", "dynamic_charging"):
                # CAPACITY based on op strat
                if self.mu_user_op_strategy in ("static_eq", "static"):
                    # PERSONAL capacity
                    e_bss_cap_hh = hh["e_bss_usable"] - hh["e_bss"] * (1 - self_discharge_step)
                else:
                    # SHARED capacity: allocate based on charge need
                    e_bss_cap_total = sum(BSS.e_bat_usable for BSS in self.bss_data.values()) - sum(
                        BSS.e_bat for BSS in self.bss_data.values()
                    ) * (1 - self_discharge_step)
                    p_balance_neg_sum = sum(
                        hh["p_balance"] for hh in self.mu_data.values() if hh["p_balance"] < 0
                    )
                    e_bss_cap_hh = (
                        floor(e_bss_cap_total * (hh["p_balance"] / p_balance_neg_sum))
                        if hh["p_balance"] < 0
                        else 0
                    )

                # DISCHARGE with 100% efficiency, as it might all be transfer
                p_min = -hh["e_bss"] * (1 - self_discharge_step) / self.step_size_h
                assert p_min <= 0

                # CHARGE with 100% efficiency, as it might all be transfer
                p_max = e_bss_cap_hh / self.step_size_h
                assert p_max >= 0

                # this produces a request to the MFH
                hh["p_trade_req"] = -min(max(-hh["p_balance"], p_min), p_max)

            elif self.mu_user_op_strategy == "dynamic_full":
                # the request is the balance, not considering any energy
                hh["p_trade_req"] = hh["p_balance"]

        # count "(dis)charge" "trade" requests in MFH
        p_req_neg = sum(hh["p_trade_req"] for hh in self.mu_data.values() if hh["p_trade_req"] < 0)
        p_req_pos = sum(hh["p_trade_req"] for hh in self.mu_data.values() if hh["p_trade_req"] > 0)

        for hh in self.mu_data.values():
            # both sides receive a percentage of their request fulfilled
            if hh["p_trade_req"] < 0:
                # OFFERS power
                p_trade = hh["p_trade_req"] * min(abs(p_req_pos / p_req_neg), 1)
            elif hh["p_trade_req"] > 0:
                # NEEDS power
                p_trade = hh["p_trade_req"] * min(abs(p_req_neg / p_req_pos), 1)
            else:
                p_trade = 0

            # trades power as virtual p_bss
            hh["p_bss"] = -p_trade
            hh["p_trade"] = p_trade

            # this results in a new balance to use in next function
            hh["p_balance"] += -p_trade

            # log expected energy change to apply in next step
            if self.mu_user_op_strategy in ("static_eq", "static", "dynamic_charging"):
                hh["e_bss_delta"] = -p_trade * self.step_size_h

    def _bss_calc_p_set(self):
        """
        Taking the transfer/virtual trades into account, this method receives partly aided balances.
        These are now handled by the BSS, for which an efficiency applies.

        .. note::
            This function bases on the eelib v1.1.0 bss_model. It only considers self_discharge_step
            and efficiencies relevant effects to predict when calculating a limit for p_set.
            Other effects are considered by dimming the p_set values into BSS's p_min/p_max.
            Additional details of a future version bss_model might have to be treated differently.
        """

        assert (
            len(self.contr_fullid_list_by_type["BSS"]) <= 1
        ), "This method is not yet implemented for more than one controlled storage."

        assert all(bss.status_curve is False for bss in self.bss_data.values()), (
            "A BSS is selected to work with dynamic efficiency, but this method is only implemented"
            " for static efficiency. Check model_data[BSS] for status_curve = false!"
        )

        # relevant effects for limits
        bss_full_id, bss = list(self.bss_data.items())[0]
        self_discharge_step = bss.self_discharge_step
        dchrg_eff, chrg_eff = bss.discharge_efficiency, bss.charge_efficiency

        for hh in self.mu_data.values():
            if self.mu_user_op_strategy in ("static_eq", "static", "dynamic_charging"):
                # CAPACITY based on op strat
                if self.mu_user_op_strategy in ("static_eq", "static"):
                    # PERSONAL capacity
                    e_bss_cap_hh = hh["e_bss_usable"] - (hh["e_bss"] + hh["e_bss_delta"]) * (
                        1 - self_discharge_step
                    )
                else:
                    # SHARED capacity: allocate based on charge need
                    e_bss_cap_total = sum(BSS.e_bat_usable for BSS in self.bss_data.values()) - sum(
                        BSS.e_bat for BSS in self.bss_data.values()
                    ) * (1 - self_discharge_step)
                    p_balance_neg_sum = sum(
                        hh["p_balance"] for hh in self.mu_data.values() if hh["p_balance"] < 0
                    )
                    e_bss_cap_hh = (
                        floor(e_bss_cap_total * (hh["p_balance"] / p_balance_neg_sum))
                        if hh["p_balance"] < 0
                        else 0
                    )

                # DISCHARGE with BSS's efficiency
                p_min = (
                    -((hh["e_bss"] + hh["e_bss_delta"]) * (1 - self_discharge_step))
                    * dchrg_eff
                    / self.step_size_h
                )

                # CHARGE with BSS's efficiency
                p_max = e_bss_cap_hh / (chrg_eff * self.step_size_h)

                # this generates a p_set
                hh["p_set_bss"] = min(max(-hh["p_balance"], p_min), p_max)

            elif self.mu_user_op_strategy == "dynamic_full":
                # p_set simply is the negative balance
                hh["p_set_bss"] = -hh["p_balance"]

        # as virtual trades were handled separatly, only allow the larger side to use the BSS
        p_set_bss = sum(hh["p_set_bss"] for hh in self.mu_data.values())
        for hh in self.mu_data.values():
            if hh["p_set_bss"] * p_set_bss <= 0:
                hh["p_set_bss"] = 0
        p_set_bss = sum(hh["p_set_bss"] for hh in self.mu_data.values())

        # see how the BSS will respond
        p_bss = self.p_set_storage[bss_full_id] = hems_bss_helper.bss_calc_balance(
            -p_set_bss, bss.p_max, bss.p_min
        )

        # the hh["p_set_bss"] should all demand only CHARGE or DISCHARGE by now
        assert all(hh["p_set_bss"] * p_set_bss >= 0 for hh in self.mu_data.values()), (
            "After transfer connections and for the dimming to work properly, the set values should"
            " only face on direction at this point."
        )

        # if the BSS can't fulfill all the requests, in the first loop we dimm according to their
        # size of the virtual partitions. In the second loop we equally distribute leftover
        # potential
        if p_set_bss != 0:
            if self.mu_user_op_strategy in ("static_eq", "static", "dynamic_charging"):
                if p_set_bss > 0:
                    # CHARGE
                    for hh in self.mu_data.values():
                        p_bss_old = hh["p_set_bss"]
                        p_set_bss_hh = min(hh["p_set_bss"], bss.p_max * hh["all_key"])
                        hh["p_set_bss"] = (
                            floor(p_set_bss_hh) if p_set_bss_hh >= 0 else ceil(p_set_bss_hh)
                        )
                        hh["p_bss_demand"] = p_bss_old - hh["p_set_bss"]
                else:
                    # DISCHARGE
                    for hh in self.mu_data.values():
                        p_bss_old = hh["p_set_bss"]
                        p_set_bss_hh = max(hh["p_set_bss"], bss.p_min * hh["all_key"])
                        hh["p_set_bss"] = (
                            floor(p_set_bss_hh) if p_set_bss_hh >= 0 else ceil(p_set_bss_hh)
                        )
                        hh["p_bss_demand"] = p_bss_old - hh["p_set_bss"]

                # p_potential is positive for charge, negative for discharge
                if p_set_bss > 0:
                    # CHARGE
                    p_potential = sum(
                        bss.p_max * hh["all_key"] - hh["p_set_bss"] for hh in self.mu_data.values()
                    )

                else:
                    # DISCHARGE
                    p_potential = sum(
                        bss.p_min * hh["all_key"] - hh["p_set_bss"] for hh in self.mu_data.values()
                    )

                # second loop to allocate p_bss_trade to other HH
                for hh in self.mu_data.values():
                    # check if there is potential for other HHs to profit from unused limits
                    if p_potential > 0:
                        # CHARGE
                        p_potential_to_charge = sum(
                            hh["p_bss_demand"]
                            for hh in self.mu_data.values()
                            if hh["p_bss_demand"] > 0
                        )

                        if hh["p_bss_demand"] > 0:
                            d = hh["p_bss_demand"] / p_potential_to_charge
                        else:
                            d = 0

                        p_potential_hh = max(min(p_potential * d, hh["p_bss_demand"]), 0)

                    elif p_potential < 0:
                        # DISCHARGE
                        p_potential_to_discharge = sum(
                            hh["p_bss_demand"]
                            for hh in self.mu_data.values()
                            if hh["p_bss_demand"] < 0
                        )

                        if hh["p_bss_demand"] < 0:
                            d = hh["p_bss_demand"] / p_potential_to_discharge
                        else:
                            d = 0

                        p_potential_hh = min(max(p_potential * d, hh["p_bss_demand"]), 0)

                    else:
                        p_potential_hh = 0

                    # round inwards, to prevent floating-point errors
                    p_potential_hh = (
                        floor(p_potential_hh) if p_potential_hh >= 0 else ceil(p_potential_hh)
                    )

                    # adjust p_set by potential from bss trade
                    hh["p_set_bss"] += p_potential_hh

                    # storing the used p_bss from other hh for each hh
                    hh["p_bss_debt"] = p_potential_hh

                    # adjust balance by potential from bss trade
                    hh["p_balance"] += hh["p_set_bss"]

                    # this generates a valid p_bss
                    hh["p_bss"] += hh["p_set_bss"]

                    # log expected energy change to apply in next step
                    hh["e_bss_delta"] += (
                        hh["p_set_bss"]
                        * ((1 / dchrg_eff) if hh["p_set_bss"] < 0 else chrg_eff)
                        * self.step_size_h
                    )
            # if the BSS can't fulfill all the requests, dimm them proportionally
            elif self.mu_user_op_strategy == "dynamic_full":
                # calculate equal dimming factor
                d = p_bss / p_set_bss
                assert 0 <= d <= 1

                # check if we actually need to dimm
                if d != 1.0:
                    for hh in self.mu_data.values():
                        # round inwards, to prevent floating-point errors
                        p_set_bss_hh = hh["p_set_bss"] * d
                        p_set_bss_hh = hh["p_set_bss"] = (
                            floor(p_set_bss_hh) if p_set_bss_hh >= 0 else ceil(p_set_bss_hh)
                        )

                for hh in self.mu_data.values():
                    # this generates a valid p_bss
                    hh["p_bss"] += hh["p_set_bss"]

                    # which can be added to balance
                    hh["p_balance"] += hh["p_set_bss"]

        # send floored values
        p_bss_mfh = sum(hh["p_set_bss"] for hh in self.mu_data.values())
        p_bss = self.p_set_storage[bss_full_id] = hems_bss_helper.bss_calc_balance(
            -p_bss_mfh, bss.p_max, bss.p_min
        )
        assert isclose(p_bss, p_bss_mfh), (
            "Even though, the p_set should be valid at this point, "
            "_bss_calc_p_set() produced a p_set that the BSS doesn't accept"
        )

    def _prepare_output(self):
        """
        Provides a dict of virtual output values for controlled households to put into db.
        This dict is a selection of 'mu_data' and reordered to match the form of hdf5.py 'inputs'.

        As mosaik does not control the households known by this HEMS, calculated values have to be
        send by this model and unpacked by 'hdf5.py' to assign to the correct HHs for evaluation.
        """
        for hh_full_id, hh_dict in self.mu_data.items():
            for attr, val in hh_dict.items():
                if (
                    attr.startswith("p_")  # log all power
                    or attr.startswith("e_")  # all energy
                    or attr in ("all_key", "all_key_hp")  # and additionals
                ):
                    try:
                        self.mu_output[attr][hh_full_id] = val
                    except KeyError:
                        self.mu_output[attr] = {}
                        self.mu_output[attr][hh_full_id] = val

    def step(self, timestep: int):
        """Gathers current energy flows and calculates balances.

        Args:
            timestep (int): Current simulation time
        """

        # execute general processes (aggregation of power values etc.)
        super().step(timestep)

        # init and reset
        if timestep == 0:
            # check that only supported types are connected
            self._check_models()
            # init data storage
            self._init_mu_data()
            # assign charging stations
            self._assign_charging_stations()
            # calculate heatpump allocation key
            self._calc_all_key_hp()

        # store the input, i.e., arising loads from household, cs and heatpump.
        self._allocate_hh_loads()

        # ALLOCATION KEYS
        self._update_allocation_keys()

        # PV
        self._allocate_pv()

        # BSS
        if "BSS" in self.contr_fullid_list_by_type:
            # allocate e_bss and e_bss_usable
            self._bss_log_energy()
            # allow and confirm e_bss exchanges between households with 100% efficiency
            self._bss_calc_vsp_exchange()
            # from this new balance, calc set values for the real p_bss
            self._bss_calc_p_set()

        # prepare the household data to add to db
        self._prepare_output()
