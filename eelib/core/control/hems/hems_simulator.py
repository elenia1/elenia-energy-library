"""
Mosaik interface for the eELib energy management system (EMS) model.
Simulator for communication between orchestrator (mosaik) and EMS entities.

Within this interface mosaik functionalities are used.
Copyright (c) LGPL

| Author: elenia@TUBS
| Copyright 2024 elenia
| This file is part of eELib, which is free software under the terms of the GNU GPL Version 3.
"""

import mosaik_api_v3
from . import hems_model
import eelib.utils.validation as vld
from copy import deepcopy
from eelib.data import BSSData, CSData, EVData, HPData, PVData, TariffSignal, ControlSignalEMS

# SIMULATION META DATA
META = {
    "type": "hybrid",
    "models": {
        "HEMS": {
            "public": True,
            "params": ["init_vals"],
            "attrs": [],  # no attributes listed because it is abstract base class
        },
        "HEMS_default": {
            "public": True,
            "params": ["init_vals"],
            "attrs": [
                "q",
                "p",
                "p_max",
                "p_min",
                "p_set_storage",
                "p_set_charging_station",
                "p_set_pv",
                "p_balance",
                "p_demand",
                "p_generation",
                "q_balance",
                "cs_data",
                "pv_data",
                "bss_data",
                "hp_data",
                "p_th_room",
                "p_th_water",
                "p_th_dem",
                "p_th_balance",
                "e_th",
                "p_th_set_heatpump",
                "tariff",
                "profit",
                "control_signal",
            ],
            "trigger": [
                "p",
                "q",
                "bss_data",
                "cs_data",
                "pv_data",
                "hp_data",
                "tariff",
                "control_signal",
            ],  # input attributes
            "non-persistent": [
                "p_set_storage",
                "p_set_charging_station",
                "p_set_pv",
                "p_th_set_heatpump",
            ],  # output attributes
        },
        "GCP_Aggregator_HEMS": {
            "public": True,
            "params": ["init_vals"],
            "attrs": [
                "p_demand",
                "p_generation",
                "q",
                "p",
                "e_th",
                "p_balance",
                "q_balance",
                "p_th_dem",
                "p_th_balance",
                "tariff",
                "profit",
                "control_signal",
            ],
            "trigger": ["p", "q", "control_signal"],
            # input attributes
            "non-persistent": [
                "p_balance",
                "q_balance",
                "p_demand",
                "p_generation",
            ],  # output attributes
        },
        "HEMS_forecast_base": {
            "public": True,
            "params": ["init_vals"],
            "attrs": [],  # no attributes listed because it is abstract base class
        },
        "HEMS_forecast_default": {
            "public": True,
            "params": ["init_vals"],
            "attrs": [
                "q",
                "p",
                "p_max",
                "p_min",
                "p_set_storage",
                "p_set_charging_station",
                "p_set_pv",
                "p_balance",
                "p_demand",
                "p_generation",
                "q_balance",
                "cs_data",
                "bss_data",
                "pv_data",
                "hp_data",
                "p_th_room",
                "p_th_water",
                "p_th_dem",
                "p_th_balance",
                "e_th",
                "p_th_set_heatpump",
                "tariff",
                "profit",
                "forecast",
                "use_forecast",
                "calc_forecast",
                "forecast_horizon",
                "forecast_frequency",
                "forecast_request",
                "forecasted_attrs",
                "schedule",
                "control_signal",
            ],
            "trigger": [
                "p",
                "q",
                "forecast",
                "cs_data",
                "bss_data",
                "pv_data",
                "hp_data",
                "tariff",
                "control_signal",
            ],  # input attributes
            "non-persistent": [
                "p_set_storage",
                "p_set_charging_station",
                "p_set_pv",
                "p_th_set_heatpump",
                "forecast_request",
            ],  # output attributes
        },
        "HEMS_forecast_opt": {
            "public": True,
            "params": ["init_vals"],
            "attrs": [
                "q",
                "p",
                "p_max",
                "p_min",
                "p_set_storage",
                "p_set_charging_station",
                "p_set_pv",
                "p_balance",
                "p_demand",
                "p_generation",
                "q_balance",
                "cs_data",
                "bss_data",
                "pv_data",
                "hp_data",
                "p_th_room",
                "p_th_water",
                "p_th_dem",
                "p_th_balance",
                "e_th",
                "p_th_set_heatpump",
                "tariff",
                "profit",
                "forecast",
                "use_forecast",
                "calc_forecast",
                "forecast_horizon",
                "forecast_frequency",
                "forecast_request",
                "forecasted_attrs",
                "schedule",
                "control_signal",
            ],
            "trigger": [
                "p",
                "q",
                "forecast",
                "cs_data",
                "bss_data",
                "pv_data",
                "hp_data",
                "tariff",
                "control_signal",
            ],  # input attributes
            "non-persistent": [
                "p_set_storage",
                "p_set_charging_station",
                "p_set_pv",
                "p_th_set_heatpump",
                "forecast_request",
            ],  # output attributes
        },
        "HEMS_mu_user": {
            "public": True,
            "params": ["init_vals"],
            "attrs": [
                "q",
                "p",
                "p_max",
                "p_min",
                "p_set_storage",
                "p_set_charging_station",
                "p_set_pv",
                "p_balance",
                "p_demand",
                "p_generation",
                "q_balance",
                "cs_data",
                "pv_data",
                "bss_data",
                "hp_data",
                "p_th_room",
                "p_th_water",
                "p_th_dem",
                "p_th_balance",
                "e_th",
                "p_th_set_heatpump",
                "tariff",
                "profit",
                "control_signal",
                "e_demand_annual",
                "mu_output",
                "e_deviation_abs",
                "e_deviation_rel",
            ],
            "trigger": [
                "p",
                "q",
                "bss_data",
                "cs_data",
                "pv_data",
                "hp_data",
                "tariff",
                "control_signal",
                "e_demand_annual",
            ],  # input attributes
            "non-persistent": [
                "p_set_storage",
                "p_set_charging_station",
                "p_set_pv",
                "p_th_set_heatpump",
            ],  # output attributes
        },
    },
    "extra_methods": ["add_controlled_entity", "get_entity_by_id"],
}

# accuracy for cached output values to avoid triggering of event-based simulators
ADAPTION_TOLERANCE = 10 ** (-5)


class Sim(mosaik_api_v3.Simulator):
    """Simulator class for eELib EMS model.

    Args:
        mosaik_api_v3 (module): defines communication between mosaik and simulator


    Yields:
        object: Initializes a mosaik event to return ``set_data``.
    """

    def __init__(self):
        """Constructs an object of the EMS:Sim class."""

        super(Sim, self).__init__(META)

        # storing of event-based output info for current same-time loop
        self.output_cache = {}

        # initiate empty dict for model entities
        self.entities = {}

    def init(self, sid, scenario_config, time_resolution=1.0):
        """Initializes parameters for an object of the EMS:Sim class.

        Args:
            sid (str): ID of the created entity of the simulator (e.g. EMSSim-0)
            scenario_config (dict): scenario configuration data, like resolution or step size
            time_resolution (float): fitting of the step size to the simulation scenario step size.

        Returns:
            meta: description of the simulator
        """

        # assign properties for simulator
        self.sid = sid
        self.scenario_config = scenario_config

        return self.meta

    def create(self, num, model_type, init_vals=None):
        """Creates entities of the eELib EMS model.
        Core function of mosaik.

        Args:
            num (int): number of models to be created
            model_type (str): type of created entity (e.g. "HEMS")
            init_vals (list): list with initial values for each EMS entity, defaults to None

        Raises:
            ValueError: If entity ID is already existing

        Returns:
            dict: created entities
        """

        # create empty list for created entities
        entities_orchestrator = []

        for eid, entity_vals in init_vals.items():
            # check if entity ID is already existing
            if eid in self.entities:
                raise ValueError(f"Entity ID {eid} for model type {model_type} already existing.")

            # create entity by specified full ID
            full_id = self.sid + "." + eid

            # get class of specific model and create entity with(out) optional init values
            entity_cls = getattr(hems_model, model_type)
            if init_vals is None or entity_vals is None:
                entity = entity_cls(eid=eid, step_size=self.scenario_config["step_size"])
            else:
                # Validate parameters' types and values
                vld.validate_init_parameters(entity_cls, entity_vals)

                entity = entity_cls(
                    eid=eid,
                    **entity_vals,
                    step_size=self.scenario_config["step_size"],
                )

            # add info to the simulators entity-list and current entities
            self.entities[eid] = {
                "eid": eid,
                "etype": model_type,
                "model": entity,
                "full_id": full_id,
            }
            entities_orchestrator.append({"eid": eid, "type": model_type})

        return entities_orchestrator

    def get_entity_by_id(self, entity_id: str):
        """Searches for a requested entity id and gives back the entity model.

        Args:
            entity_id (str): id of the entity to be searched for

        Returns:
            object: entity model if found, None otherwise
        """

        if entity_id in self.entities.keys():
            return self.entities[entity_id]["model"]
        else:
            return None

    def step(self, time, inputs, max_advance):
        """Performs simulation step calling the eELib EMS model.
        Core function of mosaik.

        Args:
            time (int): current simulation time (given by mosaik)
            inputs (dict): allocation of set values to specific models
            max_advance (int, optional): simulation time until the simulator can safely advance
                it's internal time without causing any causality errors.

        Raises:
            TypeError: if value_dict containing set values has an unknown format

        Returns:
            int: next timestep (when orchestrator calls again)
        """

        self.timestep = time

        # assign property values for each entity and attribute with entity ID
        # process input signals: for the entities (eid), attr is a dict for attributes to be set
        for eid, attrs in inputs.items():
            # for the attributes (attr), setter is a dict for entities with corresponding set values
            for attr, setter in attrs.items():
                # for transmitter (eid_setter), value_dict contains set values (with ids, when dict)
                setting_value_dict = deepcopy(getattr(self.entities[eid]["model"], attr))
                for eid_setter, value_dict in setter.items():
                    # check if no value is send -> to next
                    if value_dict is None:
                        continue

                    if isinstance(value_dict, dict):
                        if attr == "forecast":
                            # go by each id and search for corresponding entity id
                            for getter_id, value in value_dict.items():
                                if eid in getter_id:
                                    setting_value_dict = value
                        elif attr == "tariff":
                            setting_value_dict = TariffSignal.from_dict(value_dict)
                        elif attr == "control_signal":
                            setting_value_dict = ControlSignalEMS.from_dict(value_dict)
                        # check if dataclass using list of dataclasses
                        elif attr in ["bss_data", "cs_data", "pv_data", "hp_data"]:
                            if attr == "bss_data":
                                setting_value_dict[eid_setter] = BSSData.from_dict(value_dict)
                            if attr == "pv_data":
                                setting_value_dict[eid_setter] = PVData.from_dict(value_dict)
                            if attr == "hp_data":
                                setting_value_dict[eid_setter] = HPData.from_dict(value_dict)
                            elif attr == "cs_data":
                                value_dict["ev_data"] = {
                                    ev_id: EVData.from_dict(ev_dict)
                                    for ev_id, ev_dict in value_dict["ev_data"].items()
                                }
                                setting_value_dict[eid_setter] = CSData(**value_dict)
                        else:
                            setting_value_dict[eid_setter] = value_dict
                    # value_dict is not a dict, only a single value -> write directly
                    elif isinstance(value_dict, (float, int, str)):
                        setting_value_dict[eid_setter] = value_dict
                    else:
                        raise TypeError("Unknown format for value_dict")

                # set the collected value for this attribute
                setattr(self.entities[eid]["model"], attr, setting_value_dict)

        # call step function for each entity in the list
        for eid, entity_dict in self.entities.items():
            entity_dict["model"].step(time)

        # next timestamp for simulation
        return None

    def get_data(self, outputs):
        """Gets the data for the next concatenated model.
        Core function of mosaik.

        Args:
            outputs (dict): dictionary with data outputs from each control model

        Raises:
            ValueError: error if attribute not in model metadata

        Returns:
            dict: dictionary with simulation outputs
        """

        # create flag for checking if any new data is send out
        flag_output_changed = False

        # create empty output data dict
        data = {}

        # check current time step (whether a new one is reached)
        if not self.output_cache == {} and self.output_cache["time"] != self.timestep:
            self.output_cache = {}
        data["time"] = self.timestep
        self.output_cache["time"] = self.timestep

        for transmitter_ename, attrs in outputs.items():
            # get name for current entity and create dict field
            entry = self.entities[transmitter_ename]
            if transmitter_ename not in self.output_cache:
                self.output_cache[transmitter_ename] = {}

            # loop over all targeted attributes and check if info is available
            for attr in attrs:
                if attr not in self.meta["models"][type(entry["model"]).__name__]["attrs"]:
                    raise ValueError("Unknown output attribute: %s" % attr)

                # create empty field for cache and output data
                if attr not in self.output_cache[transmitter_ename]:
                    self.output_cache[transmitter_ename][attr] = {}

                output_data_to_save = getattr(entry["model"], attr)

                # check if output is a dict or just a single value
                if isinstance(output_data_to_save, dict):
                    for emitter_ename, value in output_data_to_save.items():
                        if (
                            emitter_ename not in self.output_cache[transmitter_ename][attr]
                        ):  # value currently not stored in output cache
                            self.output_cache[transmitter_ename][attr] = output_data_to_save
                            flag_output_changed = True
                        elif isinstance(value, (float, int)):  # value existent and comparable
                            if (
                                abs(
                                    self.output_cache[transmitter_ename][attr][emitter_ename]
                                    - value
                                )
                                > ADAPTION_TOLERANCE
                            ):
                                self.output_cache[transmitter_ename][attr] = output_data_to_save
                                flag_output_changed = True
                        else:  # value existent and not comparable
                            if self.output_cache[transmitter_ename][attr][emitter_ename] != value:
                                self.output_cache[transmitter_ename][attr] = output_data_to_save
                                flag_output_changed = True
                    if (
                        output_data_to_save == {}
                        and self.output_cache[transmitter_ename][attr] != {}
                    ):
                        self.output_cache[transmitter_ename][attr] = output_data_to_save
                        flag_output_changed = True
                else:  # output_data_to_save is no dict, but a single value to be stored
                    if self.output_cache[transmitter_ename][attr] == {}:  # no value existent
                        self.output_cache[transmitter_ename][attr] = output_data_to_save
                        flag_output_changed = True
                    elif isinstance(output_data_to_save, (float, int)):  # value exist. and compar.
                        if (
                            abs(self.output_cache[transmitter_ename][attr] - output_data_to_save)
                            > ADAPTION_TOLERANCE
                        ):
                            self.output_cache[transmitter_ename][attr] = output_data_to_save
                            flag_output_changed = True
                    else:  # value existent and not comparable
                        if self.output_cache[transmitter_ename][attr] != output_data_to_save:
                            self.output_cache[transmitter_ename][attr] = output_data_to_save
                            flag_output_changed = True

            # set whole output data for this entity
            data[transmitter_ename] = self.output_cache[transmitter_ename]

        # check if nothing is to be send out - send output 1 step later to avoid waiting for data
        if not flag_output_changed:
            if self.timestep == self.scenario_config["n_steps"] - 1:  # is last time step?
                data["time"] = self.timestep + 1
            else:
                data = {"time": self.timestep}

        # return data to mosaik
        return data

    def add_controlled_entity(self, control_entity_eid: str, entity_dict: dict):
        """Adds entities (e.g. pv systems) to the specific control unit entity.

        Args:
            control_entity_eid (str): entity id of control unit entity
            entity_dict (dict): Dictionary of created models to be added to the control unit entity
        """

        self.entities[control_entity_eid]["model"].add_controlled_entity(entity_dict)
