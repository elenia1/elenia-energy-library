"""
eElib battery storage system (BSS) model. The battery and inverter are both modeled together. Among
other things , dynamic efficiency is taken into account.

| Author: elenia@TUBS
| Copyright 2024 elenia
| This file is part of eELib, which is free software under the terms of the GNU GPL Version 3.
"""

import decimal
import math

decimal.getcontext().prec = 4


class BSS:
    """An electrical BSS model can be instantiated with default values or with a map of
    initial values. An energy management model can be used to determine ``p_set`` values.
    """

    # Valid values and types for each parameter
    _VALID_PARAMETERS = {
        "soc_init": {"types": [float], "values": (0, 1)},
        "e_bat_rated": {"types": [int], "values": (0, math.inf)},
        "p_rated_discharge_max": {"types": [int], "values": (-math.inf, 0)},
        "p_rated_charge_max": {"types": [int], "values": (0, math.inf)},
        "discharge_efficiency_init": {"types": [float], "values": (0, 1)},
        "charge_efficiency_init": {"types": [float], "values": (0, 1)},
        "status_curve": {"types": [bool], "values": None},
        "loss_rate": {"types": [float], "values": (0, 1)},
        "dod_max": {"types": [float], "values": (0, 1)},
        "status_aging": {"types": [bool], "values": None},
        "soh_init": {"types": [float], "values": (0, 1)},
        "soh_cycles_max": {"types": [float], "values": (0, 1)},
        "bat_cycles_max": {"types": [int], "values": (0, math.inf)},
        "bat_cycles_init": {"types": [int], "values": (0, math.inf)},
        "step_size": {"types": [int], "values": (0, math.inf)},
        "bat2ac_efficiency": {"types": [list, type(None)], "values": None},
        "ac2bat_efficiency": {"types": [list, type(None)], "values": None},
    }

    @classmethod
    def get_valid_parameters(cls):
        """Returns dictionary containing valid parameter types and values.

        Returns:
            dict: valid parameters for this model
        """
        return cls._VALID_PARAMETERS

    def __init__(
        self,
        eid: str,
        soc_init=0.5,
        e_bat_rated=10000,
        p_rated_discharge_max=-8000,
        p_rated_charge_max=8000,
        discharge_efficiency_init=0.95,
        charge_efficiency_init=0.95,
        status_curve=False,
        loss_rate=0.02,
        dod_max=0.95,
        status_aging=False,
        # capacity over time (battery aging effects)
        soh_init=1.0,
        soh_cycles_max=0.8,
        # turns into 2nd life
        bat_cycles_max=5000,
        bat_cycles_init=0,
        # Simulation properties
        step_size=60 * 15,  # step size in seconds
        bat2ac_efficiency=None,
        ac2bat_efficiency=None,
    ):
        """
        Initializes the eElib BSS model.

        Args:
            eid (str): name of the model entity
            soc_init (float, optional): Initial SOC [-]. Defaults to 0.5.
            e_bat_rated (int, optional): Rated battery capacity [Wh]. Defaults to 10000.
            p_rated_discharge_max (int, optional): Maximum rated discharge power [W].
                Defaults to -8000.
            p_rated_charge_max (int, optional): Maximum rated charge power [W]. Defaults to 8000.
            discharge_efficiency_init (float, optional): Initial discharge efficiency [-].
                Defaults to 0.95.
            charge_efficiency_init (float, optional): Initial charge efficiency [-].
                Defaults to 0.95.
            status_curve (bool, optional): Wether to use a coded curve for simulation of
                charging and discharging power. Defaults to False.
            loss_rate (float, optional): Proportion of the self-discharge
                to the real charging level [-]. Defaults to 0.02.
            dod_max (float, optional): Maximum Depth of Discharge [-]. The portion of the capacity
                that is available for charge/discharge. Defaults to 0.95.
            status_aging (bool, optional): Wether the aging should be calculated. Defaults to False.
            soh_init (int, optional): Initial state of health of the battery. Defaults to 1.
            soh_cycles_max (float, optional): _description_. Defaults to 0.8.
            bat_cycles_max (int, optional): Maximum battery cycles over a lifetime.
                Defaults to 5000.
            bat_cycles_init (int, optional): Initial battery cycles at the start of the simulation.
                Defaults to 0..
            step_size (int, optional): Time range of a step [s]. Defaults to 60*15.
            ac2bat_efficiency (list, optional): List of values for the coded charging curve.
                Defaults to None.
            bat2ac_efficiency (list, optional): List of values for the coded discharging curve.
                Defaults to None.
        """

        self.eid = eid
        self.soc_init = soc_init
        self.e_bat_rated = e_bat_rated
        self.p_rated_discharge_max = p_rated_discharge_max
        self.p_rated_charge_max = p_rated_charge_max
        self.discharge_efficiency_init = discharge_efficiency_init
        self.charge_efficiency_init = charge_efficiency_init
        self.status_curve = status_curve
        self.loss_rate = loss_rate
        self.dod_max = dod_max
        self.status_aging = status_aging
        self.soh_init = soh_init
        self.soh_cycles_max = soh_cycles_max
        self.bat_cycles_max = bat_cycles_max
        self.bat_cycles_init = bat_cycles_init
        self.step_size = step_size
        self.p_max = p_rated_charge_max
        self.p_min = p_rated_discharge_max
        self.bat_cycles = None
        self.p = 0
        self.bat2ac_efficiency = bat2ac_efficiency
        self.ac2bat_efficiency = ac2bat_efficiency

        # set actual soc
        self.soc = self.soc_init

        # set soc limits (NOTE: the upper limit of soc is 1)
        self.soc_min = 1 - self.dod_max

        # set usable battery capacity to rated
        self.e_bat_usable = self.e_bat_rated

        # set energy-based charging level
        self.e_bat = self.soc * self.e_bat_rated

        # calculate energy of one charge-discharge cycle
        self.e_cycle = 2 * self.e_bat_usable

        # set amount of battery cycles
        self.bat_cycles = float(self.bat_cycles_init)

        # set initial state of health
        self.soh = self.soh_init

        # calculate initial e_bat usable according to soh_init
        self._calculate_aging_status()

        # set initial set point for active battery power value
        self.p_set = {}

        # set initial charge/discharge efficiency
        self.charge_efficiency = self.charge_efficiency_init
        self.discharge_efficiency = self.discharge_efficiency_init
        self.charge_efficiency_max = self.charge_efficiency
        self.discharge_efficiency_max = self.discharge_efficiency

        # save step length and current timestep
        self.step_size = step_size
        self.timestep = 0

        # calculate self-discharge per timestep
        self.self_discharge_step = (self.loss_rate / (30 * 24 * 60 * 60)) * self.step_size

    def _set_power_within_limit(self):
        """
        Sets the active power limit of the battery based on its rated charge and discharge
        capacities and the current active power setpoint.

        """
        # Check for set value
        if self.p_set is None or self.p_set == {}:
            self.p = 0
        elif isinstance(self.p_set, (float, int)):
            self.p = self.p_set
        else:
            self.p = sum(self.p_set.values())

        # set power with regard to (dis)charging maximum limits
        self.p = max(min(self.p, self.p_max), self.p_min)

    def _set_energy_within_limit(self):
        """Check that battery energy limits are not surpassed."""
        if self.e_bat > self.e_bat_usable:
            self.e_bat = self.e_bat_usable
        elif self.e_bat < self.soc_min * self.e_bat_usable:
            self.e_bat = self.soc_min * self.e_bat_usable

    def _calc_charging_efficiency(self):
        """
        Sets dynamic (dis)charging efficiency levels
        ("charge_efficiency" / "discharge_efficiency").
        """
        self._calc_charge_efficiency()
        self._calc_discharge_efficiency()

    def _calc_power_limits(self):
        """Sets (dis)charging power limits.

        Raises:
            ValueError: If the discharge rate of the bss, p_min, is positive.
            ValueError: If the charge rate of the bss, p_max, is negative.
        """

        # Calc max. DISCHARGING active power (NOTE: should be <0 due to passive sign convention)
        if not self.discharge_efficiency_max == 0:
            self.p_min = max(
                -(self.e_bat - self.soc_min * self.e_bat_usable)
                * self.discharge_efficiency_max
                * (1 - self.self_discharge_step)
                / (self.step_size / 3600),
                self.p_rated_discharge_max,
            )
        else:
            self.p_min = 0
        # Calc max. CHARGING active power (NOTE: should be >0 due to passive sign convention)
        if not self.charge_efficiency_max == 0:
            self.p_max = min(
                (self.e_bat_usable - self.e_bat * (1 - self.self_discharge_step))
                / self.charge_efficiency_max
                / (self.step_size / 3600),
                self.p_rated_charge_max,
            )

        else:
            self.p_max = 0

        if self.p_min > 0:
            raise ValueError("The discharge rate of the BSS cannot be positive.")

        if self.p_max < 0:
            raise ValueError("The charge rate of the BSS cannot be negative.")

    def step(self, timestep):
        """Performs simulation step of eELib battery model.
        Calculates all of the Dynamic Properties based on the Input Properties.

        Args:
            timestep (int): Current simulation time
        """
        # handle current time step
        if not self.timestep == timestep:
            self.timestep = timestep

            # adapt energy content from last timestep ( + self-discharge)
            e_bat_self_discharge = -(self.e_bat * self.self_discharge_step)
            if self.p >= 0:  # charging
                self.e_bat_step_volume = (
                    self.p * self.charge_efficiency * (self.step_size / 3600) + e_bat_self_discharge
                )
            else:  # discharging
                self.e_bat_step_volume = (
                    self.p / self.discharge_efficiency * (self.step_size / 3600)
                    + e_bat_self_discharge
                )
            self.e_bat += self.e_bat_step_volume

            # Calculate battery cycles
            self.bat_cycles += abs(self.e_bat_step_volume / self.e_cycle)

            # Calculate battery state of health and aging properties
            if self.status_aging:
                self._calculate_aging_status()

        # Set active power and energy within limits
        self._set_power_within_limit()
        self._set_energy_within_limit()
        self.soc = self.e_bat / self.e_bat_usable

        self._calc_charging_efficiency()

        self._calc_power_limits()

    def _calculate_aging_status(self):
        """Calculates the state of health (soh) and then checks if the soh limit has been reached.

        | soh = soh_init - #cycles * aging_of_one_cycle
        | aging_of_one_cycle = (soh_init - soh_end) / #max_cycles

        Raises:
            ValueError: When minimum soh has been reached for the battery
        """

        # Calculate new soh
        self.soh = self.soh_init - self.bat_cycles / self.bat_cycles_max * (
            self.soh_init - self.soh_cycles_max
        )

        # calculate reduced e_bat_usable due to aging
        self.e_bat_usable = self.e_bat_rated * self.soh

        # Check if soh limit is already reached
        if self.soh < self.soh_cycles_max:
            raise ValueError(
                "Minimum soh of %s has been reached - battery needs to"
                " be replaced or soh limit / bat_cycles_init to be increased"
                % (self.soh_cycles_max)
            )

    def _calc_discharge_efficiency(self):
        """Calculation of dynamic discharging efficiency (``bat2ac_efficiency``)."""

        if self.bat2ac_efficiency is not None:
            if self.status_curve:
                self.bat2ac_efficiency = [decimal.Decimal(a) for a in self.bat2ac_efficiency]

                self.p_ratio = decimal.Decimal(abs(self.p / self.p_rated_discharge_max))

                if self.p_ratio <= decimal.Decimal(
                    0.0125
                ):  # battery inverter does not start for low P_ratio, so p results to 0
                    self.discharge_efficiency = math.inf
                    self.p = 0
                else:
                    # formula: # eta = a0 * (p/p_max)^a1 + a2
                    self.discharge_efficiency = float(
                        self.bat2ac_efficiency[0] * (self.p_ratio ** self.bat2ac_efficiency[1])
                        + self.bat2ac_efficiency[2]
                    )

            # calc discharge_efficiency_max = discharge_efficiency for p_rated_discharge_max
            self.discharge_efficiency_max = float(
                self.bat2ac_efficiency[0] * (1 ** self.bat2ac_efficiency[1])
                + self.bat2ac_efficiency[2]
            )

    def _calc_charge_efficiency(self):
        """Calculation of dynamic charging efficiency (``ac2bat_efficiency``)."""

        if self.ac2bat_efficiency is not None:
            if self.status_curve:
                self.ac2bat_efficiency = [decimal.Decimal(a) for a in self.ac2bat_efficiency]

                self.p_ratio = decimal.Decimal(abs(self.p / self.p_rated_charge_max))
                if self.p_ratio <= decimal.Decimal(
                    0.0125
                ):  # battery inverter does not start for low p_ratio, so p and efficiency is 0
                    self.charge_efficiency = 0
                    self.p = 0
                else:
                    # formula: # eta = a0 * (p/p_max)^a1 + a2
                    self.charge_efficiency = float(
                        self.ac2bat_efficiency[0] * (self.p_ratio ** self.ac2bat_efficiency[1])
                        + self.ac2bat_efficiency[2]
                    )

            # calc discharge_efficiency_max = discharge_efficiency for p_rated_discharge_max
            self.charge_efficiency_max = float(
                self.bat2ac_efficiency[0] * (1 ** self.bat2ac_efficiency[1])
                + self.bat2ac_efficiency[2]
            )
