"""
eElib charging station model is built to manage the charging processes of EVs.

| Author: elenia@TUBS
| Copyright 2024 elenia
| This file is part of eELib, which is free software under the terms of the GNU GPL Version 3.
"""

import warnings
import math
from eelib.data import EVData
from eelib.utils import cos_phi_fix


class ChargingStation:
    """Models a charging station for electric vehicles of different types."""

    # Valid values and types for each parameter
    _VALID_PARAMETERS = {
        "p_rated": {"types": [float, int], "values": (0, math.inf)},
        "output_type": {"types": [str], "values": ["AC", "DC"]},
        "charge_efficiency": {"types": [float, int], "values": (0, 1)},
        "discharge_efficiency": {"types": [float, int], "values": (0, 1)},
        "cos_phi": {"types": [float, int], "values": (0, 1)},
    }

    @classmethod
    def get_valid_parameters(cls):
        """Returns dictionary containing valid parameter types and values.

        Returns:
            dict: valid parameters for this model
        """
        return cls._VALID_PARAMETERS

    def __init__(
        self,
        eid: str,
        p_rated: int,
        output_type: str = "AC",
        charge_efficiency: float = 1.0,
        discharge_efficiency: float = 1.0,
        cos_phi: float = 1.0,
        step_size=60 * 15,  # step size in seconds
    ):
        """Initializes the charging station model.

        Args:
            eid (str): Specifically assigned name for this entity.
            p_rated (int): Rated active power [W]
            output_type (str): Current type of charging station - inverter in charging station (=DC)
                or in the car (=AC)
            charge_efficiency (float): Efficiency for charging process. Defaults to 1.0.
            discharge_efficiency (float): Efficiency for discharging process. Defaults to 1.0.
            cos_phi (float): Power factor for reactive power generation. Defaults to 1.0.
            step_size (int): Length of a simulation step in seconds
        """

        # Set attributes of init_vals to static properties
        self.eid = eid
        self.p_rated = p_rated  # rated active power (AC/DC) [W]
        self.output_type = output_type  # source AC/DC [-]
        self.discharge_efficiency = discharge_efficiency  # discharging efficiency [-]
        self.charge_efficiency = charge_efficiency  # charging efficiency [-]
        self.cos_phi = cos_phi

        # initialize dynamic input properties
        self.ev_data: dict[str:EVData] = {}  # input ev dataclass
        self.p_set = {}  # active power set-point [W]

        # initialize dynamic output properties
        self.p = 0  # Active Power (after control) [W]
        self.q = 0  # Reactive Power (after control) [W]
        self.p_device = {}  # active power for every vehicle [W]
        self.p_min = 0  # Minimal active Power [W]
        self.p_max = 0  # Maximal active Power [W]

        # edit efficiency rates
        if self.output_type == "AC" and (charge_efficiency != 1.0 or discharge_efficiency != 1.0):
            self.charge_efficiency = 1
            self.discharge_efficiency = 1
            warnings.warn(
                "WARNING: Efficiency of AC charging is instead set to 1!",
                UserWarning,
            )

        # save step length and current timestep
        self.step_size = step_size
        self.timestep = 0

    def _calc_power_limits(self):
        """Calculate the power limits for the charging station with the input thats coming from the
        electric vehicles.

        Raises:
            ValueError: If the power limits of at least one connected ev do not work together.
        """

        # current efficiency depending on the direction of power flow
        self._calc_current_efficiency()

        # in case no ev is connected to cs - no active power flexibility
        self.p_min = 0
        self.p_max = 0
        for ev_id, ev_data in self.ev_data.items():
            # check for each ev if connected - consider their limits, efficiency and nominal power
            if ev_data.appearance:
                # check if min. and max. power are correct
                if ev_data.p_min > ev_data.p_max:
                    raise ValueError(f"Min. and max. power of ev {ev_id} do not comply.")
                # handle the power limits
                self.p_min = max(
                    self.p_min + ev_data.p_min / self.efficiency,
                    -self.p_rated,
                )
                self.p_max = min(
                    self.p_max + ev_data.p_max / self.efficiency,
                    self.p_rated,
                )

    def _distribute_charging_power(self):
        """Distributes the charging power self.p across all connected cars.
        For this, the distribution is done evenly unless the power limits of the cars are exceeded.

        Raises:
            ValueError: If vehicles charging power does not match the power of the charging station
        """

        # current efficiency depending on the direction of power flow
        self._calc_current_efficiency()

        # check if a distribution within the limits is possible (plus rounding tolerance of 0.1 W)
        p_set = self.p * self.efficiency
        if p_set - 0.1 > sum(
            [ev_data.p_max for ev_data in self.ev_data.values() if ev_data.appearance]
        ):
            p_set = sum([ev_data.p_max for ev_data in self.ev_data.values() if ev_data.appearance])
            self.p = max(min(self.p_max, p_set / self.efficiency), self.p_min)
        elif p_set + 0.1 < sum(
            [ev_data.p_min for ev_data in self.ev_data.values() if ev_data.appearance]
        ):
            p_set = sum([ev_data.p_min for ev_data in self.ev_data.values() if ev_data.appearance])
            self.p = max(min(self.p_max, p_set / self.efficiency), self.p_min)

        # distribution within the limits is possible
        self.p_device = {ev_id: 0 for ev_id in self.ev_data.keys()}

        # first simply divide by number of connected cars for a balanced distribution
        num_considered_cars = sum([1 for ev in self.ev_data.values() if ev.appearance])
        sum_power = self.p * self.efficiency
        if num_considered_cars != 0:  # cars available at charging station
            average_power = sum_power / num_considered_cars
        else:  # no car available at charging station
            self.p = 0
            average_power = 0

        # create dict to save weather a power value for a device has been fixed
        dict_power_fixed = {ev_id: False for ev_id in self.ev_data.keys()}
        for ev_id, ev_data in self.ev_data.items():
            if not ev_data.appearance:
                dict_power_fixed[ev_id] = True

        # distribute until all car power values are fixed
        while not all(dict_power_fixed.values()):
            average_power_loop = average_power  # set av. power in this loop for verification

            # loop over all cars that are connected (appearance is True)
            for ev_id, ev_data in self.ev_data.items():
                if dict_power_fixed[ev_id]:
                    continue

                # check if the average power is within the limits of the car
                if average_power <= ev_data.p_max and average_power >= ev_data.p_min:
                    self.p_device[ev_id] = int(ev_data.appearance) * average_power
                # average power exceeds maximum power -> set maximum power and adapt for average
                elif average_power >= ev_data.p_max:
                    self.p_device[ev_id] = int(ev_data.appearance) * ev_data.p_max
                    num_considered_cars -= 1
                    sum_power -= int(ev_data.appearance) * ev_data.p_max
                    dict_power_fixed[ev_id] = True  # set this car to fix
                # avrg. power falls short of min. power -> set min. power and adapt for average
                else:
                    self.p_device[ev_id] = int(ev_data.appearance) * ev_data.p_min
                    num_considered_cars -= 1
                    sum_power -= int(ev_data.appearance) * ev_data.p_min
                    dict_power_fixed[ev_id] = True  # set this car to fix

            # check if all cars are at their limit and adapt average
            if num_considered_cars == 0:
                break
            else:
                average_power = sum_power / num_considered_cars
                # check if this distribution works and no power value changed in last iteration
                if average_power == average_power_loop:
                    break

        # check charging station power to sum of power to all vehicles
        if abs(self.p - (sum(self.p_device.values()) / self.efficiency)) > 10 ** (-1):
            raise ValueError(
                f"Vehicle power {list(self.p_device.values())} does not match power"
                f" {self.p} of cs '{self.eid}'"
            )

    def _calc_current_efficiency(self):
        """For the current timestep and based on the active power flow calculate the present
        efficiency for the charging station.
        """

        if self.p > 0:  # charging process: powerflow from grid into direction of cars
            self.efficiency = self.charge_efficiency
        elif self.p < 0:  # discharging process: powerflow from cars into direction of grid
            self.efficiency = 1 / self.discharge_efficiency
        else:  # no (dis)charging process, no power "losses"
            self.efficiency = 1

    def step(self, timestep):
        """Performs simulation step of eELib cs model, using the power limits, set values and
        assigning power to charged cars.

        Args:
            timestep (int): Current simulation time in seconds
        """

        # handle current timestep
        if not self.timestep == timestep:
            self.timestep = timestep

        # calculate power limits for the charging station
        self._calc_power_limits()

        # check for set value
        if self.p_set is None or self.p_set == {}:
            self.p = self.p_max  # charge with maximum possible power
        elif isinstance(self.p_set, (float, int)):
            self.p = self.p_set  # take setpoint value
        else:
            self.p = sum(self.p_set.values())  # take setpoint value

        # calculate possibly adapted power limits for the charging station (due to efficiency)
        self._calc_power_limits()

        # consider maximum possible power
        self.p = max(min(self.p_max, self.p), self.p_min)

        # calculate individual charging power for connected cars
        self._distribute_charging_power()

        # Calculate reactive power output from fixed cos_phi
        self.q = cos_phi_fix(self.p, self.cos_phi)
