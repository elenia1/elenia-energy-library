"""
eELib car models the mobility and charging behavior of EV. The EV's electricity demand is derived
from profiles generated by the open-source tool emopby. The model calculates its soc in every step,
based on the cars consumption and possible charging power derived from charging station.

| Author: elenia@TUBS
| Copyright 2024 elenia
| This file is part of eELib, which is free software under the terms of the GNU GPL Version 3.
"""

from eelib.utils.read_pickle import read_pickle
from datetime import datetime, timedelta
import math

TOLERANCE_OVERCHARGED = 11000 / 3600


class EV:
    """Models an electric vehicle and its change of soc depending on driving and charging."""

    # Valid values and types for each parameter
    _VALID_PARAMETERS = {
        "start_time": {"types": [str], "values": None},
        "file_emobpy": {"types": [str], "values": None},
        "step_size": {"types": [int], "values": None},
        "soc_init": {"types": [float], "values": (0, 1)},
        "set_emobpy_val": {"types": [bool], "values": None},
        "soc_min": {"types": [float], "values": (0, 1)},
        "e_max": {"types": [int], "values": (0, math.inf)},
        "p_nom_discharge_max": {"types": [int], "values": (-math.inf, 0)},
        "p_nom_charge_max": {"types": [int], "values": (0, math.inf)},
        "dcharge_efficiency": {"types": [float], "values": (0, 1)},
        "charge_efficiency": {"types": [float], "values": (0, 1)},
        "n_steps": {"types": [int], "values": (0, math.inf)},
    }

    @classmethod
    def get_valid_parameters(cls):
        """Returns dictionary containing valid parameter types and values.

        Returns:
            dict: valid parameters for this model
        """
        return cls._VALID_PARAMETERS

    def __init__(
        self,
        eid: str,
        start_time: str,
        file_emobpy: str,
        step_size: int = 60 * 15,
        n_steps: int = 1,
        soc_init: float = 0.5,
        set_emobpy_val: bool = True,
        soc_min: float = 0.02,
        e_max: int = 50000,
        p_nom_discharge_max: int = -11000,
        p_nom_charge_max: int = 11000,
        dcharge_efficiency: float = 0.95,
        charge_efficiency: float = 0.99,
    ):
        """Initializes the car model.

        Args:
            eid (str): Specifically assigned name for this entity.
            start_time (str): Datestring with start time [YYYY-mm-dd].
            file_emobpy (str): Path to emobpy pickled data
            step_size (int): Length of a timestep in seconds. Defaults to 900.
            n_steps (int): number of steps for the simulation. Defaults to 1.
            soc_init (float): Initial SoC [-]. Defaults to 0.5.
            set_emobpy_val (bool): Whether all parameters should be set to the values of the emobpy
                file. Defaults to False.
            soc_min (float): Minimal SoC [-]. Defaults to 0.02.
            e_max (int): Capacity [Wh]. Defaults to 10000.
            p_nom_discharge_max (int): Max. discharging power of the ev battery. Defaults to -11000.
            p_nom_charge_max (int): Maximum charging power of the ev battery. Defaults to 11000.
            dcharge_efficiency (float): discharging efficiency [-]. Defaults to 0.95.
            charge_efficiency (float): charging efficiency [-]. Defaults to 0.99.

        Raises:
            Warning: When the car is not connected to any charging point
        """

        self.eid = eid

        # save step length and current timestep
        self.start_time = datetime.strptime(start_time, "%Y-%m-%d %H:%M:%S")
        self.step_size = step_size
        self.timestep = 0

        # initialize ev battery power
        self.p = 0  # (Dis)Charging power of the ev battery

        # initialize inputs from charging station
        self.p_cha = {}  # charging power for ev from charging station

        # initialize dynamic car values
        self.appearance = False  # Whether ev is currently connected to a charging station
        self.appearance_start_idx = 0
        self.appearance_end_idx = 0
        self.bev_consumption_period = 0
        self.signal_prognosis_ev_cs = 0

        # EV-batterie-general
        self.soc = soc_init
        self.soc_min = soc_min
        self.e_max = e_max
        self.e_bat = self.soc * self.e_max
        self.dcharge_efficiency = dcharge_efficiency
        self.charge_efficiency = charge_efficiency
        self.p_nom_discharge_max = p_nom_discharge_max  # For bidirectional charging <0
        self.p_nom_charge_max = p_nom_charge_max

        # set data from emobpy file if desired
        self.emobpy_data = read_pickle(file_emobpy)
        self._set_emobpy_data(n_steps)
        if set_emobpy_val:
            self.soc = self.emobpy_data["soc_init"]
            self.soc_min = self.emobpy_data["soc_min"]
            self.e_max = self.emobpy_data["battery_capacity"] * 1000
            self.e_bat = self.soc * self.e_max
            self.dcharge_efficiency = self.emobpy_data["discharging_eff"]
            self.charge_efficiency = self.emobpy_data["charging_eff"]
            try:
                self.p_nom_discharge_max = (
                    -self.emobpy_data["chargingdata"]["capacity_charging_point"]["home"] * 1000
                )
                self.p_nom_charge_max = (
                    self.emobpy_data["chargingdata"]["capacity_charging_point"]["home"] * 1000
                )
            except KeyError as ker:
                raise Warning("There is no charging point home in selected emobpy-pickle") from ker

    def _set_emobpy_data(self, n_steps: int):
        """Collect data from the emobpy pickle file and save it into the model.

        Args:
            n_steps (int): number of steps for the simulation

        Raises:
            IndexError: When the time window of the pickle file does not match the simulation window
            ValueError: when the time resolutions of simulation and pickle file do not match
        """

        # save profiles
        emobpy_profile = self.emobpy_data["profile"]

        # save starting and end time of the emobpy file
        self.emobpy_data["start_time"] = datetime.strptime(self.emobpy_data["refdate"], "%d/%m/%Y")
        self.emobpy_data["end_time"] = self.emobpy_data["start_time"] + timedelta(
            hours=self.emobpy_data["totalrows"] * self.emobpy_data["t"]
        )

        # check emobpy data to the values given for the model (time horizon and granularity)
        if self.start_time < self.emobpy_data["start_time"]:
            raise IndexError(
                f"Start {self.start_time} for {self.eid} exceeds emobpy-pickle start time "
                f"{self.emobpy_data['start_time']}."
            )
        steps_emobpy_data_sec = (self.emobpy_data["end_time"] - self.start_time).total_seconds()
        if n_steps > steps_emobpy_data_sec / self.step_size:
            raise ValueError(
                f"{n_steps} steps for {self.eid} too much for emobpy-pickle, in which "
                f"{int(steps_emobpy_data_sec/self.step_size)} steps are left from start time."
            )

        if self.step_size / 3600 != self.emobpy_data["t"]:
            raise ValueError(
                "The resolution of the model differs from the resolution of emobpy-pickle."
            )

        # adjust emobpy data such that the first values corresponds to the start of the simulation
        idx_profile_init = int(
            (self.start_time - self.emobpy_data["start_time"]).total_seconds() / self.step_size
        )
        self.profile_position_idx_start = emobpy_profile.hh[idx_profile_init]  # save first index
        # remove unnecessary part at beginning and seperately save profiles
        self.consumption_profile = emobpy_profile.consumption.to_numpy()[idx_profile_init:]
        self.charging_point_profile = emobpy_profile.charging_point.to_numpy()[idx_profile_init:]

    def step(self, timestep):
        """Performs simulation step of eELib ev model.

        Args:
            timestep (int): Current simulation time
        """
        # handle current timestep
        if not self.timestep == timestep:
            self.timestep = timestep

            # adapt energy content from last timestep including efficiency
            energy_ac_side = self.p * (self.step_size / 3600)
            if self.p > 0:
                self.e_bat += energy_ac_side * self.charge_efficiency
            else:
                self.e_bat += energy_ac_side / self.dcharge_efficiency

        # get position of current emobpy profile and read possible power consumption (during drive)
        self.current_profile_position_idx = timestep
        self.consumption = self.consumption_profile[self.current_profile_position_idx]

        # check if the car is connected to a charging station
        self._check_appearance()

        # check if the car newly (profile position) arrived (appearence) at the charging station
        if self.appearance is True and (
            self.current_profile_position_idx > self.appearance_end_idx or timestep == 0
        ):
            # check for the time information of the current appearence duration
            self._calc_appearance_duration()

            # prognosis for charging stations regarding the next trip
            self._calc_bev_consumption_period()
        else:
            # check when the car will arrive next time
            self._calc_next_arrival()

        self._calc_power_limits()

        self._calc_power()

        # Set energy within limits
        self._set_energy_within_limit()
        self.soc = self.e_bat / self.e_max

    def _check_appearance(self):
        """Check whether the EV is currently connected to the charging point or not."""

        if self.charging_point_profile[self.current_profile_position_idx] == "home":
            self.appearance = True
        else:
            self.appearance = False
            self.signal_prognosis_ev_cs = 0

    def _calc_next_arrival(self):
        """Calculates when the EV is arriving (the index) for the next time."""

        # if nothing found, take last step +1
        self.appearance_start_idx = len(self.charging_point_profile)

        # otherwise go by remaining indices and check for arrival (state from not "home" to "home")
        for idx in range(self.current_profile_position_idx + 1, len(self.charging_point_profile)):
            if (
                self.charging_point_profile[idx] == "home"
                and self.charging_point_profile[idx - 1] != "home"
            ):
                self.appearance_start_idx = idx
                break

    def _calc_appearance_duration(self):
        """
        Calculate the number of timesteps that the EV is still going to be connected to the
        charging station from now on until the next trip.
        """
        # set index for start of appereance
        self.appearance_start_idx = self.current_profile_position_idx

        # calc index for step when the car is leaving again
        self.appearance_end_idx = len(self.charging_point_profile)  # if not found, take last step+1
        # otherwise go by remaining indices and check for departure (state goes to not "home")
        for idx in range(self.current_profile_position_idx + 1, len(self.charging_point_profile)):
            if self.charging_point_profile[idx] != "home":
                self.appearance_end_idx = idx
                break

        # calc duration of appearence in seconds
        self.appearance_duration = (
            self.appearance_end_idx - self.appearance_start_idx
        ) * self.step_size

    def _calc_power(self):
        """Calculate the power at the current timestep within the (dis)charging limits."""

        if self.consumption is not None and self.consumption > 0:  # during a trip
            self.p = -self.consumption * 1000 / (self.step_size / 3600)
        else:  # when connected to charging station
            # check for set value
            if self.p_cha is None or self.p_cha == {}:
                self.p = 0
            elif isinstance(self.p_cha, (float, int)):
                self.p = self.p_cha
            else:
                self.p = sum(self.p_cha.values())

            # consider power limits
            self.p = max(min(self.p, self.p_max), self.p_min)

    def _set_energy_within_limit(self):
        """Check that battery energy limits are not surpassed."""
        if self.e_bat > self.e_max:  # check overcharging
            self.e_bat = self.e_max
        elif self.e_bat < self.e_max * self.soc_min:  # check if battery fully discharged
            self.e_bat = self.e_max * self.soc_min

    def _calc_power_limits(self):
        """Calculate the maximum (dis)charging power for the ev battery depending on the power
        limits and the current stored energy.
        """

        # Calc max. discharging power (NOTE: should be <0 due to passive sign convention)
        if not self.dcharge_efficiency == 0:
            self.p_min = max(
                -self.e_bat * self.dcharge_efficiency / (self.step_size / 3600),
                self.p_nom_discharge_max,
            )
        else:
            self.p_min = 0

        # Calc max. charging power (NOTE: should be >0 due to passive sign convention)
        if not self.charge_efficiency == 0:
            self.p_max = min(
                (self.e_max - self.e_bat) / self.charge_efficiency / (self.step_size / 3600),
                self.p_nom_charge_max,
            )
        else:
            self.p_max = 0

    def _calc_bev_consumption_period(self):
        """Calculate the electr. consumption of the BEV while at home for the next trip."""

        # Set signal for prognosis TRUE/1 as a new trip of ev is initialized
        self.signal_prognosis_ev_cs = 1

        # calculate range where the car is on this trip
        car_trip_start_idx = self.appearance_end_idx
        car_trip_end_idx = len(self.charging_point_profile) - 1  # take last step if nothing found
        # go by remaining indices and check for arrival (state goes to "home"), take step before
        for idx in range(self.current_profile_position_idx, len(self.charging_point_profile) - 1):
            if (
                self.charging_point_profile[idx + 1] == "home"
                and self.charging_point_profile[idx] != "home"
            ):
                car_trip_end_idx = idx
                break

        # calculate energy consumption in next trip of EV
        self.bev_consumption_period = self.consumption_profile[
            car_trip_start_idx:car_trip_end_idx
        ].sum()
