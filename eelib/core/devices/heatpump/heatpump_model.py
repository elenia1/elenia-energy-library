"""
eElib heat pump model for calculation of the thermal generation and electrical load of a heat pump.
The model approximates the thermal calculations with power values and their impact on temperatures.

| Author: elenia@TUBS
| Copyright 2024 elenia
| This file is part of eELib, which is free software under the terms of the GNU GPL Version 3.
"""

import warnings
import math
from eelib.utils import cos_phi_fix


class Heatpump:
    """Models a heatpump."""

    _VALID_PARAMETERS = {
        "p_rated_th": {"types": [float, int], "values": (0, math.inf)},
        "p_min_th_rel": {"types": [float], "values": (0, 1)},
        "time_min": {"types": [int], "values": (0, math.inf)},
        "cop": {"types": [float], "values": (0, math.inf)},
        "modulation": {"types": [int], "values": [0, 1]},
        "cos_phi": {"types": [float, int], "values": (0, 1)},
    }

    @classmethod
    def get_valid_parameters(cls):
        """Returns dictionary containing valid parameter types and values.

        Returns:
            dict: valid parameters for this model
        """
        return cls._VALID_PARAMETERS

    def __init__(
        self,
        eid: str,
        p_rated_th=10000,  # rated thermal pwoer [W]
        modulation=1,  # modular operation of heatpump, 1 = True 0 = False
        p_min_th_rel=0.5,  # proportion of minimal thermal power to rated thermal power [%]
        time_min=2100,  # minimum time after tact [s]
        cop=3.60584,  # fixec Coefficient of Power value  [-]
        cos_phi: float = 1.0,
        step_size=60 * 15,  # step size in seconds
    ):
        """
        Initializes the eElib heatpump model.

        Args:
            eid (str): name of the model entity
            p_rated_th (int, optional): rated thermal pwoer [W]. Defaults to 10000.
            modulation (bool, optional): modular operation of heatpump, 1 = True 0 = False.
                Defaults to 1.
            p_min_th_rel (float, optional): proportion of minimal thermal power
                to rated thermal power [%]. Defaults to 0.5.
            time_min (int, optional): minimum time after tact [s]. Defaults to 2100.
            cop (float, optional): fixed Coefficient of Power value  [-]. Defaults to 3.60584.
            cos_phi (float): Power factor for reactive power generation. Defaults to 1.0.
            step_size (int, optional): step size in seconds. Defaults to 600.
        """

        self.eid = eid
        self.p_rated_th = p_rated_th
        self.p_min_th_rel = p_min_th_rel
        self.time_min = time_min
        self.time_on = self.time_min  # time the heatpump is on [s]
        self.time_off = self.time_min  # time the heatpump is turned off [s]
        self.cop = cop
        self.modulation = modulation
        self.cos_phi = cos_phi
        self.step_size = step_size

        # edit relative thermal minimum power
        if not self.modulation and p_min_th_rel != 1.0:
            self.p_min_th_rel = 1
            warnings.warn(
                "WARNING: Minimum thermal relative power is instead set to 1!",
                UserWarning,
            )

        # set initial set point for heatpump power value
        self.p_th = 0
        self.p_th_set = {}  # thermal power given by energy management system
        self.p_th_max = -self.p_rated_th
        self.p_th_min_on = -self.p_rated_th * self.p_min_th_rel
        self.p_th_min = 0
        self.state = "off"  # current state of the hp (must_on, on, must_off, off)
        # electrical power values
        self.p = 0
        self.p_min = 0
        self.p_min_on = 0
        self.p_max = 0
        self.q = 0

        # save time step length and current time step
        self.step_size = step_size
        self.timestep = None

    def _set_state(self):
        """Set the state of the heatpump with regard to the current power on on/off time."""

        if self.p_th < 0:  # heatpump is ON if the thermal generation is larger than zero
            if self.time_on >= self.time_min:
                self.state = "on"
            else:
                self.state = "must_on"
        else:
            if self.time_off >= self.time_min:
                self.state = "off"
            else:
                self.state = "must_off"

    def _calc_thermal_limits(self):
        """Calculate the thermal maximum and minimum power of the heatpump."""

        if self.state != "must_off":  # Check whether heatpump can be turned off
            self.p_th_max = -self.p_rated_th
            self.p_th_min_on = (
                -self.p_rated_th * self.p_min_th_rel if self.modulation else self.p_th_max
            )
        else:
            self.p_th_max = 0
            self.p_th_min_on = 0
        if self.state != "must_on":  # Check whether heatpump can be turned off
            self.p_th_min = 0
        else:
            self.p_th_min = (
                -self.p_rated_th * self.p_min_th_rel if self.modulation else self.p_th_max
            )

    def step(self, timestep):
        """Calculating of current thermal and active power values, as well as the minimum and
        maximal values while differentiating between modulation or on/off heatpump.

        Args:
            timestep (int): Current simulation time
        """
        # handle current time step
        if not self.timestep == timestep:
            self.timestep = timestep  # set current time

            # do not set anything for first step, both states are possible
            if self.timestep != 0:
                if self.p_th < 0:  # heatpump is ON if the thermal generation is larger than zero
                    self.time_on += self.step_size
                    self.time_off = 0
                else:
                    self.time_off += self.step_size
                    self.time_on = 0

                # possibly adjust the state of the hp
                self._set_state()

        # calculate the thermal power limits of the heatpump
        self._calc_thermal_limits()

        # Check for set value
        if self.p_th_set is None or self.p_th_set == {}:
            self.p_th = 0
        elif isinstance(self.p_th_set, (float, int)):
            self.p_th = self.p_th_set
        else:
            self.p_th = sum(self.p_th_set.values())

        # Calculation of thermal power from thermal set value and operational power limits
        if self.p_th < 0:  # need for heating power is existent
            if self.state == "must_off":  # hp cannot be turned on
                self.p_th = 0
            else:  # hp is switched on or can be switched on
                if self.modulation == 1:
                    self.p_th = min(max(self.p_th, self.p_th_max), self.p_th_min_on)
                else:
                    # for ON-OFF heat pump simply set to ON-value (maximum)
                    self.p_th = self.p_th_max

        else:  # If no heating power is existent
            if self.state == "must_on":
                self.p_th = self.p_th_min
            else:
                self.p_th = 0

        # Calculate electric power with cop (NOTE: Thermal generation = electrical demand)
        self.p = -self.p_th / self.cop
        self.p_min = -self.p_th_min / self.cop
        self.p_min_on = -self.p_th_min_on / self.cop
        self.p_max = -self.p_th_max / self.cop
        # Calculate reactive power output from fixed cos_phi for modulating heatpumps
        if self.modulation:
            self.q = cos_phi_fix(self.p, self.cos_phi)

        # NOTE: adjustment of state not needed - just for possible power values at this timestep
