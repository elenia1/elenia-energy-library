"""
Mosaik interface for the eELib heatpump model.
Simulator for communication between orchestrator (mosaik) and heatpump entities.

Within this interface mosaik functionalities are used.
Copyright (c) LGPL

| Author: elenia@TUBS
| Copyright 2024 elenia
| This file is part of eELib, which is free software under the terms of the GNU GPL Version 3.
"""

import mosaik_api_v3
from eelib.core.devices.heatpump import heatpump_model
from eelib.data import HPData
import dataclasses
import eelib.utils.validation as vld
from copy import deepcopy

# accuracy for cached output values to avoid triggering of event-based simulators
ADAPTION_TOLERANCE = 10 ** (-5)

# META DATA: Simulator information about model classes, its parameters, attributes, and methods
META = {
    "type": "hybrid",
    "models": {
        "Heatpump": {
            "public": True,
            "params": ["init_vals"],
            "attrs": [
                "p_rated_el",
                "p_rated_th",
                "p_min_th_rel",
                "time_min",
                "time_on",
                "time_off",
                "cop",
                "modulation",
                "step_size",
                "timestep",
                "p_th",
                "p_th_max",
                "p_th_min",
                "p_th_min_on",
                "p_th_calc",
                "state",
                "p_th_set",
                "p",
                "p_min",
                "p_min_on",
                "p_max",
                "q",
                "hp_data",
            ],
            "trigger": ["p_th_set"],  # input attributes
            "non-persistent": [
                "p_th",
                "p",
                "hp_data",
            ],  # output attributes
        },
    },
    "extra_methods": ["get_entity_by_id"],
}


class Sim(mosaik_api_v3.Simulator):
    """Simulator class for eELib heatpump model.

    Args:
        mosaik_api_v3 (module): defines communication between mosaik and simulator

    Raises:
        ValueError: Unknown output attribute, when not described in META of simulator
    """

    def __init__(self):
        """Constructs an object of the Heatpump:Sim class."""

        super(Sim, self).__init__(META)

        # storing of event-based output info (for same-time loop or next timestep)
        self.output_cache = {}

        # storing of device data
        self.data = {}

        # initiate empty dict for model entities
        self.entities = {}

    def init(self, sid, scenario_config, time_resolution=1.0):
        """Initializes parameters for an object of the Heatpump:Sim class.

        Args:
            sid (str): ID of the created entity of the simulator (e.g. LoadSim-0)
            scenario_config (dict): scenario configuration data, like resolution or step size
            time_resolution (float): Time resolution of current scenario.

        Returns:
            meta: description of the simulator
        """

        # assign properties for simulator
        self.sid = sid
        self.scenario_config = scenario_config

        return self.meta

    def create(self, num, model_type, init_vals):
        """Creates entities of the eELib load model.
        Core function of mosaik.

        Args:
            num (int): number of load models to created
            model_type (str): type of created instance (e.g. "household")
            init_vals (list): list with initial values for each load entity

        Raises:
            ValueError: If entity ID is already existing

        Returns:
            dict: created entities
        """

        # create empty list for created entities
        entities_orchestrator = []

        for eid, entity_vals in init_vals.items():
            # check if entity ID is already existing
            if eid in self.entities:
                raise ValueError(f"Entity ID {eid} for model type {model_type} already existing.")

            # create entity by specified full ID
            full_id = self.sid + "." + eid

            # get class of specific model and create entity with init values after validation
            entity_cls = getattr(heatpump_model, model_type)
            vld.validate_init_parameters(entity_cls, entity_vals)
            entity = entity_cls(
                eid,
                **entity_vals,
                step_size=self.scenario_config["step_size"],
            )

            # add info to the simulators entity-list and current entities
            self.entities[eid] = {
                "eid": eid,
                "etype": model_type,
                "model": entity,
                "full_id": full_id,
            }
            entities_orchestrator.append({"eid": eid, "type": model_type})
            self.data[full_id] = HPData()

        return entities_orchestrator

    def get_entity_by_id(self, entity_id: str):
        """Searches for a requested entity id and gives back the entity model.

        Args:
            entity_id (str): id of the entity to be searched for

        Returns:
            object: entity model if found, None otherwise
        """

        if entity_id in self.entities.keys():
            return self.entities[entity_id]["model"]
        else:
            return None

    def step(self, time, inputs, max_advance):
        """Performs simulation step calling the eELib load model.
        Core function of mosaik.

        Args:
            time (int): current simulation time (given by mosaik)
            inputs (dict, optional): allocation of return values to specific models (NOT NEEDED FOR
                                    LOAD MODEL)
            max_advance (int, optional): simulation time until the simulator can safely advance it's
                                        internal time without causing any causality errors.

        Raises:
            ValueError: Error if more than one set value is tried to be given to entity
            TypeError: value_dict has unknown format

        Returns:
            int: next timestep (when orchestrator calls again)
        """

        self.timestep = time

        # assign property values for each entity and attribute with entity ID
        # process input signals: for the entities (eid), attr is a dict for attributes to be set
        for eid, attrs in inputs.items():
            # for the attributes (attr), setter is a dict for entities with corresponding set values
            for attr, setter in attrs.items():
                # for transmitter (eid_setter), value_dict contains set values with ids
                setting_value_dict = deepcopy(getattr(self.entities[eid]["model"], attr))
                for eid_setter, value_dict in setter.items():
                    # check if no value is send -> to next
                    if value_dict is None:
                        continue

                    if isinstance(value_dict, dict):
                        # go by each id and search for corresponding entity id
                        for getter_id, value in value_dict.items():
                            if eid in getter_id:
                                setting_value_dict[eid_setter] = value
                    # value_dict is not a dict, only a single value -> write directly
                    elif isinstance(value_dict, (float, int)):
                        setting_value_dict[eid_setter] = value_dict
                    else:
                        raise TypeError("Unknown format for value_dict")

                # check if there is more than one power set value - otherwise directly set it
                if attr == "p_th_set":
                    if len(setting_value_dict) > 1:
                        raise ValueError("There is more than one power set value for " + eid)

                # set the collected value for this attribute
                setattr(self.entities[eid]["model"], attr, setting_value_dict)

        # call step function for each entity in the list
        for eid, entity_dict in self.entities.items():
            entity_dict["model"].step(time)
            self.data[self.entities[eid]["full_id"]] = HPData(
                p=entity_dict["model"].p,
                p_min=entity_dict["model"].p_min,
                p_max=entity_dict["model"].p_max,
                p_rated_th=entity_dict["model"].p_rated_th,
                p_min_th_rel=entity_dict["model"].p_min_th_rel,
                time_min=entity_dict["model"].time_min,
                time_on=entity_dict["model"].time_on,
                time_off=entity_dict["model"].time_off,
                state=entity_dict["model"].state,
                cop=entity_dict["model"].cop,
                p_th=entity_dict["model"].p_th,
                p_th_min=entity_dict["model"].p_th_min,
                p_th_min_on=entity_dict["model"].p_th_min_on,
                p_min_on=entity_dict["model"].p_min_on,
                p_th_max=entity_dict["model"].p_th_max,
                q=entity_dict["model"].q,
            )

        # next timestamp for simulation
        return None

    def get_data(self, outputs):
        """Gets the data for the next connected model.
        Core function of mosaik.

        Args:
            outputs (dict): dictionary with data outputs from each entity

        Raises:
            ValueError: error if attribute not in model metadata

        Returns:
            dict: dictionary with simulation outputs
        """

        # create flag for checking if any new data is send out
        flag_output_changed = False

        # create empty output data dict
        data = {}

        # check current timestep (whether a new one is reached)
        if not self.output_cache == {} and self.output_cache["time"] != self.timestep:
            self.output_cache = {}
        data["time"] = self.timestep
        self.output_cache["time"] = self.timestep

        for transmitter_ename, attrs in outputs.items():
            # get name for current entity and create dict field
            entry = self.entities[transmitter_ename]
            if transmitter_ename not in self.output_cache:
                self.output_cache[transmitter_ename] = {}

            # loop over all targeted attributes and check if info is available
            for attr in attrs:
                if attr not in self.meta["models"][type(entry["model"]).__name__]["attrs"]:
                    raise ValueError("Unknown output attribute: %s" % attr)

                # create empty field for cache and output data
                if attr not in self.output_cache[transmitter_ename]:
                    self.output_cache[transmitter_ename][attr] = {}

                # get attribute of this model entity
                if hasattr(entry["model"], attr):
                    output_data_to_save = getattr(entry["model"], attr)
                else:  # attribute not existent - dataclass object
                    output_data_to_save = self.data[entry["full_id"]]
                    # check if data empty or values outside ofadaption tolerance
                    if self.output_cache[transmitter_ename][
                        attr
                    ] == {} or output_data_to_save.check_adaption_tolerance(
                        ADAPTION_TOLERANCE,
                        self.output_cache[transmitter_ename][attr],
                    ):
                        # convert dataclass to dict
                        self.output_cache[transmitter_ename][attr] = dataclasses.asdict(
                            output_data_to_save
                        )
                        flag_output_changed = True
                    continue  # go on with next attribute

                # check if output is a dict or just a single value
                if isinstance(output_data_to_save, dict):
                    for emitter_ename, value in output_data_to_save.items():
                        if (
                            emitter_ename not in self.output_cache[transmitter_ename][attr]
                        ):  # value currently not stored in output cache
                            self.output_cache[transmitter_ename][attr] = output_data_to_save
                            flag_output_changed = True
                        elif isinstance(value, (float, int)):  # value existent and comparable
                            if (
                                abs(
                                    self.output_cache[transmitter_ename][attr][emitter_ename]
                                    - value
                                )
                                > ADAPTION_TOLERANCE
                            ):
                                self.output_cache[transmitter_ename][attr] = output_data_to_save
                                flag_output_changed = True
                        else:  # value existent and not comparable
                            if self.output_cache[transmitter_ename][attr][emitter_ename] != value:
                                self.output_cache[transmitter_ename][attr] = output_data_to_save
                                flag_output_changed = True
                    if (
                        output_data_to_save == {}
                        and self.output_cache[transmitter_ename][attr] != {}
                    ):
                        self.output_cache[transmitter_ename][attr] = output_data_to_save
                        flag_output_changed = True
                else:  # output_data_to_save is no dict, but a single value or dataclass
                    if self.output_cache[transmitter_ename][attr] == {}:  # no value existent
                        self.output_cache[transmitter_ename][attr] = output_data_to_save
                        flag_output_changed = True
                    elif isinstance(output_data_to_save, (float, int)):  # value exist. and compar.
                        if (
                            abs(self.output_cache[transmitter_ename][attr] - output_data_to_save)
                            > ADAPTION_TOLERANCE
                        ):
                            self.output_cache[transmitter_ename][attr] = output_data_to_save
                            flag_output_changed = True
                    else:  # value existent and not comparable
                        if self.output_cache[transmitter_ename][attr] != output_data_to_save:
                            self.output_cache[transmitter_ename][attr] = output_data_to_save
                            flag_output_changed = True

            # set whole output data for this entity
            data[transmitter_ename] = self.output_cache[transmitter_ename]

        # check if nothing is to be send out - send output 1 step later to avoid waiting for data
        if not flag_output_changed:
            if self.timestep == self.scenario_config["n_steps"] - 1:  # is last time step?
                data["time"] = self.timestep + 1
            else:
                data = {"time": self.timestep}

        # return data to mosaik
        return data
