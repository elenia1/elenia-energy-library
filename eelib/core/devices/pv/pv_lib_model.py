"""
eElib pvlib photovoltaic model. The pv module and inverter are both modeled together.

.. caution::
    It contains weather data from different years between 2005 and 2016 and handles them as
    if they all belong to the same simulated year.

See https://pvlib-python.readthedocs.io/en/stable/index.html for further reference:
William F. Holmgren, Clifford W. Hansen, and Mark A. Mikofski. 'pvlib python: a python package for
modeling solar energy systems.' Journal of Open Source Software, 3(29), 884, (2018).
https://doi.org/10.5281/zenodo.8368494

Copyright (c) 2023 pvlib python Contributors
Copyright (c) 2014 PVLIB python Development Team
Copyright (c) 2013 Sandia National Laboratories

| Author: elenia@TUBS
| Copyright 2024 elenia
| This file is part of eELib, which is free software under the terms of the GNU GPL Version 3.
"""

import math
from abc import ABC, abstractmethod

from pvlib import temperature as temp
from pvlib.location import Location
from pvlib import iotools, pvsystem
from pvlib.modelchain import ModelChain
import pandas as pd
from eelib.utils.resample import resample_pandas_timeseries_agg
from eelib.utils.ancillary_services.voltage_control_concepts import cos_phi_fix


class PVLibBase(ABC):
    """Baseclass model for pv lib model implementations."""

    # Valid values and types for each parameter
    _VALID_PARAMETERS = {
        "latitude": {"types": [float, int], "values": (-180, 180)},
        "longitude": {"types": [float, int], "values": (-180, 180)},
        "azimuth": {"types": [float, int], "values": (0, 360)},
        "tilt": {"types": [float, int], "values": (0, 90)},
        "cos_phi": {"types": [float, int], "values": (0, 1)},
        "start_time": {"types": [str], "values": None},
        "timezone": {"types": [str], "values": None},
        "step_size": {"types": [int], "values": (0, math.inf)},
    }

    @classmethod
    def get_valid_parameters(cls):
        """Returns dictionary containing valid parameter types and values.

        Returns:
            dict: valid parameters for this model
        """
        return cls._VALID_PARAMETERS

    def __init__(
        self,
        eid: str,
        latitude: float = 57.38,
        longitude: float = 36.23,
        start_time: str = "2016-01-01 00:00:00",
        timezone: str = "Europe/Berlin",
        azimuth: float = 180,
        tilt: float = 35.0,
        cos_phi: float = 0.9,
        step_size: int = 60 * 15,
    ):
        """Initializes the eElib pvlib photovoltaic model.

        Args:
            eid (str): name of the model entity
            latitude (float): location of the pv system (N-S). Defaults to 57.38.
            longitude (float): location of the pv system (W-E). Defaults to 36.23.
            start_time (str): starting time of the pv profile series. Defaults to
                "2016-01-01 00:00:00".
            timezone (str): time zone location of pv system. Defaults to "Europe/Berlin".
            azimuth (float): orientation of pv system (180 equals south). Defaults to 180.
            tilt (float): orientation of pv system (0 is horizontal, 90 is vertical).
                Defaults to 35.0.
            cos_phi (float): power factor for reactive power generation. Defaults to 0.9.
            step_size (int): Time range of a step [s]. Defaults to 60*15.

        Raises:
            ValueError: If simulation resolution is larger than 1 hour (resolution of weather data)
        """

        # set parameters for the pv_lib model
        self.eid = eid
        self.latitude = latitude
        self.longitude = longitude
        self.azimuth = azimuth
        self.tilt = tilt
        self.timezone = timezone
        self.cos_phi = cos_phi

        # initialize model properties
        self.p = 0
        self.p_min = 0
        self.p_max = 0
        self.q = 0
        self.p_set = {}

        # save step length and current timestep
        self.step_size = step_size
        self.timestep = None
        self.start_time = start_time

        # retrieve full weather data
        weather = iotools.get_pvgis_tmy(self.latitude, self.longitude)[0]
        weather.index.name = "utc_time"
        # clear weather data: set year to the same for all
        year_of_simulation = pd.Timestamp(self.start_time).year
        weather.index = weather.index.map(lambda x: x.replace(year=year_of_simulation))
        # clear weather data: shift by the correct time zone
        time_here = pd.Timestamp.now(tz=timezone)
        tz_shift_hour = time_here.tz_convert(None).hour - time_here.tz_localize(None).hour
        weather_shifted = weather.shift(periods=-tz_shift_hour)
        weather_shifted[:-tz_shift_hour] = weather[tz_shift_hour:]
        weather = weather_shifted
        # clear weather data: correct to fitting time resolution
        if step_size > 3600:  # simulation step size larger than 1 hour
            raise ValueError(
                "Simulation step size is larger than 60 minutes, failing the resolution of pvlib"
                " weather data."
            )
        self.weather = resample_pandas_timeseries_agg(
            weather, target_resolution=self.step_size, key=self.eid
        )
        # apply shift with start date
        self.weather.drop(
            pd.date_range(
                start=pd.to_datetime(self.weather.index[0]),
                end=pd.to_datetime(self.start_time).tz_localize("UTC"),
                freq=str(int(step_size / 60)) + "T",
            ),
            inplace=True,
        )

        self.weather_step = None

    def _calc_from_weather_full(self):
        """From stored weather data, calculate the output for all timesteps."""
        self.pvlib_mc.run_model(self.weather)

    def _set_power(self):
        """Set active power from set value or to maximum and within limits, set reactive power."""
        # check for set value
        if self.p_set is None or self.p_set == {}:
            self.p = self.p_max  # generate maximum power
        elif isinstance(self.p_set, (float, int)):
            self.p = self.p_set  # take setpoint value
        else:
            self.p = sum(self.p_set.values())  # take setpoint value

        # consider power limits of pv system for active power output (AC-side)
        self.p = min(max(self.p_max, self.p), self.p_min)

        # Calculate reactive power output from fixed cos_phi
        self.q = cos_phi_fix(self.p, self.cos_phi)

    @abstractmethod
    def step(self, timestep: int):
        """Handles stepping of pvlib entity.

        Args:
            timestep (int): Current simulation timestep
        """
        pass


class PVLib(PVLibBase):
    """An electrical photovoltaic system model from pv lib."""

    @classmethod
    def get_valid_parameters(cls):
        """Returns dictionary containing valid parameter types and values.

        Returns:
            dict: valid parameters for this model
        """

        # use parent's parameter list and modify them for this class
        result = PVLibBase.get_valid_parameters().copy()
        result.update(
            {
                "p_rated": {"types": [float, int], "values": (0, math.inf)},
                "inverter_efficiency": {"types": [float, int], "values": (0, 1)},
                "losses_standby": {"types": [float, int], "values": (0, math.inf)},
                "min_power": {"types": [float, int], "values": (0, math.inf)},
                "gamma_pdc": {"types": [float, int], "values": (-math.inf, 0)},
            }
        )
        return result

    def __init__(
        self,
        eid: str,
        p_rated: float = 10000,
        latitude: float = 57.38,
        longitude: float = 36.23,
        start_time: str = "2016-01-01 00:00:00",
        timezone: str = "Europe/Berlin",
        azimuth: float = 180,
        tilt: float = 35.0,
        gamma_pdc: float = -0.004,
        cos_phi: float = 0.9,
        inverter_efficiency: float = 0.96,
        losses_standby: float = 3.6,
        min_power: float = 56.0,
        step_size: int = 60 * 15,  # step size in seconds
    ):
        """Initializes the eElib pvlib photovoltaic model.

        Args:
            eid (str): name of the model entity
            p_rated (float): Rated power [Wp]. Defaults to 10000.
            latitude (float): location of the pv system (N-S). Defaults to 57.38.
            longitude (float): location of the pv system (W-E). Defaults to 36.23.
            start_time (str): starting time of the pv profile series. Defaults to
                "2016-01-01 00:00:00".
            timezone (str): time zone location of pv system. Defaults to "Europe/Berlin".
            azimuth (float): orientation of pv system (180 equals south). Defaults to 180.
            tilt (float): orientation of pv system (0 is horizontal, 90 is vertical).
                Defaults to 35.0.
            gamma_pdc (float): The temperature coefficient of power [1/C], typ. -0.002 to
                -0.005. Defaults to -0.004.
            cos_phi (float): power factor for reactive power generation. Defaults to 0.9.
            inverter_efficiency (float): efficiency of the inverter. Defaults to 0.96.
            losses_standby (float): power losses in standby mode [W]. Defaults to 3.6.
            min_power (float): min. power generation before stand-by. Defaults to 56.0.
            step_size (int): Time range of a step [s]. Defaults to 60*15.
        """

        # call init function of super class
        super().__init__(
            eid=eid,
            latitude=latitude,
            longitude=longitude,
            start_time=start_time,
            timezone=timezone,
            azimuth=azimuth,
            tilt=tilt,
            cos_phi=cos_phi,
            step_size=step_size,
        )

        # set parameters for the pv_lib model
        self.p_rated = p_rated
        self.inverter_efficiency = inverter_efficiency
        self.gamma_pdc = gamma_pdc
        self.losses_standby = losses_standby
        self.min_power = min_power

        # create pv system (from pvlib) with input
        temperature_model_parameters = temp.TEMPERATURE_MODEL_PARAMETERS["sapm"][
            "open_rack_glass_glass"
        ]
        module_parameters = {"pdc0": self.p_rated, "gamma_pdc": self.gamma_pdc}
        inverter_parameters = {
            "pdc0": self.p_rated,
            "eta_inv_nom": self.inverter_efficiency,
            "losses_standby": self.losses_standby,
            "min_power": self.min_power,
        }
        losses_parameters = {
            "soiling": 1,
            "shading": 0,
            "snow": 0,
            "mismatch": 1,
            "wiring": 1,
            "connections": 0.5,
            "lid": 0.5,
            "nameplate_rating": 0.5,
            "age": 0,
        }
        self.location = Location(latitude=self.latitude, longitude=self.longitude, tz=timezone)
        self.system = pvsystem.PVSystem(
            module_parameters=module_parameters,
            inverter_parameters=inverter_parameters,
            losses_parameters=losses_parameters,
            surface_tilt=self.tilt,
            surface_azimuth=self.azimuth,
            temperature_model_parameters=temperature_model_parameters,
        )

        # set up pvlib model chain
        self.pvlib_mc = ModelChain.with_pvwatts(self.system, self.location)

        # calculate the output for all time steps
        self._calc_from_weather_full()

    def step(self, timestep: int):
        """Performs simulation step of eELib pv_lib model.
        Simply extracts the value corresponding to the current simulation time.

        Args:
            timestep (int): Current simulation time
        """
        # handle current time step
        if not self.timestep == timestep:
            self.timestep = timestep

            # retrieve results for dc and ac power output
            power_dc = self.pvlib_mc.results.dc.iloc[timestep]
            # NOTE: electric power generation is negative
            power_ac = -self.pvlib_mc.results.ac.iloc[timestep]

            # stand-by losses when below minimum power of inverter
            if power_dc < self.system.inverter_parameters["min_power"]:
                power_ac = self.system.inverter_parameters["losses_standby"]

            # set min and max power limits
            self.p_min = max(-self.system.inverter_parameters["min_power"], power_ac)
            self.p_max = power_ac

        self._set_power()


class PVLibExact(PVLibBase):
    """An electrical photovoltaic system model from pv lib using specific modules and inverters."""

    @classmethod
    def get_valid_parameters(cls):
        """Returns dictionary containing valid parameter types and values.

        Returns:
            dict: valid parameters for this model
        """

        # use parent's parameter list and modify them for this class
        result = PVLibBase.get_valid_parameters().copy()
        result.update(
            {
                "altitude_m": {"types": [float, int], "values": (0, 10000)},
                "module_name": {"types": [str], "values": None},
                "num_modules_per_string": {"types": [int], "values": (0, math.inf)},
                "num_strings": {"types": [int], "values": (0, math.inf)},
                "inverter_name": {"types": [str], "values": None},
                "num_inverters": {"types": [int], "values": (0, math.inf)},
            }
        )
        return result

    def __init__(
        self,
        eid: str,
        latitude: float = 57.38,
        longitude: float = 36.23,
        altitude_m: float = 75,
        module_name: str = "SunPower_SPR_220__PVL____2006_",
        num_modules_per_string: int = 9,
        num_strings: int = 4,
        inverter_name: str = "SMA_America__SB5000US__240V_",
        num_inverters: int = 2,
        start_time: str = "2016-01-01 00:00:00",
        timezone: str = "Europe/Berlin",
        azimuth: float = 180,
        tilt: float = 35,
        cos_phi: float = 0.9,
        step_size: int = 60 * 15,  # step size in seconds
    ):
        """Initializes the eElib pvlib photovoltaic model.

        Args:
            eid (str): name of the model entity
            latitude (float): location of the pv system (N-S). Defaults to 57.38.
            longitude (float): location of the pv system (W-E). Defaults to 36.23.
            altitude_m (float): altitude of the pv system in m above zero-level. Defaults
                to 75.
            module_name (str): name of the pv modules. Defaults to
                "SunPower_SPR_220__PVL____2006_".
            num_modules_per_string (int): number of modules in one string. Defaults to 9.
            num_strings (int): number of module strings in pv system. Defaults to 4.
            inverter_name (str): name of the pv inverters. Defaults to
                "SMA_America__SB5000US__240V_".
            num_inverters (int): number of inverters in pv system. Defaults to 2.
            start_time (str): starting time of the pv profile series. Defaults to
                "2016-01-01 00:00:00".
            timezone (str): time zone location of pv system. Defaults to "Europe/Berlin".
            azimuth (float): orientation of pv system (180 equals south). Defaults to 180.
            tilt (float): orientation of pv system (0 is horizontal, 90 is vertical).
                Defaults to 35.
            cos_phi (float): power factor for reactive power generation. Defaults to 0.9.
            step_size (int): Time range of a step [s]. Defaults to 60*15.
        """

        # call init function of super class
        super().__init__(
            eid=eid,
            latitude=latitude,
            longitude=longitude,
            start_time=start_time,
            timezone=timezone,
            azimuth=azimuth,
            tilt=tilt,
            cos_phi=cos_phi,
            step_size=step_size,
        )

        # select and set inverter and module
        self.module = pvsystem.retrieve_sam("SandiaMod")[module_name]
        self.inverter = pvsystem.retrieve_sam("cecinverter")[inverter_name]
        self.num_modules_per_string = num_modules_per_string
        self.num_strings = num_strings
        self.num_inverters = num_inverters
        # adjust inverter power and voltage to number of inverters
        self.inverter.Paco = self.num_inverters * self.inverter.Paco
        self.inverter.Pdco = self.num_inverters * self.inverter.Pdco
        self.inverter.Vdco = self.num_inverters * self.inverter.Vdco
        self.inverter.Pso = self.num_inverters * self.inverter.Pso
        self.inverter.Pnt = self.num_inverters * self.inverter.Pnt

        # retrieve rated power from maximum inverter power on ac-side
        self.p_rated = self.inverter.Paco

        # create pv system (from pvlib) with input
        self.temperature_model_parameters = temp.TEMPERATURE_MODEL_PARAMETERS["sapm"][
            "open_rack_glass_glass"
        ]
        location = Location(
            latitude=latitude, longitude=longitude, altitude=altitude_m, tz=timezone
        )
        mount = pvsystem.FixedMount(surface_tilt=tilt, surface_azimuth=azimuth)
        array = pvsystem.Array(
            mount=mount,
            module_parameters=self.module,
            temperature_model_parameters=self.temperature_model_parameters,
            modules_per_string=self.num_modules_per_string,
            strings=self.num_strings,
        )
        self.system = pvsystem.PVSystem(arrays=[array], inverter_parameters=self.inverter)

        # set up pvlib model chain
        self.pvlib_mc = ModelChain(self.system, location)

        # calculate the output for all time steps
        self._calc_from_weather_full()

    def step(self, timestep: int):
        """Performs simulation step of eELib pv_lib model.
        Simply extracts the value corresponding to the current simulation time.

        Args:
            timestep (int): Current simulation time
        """
        # handle current timestep
        if not self.timestep == timestep:
            self.timestep = timestep

            # Retrieve active power output (AC-side) from results
            power_dc = self.pvlib_mc.results.dc["p_mp"].iloc[timestep]
            # NOTE: electric power generation is neg.
            power_ac = -self.pvlib_mc.results.ac.iloc[timestep]

            # set min and max power limits
            if power_dc > self.inverter.Pso:  # minimum power exceeded
                self.p_min = -self.inverter.Pso  # curtailable to min. power
                self.p_max = power_ac
            else:  # does not exceed minimum power - shut off
                self.p_min = self.system.inverter_parameters.Pnt
                self.p_max = self.system.inverter_parameters.Pnt

        self._set_power()
