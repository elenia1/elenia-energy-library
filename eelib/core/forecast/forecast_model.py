"""
eELib model for creation of forecast.

| Author: elenia@TUBS
| Copyright 2024 elenia
| This file is part of eELib, which is free software under the terms of the GNU GPL Version 3.
"""

from copy import deepcopy
import logging

_logger = logging.getLogger(__name__)


class Forecast:
    """Models a forecast creation/calculation."""

    # Valid values and types for each parameter that apply for all subclasses
    _VALID_PARAMETERS = {}

    @classmethod
    def get_valid_parameters(cls):
        """Returns dictionary containing valid parameter types and values.

        Returns:
            dict: valid parameters for this model
        """
        return cls._VALID_PARAMETERS

    def __init__(self, eid: str, step_size: int, n_steps: int):
        """Initializes the eELib forecast model.

        Args:
            eid (str): name of the model entity
            step_size (int): length of a simulation step in seconds
            n_steps (int): number of simulation steps
        """
        # Set attributes of init_vals to static properties
        self.eid = eid

        # create empty list of forecasted components and type-specific dict
        self.forecast_cache = {}
        self.forecast_ent_by_fullid = {}
        self.forecast = {}
        self.forecast_request = {}

        # save step length and current timestep
        self.step_size = step_size
        self.n_steps = n_steps
        self.timestep = None

    def step(self, timestep: int):
        """Perform simulation step for the forecast model.

        Args:
            timestep (int): Current simulation time

        Raises:
            TypeError: Forecast is requested for an entity that is not added to forecast list
        """

        # handle current (newly arrived) timestep
        if not self.timestep == timestep:
            self.timestep = timestep

        # check if request for a forecast was sent
        if self.forecast_request == {}:
            self.forecast = {}  # no forecast requested, simply return
        else:
            # clear earlier forecasts
            self.forecast = {}

            # go by all entities to create a forecast for
            for forecast_getter_id, forecast_req_fullid_dict in self.forecast_request.items():
                # create empty dict for forecasts connected to this request entity
                self.forecast[forecast_getter_id] = {}
                # check if no forecast requested
                if forecast_req_fullid_dict == {}:
                    continue

                # go by all forecasted model entities
                for forecast_fullid, forecast_info in forecast_req_fullid_dict.items():
                    # check if forecast can be done for this model type
                    if forecast_fullid in self.forecast_ent_by_fullid.keys():
                        # create structure for forecast of each attribute for this entity
                        forecast_save = {}
                        for attr in forecast_info["attr"]:
                            forecast_save[attr] = []

                        # store the copy of this model entity to execute the stepping
                        entity = self.forecast_ent_by_fullid[forecast_fullid]

                        # run the model for each timestep and collect the calculated attr values
                        for t in forecast_info["t"]:
                            # variable for whether model has to be stepped this timestep
                            bool_step_model = False

                            # check whether forecast value is already calculated
                            if forecast_fullid in self.forecast_cache:
                                for attr in forecast_info["attr"]:
                                    if (
                                        attr in self.forecast_cache[forecast_fullid]
                                        and t in self.forecast_cache[forecast_fullid]["t"]
                                    ):
                                        # value already existent: take that value
                                        idx_t = self.forecast_cache[forecast_fullid]["t"].index(t)
                                        forecast_save[attr].append(
                                            self.forecast_cache[forecast_fullid][attr][idx_t]
                                        )
                                    else:  # value not existent
                                        bool_step_model = True
                            else:  # no forecasts yet calculated for this entity
                                bool_step_model = True

                            # calculate the entity for this timestep if needed and extract results
                            if bool_step_model:
                                entity.step(t)
                                for attr in forecast_info["attr"]:
                                    forecast_save[attr].append(
                                        getattr(
                                            entity,
                                            attr,
                                        )
                                    )

                        # save calculated forecast for this model entity
                        self.forecast[forecast_getter_id][forecast_fullid] = forecast_save

                        self.forecast_cache[forecast_fullid] = forecast_save
                        self.forecast_cache[forecast_fullid]["t"] = list(forecast_info["t"])

                    else:  # model id not in the set of models that is stored for forecast
                        raise TypeError(
                            "Entity {} to be forecasted not stored in forecast model {}".format(
                                forecast_fullid, self.eid
                            )
                        )
                _logger.info(f"Forecast created for model {forecast_getter_id}.")

    def add_forecasted_entity(self, entity_dict: dict):
        """Adds a entity for the forecast to the corresponding forecast' lists.

        Args:
            entity_dict (dict): Input list with all entities of a type (e.g. pv) for which a
                forecast will be made.
        """
        # append each entity to forecast list and update collecting entity dict
        for full_eid, entity in entity_dict.items():
            # update entity dict by full ID, therefore, no element will be added twice
            self.forecast_ent_by_fullid[full_eid] = deepcopy(entity)
