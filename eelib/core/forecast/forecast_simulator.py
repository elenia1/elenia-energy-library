"""
Mosaik interface for the eELib energy management system (EMS) forecast.
Simulator for communication between orchestrator (mosaik) and forecast.

Within this interface mosaik functionalities are used.
Copyright (c) LGPL

| Author: elenia@TUBS
| Copyright 2024 elenia
| This file is part of eELib, which is free software under the terms of the GNU GPL Version 3.
"""

import mosaik_api_v3
from eelib.core.forecast import forecast_model
import eelib.utils.validation as vld
from copy import deepcopy

# SIMULATION META DATA
META = {
    "type": "hybrid",
    "models": {
        "Forecast": {
            "public": True,
            "params": [],
            "attrs": ["forecast", "forecast_request"],
            "trigger": ["forecast_request"],  # input attributes
            "non-persistent": ["forecast"],  # output attributes
        },
    },
    "extra_methods": ["add_forecasted_entity", "get_entity_by_id"],
}

# accuracy for cached output values to avoid triggering of event-based simulators
ADAPTION_TOLERANCE = 10 ** (-5)


class Sim(mosaik_api_v3.Simulator):
    """Simulator class for eELib forecast model.

    Args:
        mosaik_api_v3 (module): defines communication between mosaik and simulator
    """

    def __init__(self):
        """Constructs an object of the Prognosis:Sim class."""

        super(Sim, self).__init__(META)

        # storing of event-based output info for current same-time loop
        self.output_cache = {}

        # initiate empty dict for model entities
        self.entities = {}

    def init(self, sid, scenario_config, time_resolution=1.0):
        """Initializer for Prognosis:Sim class.

        Args:
            sid (str): ID of the created entity of the simulator (e.g. PrognosisSim-0)
            scenario_config (dict): scenario configuration data, like resolution or step size
            time_resolution (float): fitting of the step size to the simulation scenario step size.

        Returns:
            meta: description of the simulator
        """

        # assign properties for simulator
        self.sid = sid
        self.scenario_config = scenario_config

        return self.meta

    def create(self, num, model_type="forecast", init_vals={"Forecast_0": {}}):
        """Creates entities of the eELib forecast model.
        Core function of mosaik.

        Args:
            num (int): number of models to be created
            model_type (str): type of created entity, defaults to "forecast"
            init_vals (dict): list with initial values for entities, defaults to {"Forecast_0":{}}

        Raises:
            ValueError: If entity ID is already existing

        Returns:
            dict: created entities
        """

        # create empty list for created entities
        entities_orchestrator = []

        for eid, entity_vals in init_vals.items():
            # check if entity ID is already existing
            if eid in self.entities:
                raise ValueError(f"Entity ID {eid} for model type {model_type} already existing.")

            # create entity by specified full ID
            full_id = self.sid + "." + eid

            # get class of specific model and create entity with init values after validation
            entity_cls = getattr(forecast_model, model_type)
            if entity_vals == {}:
                entity = entity_cls(
                    eid,
                    step_size=self.scenario_config["step_size"],
                    n_steps=self.scenario_config["n_steps"],
                )
            else:
                vld.validate_init_parameters(entity_cls, entity_vals)
                entity = entity_cls(
                    eid,
                    **entity_vals,
                    step_size=self.scenario_config["step_size"],
                    n_steps=self.scenario_config["n_steps"],
                )

            # add info to the simulators entity-list and current entities
            self.entities[eid] = {
                "eid": eid,
                "etype": model_type,
                "model": entity,
                "full_id": full_id,
            }
            entities_orchestrator.append({"eid": eid, "type": model_type})

        return entities_orchestrator

    def get_entity_by_id(self, entity_id: str):
        """Searches for a requested entity id and gives back the entity model.

        Args:
            entity_id (str): id of the entity to be searched for

        Returns:
            object: entity model if found, None otherwise
        """

        if entity_id in self.entities.keys():
            return self.entities[entity_id]["model"]
        else:
            return None

    def step(self, time, inputs, max_advance):
        """Performs simulation step calling the eELib forecast model.
        Core function of mosaik.

        Args:
            time (int): current simulation time (given by mosaik)
            inputs (dict): allocation of set values to specific models
            max_advance (int): simulation time until the simulator can safely advance
                                        it's internal time without causing any causality errors.

        Raises:
            TypeError: if input has unknown format

        Returns:
            int: next timestep (when orchestrator calls again)
        """

        self.timestep = time

        # assign property values for each entity and attribute with entity ID
        # process input signals: for the entities (eid), attr is a dict for attributes to be set
        for eid, attrs in inputs.items():
            # for the attributes (attr), setter is a dict for entities with corresponding set values
            for attr, setter in attrs.items():
                # for transmitter (eid_setter), value_dict contains set values (with ids, when dict)
                setting_value_dict = deepcopy(getattr(self.entities[eid]["model"], attr))
                for eid_setter, value_dict in setter.items():
                    # check if no value is send -> to next
                    if value_dict is None:
                        continue

                    if isinstance(value_dict, dict):
                        setting_value_dict[eid_setter] = value_dict
                    # value_dict is not a dict, only a single value -> write directly
                    elif isinstance(value_dict, (float, int)):
                        setting_value_dict[eid_setter] = value_dict
                    else:
                        raise TypeError("Unknown format for value_dict")

                # set the collected value for this attribute
                setattr(self.entities[eid]["model"], attr, setting_value_dict)

        # call step function for each entity in the list
        for eid, entity_dict in self.entities.items():
            entity_dict["model"].step(time)

        # next timestamp for simulation depending on model step size
        return None

    def get_data(self, outputs: dict):
        """Gets the data for the next concatenated model.
        Core function of mosaik.

        Args:
            outputs (dict): dictionary with data outputs from each prognosed model

        Raises:
            ValueError: error if attribute not in model metadata

        Returns:
            dict: dictionary with simulation outputs
        """

        # create flag for checking if any new data is send out
        flag_output_changed = False

        # create empty output data dict
        data = {}

        # check current time step (whether a new one is reached)
        if not self.output_cache == {} and self.output_cache["time"] != self.timestep:
            self.output_cache = {}
        data["time"] = self.timestep
        self.output_cache["time"] = self.timestep

        for transmitter_ename, attrs in outputs.items():
            # get name for current entity and create dict field
            entry = self.entities[transmitter_ename]
            if transmitter_ename not in self.output_cache:
                self.output_cache[transmitter_ename] = {}

            # loop over all targeted attributes and check if info is available
            for attr in attrs:
                if attr not in self.meta["models"][type(entry["model"]).__name__]["attrs"]:
                    raise ValueError("Unknown output attribute: %s" % attr)

                # create empty field for cache and output data
                if attr not in self.output_cache[transmitter_ename]:
                    self.output_cache[transmitter_ename][attr] = {}

                output_data_to_save = getattr(entry["model"], attr)

                # check if output is a dict or just a single value
                if isinstance(output_data_to_save, dict):
                    for emitter_ename, value in output_data_to_save.items():
                        if (
                            emitter_ename not in self.output_cache[transmitter_ename][attr]
                        ):  # value currently not stored in output cache
                            self.output_cache[transmitter_ename][attr] = output_data_to_save
                            flag_output_changed = True
                        elif value == {}:  # no forecast calculated
                            continue
                        elif isinstance(value, (float, int)):  # value existent and comparable
                            if (
                                abs(
                                    self.output_cache[transmitter_ename][attr][emitter_ename]
                                    - value
                                )
                                > ADAPTION_TOLERANCE
                            ):
                                self.output_cache[transmitter_ename][attr] = output_data_to_save
                                flag_output_changed = True
                        else:  # value existent and not comparable
                            if self.output_cache[transmitter_ename][attr][emitter_ename] != value:
                                self.output_cache[transmitter_ename][attr] = output_data_to_save
                                flag_output_changed = True
                    if (
                        output_data_to_save == {}
                        and self.output_cache[transmitter_ename][attr] != {}
                    ):
                        self.output_cache[transmitter_ename][attr] = output_data_to_save
                        flag_output_changed = True
                else:  # output_data_to_save is no dict, but a single value to be stored
                    if self.output_cache[transmitter_ename][attr] == {}:  # no value existent
                        self.output_cache[transmitter_ename][attr] = output_data_to_save
                        flag_output_changed = True
                    elif isinstance(output_data_to_save, (float, int)):  # value exist. and compar.
                        if (
                            abs(self.output_cache[transmitter_ename][attr] - output_data_to_save)
                            > ADAPTION_TOLERANCE
                        ):
                            self.output_cache[transmitter_ename][attr] = output_data_to_save
                            flag_output_changed = True
                    else:  # value existent and not comparable
                        if self.output_cache[transmitter_ename][attr] != output_data_to_save:
                            self.output_cache[transmitter_ename][attr] = output_data_to_save
                            flag_output_changed = True

            # set whole output data for this entity
            data[transmitter_ename] = self.output_cache[transmitter_ename]

        # check if nothing is to be send out - send output 1 step later to avoid waiting for data
        if not flag_output_changed:
            if self.timestep == self.scenario_config["n_steps"] - 1:  # is last time step?
                data["time"] = self.timestep + 1
            else:
                data = {"time": self.timestep}

        # return data to mosaik
        return data

    def add_forecasted_entity(self, prognosed_entity_id: str, entity_dict: dict):
        """Adds entities (e.g. pv systems) to the specific forecast unit.

        Args:
            prognosed_entity_id (str): Dictionary of forecast entity
            entity_dict (dict): Dictionary of created models to be added to the forecast entity
        """

        self.entities[prognosed_entity_id]["model"].add_forecasted_entity(entity_dict)
