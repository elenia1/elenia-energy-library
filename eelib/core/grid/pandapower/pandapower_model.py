"""
eElib pandapower grid model (oriented at mosaik_pandapower).
Grid import could happen through Json files, and the file should exist in the working directory.

In pandapower library there exist a list of standard grids for direct import and simulation:
https://pandapower.readthedocs.io/en/v2.1.0/networks.html

Copyright (c) 2018 by University of Kassel and Fraunhofer Institute for Fraunhofer Institute for
Energy Economics and Energy System Technology (IEE) Kassel and individual contributors

| Author: elenia@TUBS
| Copyright 2024 elenia
| This file is part of eELib, which is free software under the terms of the GNU GPL Version 3.
"""

import os.path
import numpy as np

import pandapower as pp
import pandapower.networks as ppn
from pandapower.pypower.makePTDF import makePTDF
import logging

_logger = logging.getLogger(__name__)

# set list of output attributes for each grid component type
OUTPUT_ATTRS = {  # if tuple, naming if different in pandapower grid: (pp_name, name, scaling)
    "bus": [("p_mw", "p_w", 1e6), ("q_mvar", "q_var", 1e6), "vm_pu", "va_degree"],
    "load": [("p_mw", "p_w", 1e6), ("q_mvar", "q_var", 1e6)],
    "trafo": ["va_lv_degree", "loading_percent"],
    "line": [("i_ka", "i_a", 1e3), "loading_percent"],
    "ext_grid": [("p_mw", "p_w", 1e6), ("q_mvar", "q_var", 1e6)],
}

# attributes to be renamed for each grid component type due to different units in pandapower
RENAMING_ATTRS = {  # pandapower_name, eELib_name, scaling_factor)
    "bus": [],
    "load": [("p_mw", "p_w", 1e6), ("q_mvar", "q_var", 1e6), ("sn_mva", "sn_va", 1e6)],
    "trafo": [("sn_mva", "sn_va", 1e6), ("pfe_kw", "pfe_w", 1e3)],
    "line": [("max_i_ka", "max_i_a", 1e3)],
    "ext_grid": [],
}


class Pandapower(object):
    """An electrical grid model in pandapower style."""

    # Valid values and types for each parameter
    _VALID_PARAMETERS = {
        "gridfile": {"types": [str], "values": None},
        "sim_start": {"types": [str, type(None)], "values": None},
    }

    @classmethod
    def get_valid_parameters(cls):
        """Returns dictionary containing valid parameter types and values.

        Returns:
            dict: valid parameters for this model
        """
        return cls._VALID_PARAMETERS

    LIM = {}
    LIM["BUS_VM_MAX_LV"] = 1.1
    LIM["BUS_VM_MIN_LV"] = 0.9
    LIM["BUS_VM_MAX_MV"] = 1.1
    LIM["BUS_VM_MIN_MV"] = 0.9
    LIM["BUS_VM_MAX_HV"] = 1.1
    LIM["BUS_VM_MIN_HV"] = 0.95
    LIM["BUS_VM_MAX_EHV"] = 1.1
    LIM["BUS_VM_MIN_EHV"] = 1.0
    LIM["LINE_LOADING_MAX"] = 100
    LIM["TRAFO_LOADING_MAX"] = 100

    def __init__(self, eid: str, gridfile: str, sim_start: str):
        """Initializes the eELib-pandapower grid model.

        Args:
            eid (str): Specifically assigned name for this entity.
            gridfile (str): path to the grid-file in json format
            sim_start (str): start time of the simulation
        """
        self.eid = eid  # set entity name

        # initialize properties and result dict
        self.components_map = {}
        self.gridfile = gridfile
        self.sim_start = sim_start
        self.timestep = 0
        self.powerflow_results = {}
        self.grid_status = {}
        self.ptdf_mat = []
        self.vpif_mat = []
        self.calc_grid = False

        # load pandapower grid case
        self._load_case(gridfile)

    def _load_case(self, path):
        """Loads a pandapower grid. The grid should be ready in a json file or an example grid.

        Args:
            path (str): file path to the grid file

        Raises:
            ValueError: if given grid directory/file cannot be opened
        """
        # get the type of the grid file (if path to grid file is given)
        file_ending = os.path.splitext(path)[-1]

        # open the grid file corresponding to given path
        if file_ending == ".json":  # check if grid path is directing to json file
            self.grid = pp.from_json(path)
        else:
            # otherwise check for key that is given and if it corresponds to a typical pp grid
            if path == "cigre_hv":
                self.grid = ppn.create_cigre_network_hv()
            elif path == "cigre_mv":
                self.grid = ppn.create_cigre_network_mv(with_der=False)
            elif path == "cigre_mv_all":
                self.grid = ppn.create_cigre_network_mv(with_der="all")
            elif path == "cigre_mv_pv_wind":
                self.grid = ppn.create_cigre_network_mv(with_der="pv_wind")
            elif path == "cigre_lv":
                self.grid = ppn.create_cigre_network_lv()
            elif path == "kerber_land_fl_1":
                self.grid = ppn.create_kerber_landnetz_freileitung_1()
            elif path == "kerber_land_fl_2":
                self.grid = ppn.create_kerber_landnetz_freileitung_2()
            elif path == "kerber_land_kb_1":
                self.grid = ppn.create_kerber_landnetz_kabel_1()
            elif path == "kerber_land_kb_2":
                self.grid = ppn.create_kerber_landnetz_kabel_2()
            elif path == "kerber_dorf":
                self.grid = ppn.create_kerber_dorfnetz()
            elif path == "kerber_vorstd_kb_1":
                self.grid = ppn.create_kerber_vorstadtnetz_kabel_1()
            elif path == "kerber_vorstd_kb_2":
                self.grid = ppn.create_kerber_vorstadtnetz_kabel_2()
            elif path == "panda_four_load_branch":
                self.grid = ppn.panda_four_load_branch()
            elif path == "simple_four_bus_system":
                self.grid = ppn.simple_four_bus_system()
            else:
                raise ValueError("No handling for '%s' implemented" % path)

        # create elements indices, to create entities
        self.bus_id = self.grid.bus.name.to_dict()
        self.load_id = self.grid.load.name.to_dict()
        self.line_id = self.grid.line.name.to_dict()
        self.trafo_id = self.grid.trafo.name.to_dict()

        # load the components map and create all the different grid entities
        self._get_slack()
        self._get_buses()
        self._get_lines()
        self._get_trafos()
        self._get_loads()

    def _get_slack(self):
        """Create entity of the slack bus.

        Returns:
            tuple: zero with index of slack bus
        """

        # create slack index and get ID of the grid bus and for the slack entity
        self.slack_bus_idx = self.grid.ext_grid.bus[0]
        bus_id = self.bus_id[self.slack_bus_idx]

        # add this element to the map (dict) of components in the grid
        self.components_map[bus_id] = {
            "etype": "ext_grid",
            "idx": self.slack_bus_idx,
            "static": {
                "vm_pu": self.grid.ext_grid["vm_pu"],
                "va_degree": self.grid.ext_grid["va_degree"],
            },
        }

        # return tuple with slack bus index
        slack = (0, self.slack_bus_idx)
        return slack

    def _get_buses(self):
        """Create entities of the buses.

        Returns:
            list: indices of busses with their nominal voltage in kV
        """
        buses = []

        # go by each bus that is not the slack bus
        for idx in self.bus_id:
            if self.slack_bus_idx != idx:
                # get bus index and the ID of the grid bus and for this bus entity
                element = self.grid.bus.loc[idx]
                bus_id = element["name"]

                # store all relevant bus data into the map (dict) of components in the grid
                element_data = element.to_dict()
                # store into components map
                self.components_map[bus_id] = {
                    "etype": "bus",
                    "idx": idx,
                    "static": {"vn_kv": element_data["vn_kv"]},
                }

                # save tuple of bus index and nominal voltage to list of buses
                buses.append((idx, element["vn_kv"]))
            else:  # this is the slack bus, do not consider here
                pass

        # return list of tuples with bus information
        return buses

    def _get_loads(self):
        """Create load entities.

        Returns:
            list: list of load with tuples about bus index, active/reactive power, scaling factor
            and their status
        """
        loads = []

        # go by each load in the grid
        for idx in self.load_id:
            # get load index and the ID of the grid bus and for this load entity
            element = self.grid.load.loc[idx]
            load_id = element["name"]
            bus_id = self.bus_id[element["bus"]]

            # store all relevant load data into the map (dict) of components in the grid
            element_data = element.to_dict()
            # renaming of keys if needed
            for i_key in range(len(RENAMING_ATTRS["load"])):
                element_data[RENAMING_ATTRS["load"][i_key][1]] = (
                    element_data[RENAMING_ATTRS["load"][i_key][0]]
                    * RENAMING_ATTRS["load"][i_key][2]
                )
                del element_data[RENAMING_ATTRS["load"][i_key][0]]
            # remove keys that should not be considered
            keys_to_del = [
                "name",
                "const_z_percent",
                "const_i_percent",
                "min_q_mvar",
                "min_p_mw",
                "max_q_mvar",
                "max_p_mw",
            ]
            element_data_static = {
                key: element_data[key] for key in element_data if key not in keys_to_del
            }
            # store into components map
            self.components_map[load_id] = {
                "etype": "load",
                "idx": idx,
                "static": element_data_static,
                "related": [bus_id],
            }

            # save tuple of load info to list of loads
            loads.append(
                (
                    bus_id,
                    element_data["p_w"],
                    element_data["q_var"],
                    element_data["scaling"],
                    element_data["in_service"],
                )
            )

        # return list with loads information
        return loads

    def _get_lines(self):
        """Create branches entities.

        Returns:
            list: list of grid lines with tuples about bus indices, their length,
            impedance/reactance/capacitance, maximum current and their status
        """
        lines = []

        # go by each line in the grid
        for idx in self.line_id:
            # get line index and the ID of the adjacent grid bus and for this load entity
            element = self.grid.line.loc[idx]
            line_id = element["name"]
            fbus_id = self.bus_id[element["from_bus"]]
            tbus_id = self.bus_id[element["to_bus"]]

            # store all relevant line data into the map (dict) of components in the grid
            element_data = element.to_dict()
            # renaming of keys if needed
            for i_key in range(len(RENAMING_ATTRS["line"])):
                element_data[RENAMING_ATTRS["line"][i_key][1]] = (
                    element_data[RENAMING_ATTRS["line"][i_key][0]]
                    * RENAMING_ATTRS["line"][i_key][2]
                )
                del element_data[RENAMING_ATTRS["line"][i_key][0]]
            # remove keys that should not be considered
            keys_to_del = ["name", "from_bus", "to_bus"]
            element_data_static = {
                key: element_data[key] for key in element_data if key not in keys_to_del
            }
            # store into components map
            self.components_map[line_id] = {
                "etype": "line",
                "idx": idx,
                "static": element_data_static,
                "related": [fbus_id, tbus_id],
            }

            # save tuple of line info to list of lines
            lines.append(
                (
                    self.components_map[fbus_id]["idx"],
                    self.components_map[tbus_id]["idx"],
                    element_data["length_km"],
                    element_data["r_ohm_per_km"],
                    element_data["x_ohm_per_km"],
                    element_data["c_nf_per_km"],
                    element_data["max_i_a"],
                    element_data["in_service"],
                )
            )

        # return list with line information
        return lines

    def _get_trafos(self):
        """Create trafo entities.

        Returns:
            list: list of trafos with tuples about bus indices, max. power, nominal voltages,
            short circuit voltages, iron losses, open-loop current, phase shift, tap status and
            their status
        """
        trafos = []

        # go by each trafo in the grid
        for idx in self.trafo_id:
            # get trafo index and the IDs of the adjacent grid buses and for this trafo entity
            element = self.grid.trafo.loc[idx]
            trafo_id = element["name"]
            hv_bus_id = self.bus_id[element["hv_bus"]]
            lv_bus_id = self.bus_id[element["lv_bus"]]

            # store all relevant trafo data into the map (dict) of components in the grid
            element_data = element.to_dict()
            # renaming of keys if needed
            for i_key in range(len(RENAMING_ATTRS["trafo"])):
                element_data[RENAMING_ATTRS["trafo"][i_key][1]] = (
                    element_data[RENAMING_ATTRS["trafo"][i_key][0]]
                    * RENAMING_ATTRS["trafo"][i_key][2]
                )
                del element_data[RENAMING_ATTRS["trafo"][i_key][0]]
            # remove keys that should not be considered
            keys_to_del = ["name", "hv_bus", "lv_bus"]
            element_data_static = {
                key: element_data[key] for key in element_data if key not in keys_to_del
            }
            # store into components map
            self.components_map[trafo_id] = {
                "etype": "trafo",
                "idx": idx,
                "static": element_data_static,
                "related": [hv_bus_id, lv_bus_id],
            }

            # save tuple of trafo info to list of trafos
            trafos.append(
                (
                    self.components_map[hv_bus_id]["idx"],
                    self.components_map[lv_bus_id]["idx"],
                    element_data["sn_va"],
                    element_data["vn_hv_kv"],
                    element_data["vn_lv_kv"],
                    element_data["vk_percent"],
                    element_data["vkr_percent"],
                    element_data["pfe_w"],
                    element_data["i0_percent"],
                    element_data["shift_degree"],
                    element_data["tap_side"],
                    element_data["tap_pos"],
                    element_data["tap_neutral"],
                    element_data["tap_min"],
                    element_data["tap_max"],
                    element_data["in_service"],
                )
            )

        # return list with line information
        return trafos

    def set_inputs(self, etype, idx, data, static):
        """Setting the input from the simulator.

        Args:
            etype (string): entity type of element to be set
            idx (int): index of element in element list
            data (dict): input data to be set, key (parameter) and corresponding value
            static (dict): static values of this entity

        Raises:
            KeyError: If field to set input for load is not p_w or q_var
            KeyError: If field to set input for trafo is not tap_turn
            ValueError: Error if no possible entity type was given
        """
        # go by each data element to be saved into the grid components properties
        for name, value in data.items():
            if etype == "load":  # setting of data for the grid load
                # get the load elements in the grid by their type
                elements = getattr(self.grid, etype.lower())

                # check if there is a scaling factor that should be applied
                if "scaling" in elements.columns and not np.isnan(elements.at[idx, "scaling"]):
                    scaling_factor = elements.at[idx, "scaling"]
                else:
                    scaling_factor = 1

                # handle active/reactive power according to its scaling factor
                if name == "p_w":
                    elements.at[idx, "p_mw"] = value / (1e6 * scaling_factor)
                elif name == "q_var":
                    elements.at[idx, "q_mvar"] = value / (1e6 * scaling_factor)
                else:
                    raise KeyError("Field %s not possible for element load of grid" % name)

            elif etype == "trafo":  # setting of data for the grid load
                # set the trafo tap position if available in the input data
                if name == "tap_turn":
                    tap = 1 / static["tap_pos"][data["tap_turn"]]
                    self.grid.trafo.at[idx, "tap_pos"] = tap
                else:
                    raise KeyError("Field %s not possible for element trafo of grid" % name)

            else:
                raise ValueError("etype %s unknown" % etype)

    def _powerflow(self):
        """Conduct power flow for the saved grid with its options from pandapower."""
        pp.runpp(
            self.grid,
            numba=False,
            algorithm="nr",
            calculate_voltage_angles=False,
            init="auto",
            trafo_loading="current",
        )

    def step(self, timestep):
        """Performs simulation step of eELib pandapower grid model.

        Args:
            timestep (int): Current simulation time
        """
        # calculate the powerflow and store the corresponding results
        try:
            self._powerflow()
        except IndexError:
            _logger.warning(f"Powerflow did not converge in timestep '{timestep}'.")
        self._store_power_flow_results()

        # analyse the status of the grid (regarding congestions) with these results
        self._get_grid_status()

        # calculate the sensitivities of the current grid situation
        self._calc_ptdf()
        self._calc_vpif()

    def _store_power_flow_results(self):
        """Retrieve the results of the power flow and store them."""

        # go by each component and retrieve its result value from power flow calculation
        self.powerflow_results = {}
        for eid in self.components_map.keys():
            self.powerflow_results[eid] = self._get_powerflow_result_component(eid)

    def _get_powerflow_result_component(self, eid):
        """Retrieve the results of the power flow for a specific component.

        Args:
            eid (str): Entity ID of the component to be searched for

        Returns:
            (dict): dict with the results from the power flow for the component
        """

        # initialize result dict
        result_dict = {}

        # get component, type, index and its attributes to be saved
        component_dict = self.components_map[eid]
        etype = component_dict["etype"]
        idx = component_dict["idx"]
        attributes = OUTPUT_ATTRS[etype]

        # check if power flow has converged - only then results are available
        if not self.grid.res_bus.empty:
            # retrieve the element itself by its name and index
            element_name = f"res_{etype.lower()}"
            if etype != "ext_grid":
                element = getattr(self.grid, element_name).loc[idx]
            else:
                element = getattr(self.grid, element_name).loc[0]

            # correct handling of all attributes: if scaling is needed, do so - otherwise take value
            for attr in attributes:
                if isinstance(attr, tuple):
                    # rescale the output with regard to the naming different from pandapower
                    result_dict[attr[1]] = element[attr[0]] * attr[2]
                else:
                    # simply take the resulting pandapower value
                    result_dict[attr] = element[attr]

        else:  # power flow failed to converge.
            for attr in attributes:
                if isinstance(attr, tuple):
                    result_dict[attr[1]] = None
                else:
                    result_dict[attr] = None

        # return results dict for this component
        return result_dict

    def _get_grid_status(self):
        """Analyse the status of the grid and its component at the current step in time."""
        status = {}

        # initially set the congested status to false
        status["congested"] = False

        # go by all components of the grid
        for component_eid, component_dict in self.components_map.items():
            # initialize status for this component type
            if component_dict["etype"] not in status.keys():
                status[component_dict["etype"]] = {}

            # collect status with regard to the component type
            if component_dict["etype"] in ["bus", "ext_grid"]:
                status_comp = self._get_status_bus(component_dict["idx"])
            elif component_dict["etype"] in ["line", "trafo"]:
                status_comp = self._get_status_line(
                    comp_idx=component_dict["idx"], comp_type=component_dict["etype"]
                )
            else:  # component not relevant for congestion
                continue

            if status_comp["congested"]:  # if component is congested - adapt grid status
                status["congested"] = True

            status[component_dict["etype"]][component_eid] = status_comp  # save comp. status

        # set the status of the grid
        self.grid_status = status

    def _get_status_bus(self, comp_idx: int) -> dict:
        """Analyse the status of a grid bus at the current step in time.

        Args:
            comp_idx (int): index of the (bus) component in the corresponding grid.bus table

        Returns:
            dict: Status for the bus including limits and congestion status
        """
        status = {}  # initialize status dict

        # read the resulting voltage at the grid bus
        status["vm"] = self.grid.res_bus["vm_pu"].iat[comp_idx]

        # save the voltage magnitude limits with regard to the voltage level
        if self.grid.bus["vn_kv"].iat[comp_idx] <= 0.4:
            status["vmax"] = self.LIM["BUS_VM_MAX_LV"]
            status["vmin"] = self.LIM["BUS_VM_MIN_LV"]
        elif self.grid.bus["vn_kv"].iat[comp_idx] <= 60:
            status["vmax"] = self.LIM["BUS_VM_MAX_MV"]
            status["vmin"] = self.LIM["BUS_VM_MIN_MV"]
        elif self.grid.bus["vn_kv"].iat[comp_idx] <= 150:
            status["vmax"] = self.LIM["BUS_VM_MAX_HV"]
            status["vmin"] = self.LIM["BUS_VM_MIN_HV"]
        else:
            status["vmax"] = self.LIM["BUS_VM_MAX_EHV"]
            status["vmin"] = self.LIM["BUS_VM_MIN_EHV"]

        # check whether the bus is congested or not
        status["congested"] = status["vm"] > status["vmax"] or status["vm"] < status["vmin"]

        # return the status of the bus
        return status

    def _get_status_line(self, comp_idx: int, comp_type: str) -> dict:
        """Analyse the status of a grid line at the current step in time.

        Args:
            comp_idx (int): index of the (line) component in the grid.line/.trafo table
            comp_type (str): whether it is a trafo or a line

        Returns:
            dict: _description_
        """
        status = {}  # initialize status dict

        # read the resulting loading and save the loading limits with regard to its type
        if comp_type == "line":
            status["loading"] = self.grid.res_line["loading_percent"].iat[comp_idx]
            status["max_loading"] = self.LIM["LINE_LOADING_MAX"]
        elif comp_type == "trafo":
            status["loading"] = self.grid.res_trafo["loading_percent"].iat[comp_idx]
            status["max_loading"] = self.LIM["TRAFO_LOADING_MAX"]
        else:
            return None

        # check whether the line is congested or not
        status["congested"] = status["loading"] > status["max_loading"]

        # return the status of the line
        return status

    def _calc_ptdf(self):
        """Calculate the DC Power Transfer Distribution Matrix of the Grid.
        Using function from pandapower.pypower.
        Only works after the calculation of a powerflow, as the "_ppc" matrix is calculated there.
        Orientation is as follows: One list for every branch (consequence), one element in each list
        for every bus (cause).
        E.g. for a 3-Bus radial grid the matrix will look like this:
        [[0, -1, -1], [0, 0, -1], [0, 0, 0]].
        """
        self.ptdf_mat = np.round(
            makePTDF(
                baseMVA=1e-6,
                bus=self.grid["_ppc"]["bus"],
                branch=self.grid["_ppc"]["branch"],
                slack=self.slack_bus_idx,
            ),
            3,
        )

    def _calc_vpif(self):
        """Calculate the AC Voltage Power Impact Factor Matrix of the (current) Grid.
        The jacobian matrix is calculated in every iteration of pandapower powerflow calculation.
        The matrix of the last iteration is stored and will be accessed here as a sensitivity of
        adaptions.
        Only works after the calculation of a powerflow, as the "_ppc" matrix is calculated there.
        Direction corresponds to PSC (power consumption decreases voltage magnitude).
        Orientation is as follows: One list for every bus (consequence, adjustment of vm_pu), one
        element in each list for every bus times two (cause, first adaption of active power P
        afterwards adaption of reactive power Q).
        E.g. for a 2-bus grid the matrix will look like this:
        [[B1dP->B1dVm, B2dP->B1dVm, B1dQ->B1dVm, B2dQ->B1dVm], ...
        ... [B1dP->B2dVm, B2dP->B2dVm, B1dQ->B2dVm, B2dQ->B2dVm]].
        """
        # take full jacobian matrix
        if (
            self.grid["_ppc"] is None
            or self.grid["_ppc"]["internal"] is None
            or self.grid["_ppc"]["internal"]["J"] is None
        ):
            _logger.warning("Jacobian matrix not accessible, no VPIF matrix.")
            self.vpif_mat = None
            return
        jacobian = self.grid["_ppc"]["internal"]["J"].todense()

        # invert matrix
        inverse_jacobian = np.linalg.inv(jacobian)

        # just take the lower half of the matrix to only account for voltage magnitudes
        vpif = inverse_jacobian[int(inverse_jacobian.shape[0] / 2) : inverse_jacobian.shape[0], :]

        # account for passive sign convention (jacobian has active sign convention)
        vpif = -vpif

        # factor adjustment (jacobian in MW/p.u., vpif should be p.u./W - divide by 1M)
        self.vpif_mat = vpif / 1e6
