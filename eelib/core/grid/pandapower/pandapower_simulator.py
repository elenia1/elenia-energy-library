"""
Mosaik interface for the eELib pandapower grid model.
Simulator for communication between orchestrator (mosaik) and pandapower grid entities.

Within this interface mosaik functionalities are used.
Copyright (c) LGPL

| Author: elenia@TUBS
| Copyright 2024 elenia
| This file is part of eELib, which is free software under the terms of the GNU GPL Version 3.
"""

import mosaik_api_v3
import arrow
import numpy as np

from eelib.core.grid.pandapower import pandapower_model

import eelib.utils.validation as vld

# accuracy for cached output values to avoid triggering of event-based simulators
ADAPTION_TOLERANCE = 10 ** (-5)

# META DATA: Simulator information about model classes, its parameters, attributes, and methods
META = {
    "type": "hybrid",
    "models": {
        "Pandapower": {
            "public": True,
            "params": [
                "gridfile",  # Name of the file containing the grid topology.
                "sheetnames",  # Mapping of Excel sheet names, optional.
                "sim_start",  # Starting time of the simulation.
            ],
            "attrs": [
                "grid",
                "components_map",
                "powerflow_results",
                "grid_status",
                "ptdf_mat",
                "vpif_mat",
                "calc_grid",
            ],
        },
        "ext_grid": {
            "public": False,
            "params": [],
            "attrs": [
                "p_w",  # active power [W]
                "q_var",  # reactive power [var]
            ],
        },
        "bus": {
            "public": False,
            "params": [],
            "attrs": [
                "p_w",  # active power [W]
                "q_var",  # reactive power [var]
                "vn_kv",  # Nominal bus voltage [kV]
                "vm_pu",  # Voltage magnitude [p.u]
                "va_degree",  # Voltage angle [deg]
            ],
        },
        "load": {
            "public": False,
            "params": [],
            "attrs": [
                "p_w",  # active power [W]
                "q_var",  # reactive power [var]
                "in_service",  # Specifies if the load is in service.
                "controllable",  # States if load is controllable or not.
            ],
        },
        "trafo": {
            "public": False,
            "params": [],
            "attrs": [
                "p_hv_w",  # Active power at "from" side [W]
                "q_hv_var",  # Reactive power at "from" side [var]
                "p_lv_w",  # Active power at "to" side [W]
                "q_lv_var",  # Reactive power at "to" side [var]
                "sn_va",  # Rated apparent power [VA]
                "max_loading_percent",  # Maximum Loading
                "vn_hv_kv",  # Nominal primary voltage [kV]
                "vn_lv_kv",  # Nominal secondary voltage [kV]
                "pl_w",  # Active power loss [W]
                "ql_var",  # reactive power consumption of the trafo [var]
                "loading_percent",  # load utilization relative to rated power [%]
                "i_hv_a",  # current at the high voltage side of the trafo [A]
                "i_lv_a",  # current at the low voltage side of the trafo [A]
                "tap_max",  # maximum possible tap turns
                "tap_min",  # minimum possible tap turns
                "tap_pos",  # Currently active tap turn
            ],
        },
        "line": {
            "public": False,
            "params": [],
            "attrs": [
                "p_from_w",  # Active power at "from" side [W]
                "q_from_var",  # Reactive power at "from" side [var]
                "p_to_w",  # Active power at "to" side [W]
                "q_to_var",  # Reactive power at "to" side [var]
                "max_i_a",  # Maximum current [A]
                "length_km",  # Line length [km]
                "pl_w",  # active power losses of the line [W]
                "ql_var",  # reactive power consumption of the line [var]
                "i_from_a",  # Current at from bus [A]
                "i_to_a",  # Current at to bus [A]
                "loading_percent",  # line loading [%]
                "r_ohm_per_km",  # Resistance per unit length [ohm/km]
                "x_ohm_per_km",  # Reactance per unit length [ohm/km]
                "c_nf_per_km",  # Capacity per unit length [nF/km]
                "in_service",  # Boolean flag (True|False)
            ],
        },
    },
    "extra_methods": ["get_entity_by_id"],
}


class Sim(mosaik_api_v3.Simulator):
    """Simulator class for eELib pandapower grid model.

    Args:
        mosaik_api_v3 (module): defines communication between mosaik and simulator
    """

    def __init__(self):
        """Constructs an object of the Pandapower:Sim class."""

        super(Sim, self).__init__(META)

        self.step_size = None
        self.sim_start = None
        self.sid = None

        # storing of event-based output info (for same-time loop or next timestep)
        self.output_cache = {}

        # initiate empty dict for model entities
        self.entities = {}

        # create dict for creation of grid and its components
        self._components = {}
        self._relations = []  # List of pair-wise related entities (IDs)

    def init(self, sid, scenario_config, time_resolution=1.0, trigger=False):
        """Initializes parameters for an object of the Pandapower:Sim class.

        Args:
            sid (str): Id of the entity of the pandapower grid simulator (e.g. PandapowerSim-0)
            scenario_config (dict): scenario configuration data, like resolution or step size.
            time_resolution (float): Time resolution of current mosaik scenario.
            trigger (boolean): Whether power flow is triggered by events

        Returns:
            meta: meta description of the simulator
        """

        # assign properties for simulator
        self.step_size = scenario_config["step_size"]
        self.sid = sid
        self.scenario_config = scenario_config

        if trigger or self.step_size is None:
            # Add trigger for all attributes of all models due to event-based triggering
            for imodel in self.meta["models"]:
                self.meta["models"][imodel]["trigger"] = True

        return self.meta

    def create(self, num, model_type, gridfile, sim_start=None):
        """Creates instances of the eELib pandapower model.

        Args:
            num (int): Number of pandapower models to be created
            model_type (str): Description of the created eELib-pandapower instance
            gridfile (str): directory of the file with the grid data
            sim_start (str): date of the simulation start

        Returns:
            dict: return created entities
        """

        # Check input values
        if sim_start is not None:
            self.sim_start = arrow.get(sim_start, "YYYY-MM-DD HH:mm:ss")

        # generated next unused ID for entity
        next_id = len(self.entities)

        # create empty list for created entities
        entities_orchestrator = []

        for i in range(next_id, next_id + num):
            # create entity by specified name and ID
            grid_eid = "%s%s%d" % (model_type, "_", i)
            grid_full_id = self.sid + "." + grid_eid

            # get class of specific model and create entity with init values after validation
            entity_cls = getattr(pandapower_model, model_type)
            vld.validate_init_parameters(entity_cls, {"gridfile": gridfile, "sim_start": sim_start})
            grid_entity = entity_cls(eid=grid_eid, gridfile=gridfile, sim_start=sim_start)

            # add relations for the grid components for the orchestrator to know the connections
            children = []
            for comp_eid, attrs in sorted(grid_entity.components_map.items()):
                assert comp_eid not in self._components
                self._components[comp_eid] = attrs

                # Only add relations from line to nodes (not the other direction) as it is
                # sufficient for the orchestrator to build the entity graph
                relations = []
                if attrs["etype"] in ["trafo", "line", "load", "sgen", "storage"]:
                    relations = attrs["related"]

                children.append(
                    {
                        "eid": comp_eid,
                        "type": attrs["etype"],
                        "rel": relations,
                    }
                )

            # add info to the simulators entity-list and current entities
            self.entities[grid_eid] = {
                "eid": grid_eid,
                "etype": model_type,
                "model": grid_entity,
                "full_id": grid_full_id,
            }
            entities_orchestrator.append(
                {
                    "eid": grid_eid,
                    "type": model_type,
                    "rel": [],
                    "children": children,
                }
            )

        return entities_orchestrator

    def setup_done(self):
        """Adjust entity IDs and activate components at the end of the grid setup.

        Yields:
            related_components: grid components that are in relation to one another (e.g. bus and
            connected line)
        """
        for eid, entity_dict in self.entities.items():
            # Turn our entity IDs into full ids as required by get_related_entities
            entity_full_ids = list(
                "%s.%s" % (self.sid, eid)
                for eid, edict in entity_dict["model"].components_map.items()
                if edict["etype"] in ("load", "sgen")
            )
            related_components = yield self.mosaik.get_related_entities(entity_full_ids)

            for full_id, related in related_components.items():
                # We activate only those loads and sgens that are connected to other
                # simulator entities. For this, we exclude those related entities
                # whose full IDs start with our simulator ID. We include the '.' to
                # distiguish 'Grid-1' from 'Grid-10'.
                if any(not other_id.startswith(self.sid) for other_id in related.keys()):
                    eid = full_id[len(self.sid) + 1 :]  # Remove our SID and the period
                    edict = entity_dict["model"].components_map[eid]
                    if edict["etype"] == "sgen":
                        entity_dict["model"].grid.sgen.in_service.at[edict["idx"]] = True
                    elif edict["etype"] == "load":
                        entity_dict["model"].grid.load.in_service.at[edict["idx"]] = True

    def get_entity_by_id(self, entity_id: str):
        """Searches for a requested entity id and gives back the entity model.

        Args:
            entity_id (str): id of the entity to be searched for

        Returns:
            object: entity model if found, None otherwise
        """

        if entity_id in self.entities.keys():
            return self.entities[entity_id]["model"]
        else:
            return None

    def step(self, time, inputs, max_advance):
        """Performs simulation step calling the eELib pandapower grid model.
        Core function of mosaik.

        Args:
            time (int): current simulation time (given by mosaik)
            inputs (dict, optional): allocation of return values to specific models
            max_advance (int, optional): simulation time until the simulator can safely advance it's
                                        internal time without causing any causality errors.

        Returns:
            int: next timestep (when orchestrator calls again)
        """

        self.timestep = time

        # assign property values for each entity and attribute with entity ID
        # process input signals: for the entities (eid), attr is a dict for attributes to be set
        for eid, attrs in inputs.items():
            # first handle inputs for general grid (not for the components)
            if "pandapower" in eid.lower():
                for attr, vals in attrs.items():
                    setattr(self.entities[eid]["model"], attr, vals)
                continue  # go on to next entity for input

            # get some attributes of current entity to set the input
            idx = self._components[eid]["idx"]
            etype = self._components[eid]["etype"]
            static = self._components[eid]["static"]

            # for the attributes (attr), setter is a dict for entities with corresponding set values
            for attr, setter in attrs.items():
                # Store set values for the entity fields into "attrs" dict
                if attr == "in_service" or attr == "controllable":
                    attrs[attr] = setter.values()[0]
                else:
                    attrs[attr] = sum(float(v) for v in setter.values())

            # set the inputs for the corresponding pandapower entity
            for grid_eid, entity_dict in self.entities.items():
                if eid in entity_dict["model"].components_map:
                    entity_dict["model"].set_inputs(
                        etype,
                        idx,
                        attrs,
                        static,
                    )

        # call step function for each entity in the list
        for grid_eid, entity_dict in self.entities.items():
            entity_dict["model"].step(time)

        # next timestamp for simulation
        return None

    def get_data(self, outputs):
        """Gets the data for the next concatenated model.
        Core function of mosaik.

        Args:
            outputs (dict): Dictionary with data outputs from each storage model

        Raises:
            ValueError: Error if attribute not in model metadata
            KeyError: Error if component entity could not be found in pandapower grid

        Returns:
            dict: Dictionary with simulation outputs
        """

        # create flag for checking if any new data is send out
        flag_output_changed = False

        # create output data
        data = {}

        # check current timestep (whether a new one is reached)
        if not self.output_cache == {} and self.output_cache["time"] != self.timestep:
            self.output_cache = {}
        data["time"] = self.timestep
        self.output_cache["time"] = self.timestep

        for transmitter_ename, attrs in outputs.items():
            # get name for current entity and create dict field
            if transmitter_ename in self.entities.keys():
                entry = {"etype": "Pandapower"}
            else:
                entry = self._components[transmitter_ename]
            if transmitter_ename not in self.output_cache:
                self.output_cache[transmitter_ename] = {}

            # loop over all targeted attributes and check if info is available
            for attr in attrs:
                if attr not in self.meta["models"][entry["etype"]]["attrs"]:
                    raise ValueError("Unknown output attribute: %s" % attr)

                # create empty field for cache
                if attr not in self.output_cache[transmitter_ename]:
                    self.output_cache[transmitter_ename][attr] = {}

                # retrieve output data
                output_data_to_save = {}
                if transmitter_ename in self.entities.keys():  # check wether transmitter is grid
                    output_data_to_save = getattr(self.entities[transmitter_ename]["model"], attr)
                else:  # transmitter is no grid
                    # search for adjacent grid entity and save the corresponding powerflow result
                    for entity_dict in self.entities.values():
                        if transmitter_ename in entity_dict["model"].components_map:
                            try:
                                output_data_to_save = entity_dict["model"].powerflow_results[
                                    transmitter_ename
                                ][attr]
                            except KeyError:
                                output_data_to_save = entry["static"][attr]

                # check if no corresponding output data was found
                if isinstance(output_data_to_save, dict) and output_data_to_save == {}:
                    raise KeyError(
                        "Could not find component '%s' in pandapower grid entities"
                        % transmitter_ename
                    )

                # check if array/matrix is a dict or just a single value
                if isinstance(output_data_to_save, (list, np.ndarray, np.matrix)):
                    self.output_cache[transmitter_ename][attr] = output_data_to_save
                else:
                    # output_data_to_save is a single value to be stored
                    if self.output_cache[transmitter_ename][attr] == {}:  # no value existent
                        self.output_cache[transmitter_ename][attr] = output_data_to_save
                        flag_output_changed = True
                    elif isinstance(output_data_to_save, (float, int)):  # value exist. and compar
                        if (
                            abs(self.output_cache[transmitter_ename][attr] - output_data_to_save)
                            > ADAPTION_TOLERANCE
                        ):
                            self.output_cache[transmitter_ename][attr] = output_data_to_save
                            flag_output_changed = True
                    else:  # value existent and not comparable
                        if self.output_cache[transmitter_ename][attr] != output_data_to_save:
                            self.output_cache[transmitter_ename][attr] = output_data_to_save
                            flag_output_changed = True

            # set whole output data for this entity
            data[transmitter_ename] = self.output_cache[transmitter_ename]

        # check if nothing is to be send out - send output 1 step later to avoid waiting for data
        if not flag_output_changed:
            if self.timestep == self.scenario_config["n_steps"] - 1:  # is last timestep?
                data["time"] = self.timestep + 1
            else:
                data = {"time": self.timestep}

        # return data to mosaik
        return data
