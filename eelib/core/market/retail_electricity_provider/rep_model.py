"""
eELib retail electricity provider models the behavior of provider for electricity.
This basically is reduced to a provider for households and maybe small business companies.

| Author: elenia@TUBS
| Copyright 2024 elenia
| This file is part of eELib, which is free software under the terms of the GNU GPL Version 3.
"""

from math import inf
from eelib.data import TariffSignal, GridTariffSignal


class RetailElectricityProvider:
    """Models a retail electricity provider with tariff signals and market information."""

    market_info_attrs = ["price_weighted_avg", "price_low", "price_high", "price_last"]

    # Valid values and types for each parameter
    _VALID_PARAMETERS = {
        "step_size": {"types": [int], "values": None},
        "n_steps": {"types": [int], "values": (0, inf)},
        "tariff_type": {"types": [str], "values": ["static", "variable", "dynamic"]},
        "elec_price": {"types": [int, float], "values": (0, inf)},
        "feedin_tariff": {"types": [int, float], "values": (0, inf)},
    }

    @classmethod
    def get_valid_parameters(cls):
        """Returns dictionary containing valid parameter types and values.

        Returns:
            dict: valid parameters for this model
        """
        return cls._VALID_PARAMETERS

    def __init__(
        self,
        eid: str,
        step_size: int = 60 * 15,
        n_steps: int = 24 * 4,
        tariff_type: str = "static",
        elec_price: float = 0.35,
        feedin_tariff: float = 0.07,
    ):
        """Initializes the retail electricity provider model.

        Args:
            eid (str): name of the model entity
            step_size (int): length of a simulation step in seconds. Defaults to 60*15.
            n_steps (int): length of the simulation in steps. Defaults to 24*4.
            tariff_type (str): chosen type of tariff for connected entities. Defaults to "static".
            elec_price (float): electricity consumption price info [EUR/kWh]. Defaults to 35ct/kWh
            feedin_tariff (float): electricity feed-in price info [EUR/kWh]. Defaults to 7ct/kWh
        """

        self.eid = eid

        # save step length and current timestep
        self.step_size = step_size
        self.n_steps = n_steps
        self.timestep = 0

        # initilize storing of connected markets
        self.market_list = {}

        # save chosen tariff info
        self.tariff_type = tariff_type
        self.elec_price = elec_price
        self.feedin_tariff = feedin_tariff

        # initialize inputs from market and grid ems
        self.market_data = {}  # market information on current step
        self.grid_tariff_signal: GridTariffSignal = None  # grid usage fee information
        self.forecast_request = dict()  # request for forecasts to calculate
        self.forecast = dict()  # resulting forecasts for market info
        self.schedule = dict()  # schedules for the tariff signals

        # initialize tariff output info for households
        self.tariff = None

        # power information from households
        self.p_ems_dem = {}  # active power demand values from connected households
        self.p_ems_gen = {}  # active power generation values from connected households

    def add_market_entity(self, market_dict: dict):
        """Adds a market entity to the corresponding list.

        Args:
            market_dict (dict): Input list with all market models
        """

        # append each market to list
        for market in market_dict.values():
            # append entity to market list if not already present
            if market["eid"] not in self.market_list.keys():
                self.market_list[market["eid"]] = {
                    "full_id": market["full_id"],
                    "etype": market["type"],
                }

    def step(self, timestep):
        """Performs simulation step of eELib REP model.

        Args:
            timestep (int): Current simulation time
        """
        # handle current timestep
        if not self.timestep == timestep:
            self.timestep = timestep

        # calculate current tariff info
        self._calc_tariff_output()

    def _calc_tariff_output(self):
        """Combine values and market info for a tariff signal for this timestep.

        Raises:
            ValueError: If tariff type is not implemented
        """

        # check grid signal - create "empty" one
        if self.grid_tariff_signal is None:
            self.grid_tariff_signal = GridTariffSignal()

        if self.tariff_type == "static":
            self._create_static_tariff()
        elif self.tariff_type == "variable":
            self._create_variable_tariff()
        elif self.tariff_type == "dynamic":
            self._create_dynamic_tariff()
        else:
            raise ValueError(f"No tariff type {self.tariff_type} implemented for REP model!")

    def _create_static_tariff(self):
        """Creates tariff signal for static tariff."""
        self.tariff = TariffSignal(
            bool_is_list=False,
            elec_price=self.elec_price + self.grid_tariff_signal.energy_price,
            feedin_tariff=self.feedin_tariff,
            capacity_fee_dem=self.grid_tariff_signal.capacity_fee_dem,
            capacity_fee_gen=self.grid_tariff_signal.capacity_fee_gen,
            capacity_fee_horizon_sec=self.grid_tariff_signal.capacity_fee_horizon_sec,
        )

    def _create_variable_tariff(self):
        """Creates tariff signal for variable tariff (WIP)."""
        # set tariff from calculated schedule (for every timestep)
        self.tariff = TariffSignal()

    def _create_dynamic_tariff(self):
        """Creates tariff signal for dynamic tariff.

        Raises:
            ValueError: If more than one market signal is recieved.
        """
        # retrieve current market price
        if len(self.market_data.keys()) == 1:
            market_price_eur_mwh = self.market_data[list(self.market_data.keys())[0]].price
        else:
            raise ValueError("More than one market signal for retail electricity provider!")

        # set tariff based on market price - convert from mwh to kwh
        # NOTE: Currently only factorized by 1, should be advanced with charges and margin
        elec_price = 1 * market_price_eur_mwh / 1000 + self.grid_tariff_signal.energy_price
        self.tariff = TariffSignal(
            bool_is_list=False,
            elec_price=elec_price,
            feedin_tariff=self.feedin_tariff,
            capacity_fee_dem=self.grid_tariff_signal.capacity_fee_dem,
            capacity_fee_gen=self.grid_tariff_signal.capacity_fee_gen,
            capacity_fee_horizon_sec=self.grid_tariff_signal.capacity_fee_horizon_sec,
        )

    def _request_forecast(self):
        """Set markets with the needed attributes and time horizon for forecast request."""
        # clear request for forecasts
        self.forecast_request = {}

        # go by each connected market
        for market_eid in self.market_list.keys():
            # add forecast request for this market entity
            self.forecast_request[market_eid] = {
                "attr": self.market_info_attrs,
                "t": range(0, self.n_steps),
            }
