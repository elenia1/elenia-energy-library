"""
Mosaik interface for the eELib retail electricity provider model.
Simulator for communication between orchestrator (mosaik) and REP entities.

Within this interface mosaik functionalities are used.
Copyright (c) LGPL

| Author: elenia@TUBS
| Copyright 2024 elenia
| This file is part of eELib, which is free software under the terms of the GNU GPL Version 3.
"""

import mosaik_api_v3
from eelib.core.market.retail_electricity_provider import rep_model
from eelib.data import MarketData, GridTariffSignal
import dataclasses
import eelib.utils.validation as vld
from copy import deepcopy

# accuracy for cached output values to avoid triggering of event-based simulators
ADAPTION_TOLERANCE = 10 ** (-5)

# META DATA: Simulator information about model classes, its parameters, attributes, and methods
META = {
    "type": "hybrid",
    "models": {
        "RetailElectricityProvider": {
            "public": True,
            "params": ["init_vals"],
            "attrs": [
                "tariff_type",
                "elec_price",
                "feedin_tariff",
                "market_data",
                "grid_tariff_signal",
                "forecast_request",
                "forecast",
                "schedule",
                "tariff",
                "p_ems_dem",
                "p_ems_gen",
            ],
            "trigger": [
                "market_data",
                "grid_tariff_signal",
                "forecast",
                "p_ems_dem",
                "p_ems_gen",
            ],  # input attributes
            "non-persistent": ["forecast_request"],  # output attributes
        },
    },
    "extra_methods": ["get_entity_by_id", "add_market_entity"],
}


class Sim(mosaik_api_v3.Simulator):  # MODEL
    """Simulator class for eELib retail electricity provider model.

    Args:
        mosaik_api_v3 (module): defines communication between mosaik and simulator

    Raises:
        ValueError: Unknown output attribute, when not described in META of simulator
    """

    def __init__(self):
        """Constructs an object of the Car:Sim class."""

        super().__init__(META)

        # storing of event-based output info (for same-time loop or next timestep)
        self.output_cache = {}

        # storing of device data
        self.data = {}

        # initiate empty dict for model entities
        self.entities = {}

    def init(self, sid, scenario_config, time_resolution=1.0):
        """Initializes parameters for an object of the REP:Sim class.

        Args:
            sid (str): Id of the created instance of the load simulator (e.g. REPSim-0).
            scenario_config (dict): scenario configuration data, like resolution or step size.
            time_resolution (float): fitting of the step size to the simulation scenario step size.

        Returns:
            meta: description of the simulator
        """

        # assign properties for simulator
        self.sid = sid
        self.scenario_config = scenario_config

        return self.meta

    def create(self, num, model_type, init_vals):
        """Creates instances of the eELib retail electricity provider model.

        Args:
            num (int): Number of models to be created
            model_type (str): Description of the created instance (here "RetailElectricityProvider")
            init_vals (dict): initial values for REP models to be created

        Raises:
            ValueError: If entity ID is already existing

        Returns:
            dict: created entities
        """

        # create empty list for created entities
        entities_orchestrator = []

        for eid, entity_vals in init_vals.items():
            # check if entity ID is already existing
            if eid in self.entities:
                raise ValueError(f"Entity ID {eid} for model type {model_type} already existing.")

            # create entity by specified full ID
            full_id = self.sid + "." + eid

            # get class of specific model and create entity with init values after validation
            entity_cls = getattr(rep_model, model_type)
            vld.validate_init_parameters(entity_cls, entity_vals)
            entity = entity_cls(
                eid,
                **entity_vals,
                n_steps=self.scenario_config["n_steps"],
                step_size=self.scenario_config["step_size"],
            )

            # add info to the simulators entity-list and current entities
            self.entities[eid] = {
                "eid": eid,
                "etype": model_type,
                "model": entity,
                "full_id": full_id,
            }
            entities_orchestrator.append({"eid": eid, "type": model_type})

        return entities_orchestrator

    def get_entity_by_id(self, entity_id: str):
        """Searches for a requested entity id and gives back the entity model.

        Args:
            entity_id (str): id of the entity to be searched for

        Returns:
            object: entity model if found, None otherwise
        """

        if entity_id in self.entities.keys():
            return self.entities[entity_id]["model"]
        else:
            return None

    def step(self, time, inputs, max_advance):
        """Performs simulation step calling the eELib retail electricity provider model.

        Args:
            time (int): Current simulation time according to step size. Given by MOSAIK.
            inputs (dict): Is a dictionary, that maps entity IDs to
                data dictionaries which map attribute names to lists of values. Given by MOSAIK.
            max_advance (int, optional): Is the simulation time until the simulator can safely
                advance it's internal time without causing any causality errors.

        Raises:
            TypeError: value_dict has unknown format

        Returns:
            int: New timestamp (time increased by step size)
        """

        self.timestep = time

        # assign property values for each entity and attribute with entity ID
        # process input signals: for the entities (eid), attr is a dict for attributes to be set
        for eid, attrs in inputs.items():
            # setter is a dict for entities with set values for the entities getting values
            for attr, setter in attrs.items():
                # for transmitter (eid_setter), value_dict contains set values with ids
                setting_value_dict = deepcopy(getattr(self.entities[eid]["model"], attr))
                for eid_setter, value_dict in setter.items():
                    # check if no value is send -> to next
                    if value_dict is None:
                        continue

                    if isinstance(value_dict, dict):
                        if attr == "forecast":
                            # go by each id and search for corresponding entity id
                            for getter_id, value in value_dict.items():
                                if eid in getter_id:
                                    setting_value_dict = value
                        # check if dataclass
                        elif attr == "market_data":
                            setting_value_dict[eid_setter] = MarketData.from_dict(value_dict)
                        elif attr == "grid_tariff_signal":
                            setting_value_dict = GridTariffSignal.from_dict(value_dict)
                        else:
                            # go by each id and search for corresponding entity id
                            for getter_id, value in value_dict.items():
                                if eid in getter_id:
                                    setting_value_dict[eid_setter] = value
                    # value_dict is not a dict, only a single value -> write directly
                    elif isinstance(value_dict, (float, int)):
                        setting_value_dict[eid_setter] = value_dict
                    else:
                        raise TypeError("Unknown format for value_dict")

                # set the collected value for this attribute
                setattr(self.entities[eid]["model"], attr, setting_value_dict)

        # call step function for each entity in the list
        for eid, entity_dict in self.entities.items():
            entity_dict["model"].step(time)

        # next timestamp for simulation
        return None

    def get_data(self, outputs):
        """Gets the data for the next concatenated model.
        Core function of mosaik.

        Args:
            outputs (dict): Dictionary with data outputs from each REP model

        Raises:
            ValueError: Error if attribute not in model metadata

        Returns:
            dict: Dictionary with simulation outputs
        """

        # create flag for checking if any new data is send out
        flag_output_changed = False

        # create empty output data dict
        data = {}

        # check current timestep (whether a new one is reached)
        if not self.output_cache == {} and self.output_cache["time"] != self.timestep:
            self.output_cache = {}
        data["time"] = self.timestep
        self.output_cache["time"] = self.timestep

        for transmitter_ename, attrs in outputs.items():
            # get name for current entity and create dict field
            entry = self.entities[transmitter_ename]
            if transmitter_ename not in self.output_cache:
                self.output_cache[transmitter_ename] = {}

            # loop over all targeted attributes and check if info is available
            for attr in attrs:
                if attr not in self.meta["models"][type(entry["model"]).__name__]["attrs"]:
                    raise ValueError("Unknown output attribute: %s" % attr)

                # create empty field for cache and output data
                if attr not in self.output_cache[transmitter_ename]:
                    self.output_cache[transmitter_ename][attr] = {}

                # get attribute of this model entity
                output_data_to_save = getattr(entry["model"], attr)
                if dataclasses.is_dataclass(output_data_to_save):  # attribute is dataclass object
                    # check if data empty or values outside ofadaption tolerance
                    if self.output_cache[transmitter_ename][
                        attr
                    ] == {} or output_data_to_save.check_adaption_tolerance(
                        ADAPTION_TOLERANCE,
                        self.output_cache[transmitter_ename][attr],
                    ):
                        # convert dataclass to dict
                        self.output_cache[transmitter_ename][attr] = dataclasses.asdict(
                            output_data_to_save
                        )
                        flag_output_changed = True
                    continue  # go on with next attribute

                # check if output is a dict or just a single value
                if isinstance(output_data_to_save, dict):
                    for emitter_ename, value in output_data_to_save.items():
                        if (
                            emitter_ename not in self.output_cache[transmitter_ename][attr]
                        ):  # value currently not stored in output cache
                            self.output_cache[transmitter_ename][attr] = output_data_to_save
                            flag_output_changed = True
                        elif isinstance(value, (float, int)):  # value existent and comparable
                            if (
                                abs(
                                    self.output_cache[transmitter_ename][attr][emitter_ename]
                                    - value
                                )
                                > ADAPTION_TOLERANCE
                            ):
                                self.output_cache[transmitter_ename][attr] = output_data_to_save
                                flag_output_changed = True
                        else:  # value existent and not comparable
                            if self.output_cache[transmitter_ename][attr][emitter_ename] != value:
                                self.output_cache[transmitter_ename][attr] = output_data_to_save
                                flag_output_changed = True
                    if (
                        output_data_to_save == {}
                        and self.output_cache[transmitter_ename][attr] != {}
                    ):
                        self.output_cache[transmitter_ename][attr] = output_data_to_save
                        flag_output_changed = True
                else:  # output_data_to_save is no dict, but a single value to be stored
                    if self.output_cache[transmitter_ename][attr] == {}:  # no value existent
                        self.output_cache[transmitter_ename][attr] = output_data_to_save
                        flag_output_changed = True
                    elif isinstance(output_data_to_save, (float, int)):  # value exist. and compar.
                        if (
                            abs(self.output_cache[transmitter_ename][attr] - output_data_to_save)
                            > ADAPTION_TOLERANCE
                        ):
                            self.output_cache[transmitter_ename][attr] = output_data_to_save
                            flag_output_changed = True
                    else:  # value existent and not comparable
                        if self.output_cache[transmitter_ename][attr] != output_data_to_save:
                            self.output_cache[transmitter_ename][attr] = output_data_to_save
                            flag_output_changed = True

            # set whole output data for this entity
            data[transmitter_ename] = self.output_cache[transmitter_ename]

        # check if nothing is to be send out - send output 1 step later to avoid waiting for data
        if not flag_output_changed:
            if self.timestep == self.scenario_config["n_steps"] - 1:  # is last time step?
                data["time"] = self.timestep + 1
            else:
                data = {"time": self.timestep}

        # return data to mosaik
        return data

    def add_market_entity(self, market_entity_eid: str, market_dict: dict):
        """Adds market entities (e.g. market csv reader) to the specific REP entity.

        Args:
            market_entity_eid (str): entity id of REP entity
            market_dict (dict): Dictionary of market models to be added to the REP entity
        """

        self.entities[market_entity_eid]["model"].add_market_entity(market_dict)
