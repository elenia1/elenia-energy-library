"""
eElib csv-reader model for reading-in .csv-files to imitate
different model types (e.g. PV, household or heatpump).

| Author: elenia@TUBS
| Copyright 2024 elenia
| This file is part of eELib, which is free software under the terms of the GNU GPL Version 3.
"""

import warnings
import os
import math
import pandas as pd
import arrow
from eelib.utils.resample import resample_pandas_timeseries_agg

from eelib.data import MarketData


class CSVReader:
    """Parent class for reading-in .csv-files.

    Raises:
        FileNotFoundError: when .csv-file cannot be opened (e.g. path or data corrupted)
        ValueError: when start date is not in .csv-file
        IndexError: when end of .csv-file is reached before simulation end
    """

    # Valid values and types for each parameter that apply for all subclasses
    _VALID_PARAMETERS = {
        "datafile": {"types": [str], "values": None},
        "header_rows": {"types": [int], "values": (0, math.inf)},
        "start_time": {"types": [str], "values": None},
        "date_format": {"types": [str], "values": None},
        "delimiter": {"types": [str], "values": None},
        "step_size": {"types": [int], "values": (1, math.inf)},
    }

    @classmethod
    def get_valid_parameters(cls):
        """Returns dictionary containing valid parameter types and values.

        Returns:
            dict: valid parameters for this model
        """
        return cls._VALID_PARAMETERS

    def __init__(
        self,
        eid: str,
        datafile: str,
        header_rows: int,
        start_time: str,
        date_format: str = "YYYY-MM-DD HH:mm:ss",
        delimiter: str = ",",
        step_size: int = 60 * 15,
    ):
        """Initializes a csv reader model.

        Args:
            eid (str): a readable but specific name for the entity
            datafile (str): relative path of datafile
            header_rows (int): header rows of read-in .csv-file
            start_time (str): start of simulation and initial point to read .csv-file
            date_format (str): date format to configure arrow.
                                        Defaults to "YYYY-MM-DD HH:mm:ss".
            delimiter (str): delimiter in .csv-file. Defaults to ",".
            step_size (int): length of a simulated timestep in seconds. Defaults to 900 (15 min)
        """

        # Set attributes (static properties)
        self.eid = eid
        self.datafile = datafile
        self.date_format = date_format
        self.start_date = pd.to_datetime(start_time)
        self.delimiter = delimiter
        self.header_rows = header_rows
        self.step_size = step_size

        # local properties
        self.data = None  # fully read .csv data set [-]

        # dynamic attributes
        self.timestep = None  # current sim timestep
        self.attrs = []  # contains all columns as attributes
        self.values = {}  # data of current step
        self.index = 0

        # open .csv-file
        self._open_csv()

        # check if start_time in .csv-file
        self._check_data()

        self._resample_data()

    # open file
    def _open_csv(self) -> None:
        """Opens the .csv-file and loads data set.

        Raises:
            FileNotFoundError: when .csv-file cannot be read
        """

        # check if datapath is correct and file exists
        if os.path.exists(self.datafile):
            # load data for .csv-file into pandas dataframe
            self.data = pd.read_csv(
                self.datafile,
                index_col=0,
                parse_dates=[0],
                header=self.header_rows - 1,
            )
            self.data.rename(columns=lambda x: x.strip(), inplace=True)
            self.attrs = [col for col in self.data.columns]
            self.values = {attr: None for attr in self.attrs}
        else:
            raise FileNotFoundError(f"Could not load .csv file data from {self.datafile}!")

    def _check_data(self) -> None:
        """Checks if row in opened .csv-file contains the simulation start date.

        Raises:
            ValueError: Start date is not in .csv-file
        """

        # check if simulation start date is in .csv-file
        if self.start_date < self.data.index[0]:
            raise ValueError(
                f"Start {self.start_date} for {self.eid} not in .csv-file - before "
                f"{self.data.index[0]}"
            )
        elif self.start_date > self.data.index[-1]:
            raise ValueError(
                f"Start {self.start_date} for {self.eid} not in .csv-file - after "
                f"{self.data.index[-1]}"
            )

        # Drop all data before start date
        self.data = self.data[self.start_date :]

    def _resample_data(self) -> None:
        self.data = resample_pandas_timeseries_agg(
            self.data, target_resolution=self.step_size, key=self.eid
        )

    def step(self, timestep) -> None:
        """
        Performs simulation step of eELib csv_reader model, which is returning the averaged read
        values in the .csv-file.

        Args:
            timestep (int): Current simulation time in seconds.

        Raises:
            IndexError: Raises an error if the index of the data is out of bound, most likely due to
                differences between start date in scenario and in 'model_data'.
        """

        self.timestep = timestep

        if 0 <= self.index < len(self.data):
            current_row = self.data.iloc[self.index]
        else:
            raise IndexError(
                f"The index of CSV-Reader {self.eid} is out of bound! Probably due to setting a"
                f" false start/end date in model_data_x.jsong input for {self.datafile}"
            )

        for attr in self.attrs:
            self.values[attr] = current_row[attr]
        self.index += 1


class GenericCSV(CSVReader):
    """CSV-Data-Reader for profiles. Inherits from class csv_reader.
    Sets each column of the csv file as attribute.
    """

    @classmethod
    def get_valid_parameters(cls):
        """Returns dictionary containing valid parameter types and values.

        Returns:
            dict: valid parameters for this model
        """

        # use CSVReader's parameter list and modify them for this class
        return CSVReader.get_valid_parameters().copy()

    def __init__(
        self,
        eid: str,
        datafile: str,
        header_rows: int,
        start_time: str,
        date_format: str = "YYYY-MM-DD HH:mm:ss",
        delimiter: str = ",",
        step_size: int = 60 * 15,
    ):
        """Initializes the eELib generic csv reader model.

        Args:
            eid (str): Specifically assigned name for this entity.
            datafile (str): relative path of datafile
            header_rows (int): header rows of read-in .csv-file
            start_time (str): start of simulation and initial point to read .csv-file
            date_format (str): date format to configure arrow.
                                        Defaults to "YYYY-MM-DD HH:mm:ss".
            delimiter (str): delimiter in .csv-file. Defaults to ",".
            step_size (int): length of a simulated timestep in seconds. Defaults to 900 (15 min)
        """

        # call the parent init
        super().__init__(
            eid=eid,
            datafile=datafile,
            date_format=date_format,
            delimiter=delimiter,
            header_rows=header_rows,
            start_time=start_time,
            step_size=step_size,
        )

        for attr in self.attrs:
            setattr(self, attr, None)

    def step(self, timestep) -> None:
        """Performs simulation step of generic csv reader model, which is setting the attr
        according to the columns in csv file.

        Args:
            timestep (int): Current simulation time in seconds.
        """
        super().step(timestep)
        for attr, val in self.values.items():
            setattr(self, attr, val)


class RatedCSV(CSVReader):
    """CSV-Data-Reader for profiles. Inherits from class csv_reader.
    Adds support for scaling to p_rated.
    """

    @classmethod
    def get_valid_parameters(cls):
        """Returns dictionary containing valid parameter types and values.

        Returns:
            dict: valid parameters for this model
        """

        # use CSVReader's parameter list and modify them for this class
        result = CSVReader.get_valid_parameters().copy()
        result.update({"p_rated": {"types": [int], "values": (1, math.inf)}})
        result.update({"p_rated_profile": {"types": [int], "values": (1, math.inf)}})
        return result

    def __init__(
        self,
        eid: str,
        datafile: str,
        header_rows: int,
        start_time: str,
        date_format: str = "YYYY-MM-DD HH:mm:ss",
        delimiter: str = ",",
        step_size: int = 60 * 15,
        p_rated: int = 4500,
        p_rated_profile: int = 4500,
    ):
        """Initializes the eELib-load model.

        Args:
            eid (str): Specifically assigned name for this entity.
            datafile (str): relative path of datafile
            header_rows (int): header rows of read-in .csv-file
            start_time (str): start of simulation and initial point to read .csv-file
            date_format (str): date format to configure arrow.
                                        Defaults to "YYYY-MM-DD HH:mm:ss".
            delimiter (str): delimiter in .csv-file. Defaults to ",".
            step_size (int): length of a simulated timestep in seconds. Defaults to 900 (15 min)
            p_rated (int): active power of the device [W]
            p_rated_profile (int): rated active power of the profile [W]
        """

        # call the parent init
        super().__init__(
            eid=eid,
            datafile=datafile,
            date_format=date_format,
            delimiter=delimiter,
            header_rows=header_rows,
            start_time=start_time,
            step_size=step_size,
        )

        # Set attributes of init_vals to static properties
        self.p_rated = p_rated
        self.p_rated_profile = p_rated_profile
        self.scaling_factor = round(self.p_rated / self.p_rated_profile, 6)

        self._apply_p_rated()

    def _apply_p_rated(self) -> None:
        """Apply scaling factor (``p_rated``/``p_rated_profile``)."""
        for attr in self.attrs:
            self.data[attr] = self.data[attr] * self.scaling_factor


class PowerCSV(RatedCSV):
    """CSV-Data-Reader for power profiles. Inherits from class csv_reader.

    | Adds support for calculating reactive power.
    | Assumptions:
    | - first column is p
    | - second column is q, if csv has more than one column
    """

    @classmethod
    def get_valid_parameters(cls):
        """Returns dictionary containing valid parameter types and values.

        Returns:
            dict: valid parameters for this model
        """

        # use CSVReader's parameter list and modify them for this class
        result = RatedCSV.get_valid_parameters().copy()
        result.update(
            {
                "cos_phi": {"types": [float], "values": (0, 1)},
                "calc_e_demand_annual": {"types": [bool], "values": [True, False]},
            }
        )
        return result

    def __init__(
        self,
        eid: str,
        datafile: str,
        header_rows: int,
        start_time: str,
        date_format: str = "YYYY-MM-DD HH:mm:ss",
        delimiter: str = ",",
        step_size: int = 60 * 15,
        p_rated: int = 4500,
        p_rated_profile: int = 4500,
        cos_phi: float = 1.0,
        calc_e_demand_annual: bool = False,
    ):
        """Initializes the eELib power csv reader model.

        Args:
            eid (str): Specifically assigned name for this entity.
            datafile (str): relative path of datafile
            header_rows (int): header rows of read-in .csv-file
            start_time (str): start of simulation and initial point to read .csv-file
            date_format (str): date format to configure arrow.
                                        Defaults to "YYYY-MM-DD HH:mm:ss".
            delimiter (str): delimiter in .csv-file. Defaults to ",".
            step_size (int): length of a simulated timestep in seconds. Defaults to 900 (15 min)
            p_rated (int): active power of the device [W]
            p_rated_profile (int): rated active power of the profile [W]
            cos_phi (float): power factor for read-in values. Defaults to 1.
            calc_e_demand_annual (bool): wether the annual energy demand is calculated
        """
        # call the parent init
        super().__init__(
            eid=eid,
            datafile=datafile,
            date_format=date_format,
            delimiter=delimiter,
            header_rows=header_rows,
            start_time=start_time,
            step_size=step_size,
            p_rated=p_rated,
            p_rated_profile=p_rated_profile,
        )

        # Init dynamic attributes
        self.p = None
        self.q = None

        # Set attributes of init_vals to static properties
        self.cos_phi = cos_phi

        # check for reasonable cos_phi values
        if cos_phi < 0.9:
            warnings.warn(
                f"WARNING: cos_phi for {eid} selected to be {cos_phi} < 0.9 - seems unreasonable!"
            )

        self._set_reactive_power()

        self.e_demand_annual = None
        if calc_e_demand_annual:
            self._calc_e_demand_annual()

    def _calc_e_demand_annual(self):
        """Calculate the annual energy from the sum of power values, scaling up/down to 1 year."""
        # find correct name for power column
        for label in ("P", "P # [W]", "p"):
            if label in self.data.keys():
                # calc energy demand in simulation
                e_demand_sim = sum(self.data[label]) * self.step_size / 3600

                # scale into one year
                start = str(self.data.index[0])
                end = str(self.data.index[-1])
                sim_seconds = int(
                    (
                        arrow.get(end, self.date_format) - arrow.get(start, self.date_format)
                    ).total_seconds()
                    + self.step_size  # last step would be missed
                )
                year_seconds = 365 * 24 * 60 * 60

                self.e_demand_annual = e_demand_sim * (year_seconds / sim_seconds)
                return

        self.e_demand_annual = 0  # no column for power, hence no energy

    def _set_reactive_power(self) -> None:
        """Calculate reactive power if not given in csv file and if cos_phi is set."""
        if len(self.attrs) == 1:
            if self.cos_phi is not None and self.cos_phi != 0:
                self.data["q"] = -self.data[self.attrs[0]] * math.sqrt((1 / self.cos_phi**2) - 1)
            else:
                self.data["q"] = self.data[self.attrs[0]] * 0
            self.attrs.append("q")
            self.values["q"] = None

    def step(self, timestep):
        """Performs simulation step of eELib load model, which is returning the read
        active/reactive power value of the .csv-file.

        Args:
            timestep (int): Current simulation time in seconds.
        """

        # call the parent step method
        super().step(timestep)

        self.p = self.values[self.attrs[0]]
        self.q = self.values[self.attrs[1]]


class HouseholdCSV(PowerCSV):
    """CSV-Data-Reader for household load profiles. Inherits from class csv_reader."""

    @classmethod
    def get_valid_parameters(cls):
        """Returns dictionary containing valid parameter types and values.

        Returns:
            dict: valid parameters for this model
        """
        return PowerCSV.get_valid_parameters()


class PvCSV(PowerCSV):
    """CSV-Data-Reader for pv-generation profiles. Inherits from class CSVReader."""

    @classmethod
    def get_valid_parameters(cls):
        """Returns dictionary containing valid parameter types and values.

        Returns:
            dict: valid parameters for this model
        """
        return PowerCSV.get_valid_parameters()

    # perform simulation step of household load
    def step(self, timestep):
        """Performs simulation step of eELib load model, which is returning the read
        active/reactive power value of the .csv-file.

        Args:
            timestep (int): Current simulation time in seconds.
        """

        # call the parent step method
        super().step(timestep)

        # ensure electric power is negative (passive sign convention!)
        self.p = -abs(self.p)
        self.q = abs(self.q)


class HeatpumpCSV(PowerCSV):
    """CSV-Data-Reader for heatpump load profiles."""

    @classmethod
    def get_valid_parameters(cls):
        """Returns dictionary containing valid parameter types and values.

        Returns:
            dict: valid parameters for this model
        """
        return PowerCSV.get_valid_parameters()

    def __init__(
        self,
        eid: str,
        datafile: str,
        header_rows: int,
        start_time: str,
        date_format: str = "YYYY-MM-DD HH:mm:ss",
        delimiter: str = ",",
        step_size: int = 60 * 15,
        p_rated: int = 4000,
        p_rated_profile: int = 4000,
        cos_phi: float = 0.95,
    ):
        """
        Initializes the eELib heatpump csv reader model.

        Args:
            eid (str): Specifically assigned name for this entity.
            datafile (str): relative path of datafile
            header_rows (int): header rows of read-in .csv-file
            start_time (str): start of simulation and initial point to read .csv-file
            date_format (str): date format to configure arrow.
                                        Defaults to "YYYY-MM-DD HH:mm:ss".
            delimiter (str): delimiter in .csv-file. Defaults to ",".
            step_size (int): length of a simulated timestep in seconds. Defaults to 900 (15 min)
            p_rated (int): active power of the device [W]
            p_rated_profile (int): rated active power of the profile [W]
            cos_phi (float): power factor for read-in values. Defaults to 1.
        """

        # call the parent init
        super().__init__(
            eid=eid,
            datafile=datafile,
            date_format=date_format,
            delimiter=delimiter,
            header_rows=header_rows,
            start_time=start_time,
            step_size=step_size,
            p_rated=p_rated,
            p_rated_profile=p_rated_profile,
            cos_phi=cos_phi,
        )

        # dynamic output properties
        self.p_el = None  # active power of heatpump [W]
        self.q_el = None  # reactive power of heatpump [W]

    def step(self, timestep):
        """Performs simulation step of eELib load model, which is returning the read
        active/reactive power value of the .csv-file.

        Args:
            timestep (int): Current simulation time in seconds.
        """

        # call the parent step method
        super().step(timestep)

        self.p_el = self.values[self.attrs[0]]
        self.q_el = self.values[self.attrs[1]]


class ChargingStationCSV(PowerCSV):
    """CSV-Data-Reader for charging_station profiles. Inherits from class CSVReader."""

    @classmethod
    def get_valid_parameters(cls):
        """Returns dictionary containing valid parameter types and values.

        Returns:
            dict: valid parameters for this model
        """
        return PowerCSV.get_valid_parameters()


class HouseholdThermalCSV(RatedCSV):
    """CSV-Data-Reader for thermal demand profiles. Inherits from class RatedCSV."""

    @classmethod
    def get_valid_parameters(cls):
        """Returns dictionary containing valid parameter types and values.

        Returns:
            dict: valid parameters for this model
        """
        return RatedCSV.get_valid_parameters().copy()

    def __init__(
        self,
        eid: str,
        datafile: str,
        header_rows: int,
        start_time: str,
        date_format: str = "YYYY-MM-DD HH:mm:ss",
        delimiter: str = ",",
        step_size: int = 60 * 15,
        p_rated: int = 20000,
        p_rated_profile: int = 20000,
    ):
        """Initializes the eELib thermal demand csv reader model.

        Args:
            eid (str): Specifically assigned name for this entity.
            datafile (str): relative path of datafile
            header_rows (int): header rows of read-in .csv-file
            start_time (str): start of simulation and initial point to read .csv-file
            date_format (str): date format to configure arrow.
                                        Defaults to "YYYY-MM-DD HH:mm:ss".
            delimiter (str): delimiter in .csv-file. Defaults to ",".
            step_size (int): length of a simulated timestep in seconds. Defaults to 900 (15 min)
            p_rated (int): thermal power demand of the household [W]
            p_rated_profile (int): rated thermal power of the profile [W]
        """

        # call the parent init
        super().__init__(
            eid=eid,
            datafile=datafile,
            date_format=date_format,
            delimiter=delimiter,
            header_rows=header_rows,
            start_time=start_time,
            step_size=step_size,
            p_rated=p_rated,
            p_rated_profile=p_rated_profile,
        )

        self.p_th_room = None
        self.p_th_water = None

    def step(self, timestep):
        """Performs simulation step of eELib charging_station model, which is returning the read
        active power value of the .csv-file.

        Args:
            timestep (int): Current simulation time in seconds.
        """

        # call the parent step method
        super().step(timestep)

        # calculate the average of the collected values
        self.p_th_room = self.values[self.attrs[0]]
        self.p_th_water = self.values[self.attrs[1]]


class MarketDayAheadCSV(CSVReader):
    """CSV-Data-Reader for day-ahead auction market data. Inherits from class CSVReader.

    Implementation as a price-taking market: The participants have no influence or impact on the
    market price and participants have to accept the prevailing market price.
    As decentralized power systems are investigated, this assumption is fine as the whole intraday
    market includes many sellers (with the identical product 'electricity').
    See e.g. https://www.economicsonline.co.uk/definitions/price-taker.html/ for more information.

    The prices are given in the unit 'EUR/MWh'.
    """

    @classmethod
    def get_valid_parameters(cls):
        """Returns dictionary containing valid parameter types and values.

        Returns:
            dict: valid parameters for this model
        """
        return CSVReader.get_valid_parameters().copy()

    def __init__(
        self,
        eid: str,
        datafile: str,
        header_rows: int,
        start_time: str,
        date_format: str = "YYYY-MM-DD HH:mm:ss",
        delimiter: str = ",",
        step_size: int = 60 * 15,
    ):
        """Initializes the eELib market data csv reader model.

        Args:
            eid (str): Specifically assigned name for this entity.
            datafile (str): relative path of datafile
            header_rows (int): header rows of read-in .csv-file
            start_time (str): start of simulation and initial point to read .csv-file
            date_format (str): date format to configure arrow.
                                        Defaults to "YYYY-MM-DD HH:mm:ss".
            delimiter (str): delimiter in .csv-file. Defaults to ",".
            step_size (int): length of a simulated timestep in seconds. Defaults to 900 (15 min)
        """

        # call the parent init
        super().__init__(
            eid=eid,
            datafile=datafile,
            date_format=date_format,
            delimiter=delimiter,
            header_rows=header_rows,
            start_time=start_time,
            step_size=step_size,
        )

        self.price = None
        self.volume = None

    def step(self, timestep):
        """Performs simulation step of model, which is returning the read value of the .csv-file.

        Args:
            timestep (int): Current simulation time in seconds.
        """

        # call the parent step method
        super().step(timestep)

        # set the collected values
        self.price = self.values[self.attrs[0]]
        if len(self.attrs) > 1:
            self.volume = self.values[self.attrs[1]]
        else:
            self.volume = None

        self.market_data = MarketData(price=self.price, volume=self.volume)
