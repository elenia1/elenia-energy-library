"""
Mosaik interface for the eELib csv-reader model.
Simulator for communication between orchestrator (mosaik) and csv-reader entities.

Within this interface mosaik functionalities are used.
Copyright (c) LGPL

| Author: elenia@TUBS
| Copyright 2024 elenia
| This file is part of eELib, which is free software under the terms of the GNU GPL Version 3.
"""

import mosaik_api_v3
from eelib.data.csv_reader import csv_reader_model
import dataclasses
import eelib.utils.validation as vld

# META DATA: Simulator information about model classes, its parameters, attributes, and methods
META = {
    "type": "time-based",
    "models": {
        "HouseholdCSV": {
            "public": True,
            "params": ["init_vals"],
            "attrs": [
                "type",
                "datafile",
                "date_format",
                "delimiter",
                "header_rows",
                "start_time",
                "cos_phi",
                "data",
                "attrs",
                "values",
                "p_rated",
                "p_rated_profile",
                "p",
                "q",
                "e_demand_annual",
            ],
        },
        "PvCSV": {
            "public": True,
            "params": ["init_vals"],
            "attrs": [
                "type",
                "datafile",
                "date_format",
                "delimiter",
                "header_rows",
                "start_time",
                "cos_phi",
                "data",
                "attrs",
                "values",
                "p_rated",
                "p_rated_profile",
                "p",
                "q",
            ],
        },
        "HeatpumpCSV": {
            "public": True,
            "params": ["init_vals"],
            "attrs": [
                "type",
                "datafile",
                "date_format",
                "delimiter",
                "header_rows",
                "start_time",
                "cos_phi",
                "data",
                "attrs",
                "values",
                "p_rated",
                "p_rated_profile",
                "p_el",
                "q_el",
            ],
        },
        "ChargingStationCSV": {
            "public": True,
            "params": ["init_vals"],
            "attrs": [
                "type",
                "datafile",
                "date_format",
                "delimiter",
                "header_rows",
                "start_time",
                "cos_phi",
                "data",
                "attrs",
                "values",
                "p_rated",
                "p_rated_profile",
                "p",
                "q",
                "e_demand_annual",
            ],
        },
        "HouseholdThermalCSV": {
            "public": True,
            "params": ["init_vals"],
            "attrs": [
                "type",
                "datafile",
                "date_format",
                "delimiter",
                "header_rows",
                "start_time",
                "data",
                "attrs",
                "values",
                "p_rated",
                "p_rated_profile",
                "p_th_room",
                "p_th_water",
            ],
        },
        "MarketDayAheadCSV": {
            "public": True,
            "params": ["init_vals"],
            "attrs": [
                "type",
                "datafile",
                "date_format",
                "delimiter",
                "header_rows",
                "start_time",
                "data",
                "attrs",
                "values",
                "price",
                "volume",
                "market_data",
            ],
        },
    },
    "extra_methods": ["get_entity_by_id"],
}


class Sim(mosaik_api_v3.Simulator):
    """Simulator class for eELib csv-reader model.

    Args:
        mosaik_api_v3 (module): defines communication between mosaik and simulator

    Raises:
        ValueError: Unknown output attribute, when not described in META of simulator
    """

    def __init__(self):
        """Constructs an object of the CSV-Reader:Sim class."""

        super(Sim, self).__init__(META)

        # initiate empty dict for model entities
        self.entities = {}

    def init(self, sid, scenario_config, time_resolution=1.0):
        """Initializes parameters for an object of the CSV-Reader:Sim class.

        Args:
            sid (str): ID of the created entity of the simulator (e.g. LoadSim-0)
            scenario_config (dict): scenario configuration data, like resolution or step size
            time_resolution (float): Time resolution of current scenario.

        Returns:
            meta: description of the simulator
        """

        # assign properties for simulator
        self.sid = sid
        self.scenario_config = scenario_config

        return self.meta

    def create(self, num, model_type, init_vals):
        """
        Creates entities of the eELib csv-reader model.
        Core function of mosaik.

        Args:
            num (int): number of load models to be created
            model_type (str): type of created instance (e.g. "household")
            init_vals (list): list with initial values for each csv-reader entity

        Raises:
            ValueError: If entity ID is already existing

        Returns:
            dict: created entities
        """

        # create empty list for created entities
        entities_orchestrator = []

        for eid, entity_vals in init_vals.items():
            # check if entity ID is already existing
            if eid in self.entities:
                raise ValueError(f"Entity ID {eid} for model type {model_type} already existing.")

            # create entity by specified full ID
            full_id = self.sid + "." + eid

            # get class of specific model and create entity with init values after validation
            entity_cls = getattr(csv_reader_model, model_type)
            vld.validate_init_parameters(entity_cls, entity_vals)
            entity = entity_cls(
                eid,
                **entity_vals,
                step_size=self.scenario_config["step_size"],
            )

            # add info to the simulators entity-list and current entities
            self.entities[eid] = {
                "eid": eid,
                "etype": model_type,
                "model": entity,
                "full_id": full_id,
            }
            entities_orchestrator.append({"eid": eid, "type": model_type})

        return entities_orchestrator

    def get_entity_by_id(self, entity_id: str):
        """Searches for a requested entity id and gives back the entity model.

        Args:
            entity_id (str): id of the entity to be searched for

        Returns:
            object: entity model if found, None otherwise
        """

        if entity_id in self.entities.keys():
            return self.entities[entity_id]["model"]
        else:
            return None

    def step(self, time, inputs, max_advance):
        """
        Performs simulation step calling the eELib csv-reader model.
        Core function of mosaik.

        Args:
            time (int): current simulation time (given by mosaik)
            inputs (dict, optional): allocation of return values to specific models (NOT NEEDED FOR
                                    CSV-READER MODEL)
            max_advance (int, optional): simulation time until the simulator can safely advance it's
                                        internal time without causing any causality errors.

        Returns:
            int: next timestep (when orchestrator calls again)
        """

        # process input signals: for the entities (eid), attr is a dict for attributes to be set
        for eid, attrs in inputs.items():
            # for the attributes (attr), setter is a dict for entities with corresponding set values
            for attr, setter in attrs.items():
                # set given input value
                if len(setter) == 1:
                    setattr(self.entities[eid]["model"], attr, [*setter.values()][0])
                else:
                    IndexError("More than one set value %s for model entity %s" % (attr, eid))

        # call step function for each entity in the list
        for eid, entity_dict in self.entities.items():
            entity_dict["model"].step(time)

        # next timestamp for simulation depending on model step size
        return time + 1

    def get_data(self, outputs):
        """
        Gets the data for the next connected model.
        Core function of mosaik.

        Args:
            outputs (dict): dictionary with data outputs from each entity

        Raises:
            ValueError: error if attribute not in model metadata

        Returns:
            dict: dictionary with simulation outputs
        """

        # create empty output data dict
        data = {}

        for eid, attrs in outputs.items():
            # get name for current entity and create dict field
            entry = self.entities[eid]
            data[eid] = {}

            # loop over all targeted attributes and check if info is available
            for attr in attrs:
                if attr not in self.meta["models"][type(entry["model"]).__name__]["attrs"]:
                    raise ValueError("Unknown output attribute: %s" % attr)

                # get attribute of this model entity
                output_data_to_save = getattr(entry["model"], attr)

                # check type of output attribute
                if dataclasses.is_dataclass(output_data_to_save):
                    # convert dataclass to dict
                    data[eid][attr] = dataclasses.asdict(output_data_to_save)
                else:
                    data[eid][attr] = getattr(entry["model"], attr)

        return data
