"""
Interface for the event-based HDF5 database.

Within this interface mosaik functionalities are used.
Copyright (c) LGPL

| Author: elenia@TUBS
| Copyright 2024 elenia
| This file is part of eELib, which is free software under the terms of the GNU GPL Version 3.
"""

import json
import re
import numpy as np
import h5py
import mosaik_api
import networkx as nx


meta = {
    "type": "hybrid",
    "models": {
        "Database": {
            "public": True,
            "any_inputs": True,
            "params": ["filename", "buf_size", "dataset_opts"],
            "attrs": [],
        },
    },
    "extra_methods": [
        "set_meta_data",
        "set_static_data",
    ],
}


class Hdf5Database(mosaik_api.Simulator):
    """HDF5 Database class
    Inherits mosaik simulator class for management.
    """

    def __init__(self):
        """Constructor of hdf5 output database."""
        super().__init__(meta)
        self.eid = "hdf5db"

        # Set in init
        self.sid = None
        self.step_size = None
        self.duration = None
        self.ds_size = None
        self.series_path = None  # regex object for the series path
        self.series_path_repl = None  # replacement string for series path

        # Set in create
        self.db = None
        self.rels = None
        self.series = None
        self.buf_size = None
        self.dataset_opts = {}
        self.config = None  # the group for scenario_config
        self.scenario_config = None  # the dictionary for scenario_config
        self.db_structure = None  # the dictionary for db_structure

        # Used in step
        self.eid_path = {}  # Stores the series path for an entity
        self.data_buf = {}

    # override corresponding method in parent class
    def init(
        self,
        sid,
        scenario_config,
        db_structure=None,
        delimiter="@",
        additional_groups: dict = None,
        series_path=(None, None),
    ):
        r"""Initialize HDF5 Database Entity.

        Args:
            sid (str): string id of the entity to be created
            scenario_config (dict): configurations of the simulation
            db_structure (dict): configuration of order of the series in the resulting
                database. If not given a default structure is used.
            delimiter (str): symbol to separate nested data recieved from HEMS simulator. This
                should be a character that is not otherwise used in the names and paths.
            additional_groups (dict): additional groupings not defined in the model config
                If not given all unmatched components are stored in a ``misc`` grouping.
                Nested dictionaries and arrays as values are permissible.
                Assignment of a component to one of these groups is only possible if it is not
                already assigned to another group provided in db_structure. In that case,
                assignment is determined by component eid string including one of the lowest level
                strings (group names) provided here OR matching the regular expression, if the
                lowest level strings are regexes.
                Additionally, names defined first have priority.
                An example of this is as follows:
                {"E1": ["S1", "S2"],
                "E2": "S3",
                "E3": {"T1":"S8", "T2":[r'^ABC\-(.*)', "S9"]}}
            series_path (tuple): path for the series to be stored. Defaults to (None, None)

        Returns:
            meta: meta description of the simulator


        """
        self.sid = sid
        self.step_size = 1
        self.duration = scenario_config["n_steps"]
        self.ds_size = self.duration // self.step_size
        self.db_structure = db_structure
        self.delimiter = delimiter
        self.additional_groups = additional_groups
        self.additional_groups_dict = dict()
        if series_path[0] is not None:
            self.series_path = re.compile(series_path[0])
            self.series_path_repl = series_path[1]

        # hold on to scenario config
        self.scenario_config = scenario_config

        return self.meta

    def create(self, num, model, filename, buf_size=1000, dataset_opts=None):
        """Creates instances of the HDF5 output database.

        Args:
            num (int): Number of hdf5 db models to be created
            model (str): Description of the created hdf5 db entity
            filename (str): Directory of the hdf5 file
            buf_size (int): Number of elements to be stored before writing into DB. Defaults to 1000
            dataset_opts (dict): Potential set of options for the dataset. Defaults to None.

        Raises:
            ValueError: Error if more than one hdf5 DB is to be created
            ValueError: Error if an unknown model type (not Database) is given

        Returns:
            dict: discription of the hdf5 DB entity with ID, type and relations
        """
        if num != 1 or self.db is not None:
            raise ValueError("Can only create one database.")
        if model != "Database":
            raise ValueError('Unknown model: "%s"' % model)

        self.buf_size = buf_size
        if dataset_opts:
            self.dataset_opts.update(dataset_opts)

        self.db = h5py.File(filename, "w")
        self.rels = self.db.create_group("Relations")
        self.series = self.db.create_group("Series")

        # create a new group named Config
        self.config = self.db.create_group("Config")
        self._store_dict(self.config, self.scenario_config, "scenario_config")

        if self.db_structure is not None:
            group_config = self.db_structure.copy()
            if self.additional_groups is not None:
                group_config.update(self.additional_groups)
                self._create_groups_preset(
                    self.series, self.additional_groups, self.additional_groups_dict
                )
            self._store_json(self.config, group_config, "connection_data")
            self._create_groups_preset(self.series, self.db_structure)
            self.misc_group = self.series.create_group("misc")

        return [{"eid": self.eid, "type": model, "rel": []}]

    def setup_done(self):
        """Check if setup of the DB is done.

        Yields:
            list: related entity for the simulator (given by mosaik)
        """
        yield from self._store_relations()

    # override corresponding method in parent class
    def step(self, time, inputs):
        """
        Performs simulation step saving/buffering data and storing it into the DB.
        Core function of mosaik.

        Args:
            time (int): current simulation time (given by mosaik)
            inputs (dict): allocation of return values to specific models

        Returns:
            int: next timestep (when orchestrator calls again)
        """
        assert len(inputs) == 1
        inputs = inputs[self.eid]

        # unpack values assigned by HEMS to other entities
        # will be displayed as subgroups of assigning HEMS
        # NOTE: expects ALL incoming dicts to match [attr_name][eid]: val_assigned_to_eid
        for key, input in inputs.copy().items():
            if isinstance(list(input.values())[0], dict):
                del inputs[key]
                for hems_id, hems_dict in input.items():
                    for attr, attr_dict in hems_dict.items():
                        modified_dict = {
                            f"{hems_id}{self.delimiter}{k}": v for k, v in attr_dict.items()
                        }
                        try:
                            inputs[attr].update(modified_dict)
                        except KeyError:
                            inputs[attr] = {}
                            inputs[attr].update(modified_dict)

        # Store series
        g_series = self.series
        buf_size = self.buf_size
        eid_path = self.eid_path
        buf = self.data_buf
        abs_idx = time // self.step_size
        rel_idx = abs_idx % buf_size

        for attr, data in inputs.items():
            for src_id, value in data.items():
                if time == 0:
                    self._create_dataset(
                        src_id,
                        attr,
                        type(value),
                        self.ds_size,
                        buf,
                        buf_size,
                        delimiter=self.delimiter,
                    )

                path = eid_path[src_id]
                key = "%s/%s" % (path, attr)

                # Buffer data to improve performance
                buf[key][rel_idx] = value

        buf_len = rel_idx + 1
        last_step = bool(time + self.step_size >= self.duration)
        if buf_len == buf_size or last_step:
            # Write and clear buffer
            start = abs_idx - rel_idx
            end = start + buf_len
            for key, val in buf.items():
                g_series[key][start:end] = buf[key][:buf_len]

            if last_step:  # close database if this is the last step
                self.db.close()

        # next timestamp for simulation
        if time + self.step_size < self.scenario_config["n_steps"]:
            return time + self.step_size
        else:
            return None

    def _store_relations(self):
        """Query relations graph and store it in the database.

        Yields:
            list: related entities of simulator (given by mosaik)
        """
        db_full_id = "%s.%s" % (self.sid, self.eid)
        data = yield self.mosaik.get_related_entities()
        nxg = nx.Graph()
        nxg.add_nodes_from(data["nodes"].items())
        nxg.add_edges_from(data["edges"])

        s_name = self.series.name
        r_name = self.rels.name
        for node, neighbors in sorted(nxg.adj.items()):
            if node == db_full_id:
                continue
            rels = sorted(n for n in neighbors if n != db_full_id)
            rels = np.array(
                [
                    (
                        ("%s/%s" % (r_name, n)).encode(),
                        ("%s/%s" % (s_name, self._get_entity_path(n))).encode(),
                    )
                    for n in rels
                ],
                dtype=(bytes, bytes),
            )
            self.rels.create_dataset(node, data=rels, **self.dataset_opts)

    def _create_dataset(self, src_id, attr, dtype, ds_size, buf, buf_size, delimiter=None):
        """Create a dataset for the attribute ``attr`` of entity ``src_id``. The dataset will use
        the type ``dtype`` and will have the size ``ds_size``. Also initialize the buffer ``buf``
        with size ``buf_size``.

        Args:
            src_id (str): entity for the dataset to be stored in hdf5 file
            attr (str): attribute to be saved
            dtype (type): Type of the attribute
            ds_size (int): number of data elements to be stored
            buf (dict): storing of buffered elements within a list
            buf_size (int): number of elements to be buffered before storing
            delimiter (str): symbol to separate nested data recieved from HEMS simulator
        """

        if dtype is dict:  # skip dicts for 'virtual' values
            return

        if self.db_structure is None:
            if (delimiter is not None) and (delimiter in src_id):
                p1, p2 = src_id.split(delimiter)
                g1 = self._get_group_default(p1)
                g2 = self._get_group_generic(src_id, g1, group_name=p2)
                g = g2
            else:
                g = self._get_group_default(src_id)
        else:
            g = self._find_group_preset(src_id, self.series)
            if g is None and self.additional_groups is not None:
                g = self._get_group_additional(src_id)
            if g is None:
                g = self._get_group_generic(src_id, self.misc_group)

        ds = g.create_dataset(attr, (ds_size,), dtype=np.dtype(dtype), **self.dataset_opts)

        buf[ds.name] = np.empty(buf_size, dtype=dtype)

    def _get_group_generic(self, eid, supergroup, group_name=None):
        """Generic method for creating and/or finding a group within a supergroup.

        Args:
            eid (str): entity id corresponding to the group to be found/created
            supergroup (Group): the supergroup where the search/creation occurs
            group_name (str): group name in case different name is preferred

        Returns:
            Group: the resulting group
        """
        if not self.eid_path == {} and eid in self.eid_path.keys():
            path = self.eid_path[eid]
            if group_name is None:
                group_name = path
            g = supergroup[group_name]
        else:
            path = self._get_entity_path(eid)
            if group_name is None:
                group_name = path
            g = supergroup.create_group(group_name)
            self.eid_path[eid] = g.name
        return g

    def _get_group_default(self, eid):
        """Get or create group for entity ``eid`` based on the default structure.

        Args:
            eid (str): entity identification string

        Raises:
            ValueError: if the given eid format does not match the predefined pattern

        Returns:
            Group: hdf5 file group element for data to be stored into
        """
        pattern = r"^(.*?)\-"  # name of simulator (everything before first dash)
        match = re.match(pattern, eid)
        if match:
            # Extract matched text
            simulator = match.group(1)
        else:
            # If no match is found, raise error
            raise ValueError("incorrect eid format")
        if not self.series == {} and simulator in self.series.keys():
            sg = self.series[simulator]
        else:
            sg = self.series.create_group(simulator)
        return self._get_group_generic(eid, sg)

    def _create_groups_preset(self, supergroup, config, dictionary=None):
        """Recursive method to create groups based on a preset dictionary.

        Args:
            supergroup (Group): supergroup where creation occurs. Initial
                input is where all the series are stored.
            config (dict): the configuration dictionary.
            dictionary (dict): if provided, the given dictionary is updated
                with pairs of group names and Group objects for the groups
                at the lowest level.
        """
        for key, value in config.items():
            if isinstance(value, dict):
                subgroup = supergroup.create_group(key)
                self._create_groups_preset(subgroup, value, dictionary)
            elif isinstance(value, list):
                if value:
                    subgroup = supergroup.create_group(key)
                    for v in value:
                        g = subgroup.create_group(v)
                        if dictionary is not None:
                            dictionary.update({v: g})
            else:
                if value:
                    subgroup = supergroup.create_group(key)
                    g = subgroup.create_group(value)
                    if dictionary is not None:
                        dictionary.update({value: g})

    def _find_group_preset(self, eid, supergroup):
        """Recursive method to find a group within a supergroup.

        Args:
            eid (str): entity id corresponding to the group to be found
            supergroup (Group): the supergroup where the search occurs

        Raises:
            ValueError: if the given eid format does not match the predefined pattern

        Returns:
            Group: the resulting group OR ``None`` if it doesn't exist
        """
        pattern = r"^.*\.(.*)"  # everything after first dot in the eid
        match = re.match(pattern, eid)
        if match:
            name = match.group(1)
        else:
            raise ValueError("incorrect eid format")
        for key, value in supergroup.items():
            if key == name:
                return value
            elif isinstance(value, h5py.Group):
                subresult = self._find_group_preset(eid, value)
                if subresult is not None:
                    self.eid_path[eid] = subresult.name
                    return subresult
        return None

    def _get_group_additional(self, eid):
        """Get or create group for entity ``eid`` in a group among those
        added in addition to model config, if applicable.

        Args:
            eid (str): entity id corresponding to the group to be found

        Returns:
            Group: the first possible group OR ``None`` if it isn't applicable
        """
        for key, value in self.additional_groups_dict.items():
            match = re.match(key, eid)
            if match is not None:
                # if the string for eid matches the key of the group (its name)
                return self._get_group_generic(eid, value)
        return None

    def _store_dict(self, group, dictionary, dataset_name):
        """Store a dictionary as a dataset in a given group.

        Args:
            dictionary (dict): dictionary to be stored
            group (Group): the group to store the data in
            dataset_name (str): the name of the dataset
        """
        cons = np.array(
            [  # convert the dictionary into something to store
                (
                    ("%s" % (k,)).encode(),
                    ("%s" % (v,)).encode(),
                )
                for k, v in dictionary.items()
            ],
            dtype=(bytes, bytes),
        )
        # store the data into a new dataset inside the group named `dataset_name`
        group.create_dataset(dataset_name, data=cons)

    def _store_json(self, group, data, dataset_name):
        """Store json-like data as a dataset in a given group.

        Args:
            data (Any): data to be stored
            group (Group): the group to store the data in
            dataset_name (str): the name of the dataset
        """
        json_string = json.dumps(data)
        group.create_dataset(dataset_name, data=json_string.encode("utf-8"))

    def _get_entity_path(self, eid):
        """Get the database path to a searched entity by its ID.

        Args:
            eid (str): identification number of the entity to be searched

        Returns:
            str: path to the entity
        """
        if self.series_path is not None:
            path = self.series_path.sub(self.series_path_repl, eid)
        else:
            path = eid
        return path
