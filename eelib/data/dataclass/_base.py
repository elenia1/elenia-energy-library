"""Base Class for Dataclasses used in eELib.

| Author: elenia@TUBS
| Copyright 2024 elenia
| This file is part of eELib, which is free software under the terms of the GNU GPL Version 3.
"""

from abc import ABC
from dataclasses import dataclass, asdict
from dataclasses_json import dataclass_json


@dataclass_json
@dataclass
class BaseData(ABC):
    """Baseclass for Dataclasses."""

    def check_adaption_tolerance(
        self, adaption_tolerance: float, dict_cache: dict, dict_to_check: dict = None
    ) -> bool:
        """Compares the input data with its cache based on the individual values.

        Args:
            adaption_tolerance (float): fixed adaption tolerance for comparison of values
            dict_cache (dict): cached dataclass to be compared
            dict_to_check (dict): Subdict to be checked, substitutes dataclass if existent. Defaults
                to None.

        Raises:
            TypeError: if format for dataclass value is unknown

        Returns:
            bool: True if value of new dataclass has changed
        """

        if dict_to_check is not None:
            # take the explicitly given dict that needs to be checked
            dict_new = dict_to_check
        else:
            # convert dataclass into dict for comparison
            dict_new = asdict(self)

        # iterate over the keys (attributes) and values of dictionaries
        for attr, value_cached in dict_cache.items():
            value_new = dict_new[attr]

            # check if values are None (if just one if None, value has changed)
            if value_cached is None or value_new is None:
                if value_new is not None or value_cached is not None:
                    return True

            # compare list values and break loop when change of values is significant
            elif isinstance(value_cached, list) and isinstance(value_new, list):
                if not all(
                    (abs(x - y) < adaption_tolerance) for x, y in zip(value_cached, value_new)
                ):
                    return True

            # compare dict values and recursively check adaption
            elif isinstance(value_cached, dict) and isinstance(value_new, dict):
                return_flag = self.check_adaption_tolerance(
                    adaption_tolerance=adaption_tolerance,
                    dict_cache=value_cached,
                    dict_to_check=value_new,
                )
                if return_flag:
                    return True

            # compare dataclass values and recursively check adaption
            elif isinstance(value_cached, BaseData) and isinstance(value_new, dict):
                return_flag = self.check_adaption_tolerance(
                    adaption_tolerance=adaption_tolerance,
                    dict_cache=asdict(value_cached),
                    dict_to_check=value_new,
                )
                if return_flag:
                    return True

            # compare string and bool values directly with each other
            elif isinstance(value_cached, (str, bool)) and isinstance(value_new, (str, bool)):
                if not value_cached == value_new:
                    return True

            # compare int and floats depending on the adaption tolerance
            elif isinstance(value_cached, (int, float)) and isinstance(value_new, (int, float)):
                if abs(value_cached - value_new) > adaption_tolerance:
                    return True

            else:
                raise TypeError("Unknown format for dataclass value")

        return False
