"""Dataclasses for control signals used in eELib.

| Author: elenia@TUBS
| Copyright 2024 elenia
| This file is part of eELib, which is free software under the terms of the GNU GPL Version 3.
"""

from dataclasses import dataclass, field
from eelib.data.dataclass._base import BaseData


@dataclass
class ControlSignalEMS(BaseData):
    """Dataclass for Control Signals sent to an Energy Management System.
    The lists always have to be of the same length!

    steps (list): timesteps where control signal is active
    p_max (list): maximum active power for those time steps [kW]
    p_min (list): minimum active power for those time steps [kW]
    penalty_cost (float): penalty costs in case of non-compliancy [EUR/kW]
    """

    steps: list[int] = field(default_factory=list)
    p_max: list[float] = field(default_factory=list)
    p_min: list[float] = field(default_factory=list)
    penalty_cost: float = field(default=1e5)
