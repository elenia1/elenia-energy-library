"""Dataclasses for devices' data used in eELib.

| Author: elenia@TUBS
| Copyright 2024 elenia
| This file is part of eELib, which is free software under the terms of the GNU GPL Version 3.
"""

from dataclasses import dataclass, field
from eelib.data.dataclass._base import BaseData


@dataclass
class BaseDeviceData(BaseData):
    """Baseclass for Dataclasses of the devices.

    p (float): current active power
    p_min (float): minimum active power limit
    p_max (float): maximum active power limit
    """

    p: float = field(default=None)
    p_min: float = field(default=None)
    p_max: float = field(default=None)


@dataclass
class BSSData(BaseDeviceData):
    """Dataclass for eELib BSS model.

    e_bat_rated (float): rated energy amount of bss
    p_rated_charge_max (float): maximum charge power of bss
    p_rated_discharge_max (float): maximum discharge power of bss
    loss_rate (float): self-discharge (per month) of bss
    self_discharge_step (float): self-discharge rate (per simulation step) of bss
    dod_max (float): maximum depth of discharge
    status_aging (bool): whether to account for aging of battery
    status_curve (bool) = whether to use a coded curve (true) or static efficiency (false)
    soh_cycles_max (float): state of health at end of lifetime
    bat_cycles_max (float): maximum cycles to reach end of lifetime
    soc_min (float): lower soc level of bss
    bat2ac_efficiency (list[float]): exponential discharging efficiency values
    ac2bat_efficiency (list[float]): exponential charging efficiency values

    charge_efficiency (float): charging efficiency
    discharge_efficiency (float): discharging efficiency
    soh (float): state of health
    bat_cycles (float): number of cycles accumulated by the battery
    e_bat (float): current energy amount stored
    e_bat_usable (float): usable energy amount of bss considering ageing
    """

    # static
    e_bat_rated: int = field(default_factory=int)
    p_rated_discharge_max: int = field(default_factory=int)
    p_rated_charge_max: int = field(default_factory=int)
    loss_rate: float = field(default_factory=float)
    self_discharge_step: float = field(default_factory=float)
    dod_max: float = field(default_factory=float)
    status_aging: bool = field(default_factory=bool)
    status_curve: bool = field(default_factory=bool)
    soh_cycles_max: float = field(default_factory=float)
    bat_cycles_max: int = field(default_factory=int)
    soc_min: float = field(default_factory=float)
    bat2ac_efficiency: list[float] | None = field(default_factory=list)
    ac2bat_efficiency: list[float] | None = field(default_factory=list)

    # dynamic
    charge_efficiency: float = field(default=None)
    discharge_efficiency: float = field(default=None)
    soh: float = field(default=None)
    bat_cycles: float = field(default=None)
    e_bat: float = field(default=None)
    e_bat_usable: float = field(default=None)


@dataclass
class EVData(BaseDeviceData):
    """Dataclass for eELib EV model.

    soc_min (float): minimum soc level of car
    e_max (float): rated energy level of car battery
    p_nom_charge_max (float): maximum charging power of car
    p_nom_discharge_max (float): maximum discharging power of car
    charge_efficiency (float): charge efficiency of car
    dcharge_efficiency (float): discharge efficiency of car

    e_bat (float): energy amount of the car
    appearance (bool): where car is available at charging point or not
    """

    # static
    soc_min: float = field(default_factory=float)
    e_max: float = field(default_factory=float)
    p_nom_discharge_max: float = field(default_factory=float)
    p_nom_charge_max: float = field(default_factory=float)
    dcharge_efficiency: float = field(default_factory=float)
    charge_efficiency: float = field(default_factory=float)

    # dynamic
    e_bat: float = field(default=None)
    appearance: bool = field(default=None)


@dataclass
class CSData(BaseDeviceData):
    """Dataclass for eElib charging station model.

    charge_efficiency (float): charge efficiency of charging station
    discharge_efficiency (float): discharge efficiency of charging station
    p_rated (float): rated power of charging station

    ev_data (dict): information about all the connected electric vehicles
    q (float): current reactive power
    """

    # static
    discharge_efficiency: float = field(default_factory=float)
    charge_efficiency: float = field(default_factory=float)
    p_rated: float = field(default_factory=float)

    # dynamic
    ev_data: dict[str:EVData] = field(default=dict)
    q: float = field(default=None)


@dataclass
class HPData(BaseDeviceData):
    """Dataclass for eElib heatpump model.

    p_rated (float): rated thermal power of heatpumps
    p_min_th_rel (float): relative minimum power (of rated power) for modulating heatpumps
    time_min (int): minimum on/off-time for heatpumps

    time_on (int): current on-time for heatpumps
    time_off (int): current off-time for heatpumps
    state (str): current state of heatpumps (on, off, must-on or must-off)
    cop (float): current coefficient of power for heatpumps
    p_th (float): current thermal power
    p_th_min (float): minimum thermal power limit
    p_th_min_on (float): minimum thermal power limit in on-state
    p_min_on (float): minimum electrical power limit in on-state
    p_th_max (float): maximum thermal power limit
    q (float): current reactive power
    """

    # static
    p_rated_th: float = field(default=None)
    p_min_th_rel: float = field(default=None)
    time_min: int = field(default=None)

    # dynamic
    time_on: int = field(default=None)
    time_off: int = field(default=None)
    state: str = field(default=None)
    cop: float = field(default=None)
    p_th: float = field(default=None)
    p_th_min: float = field(default=None)
    p_th_min_on: float = field(default=None)
    p_min_on: float = field(default=None)
    p_th_max: float = field(default=None)
    q: float = field(default=None)


@dataclass
class PVData(BaseDeviceData):
    """Dataclass for eElib pv(lib) model.

    p_rated (float): rated power of system

    q (float): current reactive power
    """

    # static
    p_rated: float = field(default=None)

    # dynamic
    q: float = field(default=None)
