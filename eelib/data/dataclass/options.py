"""Dataclasses for options used in eELib.

| Author: elenia@TUBS
| Copyright 2024 elenia
| This file is part of eELib, which is free software under the terms of the GNU GPL Version 3.
"""

from dataclasses import dataclass, field
from eelib.data.dataclass._base import BaseData


@dataclass
class OptimOptions(BaseData):
    """Dataclass for Options of an Energy Management System Optimization.

    consider_thermal (bool): whether to consider the thermal balance for the optimization
    thermal_energy_start (float): thermal energy demand before first schedule step
    thermal_energy_restriction (bool): restrict the thermal energy within this energy range [kWh_th]
    th_e_penalty (float): penalty for th. energy demand surpassing the restricting limits

    ev_direct_cha (bool): incent. dir. charging with max. power with min. desc. obj. func. value
    bss_direct_cha (bool): incent. dir. charging using a min. descending obj. function value
    dir_cha_desc_factor (float): descending obj. func. factor for charging power (bss, ev)

    ev_end_energy_penalty (float): penalty factor if ev not entirely filled at the last step
    bss_end_energy_incentive (bool): incent. stored bss energy at last step (avoid disch. into grid)

    round_int (int): number of integers to round the optimization results to
    """

    consider_thermal: bool = field(default=False)
    thermal_energy_start: float = field(default=0)
    thermal_energy_restriction: float = field(default=1500)
    th_e_penalty: float = field(default=1e4)

    ev_direct_cha: bool = field(default=False)
    bss_direct_cha: bool = field(default=False)
    dir_cha_desc_factor: float = field(default=1 / 1e6)

    ev_end_energy_penalty: float = field(default=0.0)
    bss_end_energy_incentive: bool = field(default=False)

    round_int: int = field(default=2)
