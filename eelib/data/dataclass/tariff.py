"""Dataclasses for tariffs and market data used in eELib.

| Author: elenia@TUBS
| Copyright 2024 elenia
| This file is part of eELib, which is free software under the terms of the GNU GPL Version 3.
"""

from dataclasses import dataclass, field
from typing import Optional
from eelib.data.dataclass._base import BaseData


@dataclass
class MarketData(BaseData):
    """Dataclass for Market Data.

    price (float): market auction price in EUR/MWh
    volume (float): market auction volume in MWh
    """

    price: float = field(default=100)
    volume: Optional[float] = field(default=1000)


@dataclass
class MarketDataContinuous(BaseData):
    """Dataclass for Market Data.

    price_weighted_avg (float): weighted average price
    price_low (float): lowest market price
    price_high (float): highest market price
    price_last (float): last market price
    """

    price_weighted_avg: float = field(default=100)
    price_low: float = field(default=10)
    price_high: float = field(default=400)
    price_last: float = field(default=100)


@dataclass
class Tariff(BaseData):
    """Dataclass for Prosumer Tariff.

    elec_price (float): electricity consumption price info [EUR/kWh]. Defaults to 35ct/kWh
    feedin_tariff (float): electricity feed-in price info [EUR/kWh]. Defaults to 7ct/kWh
    capacity_fee_dem (float): Price for max. power demand at connection [EUR/kW]. Defaults to 0
    capacity_fee_gen (float): Price for max. power generation at connection [EUR/kW]. Defaults to 0
    capacity_fee_horizon_sec (int): Horizon for capacity fee [seconds]. Defaults to 86400 (1 day)
    """

    elec_price: float = field(default=0.35)
    feedin_tariff: float = field(default=0.07)
    capacity_fee_dem: float = field(default=0.0)
    capacity_fee_gen: float = field(default=0.0)
    capacity_fee_horizon_sec: int = field(default=3600 * 24)


@dataclass
class TariffSignal(BaseData):
    """Dataclass for Tariff that is sent as a Signal to a Household.

    bool_is_list (bool): Whether signal is for multi-step tariff. Defaults to False
    elec_price (float, list): electricity consumption price info [EUR/kWh]. Defaults to 0.35
    feedin_tariff (float, list): electricity feed-in price info [EUR/kWh]. Defaults to 0.07
    steps (list): timesteps for the corresponding list of prices. Defaults to empty list
    capacity_fee_dem (float): Price for max. power demand at connection [EUR/kW]. Defaults to 0
    capacity_fee_gen (float): Price for max. power generation at connection [EUR/kW]. Defaults to 0
    capacity_fee_horizon_sec (int): Horizon for capacity fee [seconds]. Defaults to 86400 (1 day)
    """

    bool_is_list: bool = field(default=False)
    elec_price: float | list = field(default=0.35)
    feedin_tariff: float | list = field(default=0.07)
    steps: list = field(default_factory=list)
    capacity_fee_dem: float = field(default=0.0)
    capacity_fee_gen: float = field(default=0.0)
    capacity_fee_horizon_sec: int = field(default=3600 * 24)


@dataclass
class GridTariff(BaseData):
    """Dataclass for Grid Usage Fees.

    grid_tariff_model (str): which control model is selected for households. Defaults to "flat-rate"
    energy_price (float): Energy-dependent price for electr. consump. [EUR/kWh]. Defaults to 8ct/kWh
    capacity_fee_dem (float): Price for max. power demand at connection [EUR/kW]. Defaults to 0
    capacity_fee_gen (float): Price for max. power generation at connection [EUR/kW]. Defaults to 0
    capacity_fee_horizon_sec (int): Horizon for capacity fee [seconds]. Defaults to 86400 (1 day)
    """

    grid_tariff_model: str = field(default="flat-rate")
    energy_price: float = field(default=0.08)
    capacity_fee_dem: float = field(default=0.0)
    capacity_fee_gen: float = field(default=0.0)
    capacity_fee_horizon_sec: int = field(default=3600 * 24)


@dataclass
class GridTariffSignal(BaseData):
    """Dataclass for Signal regarding Grid Usage Fees.

    bool_is_list (bool): Whether signal is for multi-step tariff. Defaults to False
    energy_price (float, list): Energy-dependent price for el. cons. [EUR/kWh]. Defaults to 8ct/kWh
    steps (list): timesteps for the corresponding list of prices. Defaults to empty list
    capacity_fee_dem (float): Price for max. power demand at connection [EUR/kW]. Defaults to 0
    capacity_fee_gen (float): Price for max. power generation at connection [EUR/kW]. Defaults to 0
    capacity_fee_horizon_sec (int): Horizon for capacity fee [seconds]. Defaults to 86400 (1 day)
    """

    bool_is_list: bool = field(default=False)
    energy_price: float | list = field(default=0.08)
    steps: list = field(default_factory=list)
    capacity_fee_dem: float = field(default=0.0)
    capacity_fee_gen: float = field(default=0.0)
    capacity_fee_horizon_sec: int = field(default=3600 * 24)
