"""
eElib Influx-reader models for reading-in from Influx database to imitate different model types.
Specializations for certain model types like Household series.

| Author: elenia@TUBS
| Copyright 2024 elenia
| This file is part of eELib, which is free software under the terms of the GNU GPL Version 3.
"""

import logging
import math
from dotenv import load_dotenv
from urllib3.exceptions import NameResolutionError, NewConnectionError
import warnings
import pandas as pd
import os
import arrow
from influxdb_client import InfluxDBClient
from influxdb_client.rest import ApiException

_logger = logging.getLogger(__name__)


class GenericInflux:
    """Class for reading data from an influxdb."""

    # Valid values and types for each parameter
    _VALID_PARAMETERS = {
        "measurement_name": {"types": [str], "values": None},
        "tags": {"types": [dict], "values": None},
        "fields": {"types": [list], "values": None},
        "start_time": {"types": [str], "values": None},
        "end_time": {"types": [str], "values": None},
        "step_size": {"types": [int], "values": None},
        "influx_url": {"types": [str], "values": None},
        "influx_token": {"types": [str], "values": None},
        "influx_org": {"types": [str], "values": None},
        "influx_bucket": {"types": [str], "values": None},
    }

    @classmethod
    def get_valid_parameters(cls):
        """Returns dictionary containing valid parameter types and values.

        Returns:
            dict: valid parameters for this model
        """
        return cls._VALID_PARAMETERS

    def __init__(
        self,
        eid: str,
        step_size: int,
        measurement_name: str,
        tags: dict[str, str],
        fields: list[str],
        start_time: str,
        end_time: str,
        influx_url: str | None = None,
        influx_token: str | None = None,
        influx_org: str | None = None,
        influx_bucket: str | None = None,
    ):
        """Initializes the influx_reader model.

        Args:
            eid (str): Specifically assigned name for this entity.
            step_size (int): resolution of the data in seconds
            measurement_name (str): measurement name in influxdb query
            tags (dict[str, str]): tag key in influxdb query
            fields (list[str]): field name in influxdb query
            start_time (str): start of target time period in influxdb query
            end_time (str): end of target time period in influxdb query
            influx_url (str, optional): url for influxdb api. Defaults to None
            influx_token (str, optional): token for influxdb api. Defaults to None
            influx_org (str, optional): org name for influxdb api. Defaults to None
            influx_bucket (str, optional): bucket name for influxdb api. Defaults to None

        Raises:
            ValueError: If no .env file was found
        """
        if any(attr is None for attr in [influx_bucket, influx_org, influx_token, influx_url]):
            dotenv_path = None
            root_folder = os.getcwd()
            for root, dirs, files in os.walk(root_folder):  # pragma: no cover
                # Check if .env file is present in the current directory
                if ".env" in files:
                    dotenv_path = os.path.join(root, ".env")
                    break
            else:  # pragma: no cover
                raise ValueError(
                    "No specifications to influx DB given and .env file not found in "
                    "any subfolder under the root folder"
                )
            # load influx information from .env file
            load_dotenv(dotenv_path)  # pragma: no cover

        # access the influxdb. If data not provided in arguments use .env file.
        self.influx_url = influx_url if influx_url else os.getenv("INFLUX_URL")
        self.influx_token = influx_token if influx_token else os.getenv("INFLUX_TOKEN")
        self.influx_org = influx_org if influx_org else os.getenv("INFLUX_ORG")
        self.influx_bucket = influx_bucket if influx_bucket else os.getenv("INFLUX_BUCKET")
        self.client = InfluxDBClient(
            url=self.influx_url,
            token=self.influx_token,
            org=self.influx_org,
            debug=False,
        )

        self.eid = eid
        self.measurement_name = measurement_name
        self.tags = tags
        self.fields = fields
        self.start_time = start_time
        self.end_time = end_time
        self.step_size = step_size
        self.attrs = fields  # contains all columns as attributes
        self.timestep = None  # current sim timestep
        self.index = 0

        # create dynamic attributes per 'dynamic' dict and set intitial values
        for attr in self.fields:
            setattr(self, attr, None)

        # read first 'buffer_size'-sized data slice
        self.data = self._read_db(
            self.measurement_name,
            self.tags,
            self.fields,
            self.start_time,
            self.end_time,
            self.step_size,
        )

    def __del__(self):
        """Close the client after the reader's job is done."""
        if self.client:
            self.client.close()

    @staticmethod
    def generate_tag_filter(tags: dict[str, str] = None) -> str:
        """Generates tag filters for Influx query.

        Args:
            tags (dict[str, str], optional): Tags to generate filter.

        Returns:
            str: Tag selection part of Influx query.
        """
        if not tags:
            return ""
        tag_filters = []
        for key, value in tags.items():
            tag_filters.append(f'r["{key}"] == "{value}"')
        return " and ".join(tag_filters)

    def generate_influx_query(
        self,
        measurement_name: str,
        start_time: str,
        end_time: str,
        resolution: int,
        tags: dict[str, str] = None,
        fields: list[str] = None,
    ) -> str:
        """Generates influx query based on given parameters.

        Args:
            measurement_name (str): Name of the series.
            start_time (str): Start time of the series slice.
            end_time (str): End time of the series slice.
            resolution (int): Time resolution used for aggregation.
            tags (dict[str, str], optional): Tags wirth keys and values. Defaults to None.
            fields (list[str], optional): Fields of the series. Defaults to None.

        Returns:
            str: Influx query
        """
        tag_filter = GenericInflux.generate_tag_filter(tags) if tags else ""
        field_select = f"keep(columns: {fields})" if fields else ""

        query = f"""
            from(bucket: "{self.influx_bucket}")
                |> range(start: {start_time}, stop: {end_time})
                |> filter(fn: (r) => r["_measurement"] == "{measurement_name}"
                {"" if not tag_filter else f"and {tag_filter}"})
                |> aggregateWindow(every: {f"{resolution}s"}, fn: mean)
                {f"|> {field_select}" if field_select else ""} // Add field selection if provided
        """
        return query

    def _read_db(
        self,
        measurement_name: str,
        tags: dict[str, str],
        fields: list[str],
        start_time: str,
        end_time: str,
        resolution: int,
    ):
        """Filters data in the database based on given parameters and
        returns the result as a dataframe.

        Args:
            measurement_name (str): Name to filter _measurement
            tags (dict[str, str]): Tags to filter, e.g. "type": "load"
            fields (list[str]): Fields to select
            start_time (str): Range start time
            end_time (str): Range end time
            resolution (int): time resolution to aggregate

        Returns:
            pd.DataFrame: query result with DateTime index and one column for each field.
        """

        # change the formatting of datetime data to match the formatting appropriate to influxdb
        start_time = (
            arrow.get(start_time, "YYYY-MM-DD HH:mm:ss").format("YYYY-MM-DDTHH:mm:ss") + "Z"
        )
        end_time = arrow.get(end_time, "YYYY-MM-DD HH:mm:ss").format("YYYY-MM-DDTHH:mm:ss") + "Z"

        # construct the Flux query based on parameters
        query = self.generate_influx_query(
            measurement_name=measurement_name,
            start_time=start_time,
            end_time=end_time,
            resolution=resolution,
            tags=tags,
        )
        query_api = self.client.query_api()
        try:  # pragma: no cover
            # query the database
            result = query_api.query(query=query, org=self.influx_org)
        except NameResolutionError and NewConnectionError as e:  # pragma: no cover
            _logger.critical(f"Can not connect to Database: {self.influx_url}\n{e}")
            exit(1)
        except ApiException as e:  # pragma: no cover
            _logger.critical(
                f"Can not connect to Database, probably due to wrong access permissions:\n{e}"
            )
            _logger.critical(f"INFLUX_TOKEN: {self.influx_token}")
            _logger.critical(f"INFLUX_ORG: {self.influx_org}")
            _logger.critical(f"INFLUX_BUCKET: {self.influx_bucket}")
            exit(1)
        dfs = []  # list of read tables
        # read every record that corresponds to query and store all in one df
        if not result:
            _logger.critical(
                f"Result is empty. Check parameters. ({measurement_name}, {tags}, {fields},"
                f" {start_time}, {end_time}, {resolution})"
            )
            exit(1)
        for table in result:
            records = []
            for record in table.records:
                time = record.get_time()
                values = record.values
                values["time"] = time
                records.append(values)
            df = pd.DataFrame(records)
            dfs.append(df)
        df = pd.concat(dfs, ignore_index=True)
        # only keep necessary information
        simplified_df = df[["time", "_measurement", "_field", "_value"]]
        # use the values in the _field column as the new columns
        pivoted_df = simplified_df.pivot(index="time", columns="_field", values="_value")
        pivoted_df.columns = [col.strip() for col in pivoted_df.columns]
        # filter the result based on our desired field/s
        filtered_df = pivoted_df[fields]
        return pd.DataFrame(filtered_df)

    def step(self, timestep):
        """Performs simulation step of eELib influx_reader model.

        Args:
            timestep (int): Current simulation time

        Raises:
            IndexError: when end of data is reached before simulation end
        """
        # save current simulation time
        self.timestep = timestep

        # check if the dataset has been exhausted
        if self.index >= self.data.shape[0]:
            raise IndexError("End of data reached.")

        current_row = self.data.iloc[self.index]
        # assign the values to a variable whose name corresponds to its column name in lower case
        for attr in self.fields:
            setattr(self, attr, current_row[attr])
        self.index += 1


class RatedInflux(GenericInflux):
    """Influx-Data-Reader for profiles. Inherits from class GenericInflux.
    Adds support for scaling to p_rated.
    """

    @classmethod
    def get_valid_parameters(cls):
        """Returns dictionary containing valid parameter types and values.

        Returns:
            dict: valid parameters for this model
        """

        # use GenericInflux's parameter list and modify them for this class
        result = GenericInflux.get_valid_parameters().copy()
        result.update({"p_rated": {"types": [int], "values": (1, math.inf)}})
        result.update({"p_rated_profile": {"types": [int], "values": (1, math.inf)}})
        return result

    def __init__(
        self,
        eid: str,
        step_size: int,
        measurement_name: str,
        tags: dict[str, str],
        fields: list[str],
        start_time: str,
        end_time: str,
        p_rated: int = 4500,
        p_rated_profile: int = 4500,
        influx_url: str | None = None,
        influx_token: str | None = None,
        influx_org: str | None = None,
        influx_bucket: str | None = None,
    ):
        """Initializes the influx_reader model.

        Args:
            eid (str): Specifically assigned name for this entity.
            step_size (int): resolution of the data in seconds
            measurement_name (str): measurement name in influxdb query
            tags (dict[str, str]): tag key in influxdb query
            fields (list[str]): field name in influxdb query
            start_time (str): start of target time period in influxdb query
            end_time (str): end of target time period in influxdb query
            p_rated (int): active power of the device [W]
            p_rated_profile (int): rated active power of the profile [W]
            influx_url (str, optional): url for influxdb api. Defaults to None
            influx_token (str, optional): token for influxdb api. Defaults to None
            influx_org (str, optional): org name for influxdb api. Defaults to None
            influx_bucket (str, optional): bucket name for influxdb api. Defaults to None
        """
        super().__init__(
            eid=eid,
            step_size=step_size,
            measurement_name=measurement_name,
            tags=tags,
            fields=fields,
            start_time=start_time,
            end_time=end_time,
            influx_url=influx_url,
            influx_token=influx_token,
            influx_org=influx_org,
            influx_bucket=influx_bucket,
        )

        # Set attributes of init_vals to static properties
        self.p_rated = p_rated
        self.p_rated_profile = p_rated_profile
        self.scaling_factor = round(self.p_rated / self.p_rated_profile, 6)

        self._apply_p_rated()

    def _apply_p_rated(self) -> None:
        """Apply scaling factor (``p_rated``/``p_rated_profile``)."""
        for attr in self.attrs:
            self.data[attr] = self.data[attr] * self.scaling_factor


class PowerInflux(RatedInflux):
    """Influx-Data-Reader for power profiles. Inherits from class RatedInflux.

    Adds support for calculating reactive power.
    Assumptions:
    - first field is p
    - second field is q, if more than one field is given
    """

    @classmethod
    def get_valid_parameters(cls):
        """Returns dictionary containing valid parameter types and values.

        Returns:
            dict: valid parameters for this model
        """

        # use RatedInflux's parameter list and modify them for this class
        result = RatedInflux.get_valid_parameters().copy()
        result.update({"cos_phi": {"types": [float], "values": (0, 1)}})
        return result

    def __init__(
        self,
        eid: str,
        step_size: int,
        measurement_name: str,
        tags: dict[str, str],
        fields: list[str],
        start_time: str,
        end_time: str,
        p_rated: int = 4500,
        p_rated_profile: int = 4500,
        cos_phi: float = 1.0,
        influx_url: str | None = None,
        influx_token: str | None = None,
        influx_org: str | None = None,
        influx_bucket: str | None = None,
    ):
        """Initializes the influx_reader model.

        Args:
            eid (str): Specifically assigned name for this entity.
            step_size (int): resolution of the data in seconds
            measurement_name (str): measurement name in influxdb query
            tags (dict[str, str]): tag key in influxdb query
            fields (list[str]): field name in influxdb query
            start_time (str): start of target time period in influxdb query
            end_time (str): end of target time period in influxdb query
            p_rated (int): active power of the device [W]
            p_rated_profile (int): rated active power of the profile [W]
            cos_phi (float): power factor for read-in values. Defaults to 1.
            influx_url (str, optional): url for influxdb api. Defaults to None
            influx_token (str, optional): token for influxdb api. Defaults to None
            influx_org (str, optional): org name for influxdb api. Defaults to None
            influx_bucket (str, optional): bucket name for influxdb api. Defaults to None
        """
        super().__init__(
            eid=eid,
            step_size=step_size,
            measurement_name=measurement_name,
            tags=tags,
            fields=fields,
            start_time=start_time,
            end_time=end_time,
            p_rated=p_rated,
            p_rated_profile=p_rated_profile,
            influx_url=influx_url,
            influx_token=influx_token,
            influx_org=influx_org,
            influx_bucket=influx_bucket,
        )

        # Init dynamic attributes
        self.p = None
        self.q = None

        # Set attributes of init_vals to static properties
        self.cos_phi = cos_phi

        # check for reasonable cos_phi values
        if cos_phi < 0.9:
            warnings.warn(
                f"WARNING: cos_phi for {eid} selected to be {cos_phi} < 0.9 - seems unreasonable!"
            )

        self._set_reactive_power()

    def _set_reactive_power(self) -> None:
        """Calculate reactive power if not given as field of that series in influx
        and if cos_phi is set.
        """
        if "p" in self.data and self.cos_phi is not None and self.cos_phi != 0:
            self.data["q"] = -self.data["p"] * math.sqrt((1 / self.cos_phi**2) - 1)
        else:
            self.data["q"] = 0
        if "q" not in self.attrs:
            self.attrs.append("q")

    # perform simulation step of household load
    def step(self, timestep):
        """Performs simulation step of eELib load model, which is returning the read
        active/reactive power value of the series read from Influx database.

        Args:
            timestep (int): Current simulation time in seconds.
        """

        # call the parent step method
        super().step(timestep)

        self.p = getattr(self, self.attrs[0])
        self.q = getattr(self, self.attrs[1])


class HouseholdInflux(PowerInflux):
    """Influx-Data-Reader for household load profiles. Inherits from class PowerInflux.
    Data in Influx Database must have a tag with key value pair {'type': 'load'}.
    """

    @classmethod
    def get_valid_parameters(cls):
        """Returns dictionary containing valid parameter types and values.

        Returns:
            dict: valid parameters for this model
        """
        result = PowerInflux.get_valid_parameters().copy()
        result.pop("tags")
        return result

    def __init__(
        self,
        eid: str,
        step_size: int,
        measurement_name: str,
        fields: list[str],
        start_time: str,
        end_time: str,
        p_rated: int = 4500,
        p_rated_profile: int = 4500,
        cos_phi: float = 1,
        influx_url: str | None = None,
        influx_token: str | None = None,
        influx_org: str | None = None,
        influx_bucket: str | None = None,
    ):
        """Initializes the influx_reader model.

        Args:
            eid (str): Specifically assigned name for this entity.
            step_size (int): resolution of the data in seconds
            measurement_name (str): measurement name in influxdb query
            fields (list[str]): field name in influxdb query
            start_time (str): start of target time period in influxdb query
            end_time (str): end of target time period in influxdb query
            p_rated (int): active power of the device [W]
            p_rated_profile (int): rated active power of the profile [W]
            cos_phi (float): power factor for read-in values. Defaults to 1.
            influx_url (str, optional): url for influxdb api. Defaults to None
            influx_token (str, optional): token for influxdb api. Defaults to None
            influx_org (str, optional): org name for influxdb api. Defaults to None
            influx_bucket (str, optional): bucket name for influxdb api. Defaults to None
        """
        super().__init__(
            eid=eid,
            step_size=step_size,
            measurement_name=measurement_name,
            tags={"type": "load"},
            fields=fields,
            start_time=start_time,
            end_time=end_time,
            p_rated=p_rated,
            p_rated_profile=p_rated_profile,
            cos_phi=cos_phi,
            influx_url=influx_url,
            influx_token=influx_token,
            influx_org=influx_org,
            influx_bucket=influx_bucket,
        )


class PvInflux(PowerInflux):
    """Influx-Data-Reader for pv load profiles. Inherits from class PowerInflux.
    Data in Influx Database must have a tag with key value pair {'type': 'pv'}.
    """

    @classmethod
    def get_valid_parameters(cls):
        """Returns dictionary containing valid parameter types and values.

        Returns:
            dict: valid parameters for this model
        """
        result = PowerInflux.get_valid_parameters().copy()
        result.pop("tags")
        return result

    def __init__(
        self,
        eid: str,
        step_size: int,
        measurement_name: str,
        fields: list[str],
        start_time: str,
        end_time: str,
        p_rated: int = 4500,
        p_rated_profile: int = 4500,
        cos_phi: float = 1,
        influx_url: str | None = None,
        influx_token: str | None = None,
        influx_org: str | None = None,
        influx_bucket: str | None = None,
    ):
        """Initializes the influx_reader model.

        Args:
            eid (str): Specifically assigned name for this entity.
            step_size (int): resolution of the data in seconds
            measurement_name (str): measurement name in influxdb query
            fields (list[str]): field name in influxdb query
            start_time (str): start of target time period in influxdb query
            end_time (str): end of target time period in influxdb query
            p_rated (int): active power of the device [W]
            p_rated_profile (int): rated active power of the profile [W]
            cos_phi (float): power factor for read-in values. Defaults to 1.
            influx_url (str, optional): url for influxdb api. Defaults to None
            influx_token (str, optional): token for influxdb api. Defaults to None
            influx_org (str, optional): org name for influxdb api. Defaults to None
            influx_bucket (str, optional): bucket name for influxdb api. Defaults to None
        """
        super().__init__(
            eid=eid,
            step_size=step_size,
            measurement_name=measurement_name,
            tags={"type": "pv"},
            fields=fields,
            start_time=start_time,
            end_time=end_time,
            p_rated=p_rated,
            p_rated_profile=p_rated_profile,
            cos_phi=cos_phi,
            influx_url=influx_url,
            influx_token=influx_token,
            influx_org=influx_org,
            influx_bucket=influx_bucket,
        )
