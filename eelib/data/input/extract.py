"""
Helper module to extract data from different sources.

| Author: elenia@TUBS
| Copyright 2024 elenia
| This file is part of eELib, which is free software under the terms of the GNU GPL Version 3.
"""

import numpy as np
import h5py
import pandas as pd
from typing import Optional, Sequence


def extract_htw(
    mat_file: str, idx: Optional[int | range | Sequence] = None
) -> dict[str, pd.DataFrame]:
    """Extract data from public htw Matlab files.

    Args:
        mat_file (str): String to path of HTW Matlab file.
        idx (Optional[int  |  range  |  Sequence], optional): Idx to extract, defaults to None.

    Returns:
        dict[str, pd.DataFrame]: Dict of DataFrames.
    """
    f = h5py.File(mat_file, "r")
    match idx:
        case int():
            indices = [idx]
        case range():
            indices = list(idx)
        case list() | tuple():
            indices = idx
        case _:
            indices = list(range(len(f["PL1"])))
    dt_index = pd.to_datetime(
        pd.DataFrame(
            np.stack(f["time_datevec_MEZ"]).T,
            columns=[
                "year",
                "month",
                "day",
                "hour",
                "minute",
                "second",
            ],
        )
    )
    df_store = {}
    for i in indices:
        p_data = np.stack([f["PL1"][i], f["PL2"][i], f["PL3"][i]])
        q_data = np.stack([f["QL1"][i], f["QL2"][i], f["QL3"][i]])
        data = np.concatenate([p_data, q_data], axis=0).T
        del p_data, q_data
        df = pd.DataFrame(
            data,
            columns=[
                "PL1",
                "PL2",
                "PL3",
                "QL1",
                "QL2",
                "QL3",
            ],
        )
        del data
        df["P_W"] = df.loc[:, ["PL1", "PL2", "PL3"]].sum(axis=1)
        df["Q_var"] = df.loc[:, ["QL1", "QL2", "QL3"]].sum(axis=1)
        df.set_index(dt_index, inplace=True)
        df_store[f"htw_{i}"] = df
    return df_store
