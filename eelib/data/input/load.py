"""
Helper module.
Influx Db Connector to load data to Influx Database.

| Author: elenia@TUBS
| Copyright 2024 elenia
| This file is part of eELib, which is free software under the terms of the GNU GPL Version 3.
"""

from dotenv import load_dotenv
from typing import Optional
import pandas as pd
import os
from influxdb_client import InfluxDBClient, Point, WritePrecision
from influxdb_client.client.write_api import WriteType, WriteOptions

load_dotenv()


class InfluxDbStorage:
    """Influx Db Connector class to write pandas dataframes."""

    def __init__(
        self,
        influx_url: Optional[str] = None,
        influx_token: Optional[str] = None,
        influx_org: Optional[str] = None,
        influx_bucket: Optional[str] = None,
    ) -> None:
        """Consturcor of Influx Db Connector.

        Args:
            influx_url (Optional[str], optional): Influx URL. Defaults to None.
            influx_token (Optional[str], optional): Token. Defaults to None.
            influx_org (Optional[str], optional): Organization. Defaults to None.
            influx_bucket (Optional[str], optional): Bucket. Defaults to None.
        """
        self.influx_url = influx_url if influx_url else os.getenv("INFLUX_URL")
        self.influx_token = influx_token if influx_token else os.getenv("INFLUX_TOKEN")
        self.influx_org = influx_org if influx_org else os.getenv("INFLUX_ORG")
        self.influx_bucket = influx_bucket if influx_bucket else os.getenv("INFLUX_BUCKET")
        self.client = InfluxDBClient(
            url=self.influx_url,
            token=self.influx_token,
            org=self.influx_org,
            debug=False,
        )

    def __del__(self):
        """Delete client connection on delete."""
        self.client.close()

    def write_pandas_dataframe(self, df: pd.DataFrame, name: str, tags: dict[str, str]) -> None:
        """Write pandas dataframe to influx database.

        Args:
            df (pd.DataFrame): Df to write to Influx.
            name (str): Name of the series.
            tags (dict[str, str]): Tags to add when writing to Influx.
        """
        points = []
        for idx, row in df.iterrows():
            point = Point(name).time(idx, WritePrecision.S)
            for col in df.columns:
                point.field(col, row[col])
            for key, val in tags.items():
                point.tag(key, val)
            points.append(point)

        write_api = self.client.write_api(
            write_options=WriteOptions(
                write_type=WriteType.batching, batch_size=50_000, flush_interval=10_000
            )
        )
        write_api.write(self.influx_bucket, self.influx_org, points)


if __name__ == "__main__":
    from eelib.data.input.extract import extract_htw

    data_path = "."
    file_name = "MAT_74_Loadprofiles_1s_W_var.mat"
    influx = InfluxDbStorage()
    for i in range(74):
        dfs = extract_htw(os.path.join(data_path, file_name), i)
        for name, df in dfs.items():
            print(name)
            print(df.head())
            print("Beginning database write...")
            influx.write_pandas_dataframe(df, name, tags={"type": "load"})
            print("Finished!")
