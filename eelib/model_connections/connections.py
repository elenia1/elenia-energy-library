"""
Module to load connection config.

| Author: elenia@TUBS
| Copyright 2024 elenia
| This file is part of eELib, which is free software under the terms of the GNU GPL Version 3.
"""

import os
import json


def get_default_connections() -> dict:
    """Load default connection config from model_connect_config.json file.

    Returns:
        dict: Model connect config as Python dict.
    """
    basedir = os.path.dirname(__file__)
    default_connection_config = os.path.join(basedir, "model_connect_config.json")
    with open(default_connection_config) as f:
        model_connect_config = json.load(f)
        for out_model, inp_model_dict in model_connect_config.items():
            for inp_model, connections_list in inp_model_dict.items():
                for i_val, val in enumerate(connections_list):
                    if isinstance(val, list):
                        model_connect_config[out_model][inp_model][i_val] = tuple(
                            model_connect_config[out_model][inp_model][i_val]
                        )
    return model_connect_config


def get_connection_directions_config() -> dict:
    """Load set directions for connections from connect_directions_config.json file.

    Returns:
        dict: Connect direction config as Python dict, with fields "ALWAYS_WEAK", "ALWAYS_STRONG",
            and "STRONG_DIR".
    """
    basedir = os.path.dirname(__file__)
    connection_dir_config = os.path.join(basedir, "connect_directions_config.json")
    with open(connection_dir_config) as f:
        connect_dir_config = json.load(f)
    return connect_dir_config


if __name__ == "__main__":
    conf = get_default_connections()
    print("Connection Config: ")
    print(conf)

    conf = get_connection_directions_config()
    print("Connection Direction Config: ")
    print(conf)
