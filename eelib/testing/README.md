# Testing with eELib

These folder contain unittests for the models implemented in the eELib.
For running all the tests, after installing all required packages, simply run the following:

```Powershell
$DIR_TO_PROJECT$> python -m unittest discover -s eelib/testing/unit
```

If you want to additionally run integration tests, you can run the test scenarios like this:

```Powershell
$DIR_TO_PROJECT$> python examples/test_scenario_building.py
```
