"""
Unittest for bss helper of eELib

| Author: elenia@TUBS
| Copyright 2024 elenia
| This file is part of eELib, which is free software under the terms of the GNU GPL Version 3.
"""

import unittest
import time
import eelib.core.control.hems.hems_helper.hems_bss_helper as bss_help
from eelib.data import BSSData


class TestEMS(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        pass

    @classmethod
    def tearDownClass(cls):
        pass

    def setUp(self):
        print(f"[START] {self.id()}")
        self.start = time.time()

    def tearDown(self):
        t = time.time() - self.start
        print(f"[END] {self.id()}: {t:.4f}")

    def test_bss_calc_balance(self):
        p_min = -2000
        p_max = 2000

        # for p_balance < 0 (negative balance): positive p_set value, i.e., bss charging
        p_balance = -1000
        p_set_bss = bss_help.bss_calc_balance(p_balance, p_max, p_min)
        self.assertAlmostEqual(p_set_bss, -p_balance)

        # for p_balance > 0 (positive balance): negative p_set value, i.e., bss discharging
        p_balance = 1000
        p_set_bss = bss_help.bss_calc_balance(p_balance, p_max, p_min)
        self.assertAlmostEqual(p_set_bss, -p_balance)

        # for p_balance = 0, i.e., nothing to balance
        p_balance = 0
        p_set_bss = bss_help.bss_calc_balance(p_balance=p_balance, p_max=p_max, p_min=p_min)
        self.assertAlmostEqual(p_set_bss, p_balance)

    def test_bss_calc_e_bat(self):
        step_size = 900
        loss_rate = 0.02
        self_discharge_step = 0.02 * step_size / (60 * 60 * 24 * 30)
        eff_cha = 0.95
        eff_discha = 0.95
        test_bss_data = BSSData(
            e_bat_rated=8000,
            p_rated_discharge_max=8000,
            p_rated_charge_max=8000,
            loss_rate=loss_rate,
            self_discharge_step=self_discharge_step,
            dod_max=0.95,
            status_aging=False,
            soh_cycles_max=0.8,
            bat_cycles_max=5000,
            soc_min=0.05,
            charge_efficiency=eff_cha,
            discharge_efficiency=eff_discha,
            bat2ac_efficiency=None,
            ac2bat_efficiency=None,
            e_bat_usable=8000,
        )

        # discharging
        test_bss_data.e_bat = 4000.0
        test_bss_data.p = -8000
        self.assertAlmostEqual(
            bss_help.bss_calc_e_bat(step_size, test_bss_data),
            test_bss_data.e_bat * (1 - self_discharge_step)
            - 8000 / eff_discha * (step_size / 3600),
        )

        # charging
        test_bss_data.e_bat = 4000.0
        test_bss_data.p = 8000
        self.assertAlmostEqual(
            bss_help.bss_calc_e_bat(step_size, test_bss_data),
            test_bss_data.e_bat * (1 - self_discharge_step) + 8000 * eff_cha * (step_size / 3600),
        )

    def test_bss_calc_p_limits(self):
        step_size = 900
        test_bss_data = BSSData(
            e_bat_rated=8000,
            p_rated_discharge_max=8000,
            p_rated_charge_max=8000,
            loss_rate=0.02,
            self_discharge_step=0.02 * step_size / (60 * 60 * 24 * 30),
            dod_max=0.95,
            status_aging=False,
            soh_cycles_max=0.8,
            bat_cycles_max=5000,
            soc_min=0.05,
            charge_efficiency=0.95,
            discharge_efficiency=0.95,
            bat2ac_efficiency=None,
            ac2bat_efficiency=None,
            e_bat_usable=8000,
            e_bat=4000,
        )

        p_max_bss_forecast, p_min_bss_forecast = bss_help.bss_calc_p_limits(
            step_size, test_bss_data
        )
        self.assertAlmostEqual(
            p_max_bss_forecast,
            test_bss_data.p_rated_charge_max,
        )
        self.assertAlmostEqual(
            p_min_bss_forecast,
            test_bss_data.p_rated_discharge_max,
        )

    def test_bss_set_energy_within_limit(self):
        step_size = 900
        test_bss_data = BSSData(
            e_bat_rated=8000,
            p_rated_discharge_max=8000,
            p_rated_charge_max=8000,
            loss_rate=0.02,
            self_discharge_step=0.02 * step_size / (60 * 60 * 24 * 30),
            dod_max=0.95,
            status_aging=False,
            soh_cycles_max=0.8,
            bat_cycles_max=5000,
            soc_min=0.05,
            charge_efficiency=0.95,
            discharge_efficiency=0.95,
            bat2ac_efficiency=None,
            ac2bat_efficiency=None,
            e_bat_usable=8000,
        )

        # OVERCHARGE
        test_bss_data.e_bat = 8050
        bss_in_limit = bss_help.bss_set_energy_within_limit(
            test_bss_data,
        )
        self.assertAlmostEqual(bss_in_limit, test_bss_data.e_bat_usable)

        # UNDERCHARGE
        test_bss_data.e_bat = (test_bss_data.e_bat_usable * test_bss_data.soc_min) - 50
        bss_in_limit = bss_help.bss_set_energy_within_limit(
            test_bss_data,
        )
        self.assertAlmostEqual(
            bss_in_limit,
            test_bss_data.e_bat_usable * test_bss_data.soc_min,
        )

    def test_bss_strategy_reduce_curtailment(self):
        p_max_bss = 1000
        p_min_bss = -1000
        p_balance = -1000
        bss_charging_limit = 0.8

        # negative balance: surplus of energy should be charged into bss but be reduced
        # by curtailment
        p_curtailment = bss_help.bss_strategy_reduce_curtailment(
            p_balance=p_balance,
            p_max=p_max_bss,
            p_min=p_min_bss,
            bss_charging_limit=bss_charging_limit,
        )
        self.assertEqual(p_curtailment, bss_charging_limit * p_max_bss)

        # positive balance: no curtailment
        p_balance = 1000
        p_curtailment = bss_help.bss_strategy_reduce_curtailment(
            p_balance=p_balance,
            p_max=p_max_bss,
            p_min=p_min_bss,
            bss_charging_limit=bss_charging_limit,
        )
        self.assertEqual(p_curtailment, p_min_bss)

        # already balanced power
        p_balance = 0
        p_curtailment = bss_help.bss_strategy_reduce_curtailment(
            p_balance=p_balance,
            p_max=p_max_bss,
            p_min=p_min_bss,
            bss_charging_limit=bss_charging_limit,
        )
        self.assertEqual(p_curtailment, 0)


if __name__ == "__main__":
    suite = unittest.TestLoader().loadTestsFromTestCase(TestEMS)
    unittest.TextTestRunner(verbosity=0).run(suite)
