"""
Unittest for charging station helper of eELib

| Author: elenia@TUBS
| Copyright 2024 elenia
| This file is part of eELib, which is free software under the terms of the GNU GPL Version 3.
"""

import unittest
import time
import eelib.core.control.hems.hems_helper.hems_cs_helper as cs_help
from eelib.data import CSData, EVData

# create ev dataclasses
test_ev_0 = EVData(
    soc_min=0.0,
    e_max=1000,
    p_nom_discharge_max=11000,
    p_nom_charge_max=11000,
    dcharge_efficiency=1.0,
    charge_efficiency=1.0,
    e_bat=0,
    appearance=True,
)
test_ev_1 = EVData(
    soc_min=0.0,
    e_max=1000,
    p_nom_discharge_max=11000,
    p_nom_charge_max=11000,
    dcharge_efficiency=1.0,
    charge_efficiency=1.0,
    e_bat=0,
    appearance=True,
)

# create cs dataclass
test_cs_0 = CSData(
    discharge_efficiency=0.99,
    charge_efficiency=0.99,
    p_rated=11000,
    p_min=0,
    p_max=1000,
    ev_data={"EV_0": test_ev_0, "EV_1": test_ev_1},
)


class TestEMS(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        pass

    @classmethod
    def tearDownClass(cls):
        pass

    def setUp(self):
        print(f"[START] {self.id()}")
        self.start = time.time()

    def tearDown(self):
        t = time.time() - self.start
        print(f"[END] {self.id()}: {t:.4f}")

    def test_cs_balanced_charging(self):
        pass

    def test_cs_night_charging(self):
        pass

    def test_cs_solar_charging(self):
        pass

    def test_cs_calc_power_limits(self):
        t = 0
        forecast = {
            "EV_0": {"appearance": [True for _ in range(0, 24)], "consumption": []},
            "EV_1": {"appearance": [True for _ in range(0, 24)], "consumption": []},
        }

        # EV and CS properties at the beginning of forecast period
        test_cs_0.p_rated = 11000
        test_cs_0.ev_data["EV_0"].p_max = 0.0
        test_cs_0.ev_data["EV_1"].p_max = 11000.0
        test_cs_0.ev_data["EV_0"].p_min = -11000
        test_cs_0.ev_data["EV_1"].p_min = -0
        p_max, p_min = cs_help.cs_calc_power_limits(forecast, t, test_cs_0)
        self.assertEqual(p_min, -test_cs_0.p_rated * test_cs_0.discharge_efficiency)
        self.assertEqual(p_max, test_cs_0.p_rated)

        # for two evs to be charged but exceeding the p_rated of cs
        test_cs_0.p_rated = 11000
        test_cs_0.ev_data["EV_0"].p_max = 1000.0
        test_cs_0.ev_data["EV_1"].p_max = 4000
        test_cs_0.ev_data["EV_0"].p_min = -22000
        test_cs_0.ev_data["EV_1"].p_min = -11000
        p_max, p_min = cs_help.cs_calc_power_limits(forecast, t, test_cs_0)
        self.assertEqual(p_min, -test_cs_0.p_rated)
        self.assertAlmostEqual(p_max, (4000 + 1000) / test_cs_0.charge_efficiency)

        # raise ValueError, if p_min and p_max of EV do not comply
        test_cs_0.ev_data["EV_0"].p_max = 1000
        test_cs_0.ev_data["EV_0"].p_min = 2000
        with self.assertRaises(ValueError) as message:
            p_max, p_min = cs_help.cs_calc_power_limits(forecast, t, test_cs_0)
        str_error = "FORECAST: Min. and max. power of ev EV_0 do not comply."
        self.assertEqual(str(message.exception), str_error)

        # raise ValueError, if discharging power of EV p_min positive
        test_cs_0.ev_data["EV_0"].p_max = 11000
        test_cs_0.ev_data["EV_0"].p_min = 5000
        with self.assertRaises(ValueError) as message:
            p_max, p_min = cs_help.cs_calc_power_limits(forecast, t, test_cs_0)
        str_error = "FORECAST: Maximum discharging power of EV, p_min, cannot be positive!"
        self.assertEqual(str(message.exception), str_error)

        # raise ValueError, if charging power of EV p_max negative
        test_cs_0.ev_data["EV_0"].p_max = -5000
        test_cs_0.ev_data["EV_0"].p_min = -11000
        with self.assertRaises(ValueError) as message:
            p_max, p_min = cs_help.cs_calc_power_limits(forecast, t, test_cs_0)
        str_error = "FORECAST: Maximum charging power of EV, p_max, cannot be negative!"
        self.assertEqual(str(message.exception), str_error)

    def test_cs_calc_current_efficiency(self):
        # no (dis)charging process, no power "losses"
        test_cs_0.p = 0  # only needed for efficiency calculation
        cs_efficiency = cs_help.cs_calc_current_efficiency(test_cs_0)
        self.assertEqual(cs_efficiency, 1)

        # charging process
        test_cs_0.p = 11000  # only needed for efficiency calculation
        cs_efficiency = cs_help.cs_calc_current_efficiency(test_cs_0)
        self.assertEqual(cs_efficiency, test_cs_0.charge_efficiency)

        # discharging process
        test_cs_0.p = -11000  # only needed for efficiency calculation
        cs_efficiency = cs_help.cs_calc_current_efficiency(test_cs_0)
        self.assertEqual(cs_efficiency, 1 / test_cs_0.charge_efficiency)

    def test_cs_distribute_charging_power(self):
        forecast = {
            "EV_0": {"appearance": [True for _ in range(0, 24)], "consumption": []},
            "EV_1": {"appearance": [True for _ in range(0, 24)], "consumption": []},
        }
        t = 0

        # with no EV to spread power
        test_cs_0.p = 0
        p_device = cs_help.cs_distribute_charging_power(
            forecast={}, t=t, cs_data=CSData(p=0, ev_data={})
        )
        self.assertEqual(p_device, {})

        # for an EVEN distribution
        test_cs_0.ev_data["EV_0"].p_max = 11000
        test_cs_0.ev_data["EV_1"].p_max = 11000
        test_cs_0.ev_data["EV_0"].p_min = -11000
        test_cs_0.ev_data["EV_1"].p_min = -11000
        test_cs_0.p = 11000
        p_device = cs_help.cs_distribute_charging_power(forecast, t, test_cs_0)
        self.assertEqual(p_device["EV_0"], p_device["EV_1"])

        # ev_0 needs twice as much energy as ev_1
        test_cs_0.ev_data["EV_0"].p_max = 10000
        test_cs_0.ev_data["EV_1"].p_max = 5000
        test_cs_0.ev_data["EV_0"].p_min = -10000
        test_cs_0.ev_data["EV_1"].p_min = -5000
        test_cs_0.p = 11000
        p_device = cs_help.cs_distribute_charging_power(forecast, t, test_cs_0)
        self.assertEqual(p_device["EV_0"], 5890.0)
        self.assertEqual(p_device["EV_1"], 5000.0)

        # minimum power of ev_1 reached
        test_cs_0.ev_data["EV_0"].p_max = 10000
        test_cs_0.ev_data["EV_1"].p_max = 10000
        test_cs_0.ev_data["EV_0"].p_min = -10000
        test_cs_0.ev_data["EV_1"].p_min = 6000
        test_cs_0.p = 11000
        p_device = cs_help.cs_distribute_charging_power(forecast, t, test_cs_0)
        self.assertEqual(p_device["EV_0"], 4890.0)
        self.assertEqual(p_device["EV_1"], 6000.0)

        # ev_0 and ev_1 reach their maximum power
        test_cs_0.ev_data["EV_0"].p_max = 5000
        test_cs_0.ev_data["EV_1"].p_max = 5000
        test_cs_0.ev_data["EV_0"].p_min = -5000
        test_cs_0.ev_data["EV_1"].p_min = -5000
        test_cs_0.p = 10000 / 0.99
        p_device = cs_help.cs_distribute_charging_power(forecast, t, test_cs_0)
        self.assertEqual(p_device["EV_0"], 5000.0)
        self.assertEqual(p_device["EV_1"], 5000.0)

        # raise ValueError if p_set > sum(ev_data.p_max)
        test_cs_0.p = 22000
        with self.assertRaises(ValueError):
            cs_help.cs_distribute_charging_power(forecast, t, test_cs_0)

        # raise ValueError if p_set < sum(ev_data.p_min)
        test_cs_0.p = -22000
        with self.assertRaises(ValueError):
            cs_help.cs_distribute_charging_power(forecast, t, test_cs_0)

        # raise ValueError if charging station has power value but no car connected
        test_cs_0.p = 11000
        test_cs_0.ev_data["EV_0"].appearance = False
        test_cs_0.ev_data["EV_1"].appearance = False
        with self.assertRaises(ValueError):
            cs_help.cs_distribute_charging_power(forecast, t, test_cs_0)

        # looping distribution, e.g., EVs require uneven low charging power
        test_cs_0.p = 11000
        test_cs_0.ev_data["EV_0"].appearance = True
        test_cs_0.ev_data["EV_1"].appearance = True
        test_cs_0.ev_data["EV_0"].p_max = 1500
        test_cs_0.ev_data["EV_1"].p_max = 11000
        p_device = cs_help.cs_distribute_charging_power(forecast, t, test_cs_0)
        self.assertEqual(
            p_device["EV_0"],
            test_cs_0.ev_data["EV_0"].p_max * test_cs_0.ev_data["EV_0"].charge_efficiency,
        )
        self.assertEqual(
            p_device["EV_1"], test_cs_0.p * test_cs_0.charge_efficiency - p_device["EV_0"]
        )

        # EV0 with max, EV1 not there
        test_cs_0.p = 11000
        test_cs_0.ev_data["EV_0"].appearance = True
        test_cs_0.ev_data["EV_1"].appearance = False
        test_cs_0.ev_data["EV_0"].p_max = 11000
        test_cs_0.ev_data["EV_1"].p_max = 11000
        p_device = cs_help.cs_distribute_charging_power(forecast, t, test_cs_0)
        self.assertEqual(
            p_device["EV_0"], test_cs_0.ev_data["EV_0"].p_max * test_cs_0.charge_efficiency
        )
        self.assertEqual(p_device["EV_1"], 0)


if __name__ == "__main__":
    suite = unittest.TestLoader().loadTestsFromTestCase(TestEMS)
    unittest.TextTestRunner(verbosity=0).run(suite)
