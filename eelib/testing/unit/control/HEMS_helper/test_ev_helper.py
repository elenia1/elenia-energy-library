"""
Unittest for car helper of eELib

| Author: elenia@TUBS
| Copyright 2024 elenia
| This file is part of eELib, which is free software under the terms of the GNU GPL Version 3.
"""

import unittest
import time

import eelib.core.control.hems.hems_helper.hems_ev_helper as ev_help
from eelib.data import EVData


class TestEMS(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        pass

    @classmethod
    def tearDownClass(cls):
        pass

    def setUp(self):
        print(f"[START] {self.id()}")
        self.start = time.time()

    def tearDown(self):
        t = time.time() - self.start
        print(f"[END] {self.id()}: {t:.4f}")

    def test_ev_calc_e_bat_soc(self):
        step_size = 3600

        # create ev dataclasses
        test_ev_0 = EVData(
            soc_min=0.02,
            e_max=60000.0,
            p_nom_discharge_max=-11000,
            p_nom_charge_max=11000,
            dcharge_efficiency=0.95,
            charge_efficiency=0.99,
            e_bat=60000.0,
        )

        # for fully charged, no change in e_bat or soc
        test_ev_0.e_bat = 60000.0
        test_ev_0.p = 0
        e_bat_new = ev_help.ev_calc_e_bat(step_size, test_ev_0)
        self.assertAlmostEqual(e_bat_new, 60000.0)

        # for EV should be charged
        test_ev_0.e_bat = 22500.0
        test_ev_0.p = 11000.0
        e_bat_new = ev_help.ev_calc_e_bat(step_size, test_ev_0)
        self.assertAlmostEqual(
            e_bat_new, 22500.0 + 11000 * step_size / 3600 * test_ev_0.charge_efficiency
        )

        # for EV should be discharged
        test_ev_0.e_bat = 22500.0
        test_ev_0.p = -11000.0
        e_bat_new = ev_help.ev_calc_e_bat(step_size, test_ev_0)
        self.assertAlmostEqual(
            e_bat_new, 22500.0 - 11000 * step_size / 3600 / test_ev_0.dcharge_efficiency
        )

    def test_ev_calc_power_limits(self):
        step_size = 3600
        # create ev dataclasses
        test_ev_0 = EVData(
            soc_min=0.02,
            e_max=60000.0,
            p_nom_discharge_max=-11000,
            p_nom_charge_max=11000,
            dcharge_efficiency=0.95,
            charge_efficiency=0.99,
            e_bat=60000.0,
        )

        # for fully charged ev (charge and discharge)
        test_ev_0.e_bat = 60000.0
        ev_p_min, ev_p_max = ev_help.ev_calc_power_limits(step_size, test_ev_0)
        self.assertEqual(ev_p_max, 0)
        self.assertEqual(ev_p_min, test_ev_0.p_nom_discharge_max)

        # for empty ev
        test_ev_0.e_bat = 0.0
        ev_p_min, ev_p_max = ev_help.ev_calc_power_limits(step_size, test_ev_0)
        self.assertAlmostEqual(ev_p_max, test_ev_0.p_nom_charge_max)
        self.assertAlmostEqual(ev_p_min, 0)

        # for ev at 50% soc
        test_ev_0.e_bat = 0.5 * test_ev_0.e_max
        ev_p_min, ev_p_max = ev_help.ev_calc_power_limits(step_size, test_ev_0)
        self.assertEqual(ev_p_max, test_ev_0.p_nom_charge_max)
        self.assertEqual(ev_p_min, test_ev_0.p_nom_discharge_max)

        # for (dis)charge efficiency = 0
        test_ev_0.dcharge_efficiency = 0
        test_ev_0.charge_efficiency = 0
        ev_p_min, ev_p_max = ev_help.ev_calc_power_limits(step_size, test_ev_0)
        self.assertEqual(ev_p_max, 0)
        self.assertEqual(ev_p_min, 0)

    def test_ev_calc_power(self):
        step_size = 3600

        # create ev dataclasses
        test_ev_0 = EVData(
            soc_min=0.02,
            e_max=60000.0,
            p_nom_discharge_max=-11000,
            p_nom_charge_max=11000,
            dcharge_efficiency=0.95,
            charge_efficiency=0.99,
            e_bat=60000.0,
        )

        # for an empty car
        test_ev_0.e_bat = 0.0
        test_ev_0.p_max = 11000.0
        test_ev_0.p_min = 0.0
        ev_p = ev_help.ev_calc_power(
            step_size=step_size,
            consumption_step=0,
            p_set=test_ev_0.p_nom_charge_max,
            ev_data=test_ev_0,
        )
        self.assertEqual(ev_p, test_ev_0.p_nom_charge_max)
        ev_p = ev_help.ev_calc_power(
            step_size=step_size,
            consumption_step=100,
            p_set=test_ev_0.p_nom_charge_max,
            ev_data=test_ev_0,
        )
        self.assertEqual(ev_p, -100 * 1000 / step_size * 3600)
        ev_p = ev_help.ev_calc_power(
            step_size=step_size,
            consumption_step=0,
            p_set=0,
            ev_data=test_ev_0,
        )
        self.assertEqual(ev_p, 0)

        # for a fully charged car
        test_ev_0.e_bat = 60000.0
        test_ev_0.p_max = 0.0
        test_ev_0.p_min = -11000.0
        ev_p = ev_help.ev_calc_power(
            step_size=step_size,
            consumption_step=0,
            p_set=test_ev_0.p_nom_discharge_max,
            ev_data=test_ev_0,
        )
        self.assertEqual(ev_p, test_ev_0.p_nom_discharge_max)
        ev_p = ev_help.ev_calc_power(
            step_size=step_size,
            consumption_step=100,
            p_set=test_ev_0.p_nom_charge_max,
            ev_data=test_ev_0,
        )
        self.assertEqual(ev_p, -100 * 1000 / step_size * 3600)
        ev_p = ev_help.ev_calc_power(
            step_size=step_size,
            consumption_step=0,
            p_set=0,
            ev_data=test_ev_0,
        )
        self.assertEqual(ev_p, 0)

        # for p_set is None
        ev_p = ev_help.ev_calc_power(
            step_size=step_size,
            consumption_step=0,
            p_set=None,
            ev_data=test_ev_0,
        )
        self.assertEqual(ev_p, 0)


if __name__ == "__main__":
    suite = unittest.TestLoader().loadTestsFromTestCase(TestEMS)
    unittest.TextTestRunner(verbosity=0).run(suite)
