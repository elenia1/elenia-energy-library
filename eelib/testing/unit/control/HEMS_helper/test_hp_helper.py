"""
Unittest for heatpump helper of eELib

| Author: elenia@TUBS
| Copyright 2024 elenia
| This file is part of eELib, which is free software under the terms of the GNU GPL Version 3.
"""

import unittest
import time
import eelib.core.control.hems.hems_helper.hems_hp_helper as hp_help


class TestEMS(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        pass

    @classmethod
    def tearDownClass(cls):
        pass

    def setUp(self):
        print(f"[START] {self.id()}")
        self.start = time.time()

    def tearDown(self):
        t = time.time() - self.start
        print(f"[END] {self.id()}: {t:.4f}")

    def test_hp_power_limits(self):
        self.assertEqual(hp_help.calc_hp_power_limits(2, 0.5, "on"), (-2, -1, 0))
        self.assertEqual(hp_help.calc_hp_power_limits(2, 0.5, "must_on"), (-2, -1, -1))
        self.assertEqual(hp_help.calc_hp_power_limits(2, 0.5, "off"), (-2, -1, 0))
        self.assertEqual(hp_help.calc_hp_power_limits(2, 0.5, "must_off"), (0, 0, 0))

    def test_hp_state(self):
        self.assertEqual(
            hp_help.set_hp_state(0, hp_time_on=0, hp_time_off=2, hp_time_min=1),
            "off",
        )
        self.assertEqual(
            hp_help.set_hp_state(0, hp_time_on=0, hp_time_off=1, hp_time_min=2),
            "must_off",
        )
        self.assertEqual(
            hp_help.set_hp_state(-1, hp_time_on=2, hp_time_off=0, hp_time_min=1),
            "on",
        )
        self.assertEqual(
            hp_help.set_hp_state(-1, hp_time_on=1, hp_time_off=0, hp_time_min=2),
            "must_on",
        )

    def test_hp_power_with_limits(self):
        self.assertEqual(hp_help.hp_set_power_with_limits(-2, -1, 0, -2.5), -2)
        self.assertEqual(hp_help.hp_set_power_with_limits(-2, -1, 0, -1.5), -1.5)
        self.assertEqual(hp_help.hp_set_power_with_limits(-2, -1, 0, -0.5), -1)
        self.assertEqual(hp_help.hp_set_power_with_limits(-2, -1, 0, 0.5), 0)

    def test_hp_time(self):
        step_size = 1
        self.assertEqual(hp_help.set_hp_time(step_size, 0, hp_time_on=0, hp_time_off=1), (0, 2))
        self.assertEqual(hp_help.set_hp_time(step_size, 0, hp_time_on=1, hp_time_off=0), (0, 1))
        self.assertEqual(hp_help.set_hp_time(step_size, -1, hp_time_on=1, hp_time_off=0), (2, 0))
        self.assertEqual(hp_help.set_hp_time(step_size, -1, hp_time_on=0, hp_time_off=1), (1, 0))


if __name__ == "__main__":
    suite = unittest.TestLoader().loadTestsFromTestCase(TestEMS)
    unittest.TextTestRunner(verbosity=0).run(suite)
