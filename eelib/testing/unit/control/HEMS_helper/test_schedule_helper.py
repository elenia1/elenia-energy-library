"""
Unittest for schedule helper of eELib

| Author: elenia@TUBS
| Copyright 2024 elenia
| This file is part of eELib, which is free software under the terms of the GNU GPL Version 3.
"""

import unittest
import time
import numpy as np

import eelib.core.control.hems.hems_helper.schedule_helper as schedule_help
from eelib.data import BSSData, CSData, EVData, HPData, PVData
from eelib.data import ControlSignalEMS, OptimOptions, TariffSignal


bss = BSSData(
    e_bat=2,
    e_bat_usable=4,
    p_rated_discharge_max=-4,
    p_rated_charge_max=4,
    soc_min=0,
    charge_efficiency=1,
    discharge_efficiency=1,
)

pv = PVData(p_rated=10)

hp = HPData(
    p_rated_th=3000, p_min_th_rel=0.5, time_min=3600, time_on=3600, time_off=0, state="on", cop=3
)


class TestEMS(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        pass

    @classmethod
    def tearDownClass(cls):
        pass

    def setUp(self):
        print(f"[START] {self.id()}")
        self.start = time.time()

    def tearDown(self):
        t = time.time() - self.start
        print(f"[END] {self.id()}: {t:.4f}")

    def test_forecast_thermal(self):
        forecast_water = [i for i in range(0, 24, 1)]
        forecast_room = [10 for _ in range(0, 24, 1)]
        forecast_p_res = [1 for i in range(0, 24, 1)]

        # test curtailed charging
        forecast_horizon = 24
        forecast = {
            "p_res": forecast_p_res,
            "th_house": {"p_th_water": forecast_water, "p_th_room": forecast_room, "test": []},
        }
        th_forecast = schedule_help.calc_forecast_thermal_residual(forecast, forecast_horizon)
        self.assertEqual(len(th_forecast), 24)
        self.assertEqual(sum(th_forecast), 24 * 10 + 23 * 24 / 2)

    def test_schedule_pv(self):
        fc_pv = [-0, -1, -4, -1, -0]
        fc_load = np.array([2, 2, 1, 1, 1])
        forecast = {
            "p_res": fc_load + fc_pv,
            "pv_0": {"p": fc_pv},
        }
        pv_data = {"pv_0": pv}

        pv_schedule = schedule_help.pv_calc_schedule(forecast, pv_data)
        self.assertListEqual(list(pv_schedule["pv_0"]["p"]), fc_pv)
        self.assertListEqual(list(forecast["p_res"]), list(fc_load))

    def test_schedule_hp(self):
        step_size = 3600
        e_th = 0

        # HP properties at the beginning of forecast period
        test_hp_0 = {
            "hp_0": HPData(
                p_rated_th=1000,
                p_min_th_rel=0.5,
                time_min=3600,
                cop=3,
                state="on",
                time_on=3600,
                time_off=0,
            )
        }

        # for no thermal demand at all
        th_demand_profile = [0 for _ in range(0, 24)]
        hp_schedule = schedule_help.hp_calc_schedule(
            step_size,
            th_demand_profile,
            e_th,
            test_hp_0,
        )
        self.assertEqual(sum(hp_schedule["hp_0"]["p_th"]), 0)

        # continuous thermal demand
        th_demand_profile = [800 for _ in range(0, 24)]
        hp_schedule = schedule_help.hp_calc_schedule(
            step_size,
            th_demand_profile,
            e_th,
            test_hp_0,
        )
        self.assertEqual(sum(hp_schedule["hp_0"]["p_th"]), -800 * 24)

        # switching thermal demand
        th_demand_profile[10] = 0
        hp_schedule = schedule_help.hp_calc_schedule(
            step_size,
            th_demand_profile,
            e_th,
            test_hp_0,
        )
        self.assertEqual(sum(hp_schedule["hp_0"]["p_th"]), -800 * 23)
        self.assertEqual(hp_schedule["hp_0"]["p_th"][10], 0)

        # thermal demand to activate min off/on-time
        th_demand_profile = [800 for _ in range(0, 24 * 4)]
        th_demand_profile[0] = 0
        th_demand_profile[6] = 0
        step_size = 900
        hp_schedule = schedule_help.hp_calc_schedule(
            step_size,
            th_demand_profile,
            e_th,
            test_hp_0,
        )
        self.assertListEqual(list(hp_schedule["hp_0"]["p_th"][0:4]), [0, 0, 0, 0])
        self.assertListEqual(list(hp_schedule["hp_0"]["p_th"][4:8]), [-1000, -1000, -1000, -1000])

    def test_cs_calc_schedule_uncontrolled(self):
        ev_id = "EV_0"
        step_size = 3600
        forecast_horizon = 24
        forecast = {
            "EV_0": {
                "appearance": [False for _ in range(0, 24)],
                "consumption": [0.5 for _ in range(0, 24)],
            }
        }

        # EV and CS properties at the beginning of forecast period
        test_ev_0 = EVData(
            soc_min=0.02,
            e_max=60000,
            p_nom_discharge_max=-11000,
            p_nom_charge_max=11000,
            dcharge_efficiency=0.95,
            charge_efficiency=0.99,
            e_bat=60000.0,
        )
        test_cs_0 = CSData(
            discharge_efficiency=1,
            charge_efficiency=1,
            p_rated=11000,
            p_min=0,
            p_max=1000,
            ev_data={"EV_0": test_ev_0},
        )
        test_cs_data = {"CS_0": test_cs_0}

        # for appearence always FALSE, no car at home
        cs_schedule = schedule_help.cs_calc_schedule_uncontrolled(
            step_size,
            forecast,
            forecast_horizon,
            test_cs_data,
        )
        self.assertEqual(sum(cs_schedule["CS_0"]["p"]), 0)

        # for appearance always TRUE, but car fully charged
        forecast["EV_0"]["appearance"] = [True for _ in range(0, 24)]
        forecast["EV_0"]["consumption"] = [0 for _ in range(0, 24)]
        test_cs_0.ev_data["EV_0"].e_bat = test_cs_0.ev_data["EV_0"].e_max  # fully charged EV
        cs_schedule = schedule_help.cs_calc_schedule_uncontrolled(
            step_size,
            forecast,
            forecast_horizon,
            test_cs_data,
        )
        self.assertEqual(sum(cs_schedule["CS_0"]["p"]), 0)

        # for car being home for 1 timestep and ready to be charged
        forecast = {ev_id: {"appearance": [], "consumption": []}}
        for i in range(0, 24):
            if i < 1:
                forecast[ev_id]["appearance"].append(True)
                forecast[ev_id]["consumption"].append(0)
            else:
                forecast[ev_id]["appearance"].append(False)
                forecast[ev_id]["consumption"].append(0.5)

        test_cs_0.ev_data["EV_0"].e_bat = 30000
        cs_schedule = schedule_help.cs_calc_schedule_uncontrolled(
            step_size,
            forecast,
            forecast_horizon,
            test_cs_data,
        )
        self.assertEqual(sum(cs_schedule["CS_0"]["p"]), 11000)

    def test_calc_schedule_uncontrollable(self):
        forecast = {
            "p_res": np.array([900, 0, 0, 0, 0, 1000]),
            "HouseholdCSV_0": {"p": [1000, 700, 1200, 1200, 1200, 1200]},
            "PvCSV_0": {"p": [-100, -100, -200, -200, -200, -200]},
        }
        control_result = [900, 600, 1000, 1000, 1000, 1000]
        forecast_horizon = 6

        self.assertListEqual(
            list(schedule_help.calc_forecast_residual(forecast, forecast_horizon)),
            control_result,
        )

    def test_bss_calc_schedule(self):
        bss_full_id = "BSS_0"
        step_size = 900
        test_input_schedule = [100, -100, 100, -100]
        bss_dataclass = BSSData(
            e_bat_rated=8000,
            p_rated_discharge_max=-8000,
            p_rated_charge_max=8000,
            loss_rate=0.02,
            self_discharge_step=0.02 * step_size / (60 * 60 * 24 * 30),
            dod_max=0.95,
            status_aging=True,
            soh_cycles_max=0.8,
            bat_cycles_max=5000,
            soc_min=0.05,
            charge_efficiency=0.95,
            discharge_efficiency=0.95,
            bat2ac_efficiency=[-0.000623, -1.65, 0.9591],
            ac2bat_efficiency=[-0.0006864, -1.641, 0.957],
            soh=None,
            bat_cycles=None,
            e_bat=4000,
            e_bat_usable=8000,
        )

        bss_data = {bss_full_id: bss_dataclass}

        # get result and convert into a flat list to compare
        p_set_bss_forecast = schedule_help.bss_calc_schedule(
            step_size,
            test_input_schedule,
            bss_data,
        )

        # result should always be the opposite of input schedule, to balance it
        result_validation = [-1 * value for value in test_input_schedule]

        self.assertListEqual(list(p_set_bss_forecast[bss_full_id]["p"]), result_validation)

    def test_calc_schedule_opt_only_pv(self):
        step_size = 3600
        forecast = {
            "p_res": np.array([1, 1, 1, 1, 1]),
            "pv_0": {"p": [-0, -1, -4, -1, -0]},
        }
        forecast_horizon = 5
        pv_data = {"pv_0": pv}
        tariff = TariffSignal(elec_price=10, feedin_tariff=1)

        # get result and convert into a flat list to compare
        schedules = schedule_help.calc_schedule_opt(
            step_size=step_size,
            forecast=forecast,
            forecast_horizon=forecast_horizon,
            pv_data=pv_data,
            tariff=tariff,
        )

        # retrieve results
        self.assertListEqual(list(forecast["pv_0"]["p"]), list(schedules["pv_0"]["p"]))

    def test_calc_schedule_opt_error(self):
        step_size = 3600
        forecast = {
            "p_res": np.array([1, 1, 1, 1, 1]),
            "ev_0": {
                "consumption": [0, 4, 2, 4, 0],
                "appearance": [True, False, False, False, True],
            },
        }
        forecast_horizon = 5
        cs_data = {
            "cs_0": CSData(
                charge_efficiency=1,
                discharge_efficiency=1,
                p_rated=2,
                ev_data={
                    "ev_0": EVData(
                        charge_efficiency=1,
                        dcharge_efficiency=1,
                        e_max=8,
                        e_bat=6,
                        soc_min=0,
                        p_nom_charge_max=5,
                        p_nom_discharge_max=0,
                    ),
                },
            )
        }

        # ev battery will be more than empty with this consumption profile - does not work
        with self.assertRaises(ValueError) as message:
            _ = schedule_help.calc_schedule_opt(
                step_size=step_size,
                forecast=forecast,
                forecast_horizon=forecast_horizon,
                cs_data=cs_data,
            )
        str_grb = (
            "Schedule Optimization unsuccessful: Problem proven to be infeasible or unbounded."
        )
        str_appsi = "Schedule Optimization unsuccessful: TerminationCondition.infeasible"
        self.assertTrue(str(message.exception) == str_grb or str(message.exception) == str_appsi)

    def test_calc_schedule_opt_pv_and_bss(self):
        step_size = 3600
        fc_pv = [-0, -1, -4, -1, -0]
        fc_load = np.array([2, 2, 1, 1, 1])
        forecast = {
            "p_res": fc_load + fc_pv,
            "pv_0": {"p": fc_pv},
        }
        forecast_horizon = 5
        bss.e_bat = 2
        bss_data = {"bss_0": bss}
        pv_data = {"pv_0": pv}
        tariff = TariffSignal(elec_price=10, feedin_tariff=1)
        optim_options = OptimOptions(bss_direct_cha=True, bss_end_energy_incentive=True)

        schedules = schedule_help.calc_schedule_opt(
            step_size=step_size,
            forecast=forecast,
            forecast_horizon=forecast_horizon,
            bss_data=bss_data,
            pv_data=pv_data,
            tariff=tariff,
            opt=optim_options,
        )
        result_bss = [-2, 0, 3, 0, -1]
        self.assertListEqual(list(forecast["pv_0"]["p"]), list(schedules["pv_0"]["p"]))
        self.assertListEqual(list(result_bss), list(schedules["bss_0"]["p"]))

        # try with no incentive to keep energy in bss -> does not charge from pv or feed to grid
        forecast = {
            "p_res": fc_load + fc_pv,
            "pv_0": {"p": fc_pv},
        }
        optim_options = OptimOptions(bss_direct_cha=True, bss_end_energy_incentive=False)
        bss.charge_efficiency = 0.99999999
        schedules = schedule_help.calc_schedule_opt(
            step_size=step_size,
            forecast=forecast,
            forecast_horizon=forecast_horizon,
            bss_data=bss_data,
            pv_data=pv_data,
            tariff=tariff,
            opt=optim_options,
        )
        result_bss = [-2, 0, 1, 0, -1]
        self.assertListEqual(list(forecast["pv_0"]["p"]), list(schedules["pv_0"]["p"]))
        self.assertEqual(list(result_bss), list(schedules["bss_0"]["p"]))
        bss.charge_efficiency = 1

        # bss efficiencies do not work, otherwise the same as first try
        forecast = {
            "p_res": fc_load + fc_pv,
            "pv_0": {"p": fc_pv},
        }
        optim_options = OptimOptions(bss_direct_cha=True, bss_end_energy_incentive=True)
        bss.charge_efficiency = None
        bss.discharge_efficiency = 0.0
        schedules = schedule_help.calc_schedule_opt(
            step_size=step_size,
            forecast=forecast,
            forecast_horizon=forecast_horizon,
            bss_data=bss_data,
            pv_data=pv_data,
            tariff=tariff,
            opt=optim_options,
        )
        result_bss = [-2, 0, 3, 0, -1]
        self.assertListEqual(list(forecast["pv_0"]["p"]), list(schedules["pv_0"]["p"]))
        self.assertEqual(list(result_bss), list(schedules["bss_0"]["p"]))
        bss.charge_efficiency = 1
        bss.discharge_efficiency = 1

    def test_calc_schedule_opt_control_signals(self):
        step_size = 3600
        fc_pv = [-0, -3, -6, -1, -0]
        fc_load = np.array([2, 1, 1, 3, 5])
        forecast = {
            "p_res": fc_load + fc_pv,
            "pv_0": {"p": fc_pv},
        }
        forecast_horizon = 5
        bss_data = {"bss_0": bss}
        pv_data = {"pv_0": pv}
        tariff = TariffSignal(elec_price=10, feedin_tariff=1)
        optim_options = OptimOptions(bss_direct_cha=True, bss_end_energy_incentive=True)

        # no control signals at first
        control_signals = ControlSignalEMS()
        schedules = schedule_help.calc_schedule_opt(
            step_size=step_size,
            forecast=forecast,
            forecast_horizon=forecast_horizon,
            bss_data=bss_data,
            pv_data=pv_data,
            tariff=tariff,
            control_signals=control_signals,
            opt=optim_options,
        )
        self.assertListEqual(list(forecast["pv_0"]["p"]), list(schedules["pv_0"]["p"]))
        self.assertListEqual([-2, 2, 2, -2, -2], list(schedules["bss_0"]["p"]))

        # restrict only maximum (demand) of last step
        forecast = {
            "p_res": fc_load + fc_pv,
            "pv_0": {"p": fc_pv},
        }
        control_signals = ControlSignalEMS(steps=[4], p_max=[2])
        schedules = schedule_help.calc_schedule_opt(
            step_size=step_size,
            forecast=forecast,
            forecast_horizon=forecast_horizon,
            bss_data=bss_data,
            pv_data=pv_data,
            tariff=tariff,
            control_signals=control_signals,
            opt=optim_options,
        )
        self.assertListEqual([-2, 2, 2, -1, -3], list(schedules["bss_0"]["p"]))

        # restrict only minimum (feedin) of third step - BSS power has to be saved for that
        forecast = {
            "p_res": fc_load + fc_pv,
            "pv_0": {"p": fc_pv},
        }
        control_signals = ControlSignalEMS(steps=[2], p_min=[-2])
        schedules = schedule_help.calc_schedule_opt(
            step_size=step_size,
            forecast=forecast,
            forecast_horizon=forecast_horizon,
            bss_data=bss_data,
            pv_data=pv_data,
            tariff=tariff,
            control_signals=control_signals,
            opt=optim_options,
        )
        self.assertListEqual([-2, 1, 3, -2, -2], list(schedules["bss_0"]["p"]))

        # now additionally restrict minimum (max. feedin) of third step
        forecast = {
            "p_res": fc_load + fc_pv,
            "pv_0": {"p": fc_pv},
        }
        control_signals = ControlSignalEMS(steps=[2, 4], p_min=[-1, np.inf], p_max=[-1, 2])
        schedules = schedule_help.calc_schedule_opt(
            step_size=step_size,
            forecast=forecast,
            forecast_horizon=forecast_horizon,
            bss_data=bss_data,
            pv_data=pv_data,
            tariff=tariff,
            control_signals=control_signals,
            opt=optim_options,
        )
        self.assertListEqual([-2, 0, 4, -1, -3], list(schedules["bss_0"]["p"]))

        # check impossible control signal (due to penalty variables)
        forecast = {
            "p_res": fc_load + fc_pv,
            "pv_0": {"p": fc_pv},
        }
        control_signals = ControlSignalEMS(
            steps=[2, 4, 0], p_min=[-1, -np.inf, -np.inf], p_max=[-1, 2, -1]
        )
        schedules = schedule_help.calc_schedule_opt(
            step_size=step_size,
            forecast=forecast,
            forecast_horizon=forecast_horizon,
            bss_data=bss_data,
            pv_data=pv_data,
            tariff=tariff,
            control_signals=control_signals,
            opt=optim_options,
        )
        self.assertListEqual([-2, 0, 4, -1, -3], list(schedules["bss_0"]["p"]))

    def test_calc_schedule_opt_only_ev(self):
        step_size = 3600
        forecast = {
            "p_res": np.array([1, 1, 1, 1, 1]),
            "ev_0": {
                "consumption": [0, 0, 0.002, 0, 0],
                "appearance": [True, False, False, True, True],
            },
            "ev_1": {
                "consumption": [0, 0, 0.001, 0.001, 0],
                "appearance": [True, True, False, False, True],
            },
        }
        forecast_horizon = 5
        cs_data = {
            "cs_0": CSData(
                charge_efficiency=1,
                discharge_efficiency=1,
                p_rated=2,
                ev_data={
                    "ev_0": EVData(
                        charge_efficiency=1,
                        dcharge_efficiency=1,
                        e_max=10,
                        e_bat=8,
                        soc_min=1 / 10,
                        p_nom_charge_max=5,
                        p_nom_discharge_max=0,
                    ),
                    "ev_1": EVData(
                        charge_efficiency=1,
                        dcharge_efficiency=1,
                        e_max=20,
                        e_bat=19,
                        soc_min=1 / 10,
                        p_nom_charge_max=5,
                        p_nom_discharge_max=0,
                    ),
                },
            )
        }
        tariff = TariffSignal(elec_price=10, feedin_tariff=1)
        optim_options = OptimOptions(ev_direct_cha=True, ev_end_energy_penalty=100)

        # get result and convert into a flat list to compare
        schedules = schedule_help.calc_schedule_opt(
            step_size=step_size,
            forecast=forecast,
            forecast_horizon=forecast_horizon,
            cs_data=cs_data,
            tariff=tariff,
            opt=optim_options,
        )

        # retrieve results
        result_cs = [2, 1, 0, 2, 2]
        self.assertListEqual(list(result_cs), list(schedules["cs_0"]["p"]))

    def test_calc_schedule_opt_pv_bss_ev(self):
        step_size = 3600
        forecast = {
            "p_res": np.array([1, 0, -3, 0, 1]),
            "pv_0": {"p": [-0, -1, -4, -1, -0]},
            "ev_0": {
                "consumption": [0, 0, 0.002, 0, 0],
                "appearance": [True, False, False, True, True],
            },
            "ev_1": {
                "consumption": [0, 0, 0.001, 0.001, 0],
                "appearance": [True, True, False, False, True],
            },
        }
        forecast_horizon = 5
        cs_data = {
            "cs_0": CSData(
                charge_efficiency=1,
                discharge_efficiency=1,
                p_rated=2,
                ev_data={
                    "ev_0": EVData(
                        charge_efficiency=1,
                        dcharge_efficiency=1,
                        e_max=10,
                        e_bat=8,
                        soc_min=1 / 10,
                        p_nom_charge_max=5,
                        p_nom_discharge_max=0,
                    ),
                    "ev_1": EVData(
                        charge_efficiency=1,
                        dcharge_efficiency=1,
                        e_max=20,
                        e_bat=19,
                        soc_min=1 / 10,
                        p_nom_charge_max=5,
                        p_nom_discharge_max=0,
                    ),
                },
            )
        }
        bss.e_bat = 2
        bss_data = {"bss_0": bss}
        pv_data = {"pv_0": pv}
        tariff = TariffSignal(elec_price=10, feedin_tariff=1)
        optim_options = OptimOptions(
            ev_direct_cha=True,
            ev_end_energy_penalty=100,
            bss_direct_cha=True,
            bss_end_energy_incentive=True,
        )

        schedules = schedule_help.calc_schedule_opt(
            step_size=step_size,
            forecast=forecast,
            forecast_horizon=forecast_horizon,
            cs_data=cs_data,
            bss_data=bss_data,
            pv_data=pv_data,
            tariff=tariff,
            opt=optim_options,
        )

        # retrieve results
        result_cs = [2, 1, 0, 2, 2]
        result_bss = [-2, 0, 3, -2, -1]
        self.assertListEqual(list(forecast["pv_0"]["p"]), list(schedules["pv_0"]["p"]))
        self.assertListEqual(list(result_bss), list(schedules["bss_0"]["p"]))
        self.assertListEqual(list(result_cs), list(schedules["cs_0"]["p"]))

    def test_calc_schedule_opt_only_hp(self):
        step_size = 3600
        forecast = {
            "p_res": np.array([1000, 1000, 1000, 1000, 1000]),
            "p_th_dem": [1500, 1500, 0, 0, 2000],
        }
        forecast_horizon = 5
        hp.time_min = 3 * step_size
        hp.time_on = 0 * step_size
        hp.time_off = 2 * step_size
        hp.p_rated_th = 1500
        hp.p_min_th_rel = 1
        hp_data = {"hp_0": hp}
        tariff = TariffSignal(elec_price=10, feedin_tariff=1)
        optim_options = OptimOptions(
            consider_thermal=True,
            thermal_energy_start=0,
            thermal_energy_restriction=1501,
            round_int=7,
        )

        # get result and convert into a flat list to compare
        schedules = schedule_help.calc_schedule_opt(
            step_size=step_size,
            forecast=forecast,
            forecast_horizon=forecast_horizon,
            hp_data=hp_data,
            tariff=tariff,
            opt=optim_options,
        )

        # retrieve results (hp off one more step, then on for 3 steps)
        self.assertListEqual([0.0, -1500.0, -1500.0, -1500.0, 0.0], list(schedules["hp_0"]["p_th"]))
        self.assertListEqual(
            list(np.round(forecast["p_res"] - schedules["hp_0"]["p_th"] / hp.cop, 7)),
            list(schedules["p_res"]),
        )

        # if on and needs to stay on, hp should stay on for 3 more steps and then be turned of
        forecast["p_th_dem"] = [1500, 1500, 1500, 0, 0]  # no demand in 1. step, no gen possible
        hp.time_off = 0 * step_size
        hp.time_on = 1 * step_size
        hp_data = {"hp_0": hp}
        optim_options.thermal_energy_start = 1500
        schedules = schedule_help.calc_schedule_opt(
            step_size=step_size,
            forecast=forecast,
            forecast_horizon=forecast_horizon,
            hp_data=hp_data,
            tariff=tariff,
            opt=optim_options,
        )
        self.assertListEqual([-1500.0, -1500.0, -1500.0, 0, 0], list(schedules["hp_0"]["p_th"]))

        # if on, changes possible and needs to be directly turned off (th energy limits)
        forecast["p_th_dem"] = [0, 1500, 1500, 0, 2000]  # no demand in 1. step, no gen possible
        hp.time_off = 0 * step_size
        hp.time_on = 3 * step_size
        hp_data = {"hp_0": hp}
        optim_options.thermal_energy_start = -1500
        schedules = schedule_help.calc_schedule_opt(
            step_size=step_size,
            forecast=forecast,
            forecast_horizon=forecast_horizon,
            hp_data=hp_data,
            tariff=tariff,
            opt=optim_options,
        )
        self.assertListEqual([0, 0, 0, -1500.0, -1500.0], list(schedules["hp_0"]["p_th"]))

        # if off, and needs to be turned on
        forecast["p_th_dem"] = [1500, 1500, 0, 0, 2000]  # need to turn it on in first step
        hp.time_off = 3 * step_size
        hp.time_on = 0 * step_size
        hp_data = {"hp_0": hp}
        optim_options.thermal_energy_start = 1500
        schedules = schedule_help.calc_schedule_opt(
            step_size=step_size,
            forecast=forecast,
            forecast_horizon=forecast_horizon,
            hp_data=hp_data,
            tariff=tariff,
            opt=optim_options,
        )
        self.assertListEqual([-1500.0, -1500.0, -1500.0, -1500, 0], list(schedules["hp_0"]["p_th"]))

    def test_calc_schedule_opt(self):
        step_size = 3600
        forecast = {
            "p_res": np.array([500, -500, -3500, -500, 500]),
            "p_th_dem": [1500, 1500, 0, 0, 2000],
            "pv_0": {"p": [-0, -1000, -4000, -1000, -0]},
            "ev_0": {
                "consumption": [0, 0, 2, 0, 0],
                "appearance": [True, False, False, True, True],
            },
            "ev_1": {
                "consumption": [0, 0, 1, 1, 0],
                "appearance": [True, True, False, False, True],
            },
        }
        forecast_horizon = 5
        cs_data = {
            "cs_0": CSData(
                charge_efficiency=1,
                discharge_efficiency=1,
                p_rated=3000,
                ev_data={
                    "ev_0": EVData(
                        charge_efficiency=1,
                        dcharge_efficiency=1,
                        e_max=10000,
                        e_bat=8000,
                        soc_min=1 / 10,
                        p_nom_charge_max=5000,
                        p_nom_discharge_max=0,
                    ),
                    "ev_1": EVData(
                        charge_efficiency=1,
                        dcharge_efficiency=1,
                        e_max=20000,
                        e_bat=19000,
                        soc_min=1 / 10,
                        p_nom_charge_max=5000,
                        p_nom_discharge_max=0,
                    ),
                },
            )
        }
        bss = BSSData(
            e_bat=2000,
            e_bat_usable=4000,
            p_rated_discharge_max=-4000,
            p_rated_charge_max=4000,
            soc_min=0,
            charge_efficiency=1,
            discharge_efficiency=1,
        )
        pv = PVData(p_rated=10000)
        bss_data = {"bss_0": bss}
        pv_data = {"pv_0": pv}
        hp.time_min = 3 * step_size
        hp.time_on = 0 * step_size
        hp.time_off = 2 * step_size
        hp.p_rated_th = 1500
        hp.p_min_th_rel = 1
        hp_data = {"hp_0": hp}
        tariff = TariffSignal(elec_price=0.35, feedin_tariff=0.07)
        optim_options = OptimOptions(
            consider_thermal=True,
            thermal_energy_start=0,
            thermal_energy_restriction=1501,
            ev_direct_cha=True,
            ev_end_energy_penalty=3.5,
            bss_direct_cha=True,
            bss_end_energy_incentive=True,
            round_int=7,
        )

        # get result and convert into a flat list to compare
        schedules = schedule_help.calc_schedule_opt(
            step_size=step_size,
            forecast=forecast,
            forecast_horizon=forecast_horizon,
            cs_data=cs_data,
            bss_data=bss_data,
            pv_data=pv_data,
            hp_data=hp_data,
            tariff=tariff,
            opt=optim_options,
        )

        # retrieve results
        result_cs = [3000, 0, 0, 2000, 2000]
        result_bss = [-2000, 0, 3000, -2000, -1000]
        self.assertListEqual(list(forecast["pv_0"]["p"]), list(schedules["pv_0"]["p"]))
        self.assertListEqual(list(result_cs), list(schedules["cs_0"]["p"]))
        self.assertListEqual([0.0, -1500.0, -1500.0, -1500.0, 0.0], list(schedules["hp_0"]["p_th"]))
        self.assertListEqual(list(result_bss), list(schedules["bss_0"]["p"]))

    def test_calc_schedule_opt_variable_tariff(self):
        step_size = 3600
        forecast = {
            "p_res": np.array([-2, -2, -2, 0, 0]),
            "pv_0": {"p": [-2, -2, -2, -0, -0]},
            "ev_0": {
                "consumption": [0, 0, 0.002, 0, 0],
                "appearance": [True, False, False, True, True],
            },
        }
        forecast_horizon = 5
        cs_data = {
            "cs_0": CSData(
                charge_efficiency=1,
                discharge_efficiency=1,
                p_rated=2,
                ev_data={
                    "ev_0": EVData(
                        charge_efficiency=1,
                        dcharge_efficiency=1,
                        e_max=10,
                        e_bat=10,
                        soc_min=1 / 10,
                        p_nom_charge_max=5,
                        p_nom_discharge_max=0,
                    )
                },
            )
        }
        bss_data = {
            "bss_0": BSSData(
                e_bat=0,
                e_bat_usable=4,
                p_rated_discharge_max=-4,
                p_rated_charge_max=4,
                soc_min=0,
                charge_efficiency=1,
                discharge_efficiency=1,
            )
        }
        pv_data = {"pv_0": pv}
        tariff = TariffSignal(
            elec_price=[10, 10, 10, 10, 9],  # cs shifts power to step 5
            feedin_tariff=[1, 1, 10, 1, 1],  # bss charges and discharges at step 3
            bool_is_list=True,  # bss does not leave energy until step 4 to charge ev
        )
        optim_options = OptimOptions(
            ev_direct_cha=True,
            ev_end_energy_penalty=100,
            bss_direct_cha=False,
            bss_end_energy_incentive=False,
        )

        schedules = schedule_help.calc_schedule_opt(
            step_size=step_size,
            forecast=forecast,
            forecast_horizon=forecast_horizon,
            cs_data=cs_data,
            bss_data=bss_data,
            pv_data=pv_data,
            tariff=tariff,
            opt=optim_options,
        )

        # retrieve results
        result_cs = [0, 0, 0, 0, 2]  # ev uses low grid costs in step 5
        result_bss = [2, 2, -4, 0, 0]  # bss uses high feedin remuneration in step 3
        self.assertListEqual(list(forecast["pv_0"]["p"]), list(schedules["pv_0"]["p"]))
        self.assertListEqual(list(result_cs), list(schedules["cs_0"]["p"]))
        self.assertListEqual(list(result_bss), list(schedules["bss_0"]["p"]))

    def test_calc_schedule_opt_cap_fee(self):
        step_size = 3600
        forecast = {
            "p_res": np.array([-1, -4, 1, 0]),
            "pv_0": {"p": [-1, -4, 0, 0]},
            "ev_0": {
                "consumption": [0, 0, 0.005, 0],
                "appearance": [True, False, False, True],
            },
        }
        forecast_horizon = 4
        cs_data = {
            "cs_0": CSData(
                charge_efficiency=1,
                discharge_efficiency=1,
                p_rated=5,
                ev_data={
                    "ev_0": EVData(
                        charge_efficiency=1,
                        dcharge_efficiency=1,
                        e_max=10,
                        e_bat=10,
                        soc_min=1 / 10,
                        p_nom_charge_max=5,
                        p_nom_discharge_max=0,
                    )
                },
            )
        }
        bss_data = {
            "bss_0": BSSData(
                e_bat=1,
                e_bat_usable=4,
                p_rated_discharge_max=-4,
                p_rated_charge_max=4,
                soc_min=0,
                charge_efficiency=1,
                discharge_efficiency=1,
            )
        }
        pv_data = {"pv_0": pv}
        tariff = TariffSignal(
            elec_price=3,
            feedin_tariff=2,
            capacity_fee_dem=0.001,
            capacity_fee_gen=0.001,
            capacity_fee_horizon_sec=3600 * 4,
        )
        optim_options = OptimOptions(
            ev_direct_cha=True,
            ev_end_energy_penalty=100,
            bss_direct_cha=False,
            bss_end_energy_incentive=True,
        )

        schedules = schedule_help.calc_schedule_opt(
            step_size=step_size,
            forecast=forecast,
            forecast_horizon=forecast_horizon,
            cs_data=cs_data,
            bss_data=bss_data,
            pv_data=pv_data,
            tariff=tariff,
            opt=optim_options,
        )

        # retrieve results
        result_cs = [0, 0, 0, 5]
        result_bss = [0, 3, 0, -4]  # bss first charges at 2 (to avoid peak gen)
        #  and discharges at 4 (to avoid peak dem)
        self.assertListEqual(list(forecast["pv_0"]["p"]), list(schedules["pv_0"]["p"]))
        self.assertListEqual(list(result_cs), list(schedules["cs_0"]["p"]))
        self.assertListEqual(list(result_bss), list(schedules["bss_0"]["p"]))

        # with wrong horizon for capacity signal -> first zero then it is fine
        tariff = TariffSignal(
            elec_price=3,
            feedin_tariff=2,
            capacity_fee_dem=0,
            capacity_fee_gen=0,
            capacity_fee_horizon_sec=3600 * 24 * 365,
        )
        schedules = schedule_help.calc_schedule_opt(
            step_size=step_size,
            forecast=forecast,
            forecast_horizon=forecast_horizon,
            cs_data=cs_data,
            bss_data=bss_data,
            pv_data=pv_data,
            tariff=tariff,
            opt=optim_options,
        )

        # with wrong horizon for capacity signal -> now unequal zero
        tariff = TariffSignal(
            elec_price=3,
            feedin_tariff=2,
            capacity_fee_dem=0.001,
            capacity_fee_gen=0.001,
            capacity_fee_horizon_sec=3600 * 24 * 365,
        )
        test_string_warn = "Capacity fees will not be considered due to unequal horizon."
        with self.assertRaises(UserWarning) as message:
            schedules = schedule_help.calc_schedule_opt(
                step_size=step_size,
                forecast=forecast,
                forecast_horizon=forecast_horizon,
                cs_data=cs_data,
                bss_data=bss_data,
                pv_data=pv_data,
                tariff=tariff,
                opt=optim_options,
            )
        self.assertTrue(str(message.exception) == test_string_warn)

    def test_calc_schedule_opt_options(self):
        step_size = 3600
        forecast = {
            "p_res": np.array([1, -1, 1, -1]),
            "pv_0": {"p": [-1, -2, 0, -1]},
            "ev_0": {
                "consumption": [0, 0, 0.005, 0],
                "appearance": [True, False, False, True],
            },
        }
        forecast_horizon = 4
        cs_data = {
            "cs_0": CSData(
                charge_efficiency=1,
                discharge_efficiency=1,
                p_rated=5,
                ev_data={
                    "ev_0": EVData(
                        charge_efficiency=1,
                        dcharge_efficiency=1,
                        e_max=10,
                        e_bat=8,
                        soc_min=1 / 10,
                        p_nom_charge_max=5,
                        p_nom_discharge_max=0,
                    )
                },
            )
        }
        bss_data = {
            "bss_0": BSSData(
                e_bat=1,
                e_bat_usable=4,
                p_rated_discharge_max=-4,
                p_rated_charge_max=4,
                soc_min=0,
                charge_efficiency=1,
                discharge_efficiency=1,
            )
        }
        pv_data = {"pv_0": pv}
        tariff = TariffSignal(elec_price=0.35, feedin_tariff=0.07)
        optim_options = OptimOptions(
            ev_direct_cha=False,
            ev_end_energy_penalty=None,
            bss_direct_cha=False,
            bss_end_energy_incentive=False,
        )

        schedules = schedule_help.calc_schedule_opt(
            step_size=step_size,
            forecast=forecast,
            forecast_horizon=forecast_horizon,
            cs_data=cs_data,
            bss_data=bss_data,
            pv_data=pv_data,
            tariff=tariff,
            opt=optim_options,
        )

        # retrieve results
        result_cs = [0, 0, 0, 0]  # no need to recharge at the end - would cost
        result_bss = [-1, 1, -1, 0]  # bss first charges at 2 (to avoid peak gen)
        #  and discharges at 4 (to avoid peak dem)
        self.assertListEqual(list(forecast["pv_0"]["p"]), list(schedules["pv_0"]["p"]))
        self.assertListEqual(list(result_cs), list(schedules["cs_0"]["p"]))
        self.assertListEqual(list(result_bss), list(schedules["bss_0"]["p"]))


if __name__ == "__main__":
    suite = unittest.TestLoader().loadTestsFromTestCase(TestEMS)
    unittest.TextTestRunner(verbosity=0).run(suite)
