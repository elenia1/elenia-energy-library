"""
Unittest for EMS of eELib

| Author: elenia@TUBS
| Copyright 2024 elenia
| This file is part of eELib, which is free software under the terms of the GNU GPL Version 3.
"""

import unittest
import time

from eelib.core.control.hems.hems_model.hems_default import HEMS_default
from eelib.data import CSData, BSSData, PVData, HPData, TariffSignal


class TestEMS(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        pass

    @classmethod
    def tearDownClass(cls):
        pass

    def setUp(self):
        print(f"[START] {self.id()}")
        self.start = time.time()

    def tearDown(self):
        t = time.time() - self.start
        print(f"[END] {self.id()}: {t:.4f}")

    def test_add_controlled_entitity(self):
        test_entity_dict_household = {
            "h_0": {
                "eid": "h_0",
                "type": "bl",
                "full_id": "S.h_0",
            }
        }
        test_entity_dict_pv = {
            "p_0": {
                "eid": "p_0",
                "type": "pv",
                "full_id": "S.p_0",
            }
        }
        test_entity_dict_pv_extra = {
            "p_1": {
                "eid": "p_1",
                "type": "pv",
                "full_id": "S.p_1",
            }
        }

        test_control_model = HEMS_default(eid="test_ems_0")
        # check lists are empty
        self.assertEqual(len(test_control_model.contr_entities_by_eid), 0)
        self.assertEqual(len(test_control_model.contr_fullid_list_by_type), 0)
        # check adding first entry
        test_control_model.add_controlled_entity(test_entity_dict_household)
        self.assertEqual(len(test_control_model.contr_entities_by_eid), 1)
        self.assertEqual(len(test_control_model.contr_fullid_list_by_type), 1)
        # check adding specific entry
        test_control_model.add_controlled_entity(test_entity_dict_pv)
        self.assertEqual(len(test_control_model.contr_fullid_list_by_type), 2)
        self.assertTrue(
            "p_0" in test_control_model.contr_entities_by_eid.keys()
            and test_control_model.contr_entities_by_eid["p_0"]
            == {"full_id": "S.p_0", "etype": "pv"}
        )
        self.assertTrue("pv" in test_control_model.contr_fullid_list_by_type)
        self.assertTrue("S.p_0" in test_control_model.contr_fullid_list_by_type["pv"])
        # test adding second pv
        test_control_model.add_controlled_entity(test_entity_dict_pv_extra)
        self.assertEqual(len(test_control_model.contr_fullid_list_by_type), 2)

    def test_get_components_by_type(self):
        test_entity_dict_household = {
            "h_0": {
                "eid": "h_0",
                "type": "load",
                "full_id": "S.h_0",
            }
        }
        test_entity_dict_pv = {
            "p_0": {
                "eid": "p_0",
                "type": "pv",
                "full_id": "S.p_0",
            }
        }
        test_entity_dict_pv_extra = {
            "p_1": {
                "eid": "p_1",
                "type": "pv",
                "full_id": "S.p_1",
            }
        }

        test_control_model = HEMS_default(eid="test_ems_0")
        test_control_model.add_controlled_entity(test_entity_dict_household)
        test_control_model.add_controlled_entity(test_entity_dict_pv)
        test_control_model.add_controlled_entity(test_entity_dict_pv_extra)

        pv_dev = test_control_model._get_component_by_type("pv")
        load_dev = test_control_model._get_component_by_type("load")
        self.assertEqual(len(pv_dev), 2)
        self.assertEqual(len(load_dev), 1)
        self.assertTrue("p_1" in pv_dev)

    def test_thermal_energy_demand_adaption(self):
        # test for 15 min time resolution
        test_control_model = HEMS_default(eid="test_ems_0", step_size=900)
        test_control_model.p_th_balance = 1000
        test_control_model.step(timestep=1)
        self.assertAlmostEqual(test_control_model.e_th, 1000 / 4)
        test_control_model.p_th_balance = -10000
        test_control_model.step(timestep=2)
        self.assertAlmostEqual(test_control_model.e_th, (1000 - 10000) / 4)

        # test for different step size: 1 hour time resolution
        test_control_model = HEMS_default(eid="test_ems_0", step_size=3600)
        test_control_model.p_th_balance = 1000
        test_control_model.step(timestep=0)  # no adaption for same timestep
        self.assertAlmostEqual(test_control_model.e_th, 0)
        test_control_model.p_th_balance = 1000
        test_control_model.step(timestep=1)
        self.assertAlmostEqual(test_control_model.e_th, 1000)
        test_control_model.p_th_balance = 10000
        test_control_model.step(timestep=2)
        self.assertAlmostEqual(test_control_model.e_th, 11000)

    def test_p_balance(self):
        test_control_model = HEMS_default(eid="test_ems_0")

        # first with no devices: p is zero
        test_control_model.step(timestep=0)
        self.assertAlmostEqual(test_control_model.p_balance, 0)

        # now add all device types with info
        test_control_model.contr_fullid_list_by_type = {"load": {"l_0": "test"}}
        test_control_model.p = {"l_0": 1000}
        test_control_model.bss_data = {"bss_0": BSSData(p=2000)}
        test_control_model.pv_data = {"pv_0": PVData(p=-5000)}
        test_control_model.hp_data = {"hp_0": HPData(p=500)}
        test_control_model.cs_data = {"cs_0": CSData(p=1000)}

        test_control_model.step(timestep=0)
        self.assertAlmostEqual(test_control_model.p_balance, -500)

        # now with just some devices to calculate ancillary values
        test_control_model.contr_fullid_list_by_type = {"load": {"l_0": "test"}}
        test_control_model.p = {"l_0": 2000}
        test_control_model.q = {"l_0": 0}
        test_control_model.bss_data = {"bss_0": BSSData(p=-1000, p_min=-2000, p_max=2000)}
        test_control_model.pv_data = {"pv_0": PVData(p=None)}
        test_control_model.hp_data = {
            "hp_0": HPData(p=500, p_min=500, p_max=1000, p_th=-2000, q=200)
        }
        test_control_model.cs_data = {}

        test_control_model.step(timestep=0)
        self.assertAlmostEqual(test_control_model.p_balance, 1500)
        self.assertAlmostEqual(test_control_model.p_demand, 2500)
        self.assertAlmostEqual(test_control_model.p_generation, -1000)
        self.assertAlmostEqual(test_control_model.p_min, 500)
        self.assertAlmostEqual(test_control_model.p_max, 5000)
        self.assertAlmostEqual(test_control_model.p_th_balance, -2000)
        self.assertAlmostEqual(test_control_model.q_balance, 200)

        # now with thermal demand values
        test_control_model.p_th_water = None
        test_control_model.p_th_room = None
        test_control_model.step(timestep=0)
        self.assertAlmostEqual(test_control_model.p_th_dem, 0)
        self.assertAlmostEqual(test_control_model.p_th_balance, -2000)
        test_control_model.p_th_water = {"hw_0": 1000}
        test_control_model.p_th_room = {"rh_0": 300}
        test_control_model.step(timestep=0)
        self.assertAlmostEqual(test_control_model.p_th_dem, 1300)
        self.assertAlmostEqual(test_control_model.p_th_balance, -2000 + 1300)

    def test_p_set_pv_lib(self):
        test_control_model = HEMS_default(eid="test_ems_0")
        test_control_model.add_controlled_entity(
            entities_dict={
                "p_0": {"full_id": "p_0", "type": "PVLib", "eid": "p_0"},
                "p_1": {"full_id": "p_1", "type": "PVLibExact", "eid": "p_1"},
                "p_2": {"full_id": "p_2", "type": "PVLib", "eid": "p_2"},
                "p_3": {"full_id": "p_3", "type": "PVLib", "eid": "p_3"},
            }
        )
        test_control_model.pv_data = {
            "p_0": PVData(p=-5000, p_max=-5000, p_min=0),
            "p_1": PVData(p=-10000, p_max=-10000, p_min=0),
            "p_3": PVData(p=None, p_max=None, p_min=0),
        }
        test_control_model.p_balance = -15000

        test_control_model._set_pv_max()
        self.assertAlmostEqual(test_control_model.p_set_pv["p_0"], -5000)
        self.assertAlmostEqual(test_control_model.p_set_pv["p_1"], -10000)
        self.assertTrue("p_2" not in test_control_model.p_set_pv)
        self.assertAlmostEqual(test_control_model.p_set_pv["p_3"], 0)
        self.assertAlmostEqual(test_control_model.p_balance, -15000)

    def test_p_set_hp_with_limits(self):
        test_control_model = HEMS_default(eid="test_ems_0")
        test_control_model.contr_fullid_list_by_type = {
            "Heatpump": {"hp_0": "test", "hp_1": "test"}
        }
        p_th_hp_init = -1000
        test_control_model.hp_data = {
            "hp_0": HPData(p_th=p_th_hp_init, p_th_min=-0, p_th_min_on=-1000, p_th_max=-2000),
        }

        test_control_model.e_th = 0
        test_control_model.p_th_need_step = -1500 - p_th_hp_init
        test_control_model._set_hp_p_th()
        self.assertAlmostEqual(test_control_model.p_th_set_heatpump["hp_0"], -1500)
        self.assertTrue("hp_1" not in test_control_model.p_th_set_heatpump)
        test_control_model.p_th_need_step = -500 - p_th_hp_init
        test_control_model._set_hp_p_th()
        self.assertAlmostEqual(test_control_model.p_th_set_heatpump["hp_0"], -1000)
        test_control_model.p_th_need_step = -2500 - p_th_hp_init
        test_control_model._set_hp_p_th()
        self.assertAlmostEqual(test_control_model.p_th_set_heatpump["hp_0"], -2000)
        test_control_model.p_th_need_step = 0 - p_th_hp_init
        test_control_model._set_hp_p_th()
        self.assertAlmostEqual(test_control_model.p_th_set_heatpump["hp_0"], 0)

    def test_charging_station_max_p(self):
        test_control_model = HEMS_default(eid="test_ems_0", cs_strategy="max_p")
        test_control_model.contr_fullid_list_by_type = {
            "ChargingStation": {"cs_0": "test", "cs_1": "test", "cs_2": "test"}
        }
        test_control_model.cs_data = {
            "cs_0": CSData(p=4000, p_min=0, p_max=10000),
            "cs_2": CSData(p=None, p_min=0, p_max=0),
        }
        test_control_model.p_balance = 4000

        test_control_model._set_cs_p()
        self.assertAlmostEqual(test_control_model.p_set_charging_station["cs_0"], 10000)
        self.assertTrue("cs_1" not in test_control_model.p_set_charging_station)
        self.assertAlmostEqual(test_control_model.p_balance, 10000)

    def test_storage_surplus(self):
        test_control_model = HEMS_default(eid="test_ems_0", bss_strategy="surplus")
        p_bss_init = 1000
        test_control_model.contr_fullid_list_by_type = {
            "HouseholdCSV": {"l_0": "test"},
            "PvCSV": {"p_0": "test"},
            "BSS": {"b_0": "test", "b_1": "test", "b_2": "test"},
        }
        test_control_model.bss_data = {
            "b_0": BSSData(p=p_bss_init, p_max=5000, p_min=-5000),
            "b_2": BSSData(p=None),
        }

        # test surplus charging
        test_control_model.p_balance = -2000 + p_bss_init
        test_control_model._set_bss_p()
        self.assertAlmostEqual(test_control_model.p_set_storage["b_0"], 2000)
        self.assertTrue("b_1" not in test_control_model.p_set_storage)
        self.assertAlmostEqual(test_control_model.p_set_storage["b_2"], 0)
        self.assertAlmostEqual(test_control_model.p_balance, 0)

        # test surplus discharging
        test_control_model.p_balance = 3000 + p_bss_init
        test_control_model._set_bss_p()
        self.assertAlmostEqual(test_control_model.p_set_storage["b_0"], -3000)
        self.assertAlmostEqual(test_control_model.p_balance, 0)

    def test_storage_reduce_curtailment(self):
        test_control_model = HEMS_default(eid="test_ems_0", bss_strategy="reduce_curtailment")
        p_bss_init = 1000
        test_control_model.contr_fullid_list_by_type = {
            "HouseholdCSV": {"l_0": "test"},
            "PvCSV": {"p_0": "test"},
            "BSS": {"b_0": "test"},
        }
        test_control_model.bss_data = {"b_0": BSSData(p=p_bss_init, p_max=5000, p_min=-5000)}

        # test curtailed charging
        test_control_model.p_balance = -4000 + p_bss_init
        test_control_model._set_bss_p()
        self.assertAlmostEqual(test_control_model.p_set_storage["b_0"], 0.6 * 5000)

        # test (un)curtailed discharging
        test_control_model.p_balance = 3000 + p_bss_init
        test_control_model._set_bss_p()
        self.assertAlmostEqual(test_control_model.p_set_storage["b_0"], -3000)

    def test_cs_bss_strategy_errors(self):
        test_ems = HEMS_default(
            eid="test_ems_0",
            step_size=3600,
            cs_strategy="test_not_avail",
            bss_strategy="test_not_avail",
        )
        test_ems.timestep = 0

        # now check with devices and their schedules
        test_ems.contr_fullid_list_by_type = {
            "BSS": {"bss_0": "test"},
            "ChargingStation": {"cs_0": "test"},
        }
        test_ems.bss_data = {"bss_0": BSSData(p_max=-3000, p_min=3000)}
        test_ems.cs_data = {"cs_0": CSData(p_max=11000, p_min=0)}

        # for bss
        with self.assertRaises(ValueError) as message:
            test_ems._set_bss_p()
        str_error = "BSS-Strategy test_not_avail not implemented for HEMS"
        self.assertTrue(str(message.exception) == str_error)

        # for cs
        with self.assertRaises(ValueError) as message:
            test_ems._set_cs_p()
        str_error = "CS-Strategy test_not_avail not implemented for HEMS"
        self.assertTrue(str(message.exception) == str_error)

    def test_calc_financial_output(self):
        test_control_model = HEMS_default(eid="test_ems_0", bss_strategy="reduce_curtailment")

        # with no tariff signal set
        test_control_model.p_balance = 10
        test_control_model._calc_financial_output()
        self.assertEqual(test_control_model.cost, 0)
        self.assertEqual(test_control_model.profit, 0)

        # with tariff signal set
        test_control_model.tariff = TariffSignal(
            bool_is_list=False, elec_price=0.3, feedin_tariff=0.1, steps=[]
        )
        test_control_model._handle_incoming_signals()

        # positive power (demand) - cost
        test_control_model.p_balance = 10
        test_control_model._calc_financial_output()
        self.assertEqual(test_control_model.cost, 3)
        self.assertEqual(test_control_model.revenue, 0)
        self.assertEqual(test_control_model.profit, -3)
        # negative power (generation) - revenue & profit
        test_control_model.p_balance = -10
        test_control_model._calc_financial_output()
        self.assertEqual(test_control_model.cost, 0)
        self.assertEqual(test_control_model.revenue, 1)
        self.assertEqual(test_control_model.profit, 1)

        # with tariff signal including list values
        test_control_model.tariff = TariffSignal(
            bool_is_list=True, elec_price=[0.3, 0.2, 0.3], feedin_tariff=0.1, steps=[0, 1, 2]
        )
        test_control_model._handle_incoming_signals()
        self.assertIsNone(test_control_model.tariff_step)


if __name__ == "__main__":
    suite = unittest.TestLoader().loadTestsFromTestCase(TestEMS)
    unittest.TextTestRunner(verbosity=0).run(suite)
