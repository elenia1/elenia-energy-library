"""
Unittest for EMS of eELib

| Author: elenia@TUBS
| Copyright 2024 elenia
| This file is part of eELib, which is free software under the terms of the GNU GPL Version 3.
"""

import unittest
import time

from eelib.core.control.hems.hems_model.hems_forecast_default import HEMS_forecast_default
from eelib.core.control.hems.hems_model.hems_forecast_opt import HEMS_forecast_opt
from eelib.data import PVData, HPData, BSSData, CSData, EVData


class TestEMS(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        pass

    @classmethod
    def tearDownClass(cls):
        pass

    def setUp(self):
        print(f"[START] {self.id()}")
        self.start = time.time()

    def tearDown(self):
        t = time.time() - self.start
        print(f"[END] {self.id()}: {t:.4f}")

    def test_init_error(self):
        with self.assertRaises(ValueError) as message:
            _ = HEMS_forecast_default(
                eid="test_ems_0",
                step_size=3600,
                **{
                    "forecast_type": "household_only",
                    "forecast_horizon_hours": 12,
                    "forecast_frequency_hours": 24,
                },
            )
        str_error = "For HEMS (test_ems_0) the forecast frequency cannot be > horizon."
        self.assertTrue(str(message.exception) == str_error)

    def test_request_forecast(self):
        fc_start = 24
        fc_length = 48
        test_ems = HEMS_forecast_default(
            eid="test_ems_0",
            step_size=3600,
            **{
                "forecast_type": "household_only",
                "forecast_horizon_hours": fc_length,
                "forecast_frequency_hours": 24,
            },
        )

        test_ems.timestep = fc_start

        # check calculation of no forecast
        test_ems.bool_forecast_now = False
        test_ems._request_forecast()
        self.assertEqual(test_ems.forecast_request, {})
        test_ems.bool_forecast_now = True  # no devices yet added
        test_ems._request_forecast()
        self.assertEqual(test_ems.forecast_request, {})

        # check calculation of one single forecast
        test_ems.contr_fullid_list_by_type = {"HouseholdCSV": {"l_0": "test"}}
        test_ems._request_forecast()
        self.assertEqual(len(test_ems.forecast_request), 1)
        self.assertEqual(test_ems.forecast_request["l_0"]["attr"], ["p"])
        self.assertEqual(
            test_ems.forecast_request["l_0"]["t"], range(fc_start, fc_start + fc_length)
        )

        # check no calculation if forecast already existent
        test_ems.forecast = {"l_0": {"p": [0, 0], "state": [0, 0]}}
        test_ems._request_forecast()
        self.assertEqual(test_ems.forecast_request, {})
        test_ems.forecast = {}  # delete forecast for next assessment

        # check calculation of multiple forecasts: none for bss, as it is not read-only
        test_ems.contr_fullid_list_by_type = {
            "HouseholdCSV": {"l_0": "test"},
            "PvCSV": {"p_0": "test"},
            "BSS": {"b_0": "test"},
        }
        test_ems._request_forecast()
        self.assertEqual(len(test_ems.forecast_request), 2)

    def test_set_power_in_limits(self):
        test_ems = HEMS_forecast_default(eid="test_ems_0")
        self.assertEqual(test_ems._set_power_in_limits(power=0, p_min=-1000, p_max=1000), 0)
        self.assertEqual(test_ems._set_power_in_limits(power=2000, p_min=-1000, p_max=1000), 1000)
        self.assertEqual(test_ems._set_power_in_limits(power=-2000, p_min=-1000, p_max=1000), -1000)
        self.assertEqual(test_ems._set_power_in_limits(power=0, p_min=None, p_max=1000), 0)
        self.assertEqual(test_ems._set_power_in_limits(power=0, p_min=-1000, p_max=None), 0)
        self.assertEqual(test_ems._set_power_in_limits(power=0, p_min=None, p_max=None), 0)
        self.assertEqual(test_ems._set_power_in_limits(power=2000, p_min=None, p_max=1000), 1000)
        self.assertEqual(test_ems._set_power_in_limits(power=-2000, p_min=-1000, p_max=None), -1000)

    def test_set_schedule(self):
        fc_length = 48
        step = 20 + fc_length
        test_ems = HEMS_forecast_default(
            eid="test_ems_0",
            step_size=3600,
            **{
                "forecast_type": "household_only",
                "forecast_horizon_hours": fc_length,
                "forecast_frequency_hours": 24,
            },
        )
        test_ems.timestep = step

        # first check without devices: no set values
        test_ems.contr_fullid_list_by_type = {}
        test_ems._set_schedule_values()
        self.assertEqual(test_ems.p_set_pv, {})
        self.assertEqual(test_ems.p_th_set_heatpump, {})
        self.assertEqual(test_ems.p_set_charging_station, {})
        self.assertEqual(test_ems.p_set_storage, {})
        test_ems.contr_fullid_list_by_type = {
            "PVLib": {},
            "Heatpump": {},
            "ChargingStation": {},
            "BSS": {},
        }
        test_ems._set_schedule_values()
        self.assertEqual(test_ems.p_set_pv, {})
        self.assertEqual(test_ems.p_th_set_heatpump, {})
        self.assertEqual(test_ems.p_set_charging_station, {})
        self.assertEqual(test_ems.p_set_storage, {})

        # now check with devices and their schedules
        test_ems.contr_fullid_list_by_type = {
            "PVLib": {"pv_0": "test"},
            "Heatpump": {"hp_0": "test"},
            "BSS": {"bss_0": "test"},
            "ChargingStation": {"cs_0": "test"},
        }
        test_ems.pv_data = {"pv_0": PVData(p_max=-5000, p_min=0)}
        test_ems.bss_data = {"bss_0": BSSData(p_max=-3000, p_min=3000)}
        test_ems.cs_data = {"cs_0": CSData(p_max=11000, p_min=0)}
        test_ems.hp_data = {"hp_0": HPData(p_th_max=-5000, p_th_min=0, p_th_min_on=-2500)}

        # for pv system
        test_ems.schedule = {"pv_0": {"p": [-6000] * fc_length}}
        test_ems._set_schedule_values_pv()
        self.assertEqual(test_ems.p_set_pv["pv_0"], -5000)

        # for bss
        test_ems.schedule = {"bss_0": {"p": [4000] * fc_length}}
        test_ems._set_schedule_values_bss()
        self.assertEqual(test_ems.p_set_storage["bss_0"], 3000)

        # for cs
        test_ems.schedule = {"cs_0": {"p": [-1000] * fc_length}}
        test_ems._set_schedule_values_cs()
        self.assertEqual(test_ems.p_set_charging_station["cs_0"], 0)

        # for pv and hp together
        test_ems.contr_fullid_list_by_type = {
            "PVLib": {"pv_0": "test"},
            "Heatpump": {"hp_0": "test"},
        }
        test_ems.schedule = {
            "pv_0": {"p": [-6000] * fc_length},
            "hp_0": {"p_th": [-1000] * fc_length},
        }
        test_ems._set_schedule_values()
        self.assertEqual(test_ems.p_set_pv["pv_0"], -5000)
        self.assertEqual(test_ems.p_th_set_heatpump["hp_0"], -2500)

    def test_hems_forecast_def_step(self):
        fc_length = 2
        test_ems = HEMS_forecast_default(
            eid="test_ems_0",
            step_size=3600,
            **{
                "forecast_type": "household_only",
                "forecast_horizon_hours": fc_length,
                "forecast_frequency_hours": 2,
            },
        )
        test_ems.pv_data = {"pv_0": PVData(p_max=-1500, p_min=0)}

        # first without forecast
        test_ems.forecast = {}
        test_ems.step(timestep=0)
        self.assertEqual(test_ems.schedule, {})

        # now create forecasts for all devices
        test_ems.forecast = {
            "pv_0": {"p": [-1000, -2000]},
            "baseload": {"p": [500, 500]},
            "baseload_th": {"p_th_water": [1000, 500]},
        }
        test_ems.step(timestep=0)
        self.assertListEqual(list(test_ems.schedule["p_res"]), [-500, -1500])

        # now also with corresponding devices
        test_ems.contr_fullid_list_by_type = {"PVLib": {"pv_0": "test"}}
        test_ems.step(timestep=0)
        self.assertFalse(test_ems.schedule == {})
        self.assertListEqual(list(test_ems.forecast["p_res"]), [500, 500])
        self.assertListEqual(list(test_ems.forecast["p_th_dem"]), [1000, 500])
        self.assertListEqual(list(test_ems.schedule["p_res"]), [-500, -1500])
        self.assertListEqual(list(test_ems.schedule["pv_0"]["p"]), [-1000, -2000])

        # set values for first step
        self.assertEqual(test_ems.p_set_pv["pv_0"], -1000)

        # for second step, power limit of pv has to be reckognized
        test_ems.step(timestep=1)
        self.assertEqual(test_ems.p_set_pv["pv_0"], -1500)

    def test_hems_forecast_def_step_with_dev(self):
        fc_length = 2
        test_ems = HEMS_forecast_default(
            eid="test_ems_0",
            step_size=3600,
            **{
                "forecast_type": "household_only",
                "forecast_horizon_hours": fc_length,
                "forecast_frequency_hours": 2,
            },
        )
        test_ems.cs_data = {
            "cs_0": CSData(
                p=0,
                p_min=0,
                p_max=1000,
                discharge_efficiency=1,
                charge_efficiency=1,
                p_rated=1000,
                ev_data={
                    "ev_0": EVData(
                        p=0,
                        p_max=1000,
                        p_min=0,
                        soc_min=0,
                        e_max=3000,
                        p_nom_charge_max=1000,
                        p_nom_discharge_max=0,
                        dcharge_efficiency=1,
                        charge_efficiency=1,
                        e_bat=1500,
                        appearance=True,
                    )
                },
            )
        }
        test_ems.hp_data = {
            "hp_0": HPData(
                p=0,
                p_min=0,
                p_max=500,
                p_rated_th=1000,
                p_min_th_rel=1,
                time_min=3600,
                time_on=0,
                time_off=3600,
                state="off",
                cop=2,
                p_th=0,
                p_th_min=0,
                p_th_min_on=1000,
                p_min_on=500,
                p_th_max=1000,
                q=0,
            )
        }
        test_ems.bss_data = {
            "bss_0": BSSData(
                p=0,
                p_min=-1000,
                p_max=1000,
                e_bat_rated=2000,
                p_rated_charge_max=1000,
                p_rated_discharge_max=-1000,
                loss_rate=0,
                self_discharge_step=0,
                dod_max=1,
                soc_min=0,
                charge_efficiency=1,
                discharge_efficiency=1,
                e_bat=1000,
                e_bat_usable=2000,
            )
        }

        # now create forecasts for all devices
        test_ems.forecast = {
            "ev_0": {"consumption": [-1000, 0], "appearance": [False, True]},
            "baseload": {"p": [500, 500]},
            "baseload_th": {"p_th_water": [1000, 0]},
        }
        # add corresponding devices
        test_ems.contr_fullid_list_by_type = {
            "BSS": {"bss_0": "test"},
            "Heatpump": {"hp_0": "test"},
            "ChargingStation": {"cs_0": "test"},
        }

        test_ems.step(timestep=0)
        self.assertFalse(test_ems.schedule == {})
        self.assertListEqual(list(test_ems.forecast["p_res"]), [500, 500])
        self.assertListEqual(list(test_ems.forecast["p_th_dem"]), [1000, 0])
        self.assertListEqual(list(test_ems.schedule["cs_0"]["p"]), [0, 1000])
        self.assertListEqual(list(test_ems.schedule["hp_0"]["p_el"]), [500, 0])
        self.assertListEqual(list(test_ems.schedule["bss_0"]["p"]), [-1000, 0])
        self.assertListEqual(list(test_ems.schedule["p_res"]), [0, 1500])

    def test_hems_forecast_opt_step(self):
        fc_length = 2
        test_ems = HEMS_forecast_opt(
            eid="test_ems_0",
            step_size=3600,
            **{
                "forecast_type": "household_only",
                "forecast_horizon_hours": fc_length,
                "forecast_frequency_hours": 2,
            },
        )

        # first without forecast
        test_ems.forecast = {}
        test_ems.step(timestep=0)
        self.assertEqual(test_ems.schedule, {})

        # now create forecasts
        test_ems.forecast = {
            "pv_csv_0": {"p": [-1000, -2000]},
            "baseload": {"p": [500, 500]},
            "baseload_th": {"p_th_water": [1000, 500]},
        }
        test_ems.step(timestep=0)
        self.assertFalse(test_ems.schedule == {})
        self.assertListEqual(list(test_ems.forecast["p_res"]), [-500, -1500])
        self.assertListEqual(list(test_ems.forecast["p_th_dem"]), [1000, 500])
        self.assertListEqual(list(test_ems.schedule["p_res"]), [-500, -1500])


if __name__ == "__main__":
    suite = unittest.TestLoader().loadTestsFromTestCase(TestEMS)
    unittest.TextTestRunner(verbosity=0).run(suite)
