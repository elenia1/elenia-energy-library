"""
Unittest for EMS of eELib

| Author: elenia@TUBS
| Copyright 2024 elenia
| This file is part of eELib, which is free software under the terms of the GNU GPL Version 3.
"""

import unittest
import time

from eelib.core.control.hems.hems_model.hems_gcp_aggregator import GCP_Aggregator_HEMS
from eelib.data import BSSData


class TestEMS(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        pass

    @classmethod
    def tearDownClass(cls):
        pass

    def setUp(self):
        print(f"[START] {self.id()}")
        self.start = time.time()

    def tearDown(self):
        t = time.time() - self.start
        print(f"[END] {self.id()}: {t:.4f}")

    def test_p_balance(self):
        test_control_model = GCP_Aggregator_HEMS(eid="test_ems_0")
        test_control_model.contr_fullid_list_by_type = {
            "load": {"l_0": "test"},
            "pv": {"p_0": "test"},
        }
        test_control_model.p = {"l_0": 1000, "p_0": -5000.0}

        test_control_model.step(timestep=0)
        self.assertAlmostEqual(test_control_model.p_balance, -4000)

    def test_no_set_values(self):
        test_control_model = GCP_Aggregator_HEMS(eid="test_ems_0")
        test_control_model.contr_fullid_list_by_type = {
            "load": {"l_0": "test"},
            "pv": {"p_0": "test"},
        }
        test_control_model.p = {"l_0": 5000, "p_0": -1000.0}
        test_control_model.add_controlled_entity(
            entities_dict={"bss_0": {"full_id": "bss_0", "type": "BSS", "eid": "bss_0"}}
        )
        test_control_model.bss_data = {"bss_0": BSSData(p=0, p_max=5000, p_min=5000)}

        test_control_model.step(timestep=0)
        self.assertAlmostEqual(test_control_model.p_set_storage, {})
        self.assertAlmostEqual(test_control_model.p_balance, 4000)


if __name__ == "__main__":
    suite = unittest.TestLoader().loadTestsFromTestCase(TestEMS)
    unittest.TextTestRunner(verbosity=0).run(suite)
