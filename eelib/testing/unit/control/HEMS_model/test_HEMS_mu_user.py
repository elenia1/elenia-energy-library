"""
Unittest for an the user-based multi-use HEMS. As part of the publication:
Henrik Wagner, Constantin von Luetzow, Marcel Luedecke, Michel Meinert, Bernd Engel
"Empowering Collective Self-Consumption in Multi-Family Houses: User-Based Multi-Use of Residential
Battery Storage Systems", 23rd Wind & Solar Integration Workshop 2024, Helsinki, Finland, DOI: tba.

The simulations were computed using eELib's version from the "WSIS24_user-based_mu" tag.
The used model configurations, simulation data and results can be found on Zenodo,
DOI: 10.5281/zenodo.13821117.

| Author: elenia@TUBS
| Copyright 2024 elenia
| This file is part of eELib, which is free software under the terms of the GNU GPL Version 3.
"""

import unittest
import time

from eelib.core.control.hems.hems_model.hems_mu_user import HEMS_mu_user
from eelib.data import BSSData, PVData
from math import floor


class TestEMS(unittest.TestCase):
    MU_STRATEGIES = ("static_eq", "static", "dynamic_charging", "dynamic_full")

    @classmethod
    def setUpClass(cls):
        pass

    @classmethod
    def tearDownClass(cls):
        pass

    def setUp(self):
        print(f"[START] {self.id()}")
        self.start = time.time()

    def tearDown(self):
        t = time.time() - self.start
        print(f"[END] {self.id()}: {t:.4f}")

    # test init warnings and errors
    def test_warn_bss_strategy(self):
        for strategy in self.MU_STRATEGIES:
            test_ems = HEMS_mu_user(
                eid="test_ems_0",
                mu_user_op_strategy=strategy,
                bss_strategy="unknown_strategy",
            )
            self.assertEqual(test_ems.bss_strategy, "surplus")

    # test per method
    def test_check_models(self):
        test_ems = HEMS_mu_user(
            eid="test_ems_0",
            mu_user_op_strategy="dynamic_full",
        )

        # adapt, if a type is validated as fully supported
        unsupported_types = (
            "EV",
            "ChargingStation",
            "HouseholdThermalCSV",
            "Heatpump",
        )
        supported_types = (
            "HouseholdCSV",
            "PvCSV",
            "PVLib",
            "PVLibExact",
            "ChargingStationCSV",
            "HeatpumpCSV",
            "BSS",
            "RetailElectricityProvider",
        )

        # an Assertion Error is expected from unsupported types
        for type in unsupported_types:
            test_ems.contr_fullid_list_by_type = {type: "test"}
            with self.assertRaises(AssertionError):
                test_ems._check_models()

        # no Error is expected from supported types
        for type in supported_types:
            test_ems.contr_fullid_list_by_type = {type: "test"}
            test_ems._check_models()

    def test_init_mu_data(self):
        # for dynamic_full only test update of demand_full_ids
        test_ems = HEMS_mu_user(
            eid="test_ems_0",
            mu_user_op_strategy="dynamic_full",
        )
        test_ems.contr_fullid_list_by_type = {
            "HouseholdCSV": ["hh_0", "hh_1"],
            "PvCSV": ["PV_0"],
            "ChargingStationCSV": ["CS_0"],
            "BSS": ["BSS_0"],
            "HeatpumpCSV": ["HP_0"],
        }
        test_ems._init_mu_data()
        self.assertEqual(test_ems.demand_full_ids, set(["hh_1", "hh_0", "CS_0", "HP_0"]))

        # for other strategies test updating of energy values of VSPs
        test_ems = HEMS_mu_user(
            eid="test_ems_0",
            mu_user_op_strategy="static",
        )
        test_ems.contr_fullid_list_by_type = {"HouseholdCSV": ["hh_0", "hh_1"]}
        test_ems._init_mu_data()
        self.assertEqual(test_ems.mu_data["hh_0"]["e_bss"], 0.0)
        self.assertEqual(test_ems.mu_data["hh_1"]["e_bss_delta"], 0.0)
        self.assertEqual(test_ems.mu_data["hh_0"]["e_bss_usable"], 0.0)
        self.assertEqual(test_ems.mu_data["hh_1"]["e_bss_usable"], 0.0)

    def test_assign_charging_stations(self):
        test_ems = HEMS_mu_user(
            eid="test_ems_0",
            mu_user_op_strategy="dynamic_full",
        )

        test_ems.contr_fullid_list_by_type = {
            "HouseholdCSV": ["hh_0", "hh_1"],
            "PvCSV": ["PV_0"],
            "ChargingStationCSV": ["CS_0"],
            "BSS": ["BSS_0"],
            "HeatpumpCSV": ["HP_0"],
        }

        ## one CS ##
        test_ems.mu_data = {
            "hh_0": {"all_key": 0.0, "cs_full_ids": []},
            "hh_1": {"all_key": 0.0, "cs_full_ids": []},
        }
        test_ems._assign_charging_stations()
        self.assertEqual(test_ems.mu_data["hh_0"]["cs_full_ids"], ["CS_0"])
        self.assertEqual(test_ems.mu_data["hh_1"]["cs_full_ids"], [])

        ## more CS than HHs ##
        test_ems.contr_fullid_list_by_type.update({"ChargingStationCSV": ["CS_0", "CS_1", "CS_2"]})
        test_ems.mu_data = {
            "hh_0": {"all_key": 0.0, "cs_full_ids": []},
            "hh_1": {"all_key": 0.0, "cs_full_ids": []},
        }
        test_ems._assign_charging_stations()
        self.assertEqual(test_ems.mu_data["hh_0"]["cs_full_ids"], ["CS_0", "CS_2"])
        self.assertEqual(test_ems.mu_data["hh_1"]["cs_full_ids"], ["CS_1"])

    def test_calc_all_key_hp(self):
        test_ems = HEMS_mu_user(
            eid="test_ems_0",
            mu_user_op_strategy="static",
        )

        # check for Warning if no information about residents in hh_full_id
        test_ems.contr_fullid_list_by_type = {
            "HouseholdCSV": ["hh_0", "hh_1"],
            "PvCSV": ["PV_0"],
            "ChargingStationCSV": ["CS_0"],
            "BSS": ["BSS_0"],
            "HeatpumpCSV": ["HP_0"],
        }
        test_ems.mu_data = {
            "hh_0": {"num_ppl": None, "all_key_hp": 0},
            "hh_1": {"num_ppl": None, "all_key_hp": 0},
        }
        with self.assertRaises(ValueError):
            test_ems._calc_all_key_hp()

        # check for Warning if number of residents == 0
        test_ems.contr_fullid_list_by_type = {
            "HouseholdCSV": ["hh_0p_0", "hh_0p_1"],
            "PvCSV": ["PV_0"],
            "ChargingStationCSV": ["CS_0"],
            "BSS": ["BSS_0"],
            "HeatpumpCSV": ["HP_0"],
        }
        test_ems.mu_data = {
            "hh_0p_0": {"num_ppl": None, "all_key_hp": 0},
            "hh_0p_1": {"num_ppl": None, "all_key_hp": 0},
        }
        with self.assertRaises(ValueError):
            test_ems._calc_all_key_hp()

        # check if hp allocation keys are calculated correctly
        test_ems.contr_fullid_list_by_type = {
            "HouseholdCSV": ["hh_1p_0", "hh_3p_0"],
            "PvCSV": ["PV_0"],
            "ChargingStationCSV": ["CS_0"],
            "BSS": ["BSS_0"],
            "HeatpumpCSV": ["HP_0"],
        }
        test_ems.mu_data = {
            "hh_1p_0": {"num_ppl": None, "all_key_hp": 0},
            "hh_3p_0": {"num_ppl": None, "all_key_hp": 0},
        }
        test_ems._calc_all_key_hp()
        self.assertEqual(test_ems.mu_data["hh_1p_0"]["all_key_hp"], 1 / 4)
        self.assertEqual(test_ems.mu_data["hh_3p_0"]["all_key_hp"], 3 / 4)

    def test_allocate_hh_loads(self):
        test_ems = HEMS_mu_user(
            eid="test_ems_0",
            mu_user_op_strategy="static",
        )
        test_hhs = ["hh_2p_0", "hh_1p_0"]

        test_ems.contr_fullid_list_by_type = {
            "HouseholdCSV": test_hhs,
            "PvCSV": ["PV_0"],
            "ChargingStationCSV": ["CS_0", "CS_1"],
            "BSS": ["BSS_0"],
            "HeatpumpCSV": ["HP_0"],
        }

        for hh in test_hhs:
            test_ems.mu_data[hh] = {
                "cs_full_ids": [],
                "num_ppl": None,
                "all_key": 0.0,
                "all_key_hp": 0.0,
                "p_balance": 0.0,
                "p_demand": 0.0,
                "p_load": 0.0,
                "p_cs": 0.0,
                "p_pv": 0.0,
                "p_hp": 0.0,
                "p_bss": 0.0,
                "p_set_bss": 0.0,
                "p_trade_req": 0.0,
                "p_trade": 0.0,
            }
        test_ems.mu_data["hh_2p_0"]["all_key_hp"] = 2 / 3
        test_ems.mu_data["hh_1p_0"]["all_key_hp"] = 1 / 3
        test_ems.mu_data["hh_2p_0"]["cs_full_ids"] = ["CS_0"]
        test_ems.mu_data["hh_1p_0"]["cs_full_ids"] = ["CS_1"]
        test_ems.p = {"hh_2p_0": 100, "hh_1p_0": 50, "CS_0": 11000, "CS_1": 0, "HP_0": 5000}
        test_ems._allocate_hh_loads()
        self.assertEqual(test_ems.mu_data["hh_1p_0"]["p_load"], test_ems.p["hh_1p_0"])
        self.assertEqual(test_ems.mu_data["hh_2p_0"]["p_cs"], test_ems.p["CS_0"])
        self.assertEqual(
            test_ems.mu_data["hh_1p_0"]["p_hp"],
            test_ems.mu_data["hh_1p_0"]["all_key_hp"] * test_ems.p["HP_0"],
        )

        # check if electric load of hp is not allocated
        test_ems.mu_data["hh_2p_0"]["all_key_hp"] = 0.0
        test_ems.mu_data["hh_1p_0"]["all_key_hp"] = 0.0

        with self.assertRaises(ValueError):
            test_ems._allocate_hh_loads()

    def test_update_allocation_keys(self):
        test_ems = HEMS_mu_user(
            eid="test_ems_0",
            mu_user_op_strategy="static_eq",
        )
        test_hhs = ["hh_2p_0", "hh_1p_0"]
        for hh in test_hhs:
            test_ems.mu_data[hh] = {
                "cs_full_ids": [],
                "num_ppl": None,
                "all_key": 0.0,
                "all_key_hp": 0.0,
                "p_balance": 0.0,
                "p_demand": 0.0,
                "p_load": 0.0,
                "p_cs": 0.0,
                "p_pv": 0.0,
                "p_hp": 0.0,
                "p_bss": 0.0,
                "p_set_bss": 0.0,
                "p_trade_req": 0.0,
                "p_trade": 0.0,
            }

        test_ems.contr_fullid_list_by_type = {
            "PvCSV": ["PV_0"],
            "ChargingStationCSV": ["CS_0", "CS_1"],
            "BSS": ["BSS_0"],
            "HeatpumpCSV": ["HP_0"],
        }

        # if no hhs are connected to HEMS
        with self.assertRaises(AssertionError):
            test_ems._update_allocation_keys()

        # adding hhs to controlled list
        test_ems.contr_fullid_list_by_type["HouseholdCSV"] = test_hhs

        # STATIC_EQ
        # static_eq and timestep = 0
        test_ems.timestep = 0
        test_ems._update_allocation_keys()
        # check if within equal distribuation (static_eq) all_keys are equal
        self.assertEqual(
            test_ems.mu_data["hh_1p_0"]["all_key"],
            test_ems.mu_data["hh_2p_0"]["all_key"],
        )

        # STATIC | DYNAMIC_CHARGING
        # test if e_demand_annual is not existent ins static or dynamic_charging
        test_ems.mu_user_op_strategy = "dynamic_charging"
        with self.assertRaises(AssertionError):
            test_ems._update_allocation_keys()

        # test all_keys for static or dynamic charging
        test_ems.e_demand_annual = {"hh_2p_0": 3000000, "hh_1p_0": 1500000}
        test_ems._update_allocation_keys()
        magnitude = 10**9

        self.assertEqual(
            test_ems.mu_data["hh_1p_0"]["all_key"],
            floor((1500000 / (3000000 + 1500000)) * magnitude) / magnitude,
        )
        self.assertEqual(
            test_ems.mu_data["hh_2p_0"]["all_key"],
            floor((3000000 / (3000000 + 1500000)) * magnitude) / magnitude,
        )

        # DYNAMIC FULL
        test_ems.mu_user_op_strategy = "dynamic_full"
        test_ems.demand_full_ids = set(["hh_1p_0", "hh_2p_0", "CS_0", "HP_0"])
        test_ems.p = {"hh_1p_0": 150, "hh_2p_0": 300, "CS_0": 11000, "HP_0": 6000}
        test_ems.mu_data["hh_1p_0"]["p_demand"] = hh_1_demand = 150 + 5500 + 1 / 3 * 6000
        test_ems.mu_data["hh_2p_0"]["p_demand"] = hh_2_demand = 300 + 5500 + 2 / 3 * 6000
        demand_mfh = sum(test_ems.p.values())
        test_ems._update_allocation_keys()
        self.assertEqual(
            test_ems.mu_data["hh_1p_0"]["all_key"],
            floor((hh_1_demand / demand_mfh) * magnitude) / magnitude,
        )
        self.assertEqual(
            test_ems.mu_data["hh_2p_0"]["all_key"],
            floor((hh_2_demand / demand_mfh) * magnitude) / magnitude,
        )

        # check assertation if p_demand_mfh is not close to sum(hh["p_demand"])
        test_ems.mu_data["hh_1p_0"]["p_demand"] = 150
        with self.assertRaises(AssertionError):
            test_ems._update_allocation_keys()

        # WRONG mu_user
        test_ems.mu_user_op_strategy = "wrong_strategy"
        with self.assertRaises(ValueError):
            test_ems._update_allocation_keys()

    def test_allocate_pv(self):
        ## PvCSV ##
        for strategy in self.MU_STRATEGIES:
            test_ems = HEMS_mu_user(
                eid="test_ems_0",
                mu_user_op_strategy=strategy,
            )

            test_ems.contr_fullid_list_by_type = {
                "PvCSV": ["PV_0"],
            }

            test_ems.mu_data = {
                "hh_1p_0": {"p_pv": None, "p_balance": 100, "all_key": 1 / 3},
                "hh_2p_0": {"p_pv": None, "p_balance": 200, "all_key": 2 / 3},
            }

            test_ems.p = {"hh_1p_0": 50, "hh_2p_0": 100, "PV_0": -10000}

            test_ems._allocate_pv()
            self.assertEqual(test_ems.mu_data["hh_1p_0"]["p_pv"], 1 / 3 * -10000)
            self.assertEqual(test_ems.mu_data["hh_2p_0"]["p_pv"], 2 / 3 * -10000)
            self.assertEqual(test_ems.mu_data["hh_1p_0"]["p_balance"], 100 + 1 / 3 * -10000)
            self.assertEqual(test_ems.mu_data["hh_2p_0"]["p_balance"], 200 + 2 / 3 * -10000)

        ## PVLibs and PVLibExacts ##
        for strategy in self.MU_STRATEGIES:
            test_ems = HEMS_mu_user(
                eid="test_ems_0",
                mu_user_op_strategy=strategy,
            )
            test_ems.p_balance = 0
            test_ems.mu_data = {
                "hh_1p_0": {"p_pv": None, "p_balance": 100, "all_key": 1 / 3},
                "hh_2p_0": {"p_pv": None, "p_balance": 200, "all_key": 2 / 3},
            }

            # test PVs
            test_ems.pv_ids = ["PVLibSim-0.PVLib_0", "PVLibSim-1.PVLibExact_0"]
            test_ems.pv_data = {
                "PVLibSim-0.PVLib_0": PVData(p=-5e3),
                "PVLibSim-1.PVLibExact_0": PVData(p=-10e3),
            }

            test_ems._allocate_pv()
            self.assertEqual(test_ems.mu_data["hh_1p_0"]["p_pv"], 1 / 3 * -15e3)
            self.assertEqual(test_ems.mu_data["hh_2p_0"]["p_pv"], 2 / 3 * -15e3)
            self.assertEqual(test_ems.mu_data["hh_1p_0"]["p_balance"], 100 + 1 / 3 * -15e3)
            self.assertEqual(test_ems.mu_data["hh_2p_0"]["p_balance"], 200 + 2 / 3 * -15e3)

        ## error: PVLib connected but not data received ##
        for strategy in self.MU_STRATEGIES:
            test_ems = HEMS_mu_user(
                eid="test_ems_0",
                mu_user_op_strategy=strategy,
            )
            # test PVs
            test_ems.pv_ids = ["PVLibSim-0.PVLib_0", "PVLibSim-1.PVLibExact_0"]

            # no data at all
            test_ems.pv_data = {}
            with self.assertRaises(KeyError):
                test_ems._allocate_pv()

            # no power
            test_ems.pv_data = {
                "PVLibSim-0.PVLib_0": PVData(p=None),
                "PVLibSim-1.PVLibExact_0": PVData(p=None),
            }
            with self.assertRaises(KeyError):
                test_ems._allocate_pv()

    def test_bss_log_energy(self):
        test_ems = HEMS_mu_user(
            eid="test_ems_0",
            mu_user_op_strategy="dynamic_full",
        )

        # check whether capacity is allocated in static strategies
        for strategy in ("static_eq", "static"):
            test_ems.mu_user_op_strategy = strategy
            test_ems.mu_data = {
                "hh_0": {"all_key": 0.8, "e_bss_delta": 0},
            }
            test_ems.bss_data = {"bss_0": BSSData(e_bat_usable=10000, e_bat=5000)}
            test_ems._bss_log_energy()
            self.assertEqual(test_ems.mu_data["hh_0"]["e_bss_usable"], 8000)

        # check whether init energy is allocated in strategies using VSPs
        for strategy in ("static_eq", "static", "dynamic_charging"):
            test_ems.mu_user_op_strategy = strategy
            test_ems.mu_data = {
                "hh_0": {"all_key": 0.8, "e_bss_delta": 0},
                "hh_1": {"all_key": 0.2, "e_bss_delta": 0},
            }
            test_ems.bss_data = {"bss_0": BSSData(e_bat_usable=10000, e_bat=5000)}
            test_ems._bss_log_energy()
            self.assertEqual(test_ems.mu_data["hh_0"]["e_bss"], 4000)

        # check whether unusual deviations in VSPs are reported
        for strategy in ("static_eq", "static", "dynamic_charging"):
            test_ems.mu_user_op_strategy = strategy

            # e_bss << 0
            test_ems.bss_data = {"bss_0": BSSData(e_bat_usable=10000, e_bat=2000)}
            test_ems.mu_data = {
                "hh_0": {"all_key": 0.8, "e_bss": 1000, "e_bss_delta": -2000},
                "hh_1": {"all_key": 0.2, "e_bss": 1000, "e_bss_delta": 0},
            }
            with self.assertRaises(AssertionError):
                test_ems.cache_timestep = 0
                test_ems.timestep = 1
                test_ems._bss_log_energy()

            # e_bss_usable << e_bss
            test_ems.bss_data = {"bss_0": BSSData(e_bat_usable=10000, e_bat=10000)}
            test_ems.mu_data = {
                "hh_0": {"all_key": 0.8, "e_bss": 9000, "e_bss_delta": 2000},
                "hh_1": {"all_key": 0.2, "e_bss": 1000, "e_bss_delta": 0},
            }
            with self.assertRaises(AssertionError):
                test_ems.cache_timestep = 0
                test_ems.timestep = 1
                test_ems._bss_log_energy()

        # check self-discharge
        for strategy in ("static_eq", "static", "dynamic_charging"):
            test_ems.mu_user_op_strategy = strategy
            test_ems.bss_data = {"bss_0": BSSData(e_bat_usable=10000, e_bat=1800)}
            test_ems.mu_data = {
                "hh_0": {"all_key": 0.8, "e_bss": 2000, "e_bss_delta": 0},
                "hh_1": {"all_key": 0.2, "e_bss": 0, "e_bss_delta": 0},
            }

            test_ems.cache_e_bss = -1
            test_ems._bss_log_energy()

            self.assertEqual(test_ems.mu_data["hh_0"]["e_bss"], 1800)
            self.assertEqual(test_ems.mu_data["hh_1"]["e_bss"], 0)

        # check failsafe for very small deviation -> e_cap_mfh = 0
        for strategy in ("static_eq", "static", "dynamic_charging"):
            test_ems.mu_user_op_strategy = strategy
            test_ems.bss_data = {"bss_0": BSSData(e_bat_usable=10000, e_bat=10001)}
            test_ems.mu_data = {
                "hh_0": {"all_key": 0.5, "e_bss": 5e3, "e_bss_usable": 5000},
                "hh_1": {"all_key": 0.5, "e_bss": 5e3, "e_bss_usable": 5000},
            }

            test_ems.cache_timestep = test_ems.timestep = 1
            test_ems.cache_e_bss = None

            test_ems._bss_log_energy()

        # check for run-through in case of fully dynamic
        test_ems.mu_user_op_strategy = "dynamic_full"
        cache = test_ems.mu_data = {
            "hh_0": {"all_key": 0.8, "p_bss": -500},
            "hh_1": {"all_key": 0.2, "p_bss": 1000},
        }
        test_ems._bss_log_energy()
        self.assertEqual(cache, test_ems.mu_data)

    def test_bss_calc_vsp_exchange(self):
        ## check if more than one BSS is catched ##
        for strategy in self.MU_STRATEGIES:
            test_ems = HEMS_mu_user(
                eid="test_ems_0",
                mu_user_op_strategy=strategy,
            )
            test_ems.contr_fullid_list_by_type["BSS"] = ["bss_0", "bss_1"]
            with self.assertRaises(AssertionError):
                test_ems._bss_calc_vsp_exchange()

        ## check run-through with unknown strategy (would be catched earlier) ##
        test_ems = HEMS_mu_user(
            eid="test_ems_0",
            mu_user_op_strategy="unknown_strategy",
        )
        test_ems.contr_fullid_list_by_type["BSS"] = ["bss_0"]
        test_ems.bss_data = {"bss_0": BSSData()}
        cache = test_ems.mu_data = {
            "hh_0": {
                "p_balance": 1000,
                "e_bss": 1000,
                "e_bss_delta": 200,
                "p_trade_req": 0,
            },
            "hh_1": {
                "p_balance": -1000,
                "e_bss": 350,
                "e_bss_delta": 100,
                "p_trade_req": 0,
            },
        }

        test_ems._bss_calc_vsp_exchange()
        self.assertEqual(cache, test_ems.mu_data)

    def test_bss_calc_p_set(self):
        ## check if more than one BSS is catched ##
        for strategy in self.MU_STRATEGIES:
            test_ems = HEMS_mu_user(
                eid="test_ems_0",
                mu_user_op_strategy=strategy,
            )
            test_ems.contr_fullid_list_by_type["BSS"] = ["bss_0", "bss_1"]
            with self.assertRaises(AssertionError):
                test_ems._bss_calc_p_set()

        ## check if dynamic efficiency is catched ##
        for strategy in self.MU_STRATEGIES:
            test_ems = HEMS_mu_user(
                eid="test_ems_0",
                mu_user_op_strategy=strategy,
            )
            bss_full_id = "BSSSim-0.BSS_0"
            test_ems.contr_fullid_list_by_type["BSS"] = [bss_full_id]
            test_ems.bss_data = {bss_full_id: BSSData(status_curve=True)}
            with self.assertRaises(AssertionError):
                test_ems._bss_calc_p_set()

        ## check overrequest in fully dynamic ##
        test_ems = HEMS_mu_user(
            eid="test_ems_0",
            mu_user_op_strategy="dynamic_full",
        )

        # test HH
        test_ems.mu_data = {"hh_0": {}}

        # test BSS
        bss = BSSData(
            p=0.0,
            p_min=-10e3,
            p_max=10e3,
            e_bat_rated=10e3,
            e_bat=5e3,
            e_bat_usable=10e3,
        )
        test_ems.contr_fullid_list_by_type["BSS"] = ["BSSSim-0.BSS_0"]
        test_ems.bss_data = {"BSSSim-0.BSS_0": bss}

        # -p_balance < p_min
        test_ems.mu_data["hh_0"].update(
            {
                "p_balance": 15e3,
                "p_bss": 0,
            }
        )
        test_ems._bss_calc_p_set()
        self.assertEqual(test_ems.mu_data["hh_0"]["p_bss"], bss.p_min)

        # p_max < -p_balance
        test_ems.mu_data["hh_0"].update(
            {
                "p_balance": -15e3,
                "p_bss": 0,
            }
        )
        test_ems._bss_calc_p_set()
        self.assertEqual(test_ems.mu_data["hh_0"]["p_bss"], bss.p_max)

        ## check case with no BSS request ##
        for strategy in self.MU_STRATEGIES:
            test_ems = HEMS_mu_user(
                eid="test_ems_0",
                mu_user_op_strategy=strategy,
            )
            bss_full_id = "BSSSim-0.BSS_0"
            test_ems.contr_fullid_list_by_type["BSS"] = [bss_full_id]
            test_ems.bss_data = {
                bss_full_id: BSSData(
                    p=0.0,
                    p_min=-67000.0,
                    p_max=60000.0,
                    e_bat_rated=67000,
                    p_rated_discharge_max=-67000,
                    p_rated_charge_max=60000,
                    loss_rate=0.07,
                    self_discharge_step=2.4305555555555558e-05,
                    dod_max=0.95,
                    status_aging=True,
                    status_curve=False,
                    soh_cycles_max=0.8,
                    bat_cycles_max=8000,
                    soc_min=0.050000000000000044,
                    charge_efficiency=0.98,
                    discharge_efficiency=0.98,
                    soh=1.0,
                    bat_cycles=0.0,
                    e_bat=33500.0,
                    e_bat_usable=67000.0,
                )
            }
            test_ems.mu_data = {
                "hh_0": {
                    "p_balance": 1000,
                    "p_bss": 0,
                    "e_bss": test_ems.bss_data[bss_full_id].e_bat / 2,
                    "e_bss_usable": test_ems.bss_data[bss_full_id].e_bat_usable / 2,
                    "e_bss_delta": 0,
                },
                "hh_1": {
                    "p_balance": -1000,
                    "p_bss": 0,
                    "e_bss": test_ems.bss_data[bss_full_id].e_bat / 2,
                    "e_bss_usable": test_ems.bss_data[bss_full_id].e_bat_usable / 2,
                    "e_bss_delta": 0,
                },
            }
            test_ems._bss_calc_p_set()
            self.assertEqual(test_ems.p_set_storage[bss_full_id], 0)

        ## check run-through with unknown strategy (would be catched earlier) ##
        test_ems = HEMS_mu_user(
            eid="test_ems_0",
            mu_user_op_strategy="unknown_strategy",
        )
        test_ems.contr_fullid_list_by_type["BSS"] = ["bss_0"]
        test_ems.bss_data = {"bss_0": BSSData(p_min=-10e3, p_max=10e3)}
        cache = test_ems.mu_data = {
            "hh_0": {
                "p_balance": 1500,
                "p_set_bss": 1200,
                "e_bss": 1000,
            },
            "hh_1": {
                "p_balance": -1000,
                "p_set_bss": -500,
                "e_bss": 350,
            },
        }

        test_ems._bss_calc_p_set()
        self.assertEqual(cache, test_ems.mu_data)

    def test_step(self):
        # check normal run-through
        for strategy in self.MU_STRATEGIES:
            test_ems = HEMS_mu_user(
                eid="test_ems_0",
                mu_user_op_strategy=strategy,
            )

            # test BSS
            test_ems.bss_data = {
                "BSSSim-0.BSS_0": BSSData(
                    p=0.0,
                    p_min=-67000.0,
                    p_max=60000.0,
                    e_bat_rated=67000,
                    p_rated_discharge_max=-67000,
                    p_rated_charge_max=60000,
                    loss_rate=0.07,
                    self_discharge_step=2.4305555555555558e-05,
                    dod_max=0.95,
                    status_aging=True,
                    status_curve=False,
                    soh_cycles_max=0.8,
                    bat_cycles_max=8000,
                    soc_min=0.050000000000000044,
                    charge_efficiency=0.98,
                    discharge_efficiency=0.98,
                    soh=1.0,
                    bat_cycles=0.0,
                    e_bat=33500.0,
                    e_bat_usable=67000.0,
                )
            }
            test_ems.contr_fullid_list_by_type["BSS"] = [full_id for full_id in test_ems.bss_data]

            # test PvCSV
            test_ems.contr_fullid_list_by_type["PvCSV"] = ["PvCSV_0"]
            test_ems.p.update({"PvCSV_0": 0})

            # test CS
            test_ems.contr_fullid_list_by_type["ChargingStationCSV"] = ["CS_0"]
            test_ems.p.update({"CS_0": 4200})
            test_ems.e_demand_annual.update({"CS_0": 5000e3})

            # test HHs
            test_ems.contr_fullid_list_by_type["HouseholdCSV"] = ["hh_0", "hh_1"]
            test_ems.p.update({"hh_0": 1000, "hh_1": 500})
            test_ems.e_demand_annual.update({"hh_0": 2890e3, "hh_1": 1900e6})

            # step
            test_ems.step(timestep=0)

            # change powers -> step
            test_ems.p.update({"hh_0": 700, "hh_1": 900, "PvCSV_0": -1000})
            test_ems.step(timestep=1)

            # change powers -> step
            test_ems.p.update({"hh_0": 700, "hh_1": 2500, "PvCSV_0": -5000})
            test_ems.step(timestep=2)

            # change powers -> step
            test_ems.p.update({"CS_0": 22000})
            test_ems.p.update({"hh_0": 14e3, "hh_1": 36e3, "PvCSV_0": -5000})
            test_ems.step(timestep=3)

        # check run-through without BSS
        for strategy in self.MU_STRATEGIES:
            test_ems = HEMS_mu_user(
                eid="test_ems_0",
                mu_user_op_strategy=strategy,
            )

            # test PvCSV
            test_ems.contr_fullid_list_by_type["PvCSV"] = ["PvCSV_0"]
            test_ems.p.update({"PvCSV_0": 0})

            # test HHs
            test_ems.contr_fullid_list_by_type["HouseholdCSV"] = ["hh_0", "hh_1"]
            test_ems.p.update({"hh_0": 1000, "hh_1": 500})
            test_ems.e_demand_annual.update({"hh_0": 2890e3, "hh_1": 1900e6})

            # step
            test_ems.step(timestep=0)

            # change powers -> step
            test_ems.p.update({"hh_0": 700, "hh_1": 900, "PvCSV_0": -1000})
            test_ems.step(timestep=1)

            # change powers -> step
            test_ems.p.update({"hh_0": 700, "hh_1": 900, "PvCSV_0": -5000})
            test_ems.step(timestep=2)


if __name__ == "__main__":
    suite = unittest.TestLoader().loadTestsFromTestCase(TestEMS)
    unittest.TextTestRunner(verbosity=0).run(suite)
