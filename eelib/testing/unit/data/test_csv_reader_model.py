"""
Unittest for csv_reader_model of eELib

| Author: elenia@TUBS
| Copyright 2024 elenia
| This file is part of eELib, which is free software under the terms of the GNU GPL Version 3.
"""

import unittest
import time
import os
import pandas as pd
import warnings

from eelib.data.csv_reader.csv_reader_model import (
    CSVReader,
    RatedCSV,
    PowerCSV,
    HouseholdThermalCSV,
    GenericCSV,
    PvCSV,
    HeatpumpCSV,
    MarketDayAheadCSV,
)

csv_filename_p = "input_15min_p_col.csv"
csv_filename_p_q = "input_15min_pq_col.csv"
input_df_15min = pd.DataFrame(
    {"p": [i // 900 for i in range(0, 86400, 900)], "q": [i // 900 for i in range(0, 86400, 900)]}
)
input_df_15min.index = pd.date_range("2020-01-01 00:00:00", "2020-01-01 23:59:59", freq="900S")

csv_filename_1min = "input_1min.csv"
input_df_1min = pd.DataFrame({"p": [i // 900 for i in range(0, 86400, 60)]})
input_df_1min.index = pd.date_range("2020-01-01 00:00:00", "2020-01-01 23:59:59", freq="60S")

csv_filename_generic = "input_generic_15min.csv"
input_df_generic = pd.DataFrame({"unknown_column": [i // 900 for i in range(0, 86400, 900)]})
input_df_generic.index = pd.date_range("2020-01-01 00:00:00", "2020-01-01 23:59:59", freq="900S")

csv_filename_market = "input_15min_market.csv"
input_df_market = pd.DataFrame(
    {
        "price": [i // 900 for i in range(0, 86400, 900)],
        "volume": [i // 900 for i in range(0, 86400, 900)],
    }
)
input_df_market.index = pd.date_range("2020-01-01 00:00:00", "2020-01-01 23:59:59", freq="900S")


class TestCSVReaderModel(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        if os.path.exists(csv_filename_p):
            os.remove(csv_filename_p)
        if os.path.exists(csv_filename_p_q):
            os.remove(csv_filename_p_q)
        if os.path.exists(csv_filename_1min):
            os.remove(csv_filename_1min)
        if os.path.exists(csv_filename_generic):
            os.remove(csv_filename_generic)
        if os.path.exists(csv_filename_market):
            os.remove(csv_filename_market)
        input_df_15min[["p"]].to_csv(csv_filename_p)
        input_df_15min.to_csv(csv_filename_p_q)
        input_df_1min.to_csv(csv_filename_1min)
        input_df_generic.to_csv(csv_filename_generic)
        input_df_market.to_csv(csv_filename_market)

    @classmethod
    def tearDownClass(cls):
        os.remove(csv_filename_p)
        os.remove(csv_filename_p_q)
        os.remove(csv_filename_1min)
        os.remove(csv_filename_generic)
        os.remove(csv_filename_market)

    def setUp(self):
        print(f"[START] {self.id()}")
        self.start = time.time()

    def tearDown(self):
        t = time.time() - self.start
        print(f"[END] {self.id()}: {t:.4f}")

    # GENERAL TESTS FOR METHODS
    def test_csv_reader_init(self):
        # Checks initial instantiation.
        # includes _open_csv and _check_data due to call in constructor
        test_csv_model = CSVReader(
            eid="test_csv_0",
            datafile=csv_filename_p,
            header_rows=1,
            start_time="2020-01-01 00:00:00",
            step_size=900,
        )
        self.assertEqual(len(test_csv_model.data), 96)

    def test_csv_reader_check_date_too_early(self):
        # Checks for ValueError if given start date is too early
        self.assertRaises(
            ValueError,
            CSVReader,
            eid="test_csv_0",
            datafile=csv_filename_p,
            header_rows=1,
            start_time="2019-12-31 00:00:00",
            step_size=900,
        )

    def test_csv_reader_check_date_too_late(self):
        # Checks for ValueError if given start date is too late
        self.assertRaises(
            ValueError,
            CSVReader,
            eid="test_csv_0",
            datafile=csv_filename_p,
            header_rows=1,
            start_time="2021-01-01 00:00:00",
            step_size=900,
        )

    def test_csv_reader_check_file_not_found(self):
        # Checks for FileNotFoundError if given datafile doesn't exists
        self.assertRaises(
            FileNotFoundError,
            CSVReader,
            eid="test_csv_0",
            datafile="not_exist.csv",
            header_rows=1,
            start_time="2020-01-01 00:00:00",
            step_size=900,
        )

    def test_csv_reader_attrs(self):
        # stores attrs according to csv header names
        test_csv_model = CSVReader(
            eid="test_csv_0",
            datafile=csv_filename_p_q,
            header_rows=1,
            start_time="2020-01-01 00:00:00",
            step_size=900,
        )
        self.assertEqual(test_csv_model.attrs, ["p", "q"])
        test_csv_model = CSVReader(
            eid="test_csv_0",
            datafile=csv_filename_p,
            header_rows=1,
            start_time="2020-01-01 00:00:00",
            step_size=900,
        )
        self.assertEqual(test_csv_model.attrs, ["p"])

    def test_csv_reader_drop_earlier_rows(self):
        # drops all rows before start date
        test_csv_model = CSVReader(
            eid="test_csv_0",
            datafile=csv_filename_p,
            header_rows=1,
            start_time="2020-01-01 01:00:00",
            step_size=900,
        )
        self.assertEqual(len(test_csv_model.data), 92)

    def test_csv_reader_resampling(self):
        # resampling and aggregation
        test_csv_model = CSVReader(
            eid="test_csv_0",
            datafile=csv_filename_p,
            header_rows=1,
            start_time="2020-01-01 00:00:00",
            step_size=3600,
        )
        self.assertEqual(len(test_csv_model.data), 24)
        test_csv_model = CSVReader(
            eid="test_csv_0",
            datafile=csv_filename_1min,
            header_rows=1,
            start_time="2020-01-01 00:00:00",
            step_size=900,
        )
        self.assertEqual(len(test_csv_model.data), 96)
        for step in range(96):
            test_csv_model.step(step)
            self.assertEqual(test_csv_model.values["p"], step)

    def test_rated_csv_scaling(self):
        # up and down scaling according to p_rated
        test_csv_model = RatedCSV(
            eid="test_csv_0",
            datafile=csv_filename_p_q,
            header_rows=1,
            start_time="2020-01-01 00:00:00",
            p_rated=5,
            p_rated_profile=1,
            step_size=900,
        )
        for step in range(96):
            test_csv_model.step(step)
            self.assertEqual(test_csv_model.values["p"], step * 5)
            self.assertEqual(test_csv_model.values["q"], step * 5)
        test_csv_model = RatedCSV(
            eid="test_csv_0",
            datafile=csv_filename_p_q,
            header_rows=1,
            start_time="2020-01-01 00:00:00",
            p_rated=1,
            p_rated_profile=5,
            step_size=900,
        )
        for step in range(96):
            test_csv_model.step(step)
            self.assertAlmostEqual(test_csv_model.values["p"], step / 5)
            self.assertAlmostEqual(test_csv_model.values["q"], step / 5)

    def test_csv_reader_step(self):
        # check multiple steps
        test_csv_model = CSVReader(
            eid="test_csv_0",
            datafile=csv_filename_p_q,
            header_rows=1,
            start_time="2020-01-01 00:00:00",
            step_size=900,
        )
        for step in range(96):
            test_csv_model.step(step)
            self.assertEqual(test_csv_model.values["p"], step)
            self.assertEqual(test_csv_model.values["q"], step)
        self.assertRaises(IndexError, test_csv_model.step, 96)

    def test_csv_set_reactive_power(self):
        # check reactive power calculation
        cos_phi = 0.95
        test_csv_model = PowerCSV(
            eid="test_csv_0",
            datafile=csv_filename_p,
            header_rows=1,
            start_time="2020-01-01 00:00:00",
            step_size=900,
            cos_phi=cos_phi,
        )
        test_csv_pv_model = PvCSV(
            eid="test_csv_pv_0",
            datafile=csv_filename_p,
            header_rows=1,
            start_time="2020-01-01 00:00:00",
            step_size=900,
            cos_phi=cos_phi,
        )

        for step in range(96):
            test_csv_model.step(step)
            self.assertEqual(test_csv_model.p, step)
            self.assertAlmostEqual(test_csv_model.q, -step * 0.32868410517)

            # check different signs for PV (passive sign convention)
            test_csv_pv_model.step(step)
            self.assertEqual(test_csv_pv_model.p, -step)
            self.assertAlmostEqual(test_csv_pv_model.q, step * 0.32868410517)

    def test_household_thermal_csv_step(self):
        # check attribute assignment
        test_csv_model = HouseholdThermalCSV(
            eid="test_csv_0",
            datafile=csv_filename_p_q,
            header_rows=1,
            start_time="2020-01-01 00:00:00",
            step_size=900,
        )
        for step in range(96):
            test_csv_model.step(step)
            self.assertEqual(test_csv_model.p_th_room, step)
            self.assertEqual(test_csv_model.p_th_water, step)

    def test_generic_csv_step(self):
        # check attribute assignment
        test_csv_model = GenericCSV(
            eid="test_csv_0",
            datafile=csv_filename_generic,
            header_rows=1,
            start_time="2020-01-01 00:00:00",
            step_size=900,
        )
        self.assertEqual(test_csv_model.attrs, ["unknown_column"])
        for step in range(96):
            test_csv_model.step(step)
            self.assertEqual(test_csv_model.unknown_column, step)

    def test_power_csv_cos_phi_warning(self):
        # checks for UserWarning if cos_phi is less than 0.9
        with warnings.catch_warnings(record=True) as w:
            warnings.simplefilter("always")
            PowerCSV(
                eid="test_csv_0",
                datafile=csv_filename_p,
                header_rows=1,
                start_time="2020-01-01 00:00:00",
                step_size=900,
                cos_phi=0.85,
            )
        self.assertTrue(
            any(
                "WARNING: cos_phi for test_csv_0 selected to be 0.85 < 0.9 - seems unreasonable!"
                in str(warning.message)
                for warning in w
            )
        )

    def test_power_csv_e_demand(self):
        # test with wrong column names (not existent)
        test_csv_model = PowerCSV(
            eid="test_csv_0",
            datafile=csv_filename_generic,
            header_rows=1,
            start_time="2020-01-01 00:00:00",
            step_size=900,
            cos_phi=1,
            calc_e_demand_annual=True,
        )
        self.assertAlmostEqual(test_csv_model.e_demand_annual, 0)

        # test with correct columns and check the sum
        step_size = 900
        test_csv_model = PowerCSV(
            eid="test_csv_0",
            datafile=csv_filename_p,
            header_rows=1,
            start_time="2020-01-01 00:00:00",
            step_size=step_size,
            cos_phi=1,
            calc_e_demand_annual=True,
        )
        sum_interval = input_df_15min["p"].sum() / (3600 / step_size)
        scaling_to_year_from_day = 365 / 1
        sum_compare = sum_interval * scaling_to_year_from_day
        self.assertAlmostEqual(test_csv_model.e_demand_annual, sum_compare)

    def test_heatpump_csv_init(self):
        # Checks initial instantiation.
        test_csv_model = HeatpumpCSV(
            eid="test_csv_0",
            datafile=csv_filename_p,
            header_rows=1,
            start_time="2020-01-01 00:00:00",
            step_size=900,
        )
        self.assertEqual(len(test_csv_model.data), 96)

    def test_heatpump_csv_step(self):
        # check attribute assignment
        test_csv_model = HeatpumpCSV(
            eid="test_csv_0",
            datafile=csv_filename_p_q,
            header_rows=1,
            start_time="2020-01-01 00:00:00",
            step_size=900,
        )
        for step in range(96):
            test_csv_model.step(step)
            self.assertEqual(test_csv_model.p_el, step)
            self.assertEqual(test_csv_model.q_el, step)

    def test_power_csv_set_reactive_power(self):
        # check reactive power calculation with p and cos_phi
        test_model_csv = PowerCSV(
            eid="test_csv_0",
            datafile=csv_filename_p,
            header_rows=1,
            start_time="2020-01-01 00:00:00",
            step_size=900,
            cos_phi=0.95,
        )
        for step in range(96):
            self.assertAlmostEqual(test_model_csv.data["q"].iloc[step], -step * 0.32868410517)

        # check for when cos_phi set to None
        test_model_csv.attrs = ["p"]
        test_model_csv.data = {"p": 1000}
        test_model_csv.cos_phi = None
        test_model_csv._set_reactive_power()
        self.assertAlmostEqual(test_model_csv.data["q"], 0)
        self.assertIsNone(test_model_csv.values["q"])

    def test_market_csv_init(self):
        # Checks initial instantiation.
        test_csv_model = MarketDayAheadCSV(
            eid="test_csv_0",
            datafile=csv_filename_market,
            header_rows=1,
            start_time="2020-01-01 00:00:00",
            step_size=900,
        )
        self.assertEqual(len(test_csv_model.data), 96)

    def test_market_csv_step(self):
        # check multiple steps
        test_csv_model = MarketDayAheadCSV(
            eid="test_csv_0",
            datafile=csv_filename_market,
            header_rows=1,
            start_time="2020-01-01 00:00:00",
            step_size=900,
        )
        for step in range(96):
            test_csv_model.step(step)
            self.assertEqual(test_csv_model.price, step)
            self.assertEqual(test_csv_model.volume, step)

        # test with no value for volume
        test_csv_model = MarketDayAheadCSV(
            eid="test_csv_0",
            datafile=csv_filename_market,
            header_rows=1,
            start_time="2020-01-01 00:00:00",
            step_size=900,
        )
        test_csv_model.timestep = 0
        test_csv_model.data = test_csv_model.data.drop(["volume"], axis=1)
        test_csv_model.attrs.remove("volume")
        for step in range(96):
            test_csv_model.step(step)
            self.assertEqual(test_csv_model.price, step)
            self.assertEqual(test_csv_model.volume, None)


if __name__ == "__main__":
    suite = unittest.TestLoader().loadTestsFromTestCase(TestCSVReaderModel)
    unittest.TextTestRunner(verbosity=0).run(suite)
