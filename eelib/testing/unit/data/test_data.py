"""
Unittest for dataclasses in the eELib

| Author: elenia@TUBS
| Copyright 2024 elenia
| This file is part of eELib, which is free software under the terms of the GNU GPL Version 3.
"""

import unittest
import time
import pandas

from eelib.data.dataclass._base import BaseData
from eelib.data.dataclass.devices import BaseDeviceData


class TestDataclass(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        pass

    @classmethod
    def tearDownClass(cls):
        pass

    def setUp(self):
        print(f"[START] {self.id()}")
        self.start = time.time()

    def tearDown(self):
        t = time.time() - self.start
        print(f"[END] {self.id()}: {t:.4f}")

    def test_adaption_w_dict(self):
        data_ent = BaseData()

        # no values changed
        bool_false = data_ent.check_adaption_tolerance(
            adaption_tolerance=1,
            dict_cache={"v1": 10, "v2": 20},
            dict_to_check={"v1": 10, "v2": 20.5},
        )
        self.assertFalse(bool_false)

        # value changed
        bool_true = data_ent.check_adaption_tolerance(
            adaption_tolerance=1,
            dict_cache={"v1": 10, "v2": 20},
            dict_to_check={"v1": 15, "v2": 20.5},
        )
        self.assertTrue(bool_true)

    def test_adaption_list(self):
        data_ent = BaseData()

        # no values changed
        bool_false = data_ent.check_adaption_tolerance(
            adaption_tolerance=1,
            dict_cache={"v1": [10, 12, 20]},
            dict_to_check={"v1": [10, 11.5, 19.1]},
        )
        self.assertFalse(bool_false)

        # value changed
        bool_true = data_ent.check_adaption_tolerance(
            adaption_tolerance=1,
            dict_cache={"v1": [10, 12, 20]},
            dict_to_check={"v1": [10, 11.5, 18.9]},
        )
        self.assertTrue(bool_true)

    def test_adaption_dict_in_dict(self):
        data_ent = BaseData()

        # no values changed
        bool_false = data_ent.check_adaption_tolerance(
            adaption_tolerance=1,
            dict_cache={"v1": {"v1_1": 10, "v1_2": 20}},
            dict_to_check={"v1": {"v1_1": 10.5, "v1_2": 20.9}},
        )
        self.assertFalse(bool_false)

        # value changed
        bool_true = data_ent.check_adaption_tolerance(
            adaption_tolerance=1,
            dict_cache={"v1": {"v1_1": 10, "v1_2": 20}},
            dict_to_check={"v1": {"v1_1": 8.9, "v1_2": 20.9}},
        )
        self.assertTrue(bool_true)

    def test_adaption_self_base_data(self):
        data_ent = BaseDeviceData(p=10, p_max=20, p_min=0)

        # no values changed
        bool_false = data_ent.check_adaption_tolerance(
            adaption_tolerance=1,
            dict_cache={"p": 10, "p_max": 20.5, "p_min": 0},
        )
        self.assertFalse(bool_false)

        # value changed
        bool_true = data_ent.check_adaption_tolerance(
            adaption_tolerance=1,
            dict_cache={"p": 10, "p_max": 22, "p_min": 0},
        )
        self.assertTrue(bool_true)

    def test_adaption_base_data(self):
        data_ent = BaseData()
        data_check = BaseDeviceData(p=10, p_max=20, p_min=0)

        # no values changed
        bool_false = data_ent.check_adaption_tolerance(
            adaption_tolerance=1,
            dict_cache={"v1": data_check},
            dict_to_check={"v1": {"p": 10, "p_max": 20.5, "p_min": 0}},
        )
        self.assertFalse(bool_false)

        # value changed
        bool_true = data_ent.check_adaption_tolerance(
            adaption_tolerance=1,
            dict_cache={"v1": data_check},
            dict_to_check={"v1": {"p": 10, "p_max": 20.5, "p_min": -2}},
        )
        self.assertTrue(bool_true)

    def test_adaption_str_bool(self):
        data_ent = BaseData()

        # no values changed
        bool_false = data_ent.check_adaption_tolerance(
            adaption_tolerance=1,
            dict_cache={"v1": "test", "v2": True},
            dict_to_check={"v1": "test", "v2": True},
        )
        self.assertFalse(bool_false)

        # value changed
        bool_true = data_ent.check_adaption_tolerance(
            adaption_tolerance=1,
            dict_cache={"v1": "test", "v2": True},
            dict_to_check={"v1": "not", "v2": True},
        )
        self.assertTrue(bool_true)
        # value changed
        bool_true = data_ent.check_adaption_tolerance(
            adaption_tolerance=1,
            dict_cache={"v1": "test", "v2": True},
            dict_to_check={"v1": "test", "v2": False},
        )
        self.assertTrue(bool_true)

    def test_adaption_int_float(self):
        data_ent = BaseData()

        # no values changed
        bool_false = data_ent.check_adaption_tolerance(
            adaption_tolerance=1,
            dict_cache={"v1": 10, "v2": 15.4},
            dict_to_check={"v1": 10, "v2": 15.4},
        )
        self.assertFalse(bool_false)

        # value changed
        bool_true = data_ent.check_adaption_tolerance(
            adaption_tolerance=1,
            dict_cache={"v1": 10, "v2": 15.4},
            dict_to_check={"v1": 12, "v2": 15.4},
        )
        self.assertTrue(bool_true)
        # value changed
        bool_true = data_ent.check_adaption_tolerance(
            adaption_tolerance=1,
            dict_cache={"v1": 10, "v2": 15.4},
            dict_to_check={"v1": 10, "v2": 12.4},
        )
        self.assertTrue(bool_true)

    def test_adaption_w_none_value(self):
        data_ent = BaseData()

        # other is not None
        bool_true = data_ent.check_adaption_tolerance(
            adaption_tolerance=1,
            dict_cache={"v1": None},
            dict_to_check={"v1": 10},
        )
        self.assertTrue(bool_true)
        bool_true = data_ent.check_adaption_tolerance(
            adaption_tolerance=1,
            dict_cache={"v1": 10},
            dict_to_check={"v1": None},
        )
        self.assertTrue(bool_true)

        # other is also None
        bool_false = data_ent.check_adaption_tolerance(
            adaption_tolerance=1,
            dict_cache={"v1": None},
            dict_to_check={"v1": None},
        )
        self.assertFalse(bool_false)

    def test_error(self):
        data_ent = BaseData()

        with self.assertRaises(TypeError) as message:
            _ = data_ent.check_adaption_tolerance(
                adaption_tolerance=1,
                dict_cache={"v1": pandas.DataFrame(data=[10, 20, 0])},
                dict_to_check={"v1": [10, 20, 0]},
            )
        str_error = "Unknown format for dataclass value"
        self.assertTrue(str(message.exception) == str_error)


if __name__ == "__main__":
    suite = unittest.TestLoader().loadTestsFromTestCase(TestDataclass)
    unittest.TextTestRunner(verbosity=0).run(suite)
