"""
Unittest for Forecast of eELib


| Author: elenia@TUBS
| Copyright 2024 elenia
| This file is part of eELib, which is free software under the terms of the GNU GPL Version 3.
"""

import unittest
import time
import pandas as pd
from unittest.mock import patch, MagicMock
from eelib.data.influx_reader.influx_reader_model import (
    GenericInflux,
    RatedInflux,
    PowerInflux,
    HouseholdInflux,
    PvInflux,
)

START_TIME = "2024-01-01 00:00:00"
END_TIME = "2024-01-02 00:00:00"
INFLUX_BUCKET = "Bucket"

data = {"time": ["2021-01-01T00:00:00Z", "2021-01-01T01:00:00Z"], "p": [10, 20]}

# Create DataFrame and set 'time' column as index
DUMMY_DF = pd.DataFrame(data)
DUMMY_DF.set_index("time", inplace=True)


class TestGenericInflux(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        pass

    @classmethod
    def tearDownClass(cls):
        pass

    @patch("eelib.data.influx_reader.influx_reader_model.GenericInflux._read_db")
    @patch("influxdb_client.InfluxDBClient")
    def setUp(self, mock_influx_db_client, mock_read_db):
        print(f"[START] {self.id()}")
        self.start = time.time()
        self.model = GenericInflux(
            eid="eid",
            step_size=1,
            measurement_name="measurement",
            tags={},
            fields=["p"],
            start_time=START_TIME,
            end_time=END_TIME,
            influx_url="url",
            influx_org="org",
            influx_token="token",
            influx_bucket=INFLUX_BUCKET,
        )

    def tearDown(self):
        t = time.time() - self.start
        print(f"[END] {self.id()}: {t:.4f}")

    def test_generate_tag_filter(self):
        ans = self.model.generate_tag_filter()
        self.assertEqual(ans, "")
        tags = {"tag_key1": "tag_value1", "tag_key2": "tag_value2"}
        ans = self.model.generate_tag_filter(tags)
        self.assertEqual(ans, 'r["tag_key1"] == "tag_value1" and r["tag_key2"] == "tag_value2"')

    def test_setup_of_model(self):
        # with no influx inputs
        test_string_err = (
            "No specifications to influx DB given and .env file not found in any "
            "subfolder under the root folder"
        )
        with self.assertRaises(ValueError) as message:
            _ = GenericInflux(
                eid="eid",
                step_size=1,
                measurement_name="measurement",
                tags={},
                fields=["p"],
                start_time=START_TIME,
                end_time=END_TIME,
            )
        self.assertTrue(str(message.exception) == test_string_err)

    @patch("eelib.data.influx_reader.influx_reader_model.GenericInflux.generate_tag_filter")
    def test_generate_influx_query(self, mock_generate_tag_filter):
        mock_generate_tag_filter.return_value = 'r["tag1"] == "value1"'
        ans = self.model.generate_influx_query(
            measurement_name="measurement",
            start_time=START_TIME,
            end_time=END_TIME,
            resolution=1,
            tags={"tag1": "value1"},
            fields=["p"],
        )
        expected = f"""
            from(bucket: "{INFLUX_BUCKET}")
                |> range(start: {START_TIME}, stop: {END_TIME})
                |> filter(fn: (r) => r["_measurement"] == "measurement"
                and r["tag1"] == "value1")
                |> aggregateWindow(every: {1}s, fn: mean)
                |> keep(columns: {["p"]}) // Add field selection if provided
        """
        self.assertEqual(ans, expected)

    @patch("eelib.data.influx_reader.influx_reader_model.GenericInflux.generate_tag_filter")
    def test_generate_influx_query_no_tags(self, mock_generate_tag_filter):
        mock_generate_tag_filter.return_value = ""
        ans = self.model.generate_influx_query(
            measurement_name="measurement",
            start_time=START_TIME,
            end_time=END_TIME,
            resolution=1,
            fields=["p"],
        )
        expected = f"""
            from(bucket: "{INFLUX_BUCKET}")
                |> range(start: {START_TIME}, stop: {END_TIME})
                |> filter(fn: (r) => r["_measurement"] == "measurement"
                )
                |> aggregateWindow(every: {1}s, fn: mean)
                |> keep(columns: {["p"]}) // Add field selection if provided
        """
        self.assertEqual(ans, expected)

    @patch("eelib.data.influx_reader.influx_reader_model.GenericInflux.generate_tag_filter")
    def test_generate_influx_query_no_fields(self, mock_generate_tag_filter):
        mock_generate_tag_filter.return_value = 'r["tag1"] == "value1"'
        ans = self.model.generate_influx_query(
            measurement_name="measurement",
            start_time=START_TIME,
            end_time=END_TIME,
            resolution=1,
            tags={"tag1": "value1"},
        )
        expected = f"""
            from(bucket: "{INFLUX_BUCKET}")
                |> range(start: {START_TIME}, stop: {END_TIME})
                |> filter(fn: (r) => r["_measurement"] == "measurement"
                and r["tag1"] == "value1")
                |> aggregateWindow(every: {1}s, fn: mean)
                 // Add field selection if provided
        """
        self.assertEqual(ans, expected)

    @patch("eelib.data.influx_reader.influx_reader_model.GenericInflux.generate_tag_filter")
    def test_generate_influx_query_no_tags_no_fields(self, mock_generate_tag_filter):
        mock_generate_tag_filter.return_value = ""
        ans = self.model.generate_influx_query(
            measurement_name="measurement",
            start_time=START_TIME,
            end_time=END_TIME,
            resolution=1,
        )
        expected = f"""
            from(bucket: "{INFLUX_BUCKET}")
                |> range(start: {START_TIME}, stop: {END_TIME})
                |> filter(fn: (r) => r["_measurement"] == "measurement"
                )
                |> aggregateWindow(every: {1}s, fn: mean)
                 // Add field selection if provided
        """
        self.assertEqual(ans, expected)

    @patch("eelib.data.influx_reader.influx_reader_model.GenericInflux.generate_influx_query")
    def test_read_db_empty_result(self, mock_generate_influx_query):
        mock_model = MagicMock()
        mock_model.client.query_api.return_value.query.return_value = None
        with self.assertRaises(SystemExit):
            GenericInflux._read_db(
                mock_model,
                measurement_name="measurement",
                tags=None,
                fields=None,
                start_time=START_TIME,
                end_time=END_TIME,
                resolution=1,
            )

    @patch("eelib.data.influx_reader.influx_reader_model.GenericInflux.generate_influx_query")
    def test_read_db(self, mock_generate_influx_query):
        mock_flux_table = MagicMock()
        mock_flux_record1 = MagicMock()
        mock_flux_record2 = MagicMock()

        # Set up the mock records
        mock_flux_record1.get_time.return_value = "2021-01-01T00:00:00Z"
        mock_flux_record1.values = {"_value": 10, "_measurement": "measurement", "_field": "p"}

        mock_flux_record2.get_time.return_value = "2021-01-01T01:00:00Z"
        mock_flux_record2.values = {"_value": 20, "_measurement": "measurement", "_field": "p"}

        # Set up the mock table to contain the records
        mock_flux_table.records = [mock_flux_record1, mock_flux_record2]

        mock_model = MagicMock()
        mock_model.client.query_api.return_value.query.return_value = [mock_flux_table]
        ans = GenericInflux._read_db(
            mock_model,
            measurement_name="measurement",
            tags=None,
            fields=["p"],
            start_time=START_TIME,
            end_time=END_TIME,
            resolution=1,
        )
        pd.testing.assert_frame_equal(ans, DUMMY_DF)

    def test_step(self):
        self.model.data = DUMMY_DF
        self.model.step(0)
        self.assertEqual(self.model.p, 10)
        self.assertEqual(self.model.index, 1)

        self.model.step(1)
        self.assertEqual(self.model.p, 20)
        self.assertEqual(self.model.index, 2)

    def test_end_of_data(self):
        self.model.data = DUMMY_DF
        self.model.step(1)
        self.model.step(2)
        with self.assertRaises(IndexError):
            self.model.step(3)


class TestRatedInflux(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        pass

    @classmethod
    def tearDownClass(cls):
        pass

    @patch("eelib.data.influx_reader.influx_reader_model.GenericInflux._read_db")
    @patch("influxdb_client.InfluxDBClient")
    def setUp(self, mock_influx_db_client, mock_read_db):
        print(f"[START] {self.id()}")
        self.start = time.time()
        self.model = RatedInflux(
            eid="eid",
            step_size=1,
            measurement_name="measurement",
            tags={},
            fields=["p"],
            start_time=START_TIME,
            end_time=END_TIME,
            influx_url="url",
            influx_org="org",
            influx_token="token",
            influx_bucket=INFLUX_BUCKET,
        )
        self.model.data = DUMMY_DF.copy()

    def tearDown(self):
        t = time.time() - self.start
        print(f"[END] {self.id()}: {t:.4f}")

    def test_apply_p_rated(self):
        self.model.scaling_factor = 2
        self.model._apply_p_rated()
        self.model.step(0)
        self.assertEqual(self.model.p, 20)
        self.model.step(1)
        self.assertEqual(self.model.p, 40)


class TestPowerInflux(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        pass

    @classmethod
    def tearDownClass(cls):
        pass

    def setUp(self):
        print(f"[START] {self.id()}")
        self.start = time.time()

    def tearDown(self):
        t = time.time() - self.start
        print(f"[END] {self.id()}: {t:.4f}")

    @patch("eelib.data.influx_reader.influx_reader_model.GenericInflux._read_db")
    @patch("influxdb_client.InfluxDBClient")
    def test_set_reactive_power(self, mock_influx_db_client, mock_read_db):
        model = PowerInflux(
            eid="eid",
            step_size=1,
            measurement_name="measurement",
            tags={},
            fields=["p", "q"],
            start_time=START_TIME,
            end_time=END_TIME,
            influx_url="url",
            influx_org="org",
            influx_token="token",
            influx_bucket=INFLUX_BUCKET,
            cos_phi=0.95,
        )
        model.data = DUMMY_DF
        model._set_reactive_power()
        model.step(0)
        self.assertEqual(model.p, 10)
        self.assertAlmostEqual(model.q, -10 * 0.32868410517)
        model.step(1)
        self.assertEqual(model.p, 20)
        self.assertAlmostEqual(model.q, -20 * 0.32868410517)

        # different cos_phi
        model = PowerInflux(
            eid="eid",
            step_size=1,
            measurement_name="measurement",
            tags={},
            fields=["p"],
            start_time=START_TIME,
            end_time=END_TIME,
            influx_url="url",
            influx_org="org",
            influx_token="token",
            influx_bucket=INFLUX_BUCKET,
            cos_phi=0.85,
        )
        model.data = DUMMY_DF
        model.attrs = ["p"]
        model._set_reactive_power()
        model.step(0)
        self.assertEqual(model.p, 10)
        self.assertAlmostEqual(model.q, -10 * 0.619744338403)

    @patch("eelib.data.influx_reader.influx_reader_model.GenericInflux._read_db")
    @patch("influxdb_client.InfluxDBClient")
    def test_init_household(self, mock_influx_db_client, mock_read_db):
        _ = HouseholdInflux(
            eid="eid",
            step_size=1,
            measurement_name="measurement",
            fields=["p"],
            start_time=START_TIME,
            end_time=END_TIME,
            influx_url="url",
            influx_org="org",
            influx_token="token",
            influx_bucket=INFLUX_BUCKET,
            cos_phi=1.0,
        )
        pass

    @patch("eelib.data.influx_reader.influx_reader_model.GenericInflux._read_db")
    @patch("influxdb_client.InfluxDBClient")
    def test_init_pv(self, mock_influx_db_client, mock_read_db):
        _ = PvInflux(
            eid="eid",
            step_size=1,
            measurement_name="measurement",
            fields=["p"],
            start_time=START_TIME,
            end_time=END_TIME,
            influx_url="url",
            influx_org="org",
            influx_token="token",
            influx_bucket=INFLUX_BUCKET,
            cos_phi=1.0,
        )
        pass


if __name__ == "__main__":
    suite = unittest.TestLoader().loadTestsFromTestCase(TestGenericInflux)
    unittest.TextTestRunner(verbosity=0).run(suite)
    suite = unittest.TestLoader().loadTestsFromTestCase(TestRatedInflux)
    unittest.TextTestRunner(verbosity=0).run(suite)
    suite = unittest.TestLoader().loadTestsFromTestCase(TestPowerInflux)
    unittest.TextTestRunner(verbosity=0).run(suite)
