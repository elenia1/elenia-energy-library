"""
Unittest for battery storage system of eELib

| Author: elenia@TUBS
| Copyright 2024 elenia
| This file is part of eELib, which is free software under the terms of the GNU GPL Version 3.
"""

import unittest
import time
import math

from eelib.core.devices.bss.bss_model import BSS


class TestBSS(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        pass

    @classmethod
    def tearDownClass(cls):
        pass

    def setUp(self):
        print(f"[START] {self.id()}")
        self.start = time.time()

    def tearDown(self):
        t = time.time() - self.start
        print(f"[END] {self.id()}: {t:.4f}")

    def test_soc_init(self):
        test_storage_init = BSS(eid="test_storage_0", e_bat_rated=10000, soc_init=0.7)

        self.assertAlmostEqual(test_storage_init.soc, (0.7))
        self.assertAlmostEqual(test_storage_init.e_bat, (0.7 * 10000))

    def test_charge_soc(self):
        testrange = 8
        test_storage = BSS(
            eid="test_storage_0",
            e_bat_rated=10000,
            soc_init=0.1,
            loss_rate=0.0,
            dod_max=1,
            discharge_efficiency_init=1,
            charge_efficiency_init=1,
            step_size=60 * 60,
        )
        test_storage.e_bat = test_storage.soc_init * test_storage.e_bat_rated

        for t in range(1, testrange + 1):
            test_storage.p = 1000
            test_storage.step(t)
        self.assertAlmostEqual(test_storage.soc, 0.9)

    def test_charge_e_bat(self):
        testrange = 8
        test_storage = BSS(
            eid="test_storage_0",
            e_bat_rated=10000,
            soc_init=0.1,
            loss_rate=0.0,
            dod_max=1,
            discharge_efficiency_init=1,
            charge_efficiency_init=1,
            step_size=60 * 60,
        )
        test_storage.soc_init = 0.1
        test_storage.e_bat = test_storage.soc_init * test_storage.e_bat_rated

        for t in range(1, testrange + 1):
            test_storage.p = 1000
            test_storage.step(t)
        self.assertAlmostEqual(test_storage.e_bat, 9000)

    def test_discharge_soc(self):
        testrange = 9
        test_storage = BSS(
            eid="test_storage_0",
            e_bat_rated=10000,
            soc_init=1,
            loss_rate=0.0,
            dod_max=1,
            discharge_efficiency_init=1,
            charge_efficiency_init=1,
            step_size=60 * 60,
        )
        test_storage.soc_init = 1
        test_storage.e_bat = test_storage.soc_init * test_storage.e_bat_rated

        for t in range(1, testrange + 1):
            test_storage.p = -1000
            test_storage.step(t)
        self.assertAlmostEqual(test_storage.soc, (0.1))

    def test_discharge_e_bat(self):
        testrange = 9
        test_storage = BSS(
            eid="test_storage_0",
            e_bat_rated=10000,
            soc_init=1,
            loss_rate=0.0,
            dod_max=1,
            discharge_efficiency_init=1,
            charge_efficiency_init=1,
            step_size=60 * 60,
        )
        test_storage.e_bat = test_storage.soc_init * test_storage.e_bat_rated

        for t in range(1, testrange + 1):
            test_storage.p = -1000
            test_storage.step(t)
        self.assertAlmostEqual(test_storage.e_bat, 1000)

    def test_self_discharge(self):
        testrange = 10
        test_storage = BSS(
            eid="test_storage_0",
            e_bat_rated=10000,
            soc_init=1,
            loss_rate=0.05,
            step_size=900,
            dod_max=1,
        )
        energy_test = test_storage.e_bat_rated
        for t in range(1, testrange + 1):
            test_storage.p_set = 0
            test_storage.p = 0
            test_storage.step(t)
            energy_test -= test_storage.e_bat_rated * 0.05 / (30 * 24 * 3600 / 900)
        self.assertAlmostEqual(test_storage.soc, energy_test / test_storage.e_bat_rated)

    def test_static_charge_efficiency(self):
        testrange = 2
        test_storage = BSS(
            eid="test_storage_0",
            e_bat_rated=10000,
            soc_init=0.0,
            loss_rate=0.0,
            step_size=60 * 60,
            dod_max=1,
            charge_efficiency_init=0.8,
        )
        for t in range(1, testrange + 1):
            test_storage.p = 2000
            test_storage.step(t)
        self.assertAlmostEqual(test_storage.e_bat, 2 * 2000 * 0.8)

    def test_static_discharge_efficiency(self):
        testrange = 2
        test_storage = BSS(
            eid="test_storage_0",
            e_bat_rated=10000,
            soc_init=1.00,
            loss_rate=0.0,
            step_size=60 * 60,
            dod_max=1,
            discharge_efficiency_init=0.8,
        )
        for t in range(1, testrange + 1):
            test_storage.p = -1000
            test_storage.step(t)
        self.assertAlmostEqual(test_storage.e_bat, 10000 - testrange * 1000 / 0.8)

    def test_dynamic_discharge_efficiency_low_p_ratio(self):
        testrange = 1
        test_storage = BSS(
            eid="test_storage_0",
            e_bat_rated=10000,
            soc_init=1,
            loss_rate=0.0,
            step_size=60 * 60,
            dod_max=1,
            discharge_efficiency_init=1,
            charge_efficiency_init=1,
            status_curve=True,
            bat2ac_efficiency=[-0.000623, -1.65, 0.9591],
            ac2bat_efficiency=[-0.0006864, -1.641, 0.957],
        )
        for t in range(1, testrange + 1):
            test_storage.p_set = 0.011 * test_storage.p_rated_discharge_max
            test_storage.step(t)
        self.assertAlmostEqual(test_storage.discharge_efficiency, math.inf)

    def test_dynamic_charge_efficiency(self):
        testrange = 2
        test_storage = BSS(
            eid="test_storage_0",
            e_bat_rated=10000,
            soc_init=0.5,
            loss_rate=0.0,
            step_size=60 * 60,
            dod_max=1,
            charge_efficiency_init=1,
            status_curve=True,
            bat2ac_efficiency=[-0.000623, -1.65, 0.9591],
            ac2bat_efficiency=[-0.0006864, -1.641, 0.957],
        )
        for t in range(1, testrange + 1):
            test_storage.p = 2000
            test_storage.p_set = 2000
            test_storage.step(t)
        self.assertAlmostEqual(test_storage.e_bat, 8900.6)

    def test_dynamic_discharge_efficiency(self):
        testrange = 2
        test_storage = BSS(
            eid="test_storage_0",
            e_bat_rated=10000,
            soc_init=0.5,
            loss_rate=0.0,
            step_size=60 * 60,
            dod_max=1,
            charge_efficiency_init=1,
            status_curve=True,
            bat2ac_efficiency=[-0.000623, -1.65, 0.9591],
            ac2bat_efficiency=[-0.0006864, -1.641, 0.957],
        )
        for t in range(1, testrange + 1):
            test_storage.p = -2000
            test_storage.p_set = -2000
            test_storage.step(t)
        self.assertAlmostEqual(test_storage.e_bat, 796.10095543)

    def test_bat_cycles(self):
        test_range = 10
        test_storage = BSS(
            eid="test_storage_0",
            e_bat_rated=1000,
            soc_init=1,
            loss_rate=0.0,
            step_size=60 * 60,
            dod_max=1,
            discharge_efficiency_init=1,
            charge_efficiency_init=1,
        )
        for t in range(1, test_range + 1):
            if t % 2 == 1:
                test_storage.p = -1000
            else:
                test_storage.p = +1000
            test_storage.step(t)
        self.assertAlmostEqual(test_storage.bat_cycles, (test_range / 2))

    def test_calc_aging_status(self):
        test_range = 10
        test_storage = BSS(
            eid="test_storage_0",
            e_bat_rated=1000,
            soc_init=1,
            soh_init=1,
            status_aging=True,
            soh_cycles_max=0.8,
            bat_cycles_init=0,
            bat_cycles_max=5,
            loss_rate=0.0,
            step_size=60 * 60,
            dod_max=1,
            discharge_efficiency_init=1,
            charge_efficiency_init=1,
        )
        for t in range(1, test_range + 1):
            if t % 2 == 1:
                test_storage.p = -1000
            else:
                test_storage.p = +1000
            test_storage.step(t)
        self.assertAlmostEqual(test_storage.soh, 0.8)

    def test_aging_warning(self):
        test_storage = BSS(
            eid="test_storage_0",
            e_bat_rated=1000,
            soc_init=1,
            soh_init=1,
            status_aging=True,
            soh_cycles_max=0.8,
            bat_cycles_init=5,
            bat_cycles_max=5,
            loss_rate=0.0,
            step_size=60 * 60,
            dod_max=1,
            discharge_efficiency_init=1,
            charge_efficiency_init=1,
        )
        test_storage.p = -1000
        with self.assertRaises(ValueError):
            test_storage.step(timestep=1)

    def test_set_active_power(self):
        test_storage = BSS(
            eid="test_storage_0",
            e_bat_rated=1000,
            soc_init=1,
            loss_rate=0.0,
            step_size=60 * 60,
            dod_max=1,
            discharge_efficiency_init=1,
            charge_efficiency_init=1,
        )
        test_storage.p_set = -7000
        test_storage.step(1)
        self.assertEqual(test_storage.p, test_storage.p_set)

    def test_active_power_limit_discharge(self):
        test_storage = BSS(
            eid="test_storage_0",
            e_bat_rated=24000,
            p_rated_discharge_max=-8000,
            p_rated_charge_max=8000,
            soc_init=1,
            loss_rate=0.0,
            step_size=60 * 60,
            dod_max=1,
            discharge_efficiency_init=1,
            charge_efficiency_init=1,
        )
        test_storage.p_set = -10000
        test_storage.step(1)
        self.assertEqual(test_storage.p, test_storage.p_rated_discharge_max)

    def test_active_power_limit_charge(self):
        test_storage = BSS(
            eid="test_storage_0",
            e_bat_rated=24000,
            p_rated_discharge_max=-8000,
            p_rated_charge_max=8000,
            soc_init=1,
            loss_rate=0.0,
            step_size=60 * 60,
            dod_max=1,
            discharge_efficiency_init=1,
            charge_efficiency_init=1,
        )
        test_storage.p_set = 10000
        test_storage.step(1)
        self.assertEqual(test_storage.p, test_storage.p_rated_charge_max)

    def test_discharged_storage_power_limits(self):
        test_storage = BSS(
            eid="test_storage_0",
            e_bat_rated=24000,
            p_rated_discharge_max=-8000,
            p_rated_charge_max=8000,
            soc_init=0.0,
            loss_rate=0.0,
            step_size=60 * 60,
            dod_max=0.9,
            discharge_efficiency_init=1,
            charge_efficiency_init=1,
        )
        test_storage.p = -1000
        test_storage.p_set = -1000
        test_storage.step(1)
        self.assertEqual(test_storage.p_min, 0)

    def test_fully_charged_storage_power_limits(self):
        test_storage = BSS(
            eid="test_storage_0",
            e_bat_rated=24000,
            p_rated_discharge_max=-8000,
            p_rated_charge_max=8000,
            soc_init=1.0,
            loss_rate=0.0,
            step_size=60 * 60,
            dod_max=1,
            discharge_efficiency_init=1,
            charge_efficiency_init=1,
        )
        test_storage.p = 1000
        test_storage.p_set = 1000
        test_storage.step(1)
        self.assertEqual(test_storage.p_max, 0)

    def test_calc_p_max_hourly(self):
        testrange = 2
        test_storage = BSS(
            eid="test_storage_0",
            e_bat_rated=10000,
            p_rated_discharge_max=-8000,
            p_rated_charge_max=8000,
            soc_init=1,
            loss_rate=0.0,
            step_size=60 * 60,
            dod_max=1,
            discharge_efficiency_init=1,
            charge_efficiency_init=1,
            status_curve=False,
        )
        for t in range(1, testrange + 1):
            test_storage.p = -1000
            test_storage.p_set = -1000
            test_storage.step(t)
        self.assertAlmostEqual(test_storage.p_max, 2000)

    def test_calc_p_max_quarter_hourly(self):
        testrange = 2
        test_storage = BSS(
            eid="test_storage_0",
            e_bat_rated=10000,
            p_rated_discharge_max=-8000,
            p_rated_charge_max=8000,
            soc_init=1,
            loss_rate=0.0,
            step_size=60 * 15,
            dod_max=1,
            discharge_efficiency_init=1,
            charge_efficiency_init=1,
            status_curve=False,
        )
        for t in range(1, testrange + 1):
            test_storage.p = -1000
            test_storage.p_set = -1000
            test_storage.step(t)
        self.assertAlmostEqual(test_storage.p_max, 2000)

    def test_calc_p_min_hourly(self):
        testrange = 2
        test_storage = BSS(
            eid="test_storage_0",
            e_bat_rated=10000,
            p_rated_discharge_max=-8000,
            p_rated_charge_max=8000,
            soc_init=0.0,
            loss_rate=0.0,
            step_size=60 * 60,
            dod_max=1,
            discharge_efficiency_init=1,
            charge_efficiency_init=1,
            status_curve=False,
        )
        for t in range(1, testrange + 1):
            test_storage.p = 1000
            test_storage.p_set = 1000
            test_storage.step(t)
        self.assertAlmostEqual(test_storage.p_min, -2000)

    def test_calc_p_min_quarter_hourly(self):
        testrange = 2
        test_storage = BSS(
            eid="test_storage_0",
            e_bat_rated=10000,
            p_rated_discharge_max=-8000,
            p_rated_charge_max=8000,
            soc_init=0.0,
            loss_rate=0.0,
            step_size=60 * 15,
            dod_max=1,
            discharge_efficiency_init=1,
            charge_efficiency_init=1,
            status_curve=False,
        )
        for t in range(1, testrange + 1):
            test_storage.p = 1000
            test_storage.p_set = 1000
            test_storage.step(t)
        self.assertAlmostEqual(test_storage.p_min, -2000)

    def test_overcharge(self):
        testrange = 2
        test_storage = BSS(
            eid="test_storage_0",
            e_bat_rated=24000,
            soc_init=0.99,
            loss_rate=0.0,
            step_size=60 * 60,
            dod_max=1,
            discharge_efficiency_init=1,
            charge_efficiency_init=1,
        )
        test_storage.p_set = 8000
        for t in range(0, testrange):
            test_storage.step(t)
        self.assertEqual(test_storage.soc, 1.0)

    def test_undercharge(self):
        testrange = 2
        test_storage = BSS(
            eid="test_storage_0",
            e_bat_rated=24000,
            soc_init=0.01,
            loss_rate=0.0,
            step_size=60 * 60,
            dod_max=1,
            discharge_efficiency_init=1,
            charge_efficiency_init=1,
        )
        test_storage.p_set = -8000
        for t in range(0, testrange):
            test_storage.step(t)
        self.assertEqual(test_storage.soc, 0.0)

    def test_undercharge_soc_min(self):
        testrange = 2
        test_storage = BSS(
            eid="test_storage_0",
            e_bat_rated=24000,
            soc_init=0.11,
            loss_rate=0.0,
            step_size=60 * 60,
            dod_max=0.9,
            discharge_efficiency_init=1,
            charge_efficiency_init=1,
            status_aging=False,
        )
        test_storage.p_set = -8000
        for t in range(0, testrange):
            test_storage.step(t)
        self.assertAlmostEqual(test_storage.soc, 0.1)

    def test_overcharge_soc_max(self):
        testrange = 2
        test_storage = BSS(
            eid="test_storage_0",
            e_bat_rated=24000,
            soc_init=1,
            loss_rate=0.0,
            step_size=60 * 60,
            dod_max=0.9,
            discharge_efficiency_init=1,
            charge_efficiency_init=1,
        )
        test_storage.p_set = 8000
        for t in range(0, testrange):
            test_storage.step(t)
        self.assertEqual(test_storage.soc, 1)

    def test_calc_p_max_hourly_loss(self):
        test_storage = BSS(
            eid="test_storage_0",
            e_bat_rated=10000,
            p_rated_discharge_max=-8000,
            p_rated_charge_max=8000,
            soc_init=1,
            loss_rate=0.02,
            step_size=60 * 60,
            dod_max=1,
            discharge_efficiency_init=1,
            charge_efficiency_init=1,
            status_curve=False,
        )
        p_max_expected = test_storage.e_bat * (0.02 / 24 / 30)
        for t in range(1, 3):
            test_storage.p = -1000
            test_storage.p_set = -1000
            # we discharge by 1000 Wh, loss is applied to the energy state before discharging
            test_storage.step(t)
            p_max_expected += 1000 + (test_storage.e_bat * (0.02 / 24 / 30))
            self.assertAlmostEqual(test_storage.p_max, p_max_expected)
        # if we charge by p_max we would expect the storage to result in SoC 100%
        test_storage.p = test_storage.p_max
        test_storage.p_set = test_storage.p_max
        test_storage.step(3)
        self.assertAlmostEqual(test_storage.soc, 1)

    def test_calc_p_min_hourly_loss(self):
        test_storage = BSS(
            eid="test_storage_0",
            e_bat_rated=10000,
            p_rated_discharge_max=-8000,
            p_rated_charge_max=8000,
            soc_init=0,
            loss_rate=0.02,
            step_size=60 * 60,
            dod_max=1,
            discharge_efficiency_init=1,
            charge_efficiency_init=1,
            status_curve=False,
        )
        p_min_expected = 0
        for t in range(1, 3):
            test_storage.p = 1000
            test_storage.p_set = 1000
            # we charge by 1000 Wh, loss is applied to the energy state before discharging
            test_storage.step(t)
            p_min_expected -= 1000 - (test_storage.e_bat * (0.02 / 24 / 30))
            self.assertAlmostEqual(test_storage.p_min, p_min_expected)
        # if we discharge by p_min we would expect the storage to result in SoC 0%
        # and p_min should not discharge below 0 Wh e_bat
        # => test_storage.p == p_min_expected
        test_storage.p = test_storage.p_min
        test_storage.p_set = test_storage.p_min
        test_storage.step(3)
        self.assertAlmostEqual(test_storage.p, p_min_expected)
        self.assertAlmostEqual(test_storage.soc, 0)

    def test_set_power_within_limit(self):
        test_storage = BSS(
            eid="test_storage_0",
            e_bat_rated=10000,
            p_rated_discharge_max=-8000,
            p_rated_charge_max=8000,
            soc_init=0,
            loss_rate=0.02,
            step_size=60 * 60,
            dod_max=1,
            discharge_efficiency_init=1,
            charge_efficiency_init=1,
            status_curve=False,
        )

        # p_set is a dict
        test_storage.p_set = {"BSS_0": 1000, "BSS_1": 2000}
        test_storage.step(1)
        self.assertAlmostEqual(test_storage.p, 3000)

        # p_set is None
        test_storage.p_set = None
        test_storage.step(1)
        self.assertAlmostEqual(test_storage.p, 0)

    def test_calc_power_limits(self):
        test_storage = BSS(
            eid="test_storage_0",
            e_bat_rated=10000,
            p_rated_discharge_max=-8000,
            p_rated_charge_max=8000,
            soc_init=0,
            loss_rate=0.02,
            step_size=60 * 60,
            dod_max=1,
            discharge_efficiency_init=0,
            charge_efficiency_init=0,
            status_curve=False,
        )

        # test for (dis)charge efficiency = 0
        test_storage.step(1)
        self.assertAlmostEqual(test_storage.p_min, 0)
        self.assertAlmostEqual(test_storage.p_max, 0)

    def test_calc_power_limits_errors(self):
        test_storage = BSS(
            eid="test_storage_0",
            e_bat_rated=10000,
            dod_max=0.9,
            p_rated_discharge_max=-8000,
            p_rated_charge_max=8000,
            step_size=60 * 60,
            discharge_efficiency_init=1.0,
            charge_efficiency_init=1.0,
        )
        test_storage.timestep = 1

        # create positive minimum power
        test_storage.e_bat = 10000 - 90 / 100 * 10000 - 10  # lower than minimum power
        with self.assertRaises(ValueError) as message:
            test_storage._calc_power_limits()
        str_error = "The discharge rate of the BSS cannot be positive."
        self.assertEqual(str(message.exception), str_error)

        # create positive minimum power
        test_storage.e_bat = 10000 + 10  # higher than maximum power
        with self.assertRaises(ValueError) as message:
            test_storage._calc_power_limits()
        str_error = "The charge rate of the BSS cannot be negative."
        self.assertEqual(str(message.exception), str_error)

    def test_calc_dis_charge_efficiency(self):
        test_storage = BSS(
            eid="test_storage_0",
            e_bat_rated=10000,
            p_rated_discharge_max=-8000,
            p_rated_charge_max=8000,
            soc_init=0,
            loss_rate=0.02,
            step_size=60 * 60,
            dod_max=1,
            discharge_efficiency_init=0,
            charge_efficiency_init=0,
            status_curve=True,
            bat2ac_efficiency=[-0.000623, -1.65, 0.9591],
            ac2bat_efficiency=[-0.0006864, -1.641, 0.957],
        )

        test_storage.p = 1000
        test_storage.timestep = 1
        test_storage._calc_charge_efficiency()
        test_storage._calc_discharge_efficiency()
        self.assertAlmostEqual(test_storage.charge_efficiency, 0.9362)
        self.assertAlmostEqual(test_storage.discharge_efficiency, 0.9398)

    def test_calc_efficiency_not_curve(self):
        test_storage = BSS(
            eid="test_storage_0",
            e_bat_rated=10000,
            p_rated_discharge_max=-8000,
            p_rated_charge_max=8000,
            soc_init=0,
            loss_rate=0.02,
            step_size=60 * 60,
            dod_max=1,
            discharge_efficiency_init=1.0,
            charge_efficiency_init=1.0,
            status_curve=False,
            bat2ac_efficiency=[-0.000623, -1.65, 0.9591],
            ac2bat_efficiency=[-0.0006864, -1.641, 0.957],
        )

        test_storage.p = 1000
        test_storage.timestep = 1
        test_storage._calc_charge_efficiency()
        test_storage._calc_discharge_efficiency()
        self.assertAlmostEqual(test_storage.charge_efficiency, 1.0)
        self.assertAlmostEqual(test_storage.discharge_efficiency, 1.0)


if __name__ == "__main__":
    suite = unittest.TestLoader().loadTestsFromTestCase(TestBSS)
    unittest.TextTestRunner(verbosity=0).run(suite)
