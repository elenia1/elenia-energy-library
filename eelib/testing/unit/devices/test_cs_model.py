"""
Unittest for charging station of eELib

| Author: elenia@TUBS
| Copyright 2024 elenia
| This file is part of eELib, which is free software under the terms of the GNU GPL Version 3.
"""

import unittest
import time
import warnings

from eelib.core.devices.charging_station.charging_station_model import ChargingStation
from eelib.data import EVData


class TestChargingStationModel(unittest.TestCase):
    """The first four methods/functions are infrastructure classes for the tests."""

    @classmethod
    def setUpClass(cls):
        pass

    @classmethod
    def tearDownClass(cls):
        pass

    def setUp(self):
        print(f"[START] {self.id()}")
        self.start = time.time()

    def tearDown(self):
        t = time.time() - self.start
        print(f"[END] {self.id()}: {t:.4f}")

    def test_init_efficiency_ac(self):
        # should produce NO WARNING, as efficiencies for AC charging are set to 1
        ChargingStation(
            eid="test_cs_0",
            p_rated=11000,
            output_type="AC",
            charge_efficiency=1.0,
            discharge_efficiency=1,
            step_size=60 * 15,
        )
        # should produce a warning, as efficiencies for AC charging are NOT set to 1
        with warnings.catch_warnings(record=True) as w:
            # Enable warnings to be recorded
            warnings.simplefilter("always")

            ChargingStation(
                eid="test_cs_0",
                p_rated=11000,
                output_type="AC",
                charge_efficiency=1.0,
                discharge_efficiency=0.99,
                step_size=60 * 15,
            )

            # Check if one warning of the expected type is issued
            self.assertEqual(len(w), 1)
            self.assertEqual(w[0].category, UserWarning)
            self.assertIn(
                "WARNING: Efficiency of AC charging is instead set to 1!", str(w[0].message)
            )

    def test_power_limits(self):
        test_cs = ChargingStation(
            eid="test_cs_0",
            p_rated=11000,
            output_type="AC",
            charge_efficiency=1,
            discharge_efficiency=1,
            step_size=60 * 15,
        )

        # create ev dataclasses
        test_ev_0 = EVData(
            soc_min=0.0,
            e_max=40000,
            p_nom_discharge_max=11000,
            p_nom_charge_max=11000,
            dcharge_efficiency=1.0,
            charge_efficiency=1.0,
            e_bat=70000,
        )
        test_ev_1 = EVData(
            soc_min=0.0,
            e_max=40000,
            p_nom_discharge_max=11000,
            p_nom_charge_max=11000,
            dcharge_efficiency=1.0,
            charge_efficiency=1.0,
            e_bat=70000,
        )

        # create input dict for charging station from dataclasses
        test_ev_data = {"ev_0": test_ev_0, "ev_1": test_ev_1}

        # test setting of min./max. power after limits from available cars
        test_ev_data["ev_0"].appearance = True
        test_ev_data["ev_1"].appearance = True
        test_ev_data["ev_0"].p_max = 5000
        test_ev_data["ev_1"].p_max = 1000
        test_ev_data["ev_0"].p_min = -1000
        test_ev_data["ev_1"].p_min = -2000
        test_cs.ev_data = test_ev_data
        test_cs._calc_power_limits()
        self.assertAlmostEqual(test_cs.p_max, 6000)
        self.assertAlmostEqual(test_cs.p_min, -3000)

        # test setting of min./max. power after limits from partly available cars
        test_ev_data["ev_0"].appearance = False
        test_ev_data["ev_1"].appearance = True
        test_cs.ev_data = test_ev_data
        test_cs._calc_power_limits()
        self.assertAlmostEqual(test_cs.p_max, 1000)
        self.assertAlmostEqual(test_cs.p_min, -2000)

        # test setting of min./max. power after limits from unavailable cars
        test_ev_data["ev_0"].appearance = False
        test_ev_data["ev_1"].appearance = False
        test_cs.ev_data = test_ev_data
        test_cs._calc_power_limits()
        self.assertAlmostEqual(test_cs.p_max, 0)
        self.assertAlmostEqual(test_cs.p_min, 0)

        # test raising error with incompatible limits from car
        test_ev_data["ev_0"].appearance = True
        test_ev_data["ev_0"].p_max = 5000
        test_ev_data["ev_0"].p_min = 6000
        test_cs.ev_data = test_ev_data
        with self.assertRaises(ValueError):
            test_cs._calc_power_limits()

        # test (dis)charging limits including efficiency with one-sided limits from car
        test_ev_data["ev_0"].appearance = True
        test_ev_data["ev_0"].p_max = 1000
        test_ev_data["ev_0"].p_min = -500
        test_cs.ev_data = test_ev_data
        test_cs.discharge_efficiency = 0.95
        test_cs.charge_efficiency = 0.9
        test_cs.p = 500
        test_cs._calc_power_limits()
        self.assertAlmostEqual(test_cs.p_min, -500 / 0.9)
        test_ev_data["ev_0"].appearance = True
        test_ev_data["ev_0"].p_max = 500
        test_ev_data["ev_0"].p_min = -1000
        test_cs.ev_data = test_ev_data
        test_cs.p = -500
        test_cs._calc_power_limits()
        self.assertAlmostEqual(test_cs.p_max, 500 * 0.95)

    def test_efficiency(self):
        test_cs = ChargingStation(
            eid="test_cs_0",
            p_rated=10000,
            output_type="DC",
            charge_efficiency=0.9,
            discharge_efficiency=0.95,
            step_size=60 * 15,
        )
        test_cs.p = 1000
        test_cs._calc_current_efficiency()
        self.assertAlmostEqual(test_cs.efficiency, 0.9)
        test_cs.p = -1000
        test_cs._calc_current_efficiency()
        self.assertAlmostEqual(test_cs.efficiency, 1 / 0.95)

    def test_power_setting_strict_limits(self):
        test_cs = ChargingStation(
            eid="test_cs_0",
            p_rated=10000,
            output_type="DC",
            charge_efficiency=0.9,
            discharge_efficiency=0.95,
            step_size=60 * 15,
        )

        # create ev dataclasses
        test_ev_0 = EVData(
            soc_min=0.0,
            e_max=40000,
            p_nom_discharge_max=11000,
            p_nom_charge_max=11000,
            dcharge_efficiency=1.0,
            charge_efficiency=1.0,
            e_bat=70000,
        )

        test_ev_1 = EVData(
            soc_min=0.0,
            e_max=40000,
            p_nom_discharge_max=11000,
            p_nom_charge_max=11000,
            dcharge_efficiency=1.0,
            charge_efficiency=1.0,
            e_bat=70000,
        )

        # create input dict for charging station from dataclasses
        test_ev_data = {"ev_0": test_ev_0, "ev_1": test_ev_1}

        # test power setting with strict limits for the electric vehicles
        test_cs.p_set = {}
        test_ev_data["ev_0"].p_max = 4000
        test_ev_data["ev_1"].p_max = -1000
        test_ev_data["ev_0"].p_min = 4000
        test_ev_data["ev_1"].p_min = -1000
        test_ev_data["ev_0"].appearance = True
        test_ev_data["ev_1"].appearance = True
        test_cs.ev_data = test_ev_data
        test_cs.step(timestep=0)
        self.assertAlmostEqual(test_cs.p, (4000 - 1000) / 0.9)
        test_ev_data["ev_0"].p_max = 1000
        test_ev_data["ev_1"].p_max = -4000
        test_ev_data["ev_0"].p_min = 1000
        test_ev_data["ev_1"].p_min = -4000
        test_ev_data["ev_0"].appearance = True
        test_ev_data["ev_1"].appearance = True
        test_cs.ev_data = test_ev_data
        test_cs.step(timestep=1)
        self.assertAlmostEqual(test_cs.p, (1000 - 4000) * 0.95)

        # non-working power limits for power value
        test_ev_data["ev_0"].appearance = True
        test_ev_data["ev_1"].appearance = True
        test_ev_data["ev_0"].p_max = 1000
        test_ev_data["ev_1"].p_max = 1000
        test_ev_data["ev_0"].p_min = -1000
        test_ev_data["ev_1"].p_min = -1000
        test_cs.ev_data = test_ev_data
        test_cs.p = 4000
        with self.assertRaises(ValueError):
            test_cs._distribute_charging_power()
        test_cs.p = -4000
        with self.assertRaises(ValueError):
            test_cs._distribute_charging_power()

    def test_power_setting_open_limits(self):
        test_cs = ChargingStation(
            eid="test_cs_0",
            p_rated=10000,
            output_type="DC",
            charge_efficiency=0.9,
            discharge_efficiency=0.95,
            step_size=60 * 15,
        )

        # create ev dataclasses
        test_ev_0 = EVData(
            soc_min=0.0,
            e_max=40000,
            p_nom_discharge_max=11000,
            p_nom_charge_max=11000,
            dcharge_efficiency=1.0,
            charge_efficiency=1.0,
            e_bat=70000,
        )

        test_ev_1 = EVData(
            soc_min=0.0,
            e_max=40000,
            p_nom_discharge_max=11000,
            p_nom_charge_max=11000,
            dcharge_efficiency=1.0,
            charge_efficiency=1.0,
            e_bat=70000,
        )

        # create input dict for charging station from dataclasses
        test_ev_data = {"ev_0": test_ev_0, "ev_1": test_ev_1}

        # test power setting with strict limits for the electric vehicles
        test_cs.p_set = {}
        test_ev_data["ev_0"].p_max = 40000
        test_ev_data["ev_1"].p_max = 20000
        test_ev_data["ev_0"].p_min = 0
        test_ev_data["ev_1"].p_min = -20000
        test_ev_data["ev_0"].appearance = True
        test_ev_data["ev_1"].appearance = True
        test_cs.ev_data = test_ev_data
        test_cs.step(timestep=0)
        self.assertAlmostEqual(test_cs.p, 10000)

        test_ev_data["ev_0"].p_max = 0
        test_ev_data["ev_1"].p_max = 20000
        test_ev_data["ev_0"].p_min = -40000
        test_ev_data["ev_1"].p_min = -20000
        test_cs.ev_data = test_ev_data
        test_cs.step(timestep=0)
        self.assertAlmostEqual(test_cs.p, 10000)

    def test_power_set_values(self):
        test_cs = ChargingStation(
            eid="test_cs_0",
            p_rated=10000,
            output_type="AC",
            charge_efficiency=1,
            discharge_efficiency=1,
            step_size=60 * 15,
        )

        test_ev_0 = EVData(
            soc_min=0.0,
            e_max=40000,
            p_nom_discharge_max=11000,
            p_nom_charge_max=11000,
            dcharge_efficiency=1.0,
            charge_efficiency=1.0,
            e_bat=70000,
        )
        test_ev_1 = EVData(
            soc_min=0.0,
            e_max=40000,
            p_nom_discharge_max=11000,
            p_nom_charge_max=11000,
            dcharge_efficiency=1.0,
            charge_efficiency=1.0,
            e_bat=70000,
        )

        # create input dict for charging station from dataclasses
        test_ev_data = {"ev_0": test_ev_0, "ev_1": test_ev_1}

        test_cs.p_set = {"test_ems": 10000}
        test_ev_data["ev_0"].p_max = 10000
        test_ev_data["ev_1"].p_max = 10000
        test_ev_data["ev_0"].p_min = 0
        test_ev_data["ev_1"].p_min = -10000
        test_ev_data["ev_0"].appearance = True
        test_ev_data["ev_1"].appearance = True
        test_cs.ev_data = test_ev_data
        test_cs.step(timestep=0)
        self.assertAlmostEqual(test_cs.p, 10000)

        test_cs.p_set = {"test_ems": -15000}
        test_cs.ev_data = test_ev_data
        test_cs.step(timestep=0)
        self.assertAlmostEqual(test_cs.p, -10000)

        test_cs.p_set = -15000
        test_cs.ev_data = test_ev_data
        test_cs.step(timestep=0)
        self.assertAlmostEqual(test_cs.p, -10000)

    def test_power_for_devices(self):
        test_cs = ChargingStation(
            eid="test_cs_0",
            p_rated=10000,
            output_type="AC",
            charge_efficiency=1,
            discharge_efficiency=1,
            step_size=60 * 15,
        )

        # create ev dataclasses
        test_ev_0 = EVData(
            soc_min=0.0,
            e_max=40000,
            p_nom_discharge_max=11000,
            p_nom_charge_max=11000,
            dcharge_efficiency=1.0,
            charge_efficiency=1.0,
            e_bat=70000,
        )
        test_ev_1 = EVData(
            soc_min=0.0,
            e_max=40000,
            p_nom_discharge_max=11000,
            p_nom_charge_max=11000,
            dcharge_efficiency=1.0,
            charge_efficiency=1.0,
            e_bat=70000,
        )

        # create input dict for charging station from dataclasses
        test_ev_data = {"ev_0": test_ev_0, "ev_1": test_ev_1}

        # for equal distribution of charging power
        test_ev_data["ev_0"].p_max = 1000
        test_ev_data["ev_1"].p_max = 1000
        test_ev_data["ev_0"].p_min = 0
        test_ev_data["ev_1"].p_min = -1000
        test_ev_data["ev_0"].appearance = True
        test_ev_data["ev_1"].appearance = True
        test_cs.ev_data = test_ev_data
        test_cs.p = 1000
        test_cs._distribute_charging_power()
        self.assertAlmostEqual(test_cs.p_device["ev_0"], 500)
        self.assertAlmostEqual(test_cs.p_device["ev_1"], 500)

        # for unequal distribution of charging power
        test_ev_data["ev_0"].p_max = 250
        test_ev_data["ev_1"].p_max = 1000
        test_cs.ev_data = test_ev_data
        test_cs._distribute_charging_power()
        self.assertAlmostEqual(test_cs.p_device["ev_0"], 250)
        self.assertAlmostEqual(test_cs.p_device["ev_1"], 750)

    def test_non_appearance_ev(self):
        # create cs dataclasses
        test_cs = ChargingStation(
            eid="cs1",
            p_rated=10000,
            output_type="AC",
            charge_efficiency=1.0,
            discharge_efficiency=1.0,
            cos_phi=1.0,
            step_size=900,
        )

        # create ev objects, one with appearace False
        test_ev1 = EVData(appearance=True, p_max=5000, p_min=0)
        test_ev2 = EVData(appearance=False, p_max=5000, p_min=0)

        # create input dict for charging station from dataclasses
        test_cs.ev_data = {"ev1": test_ev1, "ev2": test_ev2}
        test_cs._distribute_charging_power()

        self.assertTrue("ev2" in test_cs.p_device)
        self.assertEqual(test_cs.p_device["ev2"], 0)
        self.assertFalse(
            test_ev2 in [ev.appearance for ev in test_cs.ev_data.values() if ev.appearance]
        )

    def test_distribute_no_considered_cars(self):
        # create cs dataclasses
        test_cs = ChargingStation(
            eid="cs1",
            p_rated=10000,
            output_type="AC",
            charge_efficiency=1.0,
            discharge_efficiency=1.0,
            cos_phi=1.0,
            step_size=900,
        )

        # create ev objects
        test_ev1 = EVData(appearance=False, p_max=5000, p_min=0)
        test_ev2 = EVData(appearance=False, p_max=5000, p_min=0)
        test_cs.ev_data = {"ev1": test_ev1, "ev2": test_ev2}

        # set power for charging station (should not be possible)
        test_cs.p = 5000

        # try to distribute that power
        test_cs._distribute_charging_power()
        self.assertListEqual([0, 0], list(test_cs.p_device.values()))


if __name__ == "__main__":
    suite = unittest.TestLoader().loadTestsFromTestCase(TestChargingStationModel)
    unittest.TextTestRunner(verbosity=0).run(suite)
