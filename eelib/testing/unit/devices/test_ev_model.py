"""
Unittest for car_model of eELib

| Author: elenia@TUBS
| Copyright 2024 elenia
| This file is part of eELib, which is free software under the terms of the GNU GPL Version 3.
"""

import unittest
from unittest.mock import patch
from datetime import datetime, timedelta
import time
import os
import pandas as pd
import gzip
import pickle

from eelib.core.devices.ev.ev_model import EV

pickle_filename_state_consumption = "input_15min.pickle"
consumption_profile = [0 for i in range(0, 86400, 900)]
state_profile = ["home" for i in range(0, 86400, 900)]
for i in range(18, 23):
    consumption_profile[i] = 5  # consumption in kWh
    state_profile[i] = "none"
input_pickle_15min = {
    "refdate": "01/01/2020",
    "t": 0.25,
    "totalrows": 96,
    "soc_init": 0.5,
    "soc_min": 0.02,
    "battery_capacity": 45,
    "discharging_eff": 0.95,
    "charging_eff": 0.9,
    "chargingdata": {"capacity_charging_point": {"home": 11}},
    "profile": pd.DataFrame(
        {
            "consumption": consumption_profile,
            "charging_point": state_profile,
            "hh": pd.date_range("2020-01-01 00:00:00", "2020-01-01 23:59:59", freq="900S"),
        }
    ),
}
pickle_filename_state_consumption_error = "input_15min_err.pickle"
input_pickle_15min_error = input_pickle_15min.copy()
input_pickle_15min_error["chargingdata"] = {"capacity_charging_point": {"workplace": 11}}

pickle_filename_10min = "input_10min.pickle"
consumption_profile = [0 for i in range(0, 900 * 24 * 6, 900)]
state_profile = ["home" for i in range(0, 900 * 24 * 6, 900)]
for i in range(18, 25):
    consumption_profile[i] = 5  # consumption in kWh
    state_profile[i] = "none"
input_pickle_10min = {
    "refdate": "01/01/2020",
    "t": 10 / 60,
    "totalrows": 144,
    "soc_init": 0.5,
    "soc_min": 0.02,
    "battery_capacity": 45,
    "discharging_eff": 0.95,
    "charging_eff": 0.9,
    "chargingdata": {"capacity_charging_point": {"home": 11}},
    "profile": pd.DataFrame(
        {
            "consumption": consumption_profile,
            "charging_point": state_profile,
            "hh": pd.date_range("2020-01-01 00:00:00", "2020-01-01 23:59:59", freq="600S"),
        }
    ),
}

faulty_pickle_filename = "faulty_input.pickle"


class TestCarModel(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        if os.path.exists(pickle_filename_state_consumption):
            os.remove(pickle_filename_state_consumption)
        if os.path.exists(pickle_filename_state_consumption_error):
            os.remove(pickle_filename_state_consumption_error)
        if os.path.exists(pickle_filename_10min):
            os.remove(pickle_filename_10min)
        if os.path.exists(faulty_pickle_filename):
            os.remove(faulty_pickle_filename)
        with gzip.open(pickle_filename_state_consumption, "wb") as f:
            pickle.dump(input_pickle_15min, f)
        with gzip.open(pickle_filename_state_consumption_error, "wb") as f:
            pickle.dump(input_pickle_15min_error, f)
        with gzip.open(pickle_filename_10min, "wb") as f:
            pickle.dump(input_pickle_10min, f)

    @classmethod
    def tearDownClass(cls):
        os.remove(pickle_filename_state_consumption)
        os.remove(pickle_filename_state_consumption_error)
        os.remove(pickle_filename_10min)
        os.remove(faulty_pickle_filename)

    def setUp(self):
        print(f"[START] {self.id()}")
        self.start = time.time()

    def tearDown(self):
        t = time.time() - self.start
        print(f"[END] {self.id()}: {t:.4f}")

    def test_set_emobpy_params(self):
        test_car_model_emobpy = EV(
            eid="test_car_0",
            start_time="2020-01-01 00:00:00",
            file_emobpy=pickle_filename_state_consumption,
            step_size=900,
            soc_init=1.0,
            e_max=70000,
            dcharge_efficiency=0.99,
            charge_efficiency=0.98,
            soc_min=0.05,
            set_emobpy_val=True,
        )
        test_car_model_data = EV(
            eid="test_car_0",
            start_time="2020-01-01 00:00:00",
            file_emobpy=pickle_filename_state_consumption,
            step_size=900,
            soc_init=1.0,
            e_max=70000,
            dcharge_efficiency=0.99,
            charge_efficiency=0.98,
            soc_min=0.05,
            set_emobpy_val=False,
        )

        self.assertEqual(test_car_model_emobpy.soc, 0.5)
        self.assertEqual(test_car_model_emobpy.e_max, 45000)
        self.assertEqual(test_car_model_emobpy.dcharge_efficiency, 0.95)
        self.assertEqual(test_car_model_emobpy.charge_efficiency, 0.9)
        self.assertEqual(test_car_model_emobpy.soc_min, 0.02)

        self.assertEqual(test_car_model_data.soc, 1.0)
        self.assertEqual(test_car_model_data.e_max, 70000)
        self.assertEqual(test_car_model_data.dcharge_efficiency, 0.99)
        self.assertEqual(test_car_model_data.charge_efficiency, 0.98)
        self.assertEqual(test_car_model_data.soc_min, 0.05)

    def test_appearance(self):
        test_car_model = EV(
            eid="test_car_0",
            start_time="2020-01-01 00:00:00",
            file_emobpy=pickle_filename_state_consumption,
            step_size=900,
            soc_init=1.0,
            set_emobpy_val=False,
        )
        for step_time in range(0, 96):
            test_car_model.step(timestep=step_time)
            if step_time < 18 or step_time >= 23:
                self.assertEqual(test_car_model.appearance, True)
            else:
                self.assertEqual(test_car_model.appearance, False)

    def test_consumption(self):
        # test for 15 min profile
        test_car_model = EV(
            eid="test_car_0",
            start_time="2020-01-01 00:00:00",
            file_emobpy=pickle_filename_state_consumption,
            step_size=900,
            soc_init=1.0,
            set_emobpy_val=False,
        )
        for step_time in range(0, 96):
            test_car_model.step(step_time)
            if step_time < 18 or step_time >= 23:
                self.assertEqual(test_car_model.consumption, 0)
            else:
                self.assertEqual(test_car_model.consumption, 5)

        # test for 10 min profiles
        test_car_model = EV(
            eid="test_car_0",
            start_time="2020-01-01 00:00:00",
            file_emobpy=pickle_filename_10min,
            step_size=600,
            soc_init=1.0,
            set_emobpy_val=False,
        )
        for step_time in range(0, 144):
            test_car_model.step(step_time)
            if step_time < 18 or step_time >= 25:
                self.assertEqual(test_car_model.consumption, 0)
            else:
                self.assertEqual(test_car_model.consumption, 5)

    def test_soc_and_ebat(self):
        # test for 15 min time resolution
        step_size = 900
        test_car_model = EV(
            eid="test_car_0",
            start_time="2020-01-01 00:00:00",
            file_emobpy=pickle_filename_state_consumption,
            e_max=40000,
            step_size=step_size,
            dcharge_efficiency=1.0,
            soc_init=1.0,
            set_emobpy_val=False,
        )
        consumption_e = 5 * 5000
        for i_time in range(0, 96):
            test_car_model.step(timestep=i_time)
        self.assertAlmostEqual(test_car_model.soc, 1 - consumption_e / 40000, places=4)
        self.assertAlmostEqual(test_car_model.e_bat, 40000 - consumption_e, places=2)

        # test for 10 min time resolution
        step_size = 600
        test_car_model = EV(
            eid="test_car_0",
            start_time="2020-01-01 00:00:00",
            file_emobpy=pickle_filename_10min,
            e_max=40000,
            step_size=step_size,
            dcharge_efficiency=1.0,
            soc_init=1.0,
            set_emobpy_val=False,
        )
        consumption_e = 7 * 5000
        for i_time in range(0, 144):
            test_car_model.step(timestep=i_time)
        self.assertAlmostEqual(test_car_model.soc, 1 - consumption_e / 40000, places=4)
        self.assertAlmostEqual(test_car_model.e_bat, 40000 - consumption_e, places=2)

    def test_appearance_duration(self):
        test_car_model = EV(
            eid="test_car_0",
            start_time="2020-01-01 00:00:00",
            file_emobpy=pickle_filename_state_consumption,
            step_size=900,
            set_emobpy_val=True,
        )
        test_car_model.step(timestep=0)
        self.assertEqual(test_car_model.appearance_duration, 18 * 900)

    def test_power_limits(self):
        test_car_model = EV(
            eid="test_car_0",
            start_time="2020-01-01 00:00:00",
            file_emobpy=pickle_filename_state_consumption,
            step_size=900,
            soc_init=0.95,
            e_max=10000,
            p_nom_discharge_max=0,
            charge_efficiency=0.95,
            set_emobpy_val=False,
        )
        test_car_model.step(timestep=0)
        self.assertAlmostEqual(test_car_model.p_min, 0)
        self.assertAlmostEqual(test_car_model.p_max, (1 - 0.95) * 10000 / 0.95 * 4)

        test_car_model = EV(
            eid="test_car_0",
            start_time="2020-01-01 00:00:00",
            file_emobpy=pickle_filename_state_consumption,
            step_size=900,
            soc_init=0.05,
            e_max=10000,
            charge_efficiency=0.95,
            dcharge_efficiency=0.95,
            p_nom_discharge_max=-11000,
            p_nom_charge_max=11000,
            set_emobpy_val=False,
        )
        test_car_model.step(timestep=0)
        self.assertAlmostEqual(test_car_model.p_min, -0.05 * 10000 * 0.95 * 4)
        self.assertAlmostEqual(test_car_model.p_max, 11000)

    def test_set_values(self):
        test_car_model = EV(
            eid="test_car_0",
            start_time="2020-01-01 00:00:00",
            file_emobpy=pickle_filename_state_consumption,
            step_size=900,
            soc_init=0.80,
        )

        test_car_model.p_cha = {"test_charging_station": 11000}
        test_car_model.step(timestep=0)
        self.assertAlmostEqual(test_car_model.p, 11000)

        test_car_model.p_cha = {"test_charging_station": 4000}
        test_car_model.step(timestep=10)
        self.assertAlmostEqual(test_car_model.p, 4000)

        test_car_model.p_cha = {"test_charging_station": 4000}
        test_car_model.step(timestep=20)
        self.assertAlmostEqual(test_car_model.p, -5000 * 4)

        test_car_model.p_cha = {"test_charging_station": -15000}
        test_car_model.step(timestep=30)
        self.assertAlmostEqual(test_car_model.p, -11000)

    def test_e_with_set_values(self):
        test_car_model = EV(
            eid="test_car_0",
            start_time="2020-01-01 00:00:00",
            file_emobpy=pickle_filename_state_consumption,
            step_size=900,
            soc_init=0.95,
            soc_min=0.0,
            e_max=50000,
            charge_efficiency=0.9,
            dcharge_efficiency=0.9,
            set_emobpy_val=False,
        )

        test_car_model.p_cha = {"test_charging_station": 11000}
        test_car_model.step(timestep=0)
        test_car_model.step(timestep=1)
        self.assertAlmostEqual(test_car_model.e_bat, 0.95 * 50000 + 11000 * 0.9 / 4)
        test_car_model.step(timestep=2)
        self.assertAlmostEqual(test_car_model.e_bat, 50000)

        test_car_model.p_cha = {"test_charging_station": -6000}
        test_car_model.step(timestep=3)
        test_car_model.e_bat = 2000
        test_car_model.step(timestep=4)
        self.assertAlmostEqual(test_car_model.e_bat, 2000 - 6000 / 0.9 / 4)
        test_car_model.step(timestep=5)
        self.assertAlmostEqual(test_car_model.e_bat, 0)

    def test_efficiency(self):
        test_car_model = EV(
            eid="test_car_0",
            start_time="2020-01-01 00:00:00",
            file_emobpy=pickle_filename_state_consumption,
            step_size=900,
            soc_init=0.90,
            soc_min=0.0,
            e_max=10000,
            charge_efficiency=0.95,
            dcharge_efficiency=0.98,
            set_emobpy_val=False,
        )
        test_car_model.step(timestep=0)
        self.assertAlmostEqual(test_car_model.p_max, (1 - 0.9) * 10000 * 4 / 0.95)
        test_car_model.p = -1000
        test_car_model.step(timestep=1)
        self.assertAlmostEqual(test_car_model.e_bat, 0.9 * 10000 - 1000 / 4 / 0.98)

        test_car_model = EV(
            eid="test_car_0",
            start_time="2020-01-01 00:00:00",
            file_emobpy=pickle_filename_state_consumption,
            step_size=900,
            soc_init=0.10,
            soc_min=0.0,
            e_max=10000,
            charge_efficiency=0.95,
            dcharge_efficiency=0.98,
            set_emobpy_val=False,
        )
        test_car_model.step(timestep=0)
        self.assertAlmostEqual(test_car_model.p_min, -0.1 * 10000 * 4 * 0.98)
        test_car_model.p = 1000
        test_car_model.step(timestep=1)
        self.assertAlmostEqual(test_car_model.e_bat, 0.1 * 10000 + 1000 / 4 * 0.95)

    def test_profile_length(self):
        test_car_model = EV(
            eid="test_car_0",
            start_time="2020-01-01 00:00:00",
            file_emobpy=pickle_filename_state_consumption,
            step_size=900,
            soc_init=0.95,
            soc_min=0.0,
            e_max=10000,
            charge_efficiency=1,
            dcharge_efficiency=1,
            set_emobpy_val=False,
        )
        for time_step in range(0, 24 * 4):
            test_car_model.step(timestep=time_step)
        self.assertEqual(test_car_model.timestep, 24 * 4 - 1)

        # if one step to far, check for thrown index error
        with self.assertRaises(IndexError):
            test_car_model.step(timestep=time_step + 1)

    def test_init_with_key_error(self):
        consumption_profile = [0] * 96
        state_profile = [0] * 96
        faulty_pickle_data = {
            "refdate": "01/01/2020",
            "t": 0.25,
            "totalrows": 96,
            "battery_capacity": 45,
            "discharging_eff": 0.95,
            "charging_eff": 0.9,
            "chargingdata": {"capacity_charging_point": {"home": 11}},
            "profile": pd.DataFrame(
                {
                    "consumption": consumption_profile,
                    "charging_point": state_profile,
                    "hh": pd.date_range("2020-01-01 00:00:00", "2020-01-01 23:59:59", freq="900S"),
                }
            ),
        }
        with gzip.open(faulty_pickle_filename, "wb") as f:
            pickle.dump(faulty_pickle_data, f)

        with self.assertRaises(KeyError):
            EV(
                eid="test_car_0",
                start_time="2020-01-01 00:00:00",
                file_emobpy=faulty_pickle_filename,
                step_size=900,
                soc_init=1.0,
                e_max=70000,
                dcharge_efficiency=0.99,
                charge_efficiency=0.98,
                soc_min=0.05,
            )
            _ = faulty_pickle_data["soc_init"]

    def test_calc_power_with_float_and_none(self):
        step_size = 900
        start_time = "2020-01-01 00:00:00"
        soc_init = 1.0
        e_max = 70000
        ev = EV(
            eid="test_car_0",
            start_time=start_time,
            file_emobpy=pickle_filename_state_consumption,
            step_size=step_size,
            soc_init=soc_init,
            e_max=e_max,
            dcharge_efficiency=0.99,
            charge_efficiency=0.98,
            soc_min=0.05,
        )
        ev.consumption = 0
        ev.p_cha = 7.5
        ev.p_max = 10000
        ev.p_min = -10000
        ev.p = None
        ev._calc_power()
        self.assertEqual(ev.p, 7.5)

    def test_set_emobpy_data_index_error(self):
        with patch.object(EV, "__init__", lambda x, **kwargs: None):
            ev = EV()
            ev.eid = "test_car_0"
            ev.start_time = datetime(2019, 1, 1)
            ev.file_emobpy = pickle_filename_state_consumption
            ev.step_size = 900
            ev.soc_init = 1.0
            ev.e_max = 70000
            ev.dcharge_efficiency = 0.99
            ev.charge_efficiency = 0.98
            ev.soc_min = 0.05
            ev.emobpy_data = {"profile": None, "refdate": "01/01/2020", "totalrows": 96, "t": 0.25}
            ev.emobpy_data["start_time"] = datetime.strptime(ev.emobpy_data["refdate"], "%d/%m/%Y")
            ev.emobpy_data["end_time"] = ev.emobpy_data["start_time"] + timedelta(hours=24)
            with self.assertRaises(IndexError):
                ev._set_emobpy_data(10)

    def test_set_emobpy_data_error_num_steps(self):
        with self.assertRaises(ValueError) as message:
            _ = EV(
                eid="test_car_0",
                start_time="2020-01-01 18:00:00",
                file_emobpy=pickle_filename_state_consumption,
                step_size=900,
                n_steps=8 * 4,  # too many steps for profile of 1 day (8h with 6h left)
            )
        str_error = (
            "32 steps for test_car_0 too much for emobpy-pickle, in which "
            "24 steps are left from start time."
        )
        self.assertEqual(str(message.exception), str_error)

    def test_emobpy_charging_point_error(self):
        with self.assertRaises(Warning) as message:
            _ = EV(
                eid="test_car_0",
                start_time="2020-01-01 18:00:00",
                file_emobpy=pickle_filename_state_consumption_error,
                step_size=900,
                set_emobpy_val=True,
            )
        str_error = "There is no charging point home in selected emobpy-pickle"
        self.assertEqual(str(message.exception), str_error)

    def test_set_emobpy_data_value_error(self):
        ev = EV(
            eid="test_car_0",
            start_time="2020-01-01 00:00:00",
            file_emobpy=pickle_filename_state_consumption,
            step_size=900,
            soc_init=1.0,
            e_max=70000,
            dcharge_efficiency=0.99,
            charge_efficiency=0.98,
            soc_min=0.05,
        )
        ev.emobpy_data["t"] = 0.2
        with self.assertRaises(ValueError):
            ev._set_emobpy_data(10)

    def test_set_energy_within_limit_over_max(self):
        with patch.object(EV, "__init__", lambda x, **kwargs: None):
            ev = EV()
            ev.e_max = 10000
            ev.e_bat = 15000
            ev._set_energy_within_limit()
            self.assertEqual(ev.e_bat, ev.e_max)

    def test_calc_power_limits_with_zero_efficiencies(self):
        with patch.object(EV, "__init__", lambda x, **kwargs: None):
            ev = EV()
            ev.dcharge_efficiency = 0
            ev.charge_efficiency = 0
            ev.e_bat = 5000
            ev.e_max = 10000
            ev.step_size = 900
            ev.p_nom_discharge_max = -1000
            ev.p_nom_charge_max = 2000
            ev._calc_power_limits()
            self.assertEqual(ev.p_min, 0)
            self.assertEqual(ev.p_max, 0)


if __name__ == "__main__":
    suite = unittest.TestLoader().loadTestsFromTestCase(TestCarModel)
    unittest.TextTestRunner(verbosity=0).run(suite)
