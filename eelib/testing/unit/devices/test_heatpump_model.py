"""
Unittest for heatpump model of eELib.

| Author: elenia@TUBS
| Copyright 2024 elenia
| This file is part of eELib, which is free software under the terms of the GNU GPL Version 3.
"""

import unittest
import time
import warnings

from eelib.core.devices.heatpump.heatpump_model import Heatpump


class TestHeatpumpModel(unittest.TestCase):
    """The first four methods/functions are infrastructure classes for the tests."""

    @classmethod
    def setUpClass(cls):
        pass

    @classmethod
    def tearDownClass(cls):
        pass

    def setUp(self):
        print(f"[START] {self.id()}")
        self.start = time.time()

    def tearDown(self):
        t = time.time() - self.start
        print(f"[END] {self.id()}: {t:.4f}")

    def test_init_p_min_rel(self):
        # should produce NO WARNING, as modulation is set to 1
        Heatpump(
            eid="test_hp_0",
            p_rated_th=10000,
            modulation=0,
            p_min_th_rel=1.0,
            cop=3.60584,
        )
        # should produce a warning, as modulation is NOT set to 1
        with warnings.catch_warnings(record=True) as w:
            # Enable warnings to be recorded
            warnings.simplefilter("always")

            Heatpump(
                eid="test_hp_0",
                p_rated_th=10000,
                modulation=0,
                p_min_th_rel=0.5,
                cop=3.60584,
            )

            # Check if one warning of the expected type is issued
            self.assertEqual(len(w), 1)
            self.assertEqual(w[0].category, UserWarning)
            self.assertIn(
                "WARNING: Minimum thermal relative power is instead set to 1!", str(w[0].message)
            )

    def test_p_el_with_set_value(self):
        # create model entity with init_vals
        cop_fix = 3.60584
        p_th_rated = 15000
        test_heatpump = Heatpump(
            eid="test_hp_0",
            p_rated_th=p_th_rated,
            modulation=1,
            p_min_th_rel=0.5,
            cop=cop_fix,
        )
        # execute step function of tested model
        test_heatpump.p_th_set = -7000
        test_heatpump.step(timestep=0)
        # compare results
        self.assertAlmostEqual(test_heatpump.p, p_th_rated / cop_fix * 0.5, delta=0.0001)

    def test_p_th_with_set_value(self):
        # create model entity with init_vals
        test_heatpump = Heatpump(
            eid="test_hp_0",
            p_rated_th=15000,
            modulation=1,
            p_min_th_rel=0.5,
            cop=3.60584,
        )
        # execute step function of tested model
        test_heatpump.p_th_set = -7000
        test_heatpump.step(timestep=0)
        # compare results
        self.assertAlmostEqual(test_heatpump.p_th, -7500)

    def test_p_th_without_set_value(self):
        # create model entity with init_vals
        test_heatpump = Heatpump(
            eid="test_hp_0",
            p_rated_th=15000,
            modulation=1,
            p_min_th_rel=0.5,
            cop=3.60584,
        )
        # execute step function of tested model
        test_heatpump.step(timestep=0)
        # compare results
        self.assertAlmostEqual(test_heatpump.p_th, 0)

    def test_p_th_with_set_dict(self):
        # create model entity with init_vals
        test_heatpump = Heatpump(
            eid="test_hp_0",
            p_rated_th=15000,
            modulation=1,
            p_min_th_rel=0.5,
            cop=3.60584,
        )
        # execute step function of tested model
        test_heatpump.p_th_set = {"test_hp_p": -10000}
        test_heatpump.step(timestep=0)
        # compare results
        self.assertAlmostEqual(test_heatpump.p_th, -10000)

    def test_p_th_non_modulating(self):
        # create model entity with init_vals
        test_heatpump = Heatpump(
            eid="test_hp_0",
            p_rated_th=15000,
            modulation=0,
            p_min_th_rel=1,
            cop=3.60584,
        )
        # execute step function of tested model
        test_heatpump.p_th_set = -12000
        test_heatpump.step(timestep=0)
        # compare results
        self.assertAlmostEqual(test_heatpump.p_th, -15000)

    def test_state(self):
        # create model entity with init_vals
        time_min = 1800
        test_heatpump = Heatpump(eid="test_hp_0", time_min=time_min)
        # execute step function of tested model with different operation
        test_heatpump.time_on = 0
        test_heatpump.time_off = 0
        test_heatpump.p_th = -1000
        test_heatpump._set_state()
        self.assertIs(test_heatpump.state, "must_on")
        test_heatpump.time_on = time_min + 200
        test_heatpump._set_state()
        self.assertIs(test_heatpump.state, "on")
        test_heatpump.time_on = 0
        test_heatpump.time_off = 0
        test_heatpump.p_th = 0
        test_heatpump._set_state()
        self.assertIs(test_heatpump.state, "must_off")
        test_heatpump.time_off = time_min + 200
        test_heatpump._set_state()
        self.assertIs(test_heatpump.state, "off")

    def test_thermal_limits(self):
        # create model entity with init_vals
        p_th_max = 1000
        p_min_th_rel = 0.4
        test_heatpump = Heatpump(eid="test_hp_0", p_rated_th=p_th_max, p_min_th_rel=p_min_th_rel)
        # execute step function of tested model with different states
        test_heatpump.state = "must_on"
        test_heatpump._calc_thermal_limits()
        self.assertEqual(test_heatpump.p_th_max, -p_th_max)
        self.assertEqual(test_heatpump.p_th_min_on, -p_th_max * p_min_th_rel)
        self.assertEqual(test_heatpump.p_th_min, -p_th_max * p_min_th_rel)
        test_heatpump.state = "on"
        test_heatpump._calc_thermal_limits()
        self.assertEqual(test_heatpump.p_th_max, -p_th_max)
        self.assertEqual(test_heatpump.p_th_min_on, -p_th_max * p_min_th_rel)
        self.assertEqual(test_heatpump.p_th_min, 0)
        test_heatpump.state = "must_off"
        test_heatpump._calc_thermal_limits()
        self.assertEqual(test_heatpump.p_th_max, 0)
        self.assertEqual(test_heatpump.p_th_min_on, 0)
        self.assertEqual(test_heatpump.p_th_min, 0)
        test_heatpump.state = "off"
        test_heatpump._calc_thermal_limits()
        self.assertEqual(test_heatpump.p_th_max, -p_th_max)
        self.assertEqual(test_heatpump.p_th_min_on, -p_th_max * p_min_th_rel)
        self.assertEqual(test_heatpump.p_th_min, 0)

    def test_time_on(self):
        # create model entity with init_vals
        test_heatpump = Heatpump(
            eid="test_hp_0",
            p_rated_th=15000,
            modulation=1,
            p_min_th_rel=0.5,
            cop=3.60584,
        )
        # execute step function of tested model
        test_heatpump.p_th = 0
        test_heatpump.step(timestep=900)
        for time_sec in range(2700, 4500, 900):
            test_heatpump.p_th = -12000 + time_sec
            test_heatpump.step(timestep=time_sec)
        # compare results
        self.assertAlmostEqual(test_heatpump.time_on, 4500 - 2700)

    def test_step_time(self):
        # create model entity with init_vals
        test_heatpump = Heatpump(
            eid="test_hp_0",
        )
        # execute step function of tested model
        test_heatpump.p_th = -10000
        test_heatpump.p_th_set = -10000
        test_heatpump.step(timestep=0)
        # at the beginning, timing is set to minimum (all states possible)
        self.assertEqual(test_heatpump.time_on, test_heatpump.time_min)
        self.assertEqual(test_heatpump.time_off, test_heatpump.time_min)
        test_heatpump.step(timestep=0)
        # no change, as this is the same timestep
        self.assertEqual(test_heatpump.time_on, test_heatpump.time_min)
        self.assertEqual(test_heatpump.time_off, test_heatpump.time_min)
        test_heatpump.step(timestep=900)
        # afterwards, times are adapted
        self.assertEqual(test_heatpump.time_on, test_heatpump.time_min + 900)
        self.assertEqual(test_heatpump.time_off, 0)
        test_heatpump.p_th = 0
        test_heatpump.p_th_set = 0
        test_heatpump.step(timestep=1800)
        # afterwards, times are adapted
        self.assertEqual(test_heatpump.time_on, 0)
        self.assertEqual(test_heatpump.time_off, 900)

    def test_set_with_must_off(self):
        # create model entity with init_vals
        test_heatpump = Heatpump(
            eid="test_hp_0",
        )
        # execute step function of tested model
        test_heatpump.p_th = -10000
        test_heatpump.p_th_set = -10000
        test_heatpump.state = "must_off"
        test_heatpump.step(timestep=0)
        # check that no thermal generation
        self.assertEqual(test_heatpump.p_th, 0)

    def test_set_with_must_on(self):
        # create model entity with init_vals
        test_heatpump = Heatpump(eid="test_hp_0", modulation=0, p_rated_th=10000)
        # execute step function of tested model
        test_heatpump.p_th = 0
        test_heatpump.p_th_set = 0
        test_heatpump.state = "must_on"
        test_heatpump.step(timestep=0)
        # check that no thermal generation
        self.assertEqual(test_heatpump.p_th, -10000)


if __name__ == "__main__":
    suite = unittest.TestLoader().loadTestsFromTestCase(TestHeatpumpModel)
    unittest.TextTestRunner(verbosity=0).run(suite)
