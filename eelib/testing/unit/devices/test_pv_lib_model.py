"""
Unittest for pv_lib model of eELib

| Author: elenia@TUBS
| Copyright 2024 elenia
| This file is part of eELib, which is free software under the terms of the GNU GPL Version 3.
"""

import unittest
import time

from eelib.core.devices.pv.pv_lib_model import PVLib, PVLibExact


class TestPVLibModel(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        pass

    @classmethod
    def tearDownClass(cls):
        pass

    def setUp(self):
        print(f"[START] {self.id()}")
        self.start = time.time()

    def tearDown(self):
        t = time.time() - self.start
        print(f"[END] {self.id()}: {t:.4f}")

    def test_init_error(self):
        with self.assertRaises(ValueError) as message:
            _ = PVLib(eid="test_pv_lib_0", step_size=2 * 3600)
        str_error = (
            "Simulation step size is larger than 60 minutes, failing the resolution of pvlib"
            " weather data."
        )
        self.assertTrue(str(message.exception) == str_error)

    # Test power limits for both models
    def test_pv_lib_limits(self):
        standby_losses = 5
        min_power = 100
        test_pv_model = PVLib(
            eid="test_pv_lib_0", losses_standby=standby_losses, min_power=min_power
        )

        test_pv_model.pvlib_mc.results.dc.iloc[45] = 1010
        test_pv_model.pvlib_mc.results.ac.iloc[45] = 1000
        test_pv_model.step(timestep=45)
        self.assertAlmostEqual(test_pv_model.p_min, -min_power)
        self.assertAlmostEqual(test_pv_model.p_max, -1000)

        test_pv_model.pvlib_mc.results.dc.iloc[10] = 2
        test_pv_model.pvlib_mc.results.ac.iloc[10] = 1.8
        test_pv_model.step(timestep=10)
        self.assertAlmostEqual(test_pv_model.p_min, standby_losses)
        self.assertAlmostEqual(test_pv_model.p_max, standby_losses)

    def test_pv_lib_exact_limits(self):
        test_pv_model = PVLibExact(
            eid="test_pv_lib_exact_0",
        )

        test_pv_model.pvlib_mc.results.dc["p_mp"].iloc[45] = 1010
        test_pv_model.pvlib_mc.results.ac.iloc[45] = 1000
        test_pv_model.step(timestep=45)
        self.assertAlmostEqual(test_pv_model.p_min, -108.314354)
        self.assertAlmostEqual(test_pv_model.p_max, -1000)

        test_pv_model.pvlib_mc.results.dc["p_mp"].iloc[10] = 10
        test_pv_model.pvlib_mc.results.ac.iloc[10] = 10
        test_pv_model.step(timestep=10)
        self.assertAlmostEqual(test_pv_model.p_min, 3)
        self.assertAlmostEqual(test_pv_model.p_max, 3)

    # Test set values for base model
    def test_p_set(self):
        set_value_p = -1000
        test_pv_model = PVLib(eid="test_pv_lib_0", cos_phi=0.9)
        test_pv_model.p_max = -2000
        test_pv_model.p_min = 0

        # set value as dict
        test_pv_model.p_set = {"test_ems": set_value_p}
        test_pv_model._set_power()
        self.assertAlmostEqual(test_pv_model.p, set_value_p)

        # set value as float
        test_pv_model.p_set = set_value_p
        test_pv_model._set_power()
        self.assertAlmostEqual(test_pv_model.p, set_value_p)

        # set value cannot be set
        test_pv_model.p_max = 3.6
        test_pv_model.p_min = 3.6
        test_pv_model._set_power()
        self.assertAlmostEqual(test_pv_model.p, 3.6)

    # Test efficiency for pv_lib model
    def test_pv_lib_efficiency(self):
        eff_test = 0.99
        test_pv_model = PVLib(eid="test_pv_lib_0", inverter_efficiency=eff_test)
        test_pv_model_full = PVLib(eid="test_pv_lib_0", inverter_efficiency=1.0)

        for step in range(41, 47):  # test generation during noon time (gen > 0)
            test_pv_model.step(timestep=step)
            test_pv_model_full.step(timestep=step)
            self.assertAlmostEqual(test_pv_model.p / test_pv_model_full.p, eff_test)

    # Test different inverter and module numbers for pv_lib_exact model
    def test_pv_lib_exact_mod_and_inv(self):
        test_pv_model = PVLibExact(
            eid="test_pv_lib_exact_0", num_modules_per_string=18, num_strings=1, num_inverters=3
        )
        test_pv_model.pvlib_mc.results.dc.iloc[45] = 1100
        test_pv_model.pvlib_mc.results.ac.iloc[45] = 1000
        test_pv_model.step(timestep=45)
        self.assertAlmostEqual(test_pv_model.p, -1000)

        test_pv_model_1 = PVLibExact(
            eid="test_pv_lib_exact_1", num_modules_per_string=18, num_strings=8, num_inverters=4
        )
        test_pv_model_1.pvlib_mc.results.dc.iloc[45] = 20500
        test_pv_model_1.pvlib_mc.results.ac.iloc[45] = 20000
        test_pv_model_1.step(timestep=45)
        self.assertAlmostEqual(test_pv_model_1.p, -20000.0)

    # Test different dates for both models
    def test_pv_lib_date(self):
        test_pv = PVLib(eid="test_pv_0", start_time="2016-01-01 00:00:00", step_size=600)
        test_pv_shift = PVLib(eid="test_pv_shi_0", start_time="2016-01-01 03:00:00", step_size=600)

        # test if shifted model produces energy first
        step = 0
        test_pv.step(timestep=step)
        test_pv_shift.step(timestep=step)
        while test_pv_shift.p >= 0:
            step += 1
            test_pv.step(timestep=step)
            test_pv_shift.step(timestep=step)
        # now test_pv should still be zero or above
        self.assertGreaterEqual(test_pv.p, 0)

    def test_pv_lib_exact_date(self):
        t_pv = PVLibExact(eid="test_pv_0", start_time="2016-01-01 00:00:00", step_size=600)
        t_pv_sh = PVLibExact(eid="test_pv_shi_0", start_time="2016-01-01 03:00:00", step_size=600)

        # test if shifted model produces energy first
        step = 0
        t_pv.step(timestep=step)
        t_pv_sh.step(timestep=step)
        while t_pv_sh.p >= 0:
            step += 1
            t_pv.step(timestep=step)
            t_pv_sh.step(timestep=step)
        # now test_pv should still be zero or above
        self.assertGreaterEqual(t_pv.p, 0)

    def test_step(self):
        test_pv_model = PVLib(eid="test_pv_lib_0", start_time="2016-01-01 00:00:00", step_size=3600)
        test_pv_model.step(timestep=0)
        self.assertEqual(test_pv_model.timestep, 0)
        self.assertEqual(test_pv_model.p_max, 3.6)
        self.assertEqual(test_pv_model.p_min, 3.6)
        test_pv_model.step(timestep=0)  # same step, nothing should change
        self.assertEqual(test_pv_model.timestep, 0)
        self.assertEqual(test_pv_model.p_max, 3.6)
        self.assertEqual(test_pv_model.p_min, 3.6)
        test_pv_model.step(timestep=1)  # different step now
        self.assertEqual(test_pv_model.timestep, 1)

        test_pv_model = PVLibExact(
            eid="test_pv_lib_exact_0", start_time="2016-01-01 00:00:00", step_size=3600
        )
        test_pv_model.step(timestep=0)
        self.assertEqual(test_pv_model.timestep, 0)
        self.assertEqual(test_pv_model.p_max, 3.0)
        self.assertEqual(test_pv_model.p_min, 3.0)
        test_pv_model.step(timestep=0)  # same step, nothing should change
        self.assertEqual(test_pv_model.timestep, 0)
        self.assertEqual(test_pv_model.p_max, 3.0)
        self.assertEqual(test_pv_model.p_min, 3.0)
        test_pv_model.step(timestep=1)  # different step now
        self.assertEqual(test_pv_model.timestep, 1)


if __name__ == "__main__":
    suite = unittest.TestLoader().loadTestsFromTestCase(TestPVLibModel)
    unittest.TextTestRunner(verbosity=0).run(suite)
