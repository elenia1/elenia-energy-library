"""
Unittest for pandapower grid model of eELib

| Author: elenia@TUBS
| Copyright 2024 elenia
| This file is part of eELib, which is free software under the terms of the GNU GPL Version 3.
"""

import unittest
import time

from eelib.core.grid.pandapower.pandapower_model import Pandapower
import pandapower.networks as ppn
import numpy as np


class TestPandapower(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        pass

    @classmethod
    def tearDownClass(cls):
        pass

    def setUp(self):
        print(f"[START] {self.id()}")
        self.start = time.time()

    def tearDown(self):
        t = time.time() - self.start
        print(f"[END] {self.id()}: {t:.4f}")

    def test_load_example_cases(self):
        test_grid_0 = Pandapower(eid="Grid-0.test_grid_0", gridfile="cigre_lv", sim_start=None)
        test_grid_1 = Pandapower(
            eid="Grid-0.test_grid_1", gridfile="kerber_land_kb_1", sim_start=None
        )
        test_grid_2 = Pandapower(
            eid="Grid-0.test_grid_2", gridfile="kerber_land_fl_1", sim_start=None
        )
        test_grid_3 = Pandapower(eid="Grid-0.test_grid_3", gridfile="cigre_hv", sim_start=None)
        test_grid_4 = Pandapower(eid="Grid-0.test_grid_4", gridfile="cigre_mv", sim_start=None)
        test_grid_5 = Pandapower(eid="Grid-0.test_grid_5", gridfile="cigre_mv_all", sim_start=None)
        test_grid_6 = Pandapower(
            eid="Grid-0.test_grid_6", gridfile="cigre_mv_pv_wind", sim_start=None
        )
        test_grid_7 = Pandapower(
            eid="Grid-0.test_grid_7", gridfile="kerber_land_kb_2", sim_start=None
        )
        test_grid_8 = Pandapower(
            eid="Grid-0.test_grid_8", gridfile="kerber_land_fl_2", sim_start=None
        )
        test_grid_9 = Pandapower(eid="Grid-0.test_grid_9", gridfile="kerber_dorf", sim_start=None)
        test_grid_10 = Pandapower(
            eid="Grid-0.test_grid_10", gridfile="kerber_vorstd_kb_1", sim_start=None
        )
        test_grid_11 = Pandapower(
            eid="Grid-0.test_grid_11", gridfile="kerber_vorstd_kb_2", sim_start=None
        )
        # test first grid
        self.assertIsNotNone(test_grid_0.grid)
        self.assertTrue(
            test_grid_0.grid.line.iloc[4].equals(ppn.create_cigre_network_lv().line.iloc[4]),
        )
        # test second grid for different fields
        self.assertEqual(len(test_grid_1.grid), len(ppn.create_kerber_landnetz_kabel_1()))
        self.assertTrue(test_grid_1.grid.bus.equals(ppn.create_kerber_landnetz_kabel_1().bus))
        self.assertTrue(test_grid_1.grid.trafo.equals(ppn.create_kerber_landnetz_kabel_1().trafo))
        # test all other grids to check if non-empty
        self.assertIsNot(test_grid_2.grid, {})
        self.assertIsNot(test_grid_3.grid, {})
        self.assertIsNot(test_grid_4.grid, {})
        self.assertIsNot(test_grid_5.grid, {})
        self.assertIsNot(test_grid_6.grid, {})
        self.assertIsNot(test_grid_7.grid, {})
        self.assertIsNot(test_grid_8.grid, {})
        self.assertIsNot(test_grid_9.grid, {})
        self.assertIsNot(test_grid_10.grid, {})
        self.assertIsNot(test_grid_11.grid, {})

    def test_init_errors(self):
        # wrong name for pandapower network given
        with self.assertRaises(ValueError) as message:
            _ = Pandapower(eid="Grid-0.tg0", gridfile="notexist", sim_start=None)
        str_error = "No handling for 'notexist' implemented"
        self.assertEqual(str(message.exception), str_error)

        # wrong name for json file given
        with self.assertRaises(UserWarning) as message:
            _ = Pandapower(eid="Grid-0.tg0", gridfile="notexist.json", sim_start=None)
        str_error = "File notexist.json does not exist!!"
        self.assertEqual(str(message.exception), str_error)

    def test_get_components(self):
        test_grid = Pandapower(
            eid="Grid-0.test_grid", gridfile="simple_four_bus_system", sim_start=None
        )

        # testing getting components
        self.assertEqual(test_grid._get_slack(), (0, 0))
        self.assertEqual(len(test_grid._get_buses()), 3)
        self.assertEqual(test_grid._get_buses()[1], (2, 0.4))
        self.assertEqual(len(test_grid._get_loads()), 2)
        self.assertEqual(test_grid._get_loads()[1], ("bus4", 30000.0, 10000.0, 1.0, True))
        self.assertEqual(len(test_grid._get_lines()), 2)
        self.assertEqual(test_grid._get_lines()[1], (2, 3, 0.5, 0.642, 0.083, 210.0, 142.0, True))
        self.assertEqual(len(test_grid._get_trafos()), 1)
        self.assertEqual(
            test_grid._get_trafos()[0][0:11],
            (
                0,
                1,
                250000.0,
                10.0,
                0.4,
                4.0,
                1.2,
                600,
                0.24,
                150.0,
                "hv",
            ),
        )

    def test_set_inputs(self):
        test_grid = Pandapower(eid="Grid-0.test_grid", gridfile="kerber_land_kb_1", sim_start=None)

        power_load = 10
        tap_trafo = 2
        possible_taps = [0, 1, 2, 3, 4, 5, 6, 7]

        # set power value
        test_grid.set_inputs(etype="load", idx=5, data={"p_w": power_load}, static=None)
        self.assertAlmostEqual(
            test_grid.grid.load["p_mw"][5], power_load / 1000000 / test_grid.grid.load["scaling"][5]
        )

        # set power value with adjusted scaling value
        test_grid.grid.load.at[5, "scaling"] = None
        test_grid.set_inputs(etype="load", idx=5, data={"p_w": power_load}, static=None)
        self.assertAlmostEqual(test_grid.grid.load["p_mw"][5], power_load / 1000000 / 1)

        # set reactive power value (at different bus)
        test_grid.set_inputs(etype="load", idx=2, data={"q_var": power_load / 4}, static=None)
        self.assertAlmostEqual(
            test_grid.grid.load["q_mvar"][2],
            power_load / 4 / 1000000 / test_grid.grid.load["scaling"][2],
        )

        # set trafo tap
        test_grid.set_inputs(
            etype="trafo", idx=0, data={"tap_turn": tap_trafo}, static={"tap_pos": possible_taps}
        )
        self.assertEqual(test_grid.grid.trafo["tap_pos"][0], 1 / possible_taps[tap_trafo])

        # set input with unavailabe element
        with self.assertRaises(ValueError) as message:
            test_grid.set_inputs(etype="bus", idx=3, data={"vm_pu": 1.0}, static=None)
        str_error = "etype bus unknown"
        self.assertEqual(str(message.exception), str_error)

        # set input with unavailabe field
        with self.assertRaises(KeyError) as message:
            test_grid.set_inputs(etype="load", idx=3, data={"no_field": 1.0}, static=None)
        str_error = "'Field no_field not possible for element load of grid'"
        self.assertEqual(str(message.exception), str_error)

        # set input with unavailabe field
        with self.assertRaises(KeyError) as message:
            test_grid.set_inputs(etype="trafo", idx=0, data={"no_field": 1.0}, static=None)
        str_error = "'Field no_field not possible for element trafo of grid'"
        self.assertEqual(str(message.exception), str_error)

    def test_step(self):
        test_grid = Pandapower(
            eid="Grid-0.test_grid", gridfile="simple_four_bus_system", sim_start=None
        )
        test_grid.set_inputs("load", 0, {"p_w": 1000, "q_var": 0}, {})
        test_grid.set_inputs("load", 0, {"p_w": 1000, "q_var": 200}, {})
        self.assertEqual(test_grid.powerflow_results, {})
        self.assertEqual(test_grid.grid_status, {})
        self.assertEqual(test_grid.ptdf_mat, [])
        test_grid.step(timestep=0)
        self.assertNotEqual(test_grid.powerflow_results, {})
        self.assertNotEqual(test_grid.grid_status, {})
        self.assertEqual(test_grid.ptdf_mat.shape, (3, 4))

    def test_step_error(self):
        test_grid = Pandapower(
            eid="Grid-0.test_grid", gridfile="simple_four_bus_system", sim_start=None
        )
        test_grid.grid.line.at[1, "max_i_ka"] = 0
        test_grid.grid.trafo.at[1, "sn_mva"] = 0.1
        test_grid.set_inputs("load", 0, {"p_w": 1000, "q_var": 0}, {})
        test_grid.set_inputs("load", 0, {"p_w": 1000, "q_var": 200}, {})

        with self.assertRaises(np.linalg.LinAlgError) as message:
            test_grid.step(timestep=0)  # powerflow will not converge
        str_error = "Singular matrix"
        self.assertEqual(str(message.exception), str_error)

    def test_powerflow(self):
        test_grid = Pandapower(
            eid="Grid-0.test_grid", gridfile="simple_four_bus_system", sim_start=None
        )
        test_grid.set_inputs("load", 0, {"p_w": 1000, "q_var": 0}, {})
        test_grid.set_inputs("load", 0, {"p_w": 1000, "q_var": 200}, {})
        test_grid._powerflow()

        self.assertAlmostEqual(test_grid.grid.res_bus["vm_pu"][2], 1.00553219825)
        self.assertAlmostEqual(test_grid.grid.res_line["i_ka"][1], 0.025234556)
        self.assertAlmostEqual(test_grid.grid.res_trafo["loading_percent"][0], 1.87243323)
        self.assertAlmostEqual(test_grid.grid.res_ext_grid["p_mw"][0], -0.00274212306)

    def test_store_powerflow_results(self):
        test_grid = Pandapower(
            eid="Grid-0.test_grid", gridfile="simple_four_bus_system", sim_start=None
        )
        test_grid.set_inputs("load", 0, {"p_w": 1000, "q_var": 0}, {})
        test_grid.set_inputs("load", 0, {"p_w": 1000, "q_var": 200}, {})
        test_grid._powerflow()
        test_grid._store_power_flow_results()

        self.assertAlmostEqual(test_grid.powerflow_results["bus3"]["vm_pu"], 1.005532198)
        self.assertAlmostEqual(test_grid.powerflow_results["bus1ref"]["p_w"], -2742.1230617)
        self.assertAlmostEqual(test_grid.powerflow_results["line1"]["loading_percent"], 4.761892885)
        self.assertAlmostEqual(
            test_grid.powerflow_results["transformer"]["va_lv_degree"], 0.03562987166
        )

    def test_store_pf_results_unconverged(self):
        test_grid = Pandapower(
            eid="Grid-0.test_grid", gridfile="simple_four_bus_system", sim_start=None
        )
        test_grid.grid.line.at[1, "max_i_ka"] = 0
        test_grid.grid.trafo.at[1, "sn_mva"] = 0.1
        test_grid.set_inputs("load", 0, {"p_w": 1000, "q_var": 0}, {})
        test_grid.set_inputs("load", 0, {"p_w": 1000, "q_var": 200}, {})
        test_grid._store_power_flow_results()  # with no powerflow results

        self.assertIsNone(test_grid.powerflow_results["bus3"]["vm_pu"])
        self.assertIsNone(test_grid.powerflow_results["bus1ref"]["p_w"])
        self.assertIsNone(test_grid.powerflow_results["line1"]["loading_percent"])
        self.assertIsNone(test_grid.powerflow_results["transformer"]["va_lv_degree"])

    def test_grid_status(self):
        test_grid = Pandapower(
            eid="Grid-0.test_grid", gridfile="simple_four_bus_system", sim_start=None
        )
        test_grid.set_inputs("load", 1, {"p_w": 1000, "q_var": 500}, {})
        test_grid._powerflow()
        test_grid._get_grid_status()
        self.assertEqual(test_grid.grid_status["congested"], False)
        test_grid.set_inputs("load", 1, {"p_w": 50000, "q_var": 50000}, {})
        test_grid._powerflow()
        test_grid._get_grid_status()
        self.assertEqual(test_grid.grid_status["congested"], True)
        self.assertEqual("bus" in test_grid.grid_status.keys(), True)
        self.assertEqual("line" in test_grid.grid_status.keys(), True)
        self.assertEqual("trafo" in test_grid.grid_status.keys(), True)
        self.assertEqual("ext_grid" in test_grid.grid_status.keys(), True)

        # for MV grid with HV bus
        test_grid = Pandapower(eid="Grid-0.test_grid", gridfile="cigre_hv", sim_start=None)
        test_grid.set_inputs("load", 1, {"p_w": 1000, "q_var": 500}, {})
        test_grid._powerflow()
        test_grid._get_grid_status()
        self.assertEqual("bus" in test_grid.grid_status.keys(), True)
        self.assertEqual(test_grid.grid_status["bus"]["Bus 1"]["vmax"], 1.1)
        self.assertEqual(test_grid.grid_status["bus"]["Bus 1"]["vmin"], 1.0)

        # for MV grid with HV bus
        test_grid = Pandapower(eid="Grid-0.test_grid", gridfile="cigre_mv", sim_start=None)
        test_grid.set_inputs("load", 1, {"p_w": 1000, "q_var": 500}, {})
        test_grid._powerflow()
        test_grid._get_grid_status()
        self.assertEqual("ext_grid" in test_grid.grid_status.keys(), True)
        self.assertEqual(test_grid.grid_status["ext_grid"]["Bus 0"]["vmax"], 1.1)
        self.assertEqual(test_grid.grid_status["ext_grid"]["Bus 0"]["vmin"], 0.95)

    def test_status_bus(self):
        test_grid = Pandapower(
            eid="Grid-0.test_grid", gridfile="panda_four_load_branch", sim_start=None
        )
        test_grid.set_inputs("load", 2, {"p_w": 200000, "q_var": 100000}, {})
        test_grid._powerflow()
        bus_status_zero = test_grid._get_status_bus(comp_idx=0)
        bus_status_end = test_grid._get_status_bus(comp_idx=5)
        self.assertEqual(bus_status_zero["vm"], 1.0)
        self.assertEqual(bus_status_zero["congested"], False)
        self.assertAlmostEqual(bus_status_end["vm"], 0.88745370)
        self.assertEqual(bus_status_end["congested"], True)

    def test_status_line(self):
        test_grid = Pandapower(
            eid="Grid-0.test_grid", gridfile="panda_four_load_branch", sim_start=None
        )
        test_grid.set_inputs("load", 2, {"p_w": 200000, "q_var": 100000}, {})
        test_grid._powerflow()
        trafo_status = test_grid._get_status_line(comp_idx=0, comp_type="trafo")
        line_status = test_grid._get_status_line(comp_idx=3, comp_type="line")
        line_status_none = test_grid._get_status_line(comp_type="none", comp_idx=0)
        self.assertAlmostEqual(line_status["loading"], 21.252905496)
        self.assertEqual(line_status["congested"], False)
        self.assertAlmostEqual(trafo_status["loading"], 142.0682592176)
        self.assertEqual(trafo_status["congested"], True)
        self.assertEqual(line_status_none, None)

    def test_ptdf(self):
        test_grid = Pandapower(
            eid="Grid-0.test_grid", gridfile="simple_four_bus_system", sim_start=None
        )
        test_grid._powerflow()
        test_grid._calc_ptdf()
        # grid has 4 buses (idx=0 is trafo hv, reference bus) and 3 branches (1 trafo, 2 lines)
        self.assertEqual(test_grid.ptdf_mat.shape, (3, 4))
        # at minimum, shift of 1W has influence of -1W on a line (radial grid)
        self.assertEqual(test_grid.ptdf_mat.min(), -1)
        # for radial grid no impact on the lines behind the change, 0W is maximum
        self.assertEqual(test_grid.ptdf_mat.max(), 0)
        # exactly -1W/W impact on lines before, none on lines after (e.g. ref has no impact at all)
        # on first line (between bus 1 and 2) only bus 2 and 3 have impact
        self.assertListEqual(list(test_grid.ptdf_mat[0]), [0, 0, -1, -1])
        # on second line (between bus 2 and 3) only bus 3 has impact
        self.assertListEqual(list(test_grid.ptdf_mat[1]), [0, 0, 0, -1])
        # on trafo (between bus 0 and 1) only buses 1-3 have impact
        self.assertListEqual(list(test_grid.ptdf_mat[2]), [0, -1, -1, -1])

    def test_vpif(self):
        test_grid = Pandapower(
            eid="Grid-0.test_grid", gridfile="simple_four_bus_system", sim_start=None
        )
        test_grid.set_inputs("load", 0, {"p_w": 0, "q_var": 0}, {})
        test_grid.set_inputs("load", 0, {"p_w": 0, "q_var": 0}, {})
        test_grid._powerflow()
        test_grid._calc_vpif()
        # grid has 4 buses (idx=0 is trafo hv, reference bus) - ref bus not taken into matrix
        self.assertEqual(test_grid.vpif_mat.shape, (3, 6))
        # at maximum an adjustment of 0.01 per kW/kvar
        self.assertEqual(test_grid.vpif_mat.min() > -1e-5, True)
        # at minimum an adjustment of 0.01 per MW/Mvar
        self.assertEqual(test_grid.vpif_mat.max() < -1e-8, True)

        # for last bus, impact is increasing for p (vals decr.) and increasing for q (vals dec.)
        self.assertEqual(
            test_grid.vpif_mat[2, 0] > test_grid.vpif_mat[2, 1]
            and test_grid.vpif_mat[2, 1] > test_grid.vpif_mat[2, 2],
            True,
        )
        self.assertEqual(
            test_grid.vpif_mat[2, 3] > test_grid.vpif_mat[2, 4]
            and test_grid.vpif_mat[2, 4] > test_grid.vpif_mat[2, 5],
            True,
        )

    def test_vpif_failed(self):
        test_grid = Pandapower(
            eid="Grid-0.test_grid", gridfile="simple_four_bus_system", sim_start=None
        )
        test_grid._calc_vpif()  # not possible with no powerflow
        self.assertIsNone(test_grid.vpif_mat)


if __name__ == "__main__":
    suite = unittest.TestLoader().loadTestsFromTestCase(TestPandapower)
    unittest.TextTestRunner(verbosity=0).run(suite)
