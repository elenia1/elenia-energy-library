"""
Unittest for retail electricity provider of eELib

| Author: elenia@TUBS
| Copyright 2024 elenia
| This file is part of eELib, which is free software under the terms of the GNU GPL Version 3.
"""

import unittest
import time
from eelib.data import GridTariffSignal, MarketData

from eelib.core.market.retail_electricity_provider.rep_model import RetailElectricityProvider


class TestREP(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        pass

    @classmethod
    def tearDownClass(cls):
        pass

    def setUp(self):
        print(f"[START] {self.id()}")
        self.start = time.time()

    def tearDown(self):
        t = time.time() - self.start
        print(f"[END] {self.id()}: {t:.4f}")

    def test_add_market_entity(self):
        test_entity_dict_market = {
            "spot_0": {
                "eid": "spot_0",
                "type": "spot",
                "full_id": "S.spot_0",
            }
        }

        test_model = RetailElectricityProvider(eid="test_rep_0")
        # check list is empty
        self.assertEqual(len(test_model.market_list), 0)
        # check adding first entry
        test_model.add_market_entity(test_entity_dict_market)
        self.assertEqual(len(test_model.market_list), 1)
        # check adding specific entry
        self.assertDictContainsSubset(
            {"spot_0": {"full_id": "S.spot_0", "etype": "spot"}}, test_model.market_list
        )
        self.assertTrue("spot_0" in test_model.market_list.keys())
        # check adding same entry again
        test_model.add_market_entity({"spot_0": {"eid": "spot_0", "type": "spot"}})
        self.assertEqual(len(test_model.market_list), 1)

    def test_step(self):
        test_model = RetailElectricityProvider(
            eid="test_rep_0", tariff_type="static", elec_price=0.30, feedin_tariff=0.10
        )
        test_model.step(timestep=0)
        self.assertEqual(test_model.timestep, 0)
        test_model.step(timestep=1)
        self.assertEqual(test_model.timestep, 1)

    def test_request_forecast(self):
        test_model = RetailElectricityProvider(eid="test_rep_0")
        test_model.timestep = 0
        # add market entity
        test_entity_dict_market = {
            "spot_0": {
                "eid": "spot_0",
                "type": "spot",
                "full_id": "S.spot_0",
            }
        }
        test_model.add_market_entity(test_entity_dict_market)

        # without call, request should be empty
        self.assertTrue(test_model.forecast_request == {})

        # now with call, check length and fields
        test_model._request_forecast()
        self.assertEqual(len(test_model.forecast_request), 1)
        self.assertTrue("attr" in test_model.forecast_request["spot_0"].keys())
        self.assertTrue("t" in test_model.forecast_request["spot_0"].keys())

    def test_calc_tariff_error(self):
        test_model = RetailElectricityProvider(eid="test_rep_0", tariff_type="not_avail")

        # with wrong tariff type
        with self.assertRaises(ValueError) as message:
            test_model._calc_tariff_output()
        str_error = "No tariff type not_avail implemented for REP model!"
        self.assertTrue(str(message.exception) == str_error)

    def test_calc_tariff_static(self):
        test_model = RetailElectricityProvider(
            eid="test_rep_0", tariff_type="static", elec_price=0.30, feedin_tariff=0.10
        )

        # with no grid tariff
        test_model._calc_tariff_output()
        # checks with default grid energy tariff value
        self.assertEqual(test_model.tariff.elec_price, 0.30 + 0.08)
        self.assertEqual(test_model.tariff.feedin_tariff, 0.10)

        # assign empty grid tariff
        test_model.grid_tariff_signal = GridTariffSignal(
            bool_is_list=False, energy_price=0.0, steps=[]
        )
        # calc tariff
        test_model._calc_tariff_output()
        # checks
        self.assertEqual(test_model.tariff.elec_price, 0.30)
        self.assertEqual(test_model.tariff.feedin_tariff, 0.10)
        self.assertEqual(test_model.tariff.bool_is_list, False)

        # assign grid tariff
        test_model.grid_tariff_signal = GridTariffSignal(
            bool_is_list=False,
            energy_price=0.10,
            capacity_fee_dem=0.05,
            capacity_fee_gen=0.02,
            steps=[],
        )
        # calc tariff
        test_model._calc_tariff_output()
        # checks
        self.assertEqual(test_model.tariff.elec_price, 0.40)
        self.assertEqual(test_model.tariff.feedin_tariff, 0.10)
        self.assertEqual(test_model.tariff.capacity_fee_gen, 0.02)

    def test_calc_tariff_variable(self):
        test_model = RetailElectricityProvider(eid="test_rep_0", tariff_type="variable")
        # add market entity data for forecast
        test_model.market_data = {"spot_0": MarketData(price=100)}

        # assign cost terms
        elec_price = 0.3
        feedin_tariff = 0.1
        grid_elec_price = 0.05
        test_model.elec_price = elec_price
        test_model.feedin_tariff = feedin_tariff
        test_model.grid_tariff_signal = GridTariffSignal()
        test_model.grid_tariff_signal.energy_price = grid_elec_price

        # calc tariff
        test_model._calc_tariff_output()
        # check for value list
        pass

    def test_calc_tariff_dynamic(self):
        test_model = RetailElectricityProvider(eid="test_rep_0", tariff_type="dynamic")
        # add market entity data
        test_model.market_data = {"spot_0": MarketData(price=100)}

        # assign cost terms
        feedin_tariff = 0.1
        grid_elec_price = 0.05
        test_model.elec_price = 0.3
        test_model.feedin_tariff = feedin_tariff
        test_model.grid_tariff_signal = GridTariffSignal()
        test_model.grid_tariff_signal.energy_price = grid_elec_price

        # calc tariff
        test_model._calc_tariff_output()
        # check for value list
        self.assertAlmostEqual(test_model.tariff.elec_price, 0.1 + grid_elec_price)
        self.assertAlmostEqual(test_model.tariff.feedin_tariff, feedin_tariff)

        # test get error with more than one market data
        test_model.market_data["spot_1"] = MarketData(price=1000)
        with self.assertRaises(ValueError) as message:
            test_model._calc_tariff_output()
        str_error = "More than one market signal for retail electricity provider!"
        self.assertTrue(str(message.exception) == str_error)


if __name__ == "__main__":
    suite = unittest.TestLoader().loadTestsFromTestCase(TestREP)
    unittest.TextTestRunner(verbosity=0).run(suite)
