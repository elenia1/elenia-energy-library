"""
Unittest for Forecast of eELib

| Author: elenia@TUBS
| Copyright 2024 elenia
| This file is part of eELib, which is free software under the terms of the GNU GPL Version 3.
"""

import unittest
import time
import os
import pandas as pd
import gzip
import pickle

from eelib.core.forecast.forecast_model import Forecast

csv_filename_1h = "input_csv.csv"
input_df_1h = pd.DataFrame({"p": [i // 3600 for i in range(0, 86400, 3600)]})
input_df_1h.index = pd.date_range("2020-01-01 00:00:00", "2020-01-01 23:00:00", freq="3600S")

pickle_filename_1h = "input_pickle.pickle"
p_profile = [i // 3600 for i in range(0, 86400, 3600)]
input_pickle_1h = {
    "profile": pd.DataFrame(
        {
            "p": p_profile,
            "time": pd.date_range("2020-01-01 00:00:00", "2020-01-01 23:00:0", freq="3600S"),
        }
    ),
}


class ForecastEntity:
    def __init__(self, id, path):
        self.eid = id
        self.full_id = "Ent." + id
        self.path = path
        if path.split(".")[-1] == "csv":
            self.data = pd.read_csv(path, index_col=0)
        elif path.split(".")[-1] == "pickle":
            pickle_off = gzip.open(path, "rb")
            self.data = pickle.load(pickle_off)
            pickle_off.close()
        self.p = 0

    def step(self, timestep):
        if self.path.split(".")[-1] == "csv":
            self.p = self.data["p"].iat[timestep]
        elif self.path.split(".")[-1] == "pickle":
            self.p = self.data["profile"]["p"].iat[timestep]


class TestForecast(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        if os.path.exists(csv_filename_1h):
            os.remove(csv_filename_1h)
        input_df_1h[["p"]].to_csv(csv_filename_1h)
        with gzip.open(pickle_filename_1h, "wb") as f:
            pickle.dump(input_pickle_1h, f)

    @classmethod
    def tearDownClass(cls):
        os.remove(csv_filename_1h)
        os.remove(pickle_filename_1h)

    def setUp(self):
        print(f"[START] {self.id()}")
        self.start = time.time()

    def tearDown(self):
        t = time.time() - self.start
        print(f"[END] {self.id()}: {t:.4f}")

    def test_add_forecast_entitity(self):
        test_ent_csv = ForecastEntity(id="csv_0", path=csv_filename_1h)
        test_ent_pickle = ForecastEntity(id="pickle_0", path=pickle_filename_1h)

        test_fc_model = Forecast(eid="test_forecast_0", step_size=3600, n_steps=24)

        # check lists are empty
        self.assertEqual(len(test_fc_model.forecast_ent_by_fullid), 0)
        # check adding first entry
        test_fc_model.add_forecasted_entity({test_ent_csv.full_id: test_ent_csv})
        self.assertEqual(len(test_fc_model.forecast_ent_by_fullid), 1)
        # check adding second entry
        test_fc_model.add_forecasted_entity({test_ent_pickle.full_id: test_ent_pickle})
        self.assertEqual(len(test_fc_model.forecast_ent_by_fullid), 2)

    def test_full_day(self):
        test_ent_csv = ForecastEntity(id="csv_0", path=csv_filename_1h)
        test_ent_pickle = ForecastEntity(id="pickle_0", path=pickle_filename_1h)

        test_fc_model = Forecast(eid="test_forecast_0", step_size=3600, n_steps=24)
        test_fc_model.add_forecasted_entity({test_ent_csv.full_id: test_ent_csv})
        test_fc_model.add_forecasted_entity({test_ent_pickle.full_id: test_ent_pickle})

        time_range = range(0, 24)

        # check full forecast in first step
        test_fc_model.forecast_request = {
            "FE": {
                test_ent_csv.full_id: {"attr": "p", "t": time_range},
                test_ent_pickle.full_id: {"attr": "p", "t": time_range},
            }
        }
        test_fc_model.step(0)
        self.assertIsNot(test_fc_model.forecast, {})
        self.assertEqual(len(test_fc_model.forecast["FE"]["Ent.csv_0"]["p"]), 24)
        self.assertEqual(sum(test_fc_model.forecast["FE"]["Ent.csv_0"]["p"]), 23 * 24 / 2)
        self.assertEqual(len(test_fc_model.forecast["FE"]["Ent.pickle_0"]["p"]), 24)
        self.assertEqual(sum(test_fc_model.forecast["FE"]["Ent.pickle_0"]["p"]), 23 * 24 / 2)

        # check no forecasts in other steps
        test_fc_model.forecast_request = {}
        for step in range(1, 24):
            test_fc_model.step(step)
            self.assertEqual(test_fc_model.forecast, {})

    def test_rolling_horizon(self):
        test_ent_csv = ForecastEntity(id="csv_0", path=csv_filename_1h)
        test_ent_pickle = ForecastEntity(id="pickle_0", path=pickle_filename_1h)

        test_fc_model = Forecast(eid="test_forecast_0", step_size=3600, n_steps=24)
        test_fc_model.add_forecasted_entity({test_ent_csv.full_id: test_ent_csv})
        test_fc_model.add_forecasted_entity({test_ent_pickle.full_id: test_ent_pickle})

        forecast_steps = range(0, 18, 6)  # forecast every 6 hours
        forecast_length = 12  # forecast for 12 hours

        for step in range(0, 24):
            if step in forecast_steps:  # request and produce forecast
                forecast_t = range(step, step + forecast_length)
                test_fc_model.forecast_request = {
                    "FE": {test_ent_csv.full_id: {"attr": "p", "t": forecast_t}}
                }
            else:  # no forecast request
                test_fc_model.forecast_request = {}

            test_fc_model.step(step)

            if step in forecast_steps:  # request and produce forecast
                self.assertEqual(
                    len(test_fc_model.forecast["FE"]["Ent.csv_0"]["p"]), forecast_length
                )
            else:  # no forecast request
                self.assertEqual(test_fc_model.forecast, {})

    def test_error_no_entity(self):
        test_fc_model = Forecast(eid="test_forecast_0", step_size=3600, n_steps=24)
        test_fc_model.forecast_request = {"FE": {"Ent.csv_0": {"attr": "p", "t": range(0, 24)}}}

        with self.assertRaises(TypeError):
            test_fc_model.step(0)

    def test_empty_forecast_req_diff_timestep(self):
        test_fc_model = Forecast(eid="test_forecast_0", step_size=3600, n_steps=24)
        test_fc_model.timestep = 0
        # check if forecast model can handle an empty forecast request
        test_fc_model.forecast_request = {}
        test_fc_model.step(0)
        self.assertEqual(test_fc_model.forecast, {})

        # check if forecast model can handle an forecast request with empty info
        test_ent_csv = ForecastEntity(id="csv_0", path=csv_filename_1h)
        test_fc_model.add_forecasted_entity({test_ent_csv.full_id: test_ent_csv})
        test_fc_model.forecast_request = {"FE": {}}
        test_fc_model.step(0)
        self.assertEqual(test_fc_model.forecast, {"FE": {}})

        # check handling of newly arrived timestep
        test_fc_model.step(1)
        self.assertEqual(test_fc_model.timestep, 1)


if __name__ == "__main__":
    suite = unittest.TestLoader().loadTestsFromTestCase(TestForecast)
    unittest.TextTestRunner(verbosity=0).run(suite)
