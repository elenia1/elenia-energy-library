"""
Unittest for Grid EMS Model of eELib

| Author: elenia@TUBS
| Copyright 2024 elenia
| This file is part of eELib, which is free software under the terms of the GNU GPL Version 3.
"""

import unittest
import time

from eelib.core.control.grid.grid_ems_model import GridEMS


class TestForecast(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        pass

    @classmethod
    def tearDownClass(cls):
        pass

    def setUp(self):
        print(f"[START] {self.id()}")
        self.start = time.time()

    def tearDown(self):
        t = time.time() - self.start
        print(f"[END] {self.id()}: {t:.4f}")

    def test_congested_status(self):
        test_grid_ems = GridEMS(eid="grid_ems_0", strategy="test", grid_model_config={})

        # no grid status available
        test_grid_ems.grid_status = None
        test_grid_ems.step(timestep=0)
        self.assertEqual(test_grid_ems.congested, 0)

        # grid status positive
        test_grid_ems.grid_status = {"congested": False}
        test_grid_ems.step(timestep=1)
        self.assertEqual(test_grid_ems.congested, 0)

        # no grid status available
        test_grid_ems.grid_status = {"congested": True}
        test_grid_ems.step(timestep=2)
        self.assertEqual(test_grid_ems.congested, 1)

    def test_tariff_signal(self):
        # test static grid tariff
        test_grid_ems_static = GridEMS(
            eid="grid_ems_0", strategy="test", grid_model_config={}, grid_tariff_model="flat-rate"
        )
        test_grid_ems_static._create_grid_tariff_signal()

        # test static grid tariff
        test_grid_ems_static = GridEMS(
            eid="grid_ems_0", strategy="test", grid_model_config={}, grid_tariff_model="percentage"
        )
        test_grid_ems_static._create_grid_tariff_signal()

        # test static grid tariff
        test_grid_ems_static = GridEMS(
            eid="grid_ems_0",
            strategy="test",
            grid_model_config={},
            grid_tariff_model="time-variable",
        )
        test_grid_ems_static._create_grid_tariff_signal()

        # test wrong grid tariff
        test_grid_ems_static = GridEMS(
            eid="grid_ems_0", strategy="test", grid_model_config={}, grid_tariff_model="not_avail"
        )
        with self.assertRaises(ValueError) as message:
            test_grid_ems_static._create_grid_tariff_signal()
        str_error = "Chosen grid tariff model 'not_avail' for GridEMS is not implemented."
        self.assertTrue(str(message.exception) == str_error)


if __name__ == "__main__":
    suite = unittest.TestLoader().loadTestsFromTestCase(TestForecast)
    unittest.TextTestRunner(verbosity=0).run(suite)
