"""
Unittest for reading of model connections within eELib.

| Author: elenia@TUBS
| Copyright 2024 elenia
| This file is part of eELib, which is free software under the terms of the GNU GPL Version 3.
"""

import time
import unittest

##########################################
# imports from tested package
from eelib.model_connections.connections import (
    get_default_connections,
    get_connection_directions_config,
)


class TestHelper(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        pass

    @classmethod
    def tearDownClass(cls):
        pass

    def setUp(self):
        print(f"[START] {self.id()}")
        self.start = time.time()

    def tearDown(self):
        t = time.time() - self.start
        print(f"[END] {self.id()}: {t:.4f}")

    def test_default_connections(self):
        model_conn_config = get_default_connections()
        self.assertIsInstance(model_conn_config, dict)
        self.assertTrue(model_conn_config != {})
        self.assertTrue("__OUTPUT_MODEL" in model_conn_config.keys())
        self.assertTrue("__INPUT_MODEL_1" in model_conn_config["__OUTPUT_MODEL"].keys())
        self.assertIsInstance(model_conn_config["__OUTPUT_MODEL"]["__INPUT_MODEL_1"], list)
        for output_model_name in model_conn_config.keys():
            for input_model_name in model_conn_config[output_model_name].keys():
                self.assertIsInstance(model_conn_config[output_model_name][input_model_name], list)
                for conn_elem in model_conn_config[output_model_name][input_model_name]:
                    # either string value or tuple with exactly two elements
                    self.assertTrue(
                        isinstance(conn_elem, str)
                        or (isinstance(conn_elem, tuple) and len(conn_elem) == 2)
                    )

    def test_connections_dir(self):
        conn_dir_config = get_connection_directions_config()
        self.assertIsInstance(conn_dir_config, dict)
        self.assertTrue(conn_dir_config != {})

        # check fields
        self.assertTrue("ALWAYS_WEAK" in conn_dir_config.keys())
        self.assertTrue("ALWAYS_STRONG" in conn_dir_config.keys())
        self.assertTrue("STRONG_DIR" in conn_dir_config.keys())

        # check strong and weak
        for key in ["ALWAYS_WEAK", "ALWAYS_STRONG"]:
            for dir_conn in conn_dir_config[key].keys():
                self.assertTrue("includes" in conn_dir_config[key][dir_conn])
                self.assertTrue("full_name" in conn_dir_config[key][dir_conn])
                self.assertTrue(
                    conn_dir_config[key][dir_conn]["includes"] == ""
                    or conn_dir_config[key][dir_conn]["full_name"] == ""
                )

        # check set direction
        for dir_conn in conn_dir_config["STRONG_DIR"].keys():
            self.assertTrue("from" in conn_dir_config["STRONG_DIR"][dir_conn])
            self.assertTrue("to" in conn_dir_config["STRONG_DIR"][dir_conn])
            self.assertTrue(conn_dir_config["STRONG_DIR"][dir_conn]["from"] != "")
            self.assertTrue(conn_dir_config["STRONG_DIR"][dir_conn]["to"] != "")


if __name__ == "__main__":
    suite = unittest.TestLoader().loadTestsFromTestCase(TestHelper)
    unittest.TextTestRunner(verbosity=0).run(suite)
