"""
Unittest for TEMPLATE of eELib

| Author: elenia@TUBS
| Copyright 2024 elenia
| This file is part of eELib, which is free software under the terms of the GNU GPL Version 3.
"""

import unittest
import time


def test_method_sum(a: int, b: int):
    return a + b


class TestTemplate(unittest.TestCase):
    """The first four methods/functions are infrastructure classes for the tests.

    Args:
        unittest (_type_): _description_
    """

    @classmethod
    def setUpClass(cls):
        pass

    @classmethod
    def tearDownClass(cls):
        pass

    def setUp(self):
        print(f"[START] {self.id()}")
        self.start = time.time()

    def tearDown(self):
        t = time.time() - self.start
        print(f"[END] {self.id()}: {t:.4f}")

    def test_template(self):
        a = 5
        b = 8
        res = test_method_sum(a, b)
        self.assertAlmostEqual(res, a + b)
        # 1: create model entity with init_vals
        # 2.1: define time span for testing & execute step function of tested model
        # 2.2: execute single method of this model
        # 3: compare results, e.g. assertequal(test_template_model.p, 1000)

    def test_template_error(self):
        test_string_err = "Test Message 0"
        with self.assertRaises(ValueError) as message:
            raise ValueError(test_string_err)
        self.assertTrue(str(message.exception) == test_string_err)


if __name__ == "__main__":
    suite = unittest.TestLoader().loadTestsFromTestCase(TestTemplate)
    unittest.TextTestRunner(verbosity=0).run(suite)
