"""
Unittest for pickle reader of eELib

| Author: elenia@TUBS
| Copyright 2024 elenia
| This file is part of eELib, which is free software under the terms of the GNU GPL Version 3.
"""

import unittest
import time
import os
import gzip
import pickle

from eelib.utils.read_pickle import read_pickle

pickle_filename = "input.pickle"
test_profile = [0 for i in range(0, 24, 1)]
for i in range(12, 15):
    test_profile[i] = 1


class TestPickleReader(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        if os.path.exists(pickle_filename):
            os.remove(pickle_filename)
        with gzip.open(pickle_filename, "wb") as f:
            pickle.dump(test_profile, f)

    @classmethod
    def tearDownClass(cls):
        os.remove(pickle_filename)

    def setUp(self):
        print(f"[START] {self.id()}")
        self.start = time.time()

    def tearDown(self):
        t = time.time() - self.start
        print(f"[END] {self.id()}: {t:.4f}")

    def test_read(self):
        pickle_input = read_pickle(pickle_filename)

        # compare with initial list
        self.assertListEqual(test_profile, pickle_input)

    def test_error(self):
        with self.assertRaises(FileNotFoundError) as message:
            read_pickle("no_pickle_name.pickle")
        str_error = "Path to pickle file is not existent and could not be loaded!"
        self.assertTrue(str(message.exception) == str_error)


if __name__ == "__main__":
    suite = unittest.TestLoader().loadTestsFromTestCase(TestPickleReader)
    unittest.TextTestRunner(verbosity=0).run(suite)
