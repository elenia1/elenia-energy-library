"""
Unittest for resampling Pandas DataFrame of eELib.

| Author: elenia@TUBS
| Copyright 2024 elenia
| This file is part of eELib, which is free software under the terms of the GNU GPL Version 3.
"""

from io import StringIO
import pandas as pd
import numpy as np
import time
import unittest

##########################################
# imports from tested package
from eelib.utils.resample import resample_pandas_timeseries_agg, get_resolution_pandas_timeseries

data = """timestamp,value
2010-01-01 00:00:00,1
2010-01-01 01:00:00,1
2010-01-01 02:00:00,1
2010-01-01 03:00:00,1
2010-01-01 04:00:00,1
2010-01-01 05:00:00,1
2010-01-01 06:00:00,1
2010-01-01 07:00:00,1
2010-01-01 08:00:00,1
2010-01-01 09:00:00,1
2010-01-01 10:00:00,1
2010-01-01 11:00:00,1
2010-01-01 12:00:00,1
2010-01-01 13:00:00,1
2010-01-01 14:00:00,1
2010-01-01 15:00:00,1
2010-01-01 16:00:00,1
2010-01-01 17:00:00,1
2010-01-01 18:00:00,1
2010-01-01 19:00:00,1
2010-01-01 20:00:00,1
2010-01-01 21:00:00,1
2010-01-01 22:00:00,1
2010-01-01 23:00:00,1"""
df_60min = pd.read_csv(StringIO(data), parse_dates=["timestamp"], index_col="timestamp")

data = """timestamp,value
2010-01-01 23:00:00,1
2010-01-01 23:05:00,1
2010-01-01 23:10:00,1
2010-01-01 23:15:00,1
2010-01-01 23:20:00,1
2010-01-01 23:25:00,1
2010-01-01 23:30:00,1
2010-01-01 23:35:00,1
2010-01-01 23:40:00,1
2010-01-01 23:45:00,1
2010-01-01 23:50:00,1
2010-01-01 23:55:00,1"""

df_5min = pd.read_csv(StringIO(data), parse_dates=["timestamp"], index_col="timestamp")

data_5_miss = """timestamp,value
2010-01-01 23:00:00,1
2010-01-01 23:05:00,2
2010-01-01 23:10:00,3
2010-01-01 23:15:00
2010-01-01 23:20:00
2010-01-01 23:25:00
2010-01-01 23:30:00"""

df_5min_missing = pd.read_csv(
    StringIO(data_5_miss), parse_dates=["timestamp"], index_col="timestamp"
)


class TestHelper(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        pass

    @classmethod
    def tearDownClass(cls):
        pass

    def setUp(self):
        print(f"[START] {self.id()}")
        self.start = time.time()

    def tearDown(self):
        t = time.time() - self.start
        print(f"[END] {self.id()}: {t:.4f}")

    def test_resample_pandas_upsampling_3600s_to_900s(self):
        resampled_df = resample_pandas_timeseries_agg(df_60min, target_resolution=900)
        self.assertEqual(resampled_df.index[-1], pd.to_datetime("2010-01-01 23:45:00"))
        self.assertEqual(resampled_df.size, 96)

    def test_resample_pandas_upsampling_3600s_to_60s(self):
        resampled_df = resample_pandas_timeseries_agg(df_60min, target_resolution=60)
        self.assertEqual(resampled_df.index[-1], pd.to_datetime("2010-01-01 23:59:00"))
        self.assertEqual(resampled_df.size, 1440)

    def test_resample_pandas_downsampling_300s_to_900s(self):
        resampled_df = resample_pandas_timeseries_agg(df_5min, target_resolution=900)
        self.assertEqual(resampled_df.index[-1], pd.to_datetime("2010-01-01 23:45:00"))
        self.assertEqual(resampled_df.size, 4)

    def test_resample_pandas_upsampling_300s_to_60s(self):
        resampled_df = resample_pandas_timeseries_agg(df_5min, target_resolution=60)
        self.assertEqual(resampled_df.index[-1], pd.to_datetime("2010-01-01 23:59:00"))
        self.assertEqual(resampled_df.size, 60)

    def test_resample_mean_columns(self):
        resampled_df = resample_pandas_timeseries_agg(df_5min_missing, target_resolution=150)
        self.assertEqual(
            resampled_df.iloc[1].value,
            (df_5min_missing.iloc[0].value + df_5min_missing.iloc[1].value) / 2,
        )

    def test_resample_ffill_columns(self):
        resampled_df = resample_pandas_timeseries_agg(
            df_5min_missing, target_resolution=150, ffill_columns=["value"]
        )
        self.assertEqual(resampled_df.iloc[1].value, df_5min_missing.iloc[0].value)
        self.assertEqual(resampled_df.iloc[13].value, df_5min_missing.iloc[2].value)

    def test_resample_distribute_columns(self):
        resampled_df = resample_pandas_timeseries_agg(
            df_5min, target_resolution=150, distribute_columns=["value"]
        )
        self.assertEqual(resampled_df.iloc[0].value, 0.5)
        resampled_df = resample_pandas_timeseries_agg(
            df_5min, target_resolution=600, distribute_columns=["value"]
        )
        self.assertEqual(resampled_df.iloc[0].value, 2)

    def test_resample_interpolation(self):
        nearest = resample_pandas_timeseries_agg(
            df_5min, target_resolution=150, interpolation_method="nearest"
        )
        polynomial = resample_pandas_timeseries_agg(
            df_5min,
            target_resolution=150,
            interpolation_method="polynomial",
            interpolation_params={"order": 2},
        )
        self.assertNotIsInstance(nearest, type(None))
        self.assertNotIsInstance(polynomial, type(None))

    def test_get_resolution_df(self):
        resolution_3600 = get_resolution_pandas_timeseries(df_60min)
        resolution_300 = get_resolution_pandas_timeseries(df_5min)
        self.assertEqual(resolution_3600, 3600)
        self.assertEqual(resolution_300, 300)

    def test_get_resolution_series(self):
        resolution_3600 = get_resolution_pandas_timeseries(pd.Series(df_60min.index))
        resolution_300 = get_resolution_pandas_timeseries(pd.Series(df_5min.index))
        self.assertEqual(resolution_3600, 3600)
        self.assertEqual(resolution_300, 300)

    def test_get_resolution_ndarray(self):
        df_60_with_index = df_60min.reset_index()
        df_5_with_index = df_5min.reset_index()
        resolution_3600_1d = get_resolution_pandas_timeseries(np.array(df_60min.index))
        resolution_300_1d = get_resolution_pandas_timeseries(np.array(df_5min.index))
        resolution_3600_2d = get_resolution_pandas_timeseries(np.array(df_60_with_index))
        resolution_300_2d = get_resolution_pandas_timeseries(np.array(df_5_with_index))
        self.assertEqual(resolution_3600_1d, 3600)
        self.assertEqual(resolution_300_1d, 300)
        self.assertEqual(resolution_3600_2d, 3600)
        self.assertEqual(resolution_300_2d, 300)

        with self.assertRaises(TypeError):
            arr = np.array(df_5_with_index)
            arr_reshaped = arr.reshape(1, *arr.shape)
            get_resolution_pandas_timeseries(arr_reshaped)

    def test_get_resolution_list(self):
        df_60_with_index = df_60min.reset_index()
        df_5_with_index = df_5min.reset_index()
        resolution_3600_1d = get_resolution_pandas_timeseries(list(np.array(df_60min.index)))
        resolution_300_1d = get_resolution_pandas_timeseries(list(np.array(df_5min.index)))
        resolution_3600_2d = get_resolution_pandas_timeseries(list(np.array(df_60_with_index)))
        resolution_300_2d = get_resolution_pandas_timeseries(list(np.array(df_5_with_index)))
        self.assertEqual(resolution_3600_1d, 3600)
        self.assertEqual(resolution_300_1d, 300)
        self.assertEqual(resolution_3600_2d, 3600)
        self.assertEqual(resolution_300_2d, 300)

        with self.assertRaises(TypeError):
            arr = np.array(df_5_with_index)
            arr_reshaped = arr.reshape(1, *arr.shape)
            get_resolution_pandas_timeseries(list(arr_reshaped))

    def test_undefined_type(self):
        test_ts = 0
        with self.assertRaises(TypeError) as message:
            _ = get_resolution_pandas_timeseries(timeseries=test_ts)
        str_error = "Timeseries is of type <class 'int'>, which cannot be resampled."
        self.assertTrue(str(message.exception) == str_error)

    def test_dates_as_strings(self):
        df_5min_str = pd.read_csv(StringIO(data), index_col="timestamp")
        resolution_300 = get_resolution_pandas_timeseries(df_5min_str)
        self.assertEqual(resolution_300, 300)

        # and check if unconvertible
        data_error = data[:20] + "+" + data[21:]
        df_5min_str_error = pd.read_csv(StringIO(data_error), index_col="timestamp")
        with self.assertRaises(TypeError) as message:
            _ = get_resolution_pandas_timeseries(timeseries=df_5min_str_error)
        str_error = "Cannot process time index of type <class 'str'>."
        self.assertTrue(str(message.exception) == str_error)

    def test_downsampling_with_ffill(self):
        resampled_df = resample_pandas_timeseries_agg(
            df_5min, target_resolution=900, ffill_columns=["value"]
        )
        self.assertEqual(resampled_df.iloc[0].value, df_5min.iloc[0].value)
        self.assertEqual(resampled_df.iloc[2].value, df_5min.iloc[2 * 3].value)
        self.assertEqual(resampled_df.iloc[2].index, df_5min.iloc[2 * 3].index)


if __name__ == "__main__":
    suite = unittest.TestLoader().loadTestsFromTestCase(TestHelper)
    unittest.TextTestRunner(verbosity=0).run(suite)
