"""
Unittest for ancillary service functions of eELib

| Author: elenia@TUBS
| Copyright 2024 elenia
| This file is part of eELib, which is free software under the terms of the GNU GPL Version 3.
"""

import unittest
import time

from eelib.utils.ancillary_services import voltage_control_concepts

cos_phi_factor_0_9 = 0.484322105
cos_phi_factor_0_95 = 0.328684105


class TestUtilAncillaryServices(unittest.TestCase):
    """The first four methods/functions are infrastructure classes for the tests."""

    @classmethod
    def setUpClass(cls):
        pass

    @classmethod
    def tearDownClass(cls):
        pass

    def setUp(self):
        print(f"[START] {self.id()}")
        self.start = time.time()

    def tearDown(self):
        t = time.time() - self.start
        print(f"[END] {self.id()}: {t:.4f}")

    def test_cos_phi_fix(self):
        # no active power - no reactive power
        self.assertAlmostEqual(voltage_control_concepts.cos_phi_fix(p=0, cos_phi=1), 0)
        self.assertAlmostEqual(voltage_control_concepts.cos_phi_fix(p=0, cos_phi=0.9), 0)

        # positive active power - check negative reactive power
        self.assertAlmostEqual(voltage_control_concepts.cos_phi_fix(p=1000, cos_phi=1), 0)
        self.assertAlmostEqual(
            voltage_control_concepts.cos_phi_fix(p=1000, cos_phi=0.9),
            -1000 * cos_phi_factor_0_9,
            places=5,
        )

        # positive active power - check negative reactive power
        self.assertAlmostEqual(voltage_control_concepts.cos_phi_fix(p=-1000, cos_phi=1), 0)
        self.assertAlmostEqual(
            voltage_control_concepts.cos_phi_fix(p=-1000, cos_phi=0.95),
            1000 * cos_phi_factor_0_95,
            places=5,
        )

        # error for unavailable cos_phi
        with self.assertRaises(ValueError) as message:
            _ = voltage_control_concepts.cos_phi_fix(p=1000, cos_phi=-1)
        str_error = "Power Factor outside of 0 to 1 not possible - Value is -1"
        self.assertTrue(str(message.exception) == str_error)
        with self.assertRaises(ValueError) as message:
            _ = voltage_control_concepts.cos_phi_fix(p=1000, cos_phi=2)
        str_error = "Power Factor outside of 0 to 1 not possible - Value is 2"
        self.assertTrue(str(message.exception) == str_error)


if __name__ == "__main__":
    suite = unittest.TestLoader().loadTestsFromTestCase(TestUtilAncillaryServices)
    unittest.TextTestRunner(verbosity=0).run(suite)
