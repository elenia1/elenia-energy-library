"""
Methods for various concepts for voltage control of inverter-based devices.

| Author: elenia@TUBS
| Copyright 2024 elenia
| This file is part of eELib, which is free software under the terms of the GNU GPL Version 3.
"""

import math


def cos_phi_fix(p: float, cos_phi: float):
    """Calculate reactive power output from active power and a fix cosinus phi (power factor).
    Negative active power (generation!) leads to positive reactive power (inductive behaviour).
    Positive active power (demand!) leads to negative reactive power (capacitive behaviour).

    Args:
        p (float): active power of device (>0 is demand, <0 is generation)
        cos_phi (float): power factor, value between 0 (not included!) and 1

    Raises:
        ValueError: If cos_phi is outside of range from 0 to 1

    Returns:
        float: reactive power output from device
    """

    if cos_phi <= 0 or cos_phi > 1:
        raise ValueError(
            "Power Factor outside of 0 to 1 not possible - Value is {}".format(cos_phi)
        )

    q = -p * math.sqrt((1 / cos_phi**2) - 1)

    return q
