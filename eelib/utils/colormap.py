"""
Provides an elenia colormap to use standardized colors across the project.

| Author: elenia@TUBS
| Copyright 2024 elenia
| This file is part of eELib, which is free software under the terms of the GNU GPL Version 3.
"""

import matplotlib as mpl
import matplotlib.colors as mcolors
import matplotlib.pyplot as plt

# elenia primary colors extended by dark red
PRIM_ELENIA_CMAP = [
    "#005374",
    "#00709B",
    "#66B4D3",
    "#BE1E3C",
    "#711C2F",
]

# elenia secondary colors
SEC_ELENIA_CMAP = [
    "#711C2F",
    "#D46700",
    "#E3B225",
    "#ACC13A",
    "#6D8300",
    "#00534A",
    "#003F57",
    "#00709B",
    "#66B4D3",
    "#B983B2",
    "#8A307F",
    "#4C1830",
]

# elenia all colors
ELENIA_CMAP = PRIM_ELENIA_CMAP + SEC_ELENIA_CMAP

# discrete colormap primary
PRIM_DISC_CMAP = mcolors.ListedColormap(PRIM_ELENIA_CMAP, name="prim_elenia_disc")

# discrete colormap secondary
SEC_DISC_CMAP = mcolors.ListedColormap(SEC_ELENIA_CMAP, name="sec_elenia_disc")

# discrete colormap all
DISC_CMAP = mcolors.ListedColormap(ELENIA_CMAP, name="elenia_disc")

# interpolated colormap
PRIM_INT_CMAP = mcolors.LinearSegmentedColormap.from_list("prim_elenia_int", PRIM_ELENIA_CMAP)

SEC_INT_CMAP = mcolors.LinearSegmentedColormap.from_list("sec_elenia_int", SEC_ELENIA_CMAP)

# interpolated colormap from elenia red to elenia green
RED_GREEN_INT_CMAP = mcolors.LinearSegmentedColormap.from_list(
    "prim_elenia_int", ["#BE1E3C", "#ACC13A"]
)
RED_GREEN_RED_INT_CMAP = mcolors.LinearSegmentedColormap.from_list(
    "prim_elenia_int", ["#BE1E3C", "#ACC13A", "#BE1E3C"]
)


def register_and_use_elenia_colormap():
    """Register the elenia colormap and set it as matplotlib.pyplot's default."""
    try:  # register colormap
        mpl.colormaps.register(cmap=DISC_CMAP)
    except ValueError:  # if it has already been registered, skip this step
        print("colormap already registered.")
    try:
        mpl.colormaps.register(cmap=PRIM_INT_CMAP)
    except ValueError:
        print("colormap already registered.")
    try:
        mpl.colormaps.register(cmap=SEC_INT_CMAP)
    except ValueError:
        print("colormap already registered.")
    try:
        mpl.colormaps.register(cmap=RED_GREEN_INT_CMAP)
    except ValueError:
        print("colormap already registered.")
    try:
        mpl.colormaps.register(cmap=RED_GREEN_RED_INT_CMAP)
    except ValueError:
        print("colormap already registered.")

    # set registed colormap as default use
    plt.set_cmap(PRIM_INT_CMAP.name)
    plt.set_cmap(SEC_INT_CMAP.name)
    plt.set_cmap(RED_GREEN_INT_CMAP.name)
    plt.set_cmap(RED_GREEN_RED_INT_CMAP.name)
    plt.set_cmap(DISC_CMAP.name)
