"""
Here the functionalities for evaluating and plotting/presenting outputs of
simulations are stored, mostly in accessible and modifiable python scripts or jupyter notebooks.
"""
