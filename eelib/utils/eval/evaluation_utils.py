"""
Useful helper methods for evaluating .hdf5 results in jupyter notebooks.

| Author: elenia@TUBS
| Copyright 2024 elenia
| This file is part of eELib, which is free software under the terms of the GNU GPL Version 3.
"""

# packages
import h5py
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import re
import os
from datetime import datetime


def _extract_datasets_from_group(group, parent_name="", pattern=None):
    """Recursively extract data from hdf5 file.

    Args:
        group (Group): The group where the search is currently occuring.
        parent_name (str, optional): The parent name of that group.
        pattern (str): regex pattern. If given, only the part matching the first
            grouping in the pattern remains as the name for each timeseries.
            Otherwise, the full path of the timeseries is used as its name.

    Raises:
        ValueError: if pattern is given and at least one name does not match it

    Returns:
        dict: the fully formed subgroup for the current group.
    """
    data = {}
    for key in group.keys():
        item = group[key]
        path = f"{parent_name}/{key}" if parent_name else key
        if isinstance(item, h5py.Dataset):
            if pattern:
                match = re.search(pattern, path)
                if match:
                    title = match.group(1)
                else:
                    raise ValueError("Incorrect Pattern or Format.")
            else:
                title = path
            data[title] = item[()]
        elif isinstance(item, h5py.Group):
            data.update(_extract_datasets_from_group(item, path, pattern))
    return data


def hdf5_file_as_pandas(path: str, datetime_col=False, pattern=None):
    """Return a dataframe representation of the hdf5 file.

    Args:
        path (str): hdf5 file path.
        datetime_col (bool, optional): determines whether a datetime column is
            created using the Config section of the database.
        pattern (str): regex pattern. If given, only the part matching the first
            grouping in the pattern remains as the name for each timeseries.
            Otherwise, the full path of the timeseries is used as its name.

    Returns:
        Dataframe: The resulting dataframe.
    """
    config = None  # the configuration data

    # open the file
    with h5py.File(path, "r") as f:
        # iterate through each supergroup and its data
        data = _extract_datasets_from_group(f["Series"], pattern=pattern)

        if datetime_col:
            config = _read_config(f)["scenario_config"]

    # Create a Pandas DataFrame from the lists
    result = pd.DataFrame(data)

    if datetime_col:
        # the difference of time between consecutive steps in seconds
        step_size = config["step_size"]
        zero_datetime = np.datetime64(config["start"])  # datetime corresponding to timestep 0
        timesteps = result.index.to_numpy()  # list of timesteps
        datetimes = timestep_to_datetime(timesteps, zero_datetime, step_size)
        result.insert(0, "datetime", datetimes)

    return result


def get_config(path: str) -> dict:
    """Return scenario configuration of the hdf5 file.

    Args:
        path (str): path of the hdf5 file.

    Returns:
        dict: scenario configuration dict.
    """
    with h5py.File(path, "r") as data:
        config = _read_config(data)["scenario_config"]

    return config


def convert_hdf5_to_csv(
    input_path: str,
    output_path: str,
    sep=",",
    na_rep="",
    datetime_col=False,
):
    """Converts an hdf5 file with the proper format into a csv file. Uses non-compact representation
    unless specified.

    Args:
        input_path (str): path for input hdf5 file.
        output_path (str): path for the output csv file.
        sep (str): String of length 1. Field delimiter for the output file. Defaults to ','.
        na_rep (str): Missing data representation. Defaults to ''.
        datetime_col (bool): Whether the function adds a column to dataframe to display the actual
            date and time in addition to timesteps. Only works if compact=True. Defaults to False
    """

    # convert to pandas dataframe and then save the data into a csv file
    df = hdf5_file_as_pandas(input_path, datetime_col=datetime_col)
    directory = os.path.dirname(output_path)
    if not os.path.exists(directory):
        try:
            os.makedirs(directory)
        except PermissionError as e:
            print(f"PermissionError: {e}")
    df.to_csv(output_path, sep=sep, na_rep=na_rep)


def timestep_to_datetime(timestep, zero_datetime: datetime, step_size: int):
    """Converts a (numpy list of) timestep(s) to a (numpy list of) actual date(s).

    Args:
        timestep: a timestep or a numpy array of timesteps.
        zero_datetime (datetime): the datetime corresponding to timestep 0. Inclusion of zero in the
            list is not mandatory.
        step_size (int): size of each timestep in seconds.

    Returns:
        numpy.datetime64: calculated date(s) corresponding to timestep(s).
    """

    # calculate the difference of actual time between timestep 0 and given timestep
    time_delta = np.timedelta64(step_size, "s") * timestep
    # calculate the datetime corresponding to `timestep`
    result_date = np.datetime64(zero_datetime) + time_delta
    return result_date


def save_figure(fig, ax, filename, path, figsize=(15, 5), dpi=300, format="svg", rasterized=True):
    """Saves a Matplotlib figure with standardized sizing and format.

    Args:
        fig (Figure): Matplotlib figure to be saved.
        ax (Axes): Matplotlib ax.
        filename (str): Name of the output file.
        path (str): path of the output file.
        figsize (tuple): Size of the figure in inches (width, height).
        dpi (int): Dots per inch for image resolution.
        format (str): Output file format (e.g., 'svg', 'png', 'jpg', etc.).
        rasterized (bool): Whether to rasterize vector elements (True) or not (False).
    """
    # Set the figure size
    fig.set_size_inches(figsize)
    ax.set_rasterized(rasterized)
    # We check if the directory exists, if not we create it.
    if not os.path.exists(path):
        os.makedirs(path)
    # Save the figure with specified format and dpi
    plt.savefig(
        os.path.join(path, f"{filename}.{format}"), dpi=dpi, format=format, bbox_inches="tight"
    )


def _read_config(hdf5_data):
    """Return dictionary corresponding to Config grouping.

    Args:
        hdf5_data: data read from an hdf5 file using h5py.

    Returns:
        dict: the Config as stored.
    """

    data_config = hdf5_data["Config"]
    config_dict = {}
    for name, data in data_config.items():
        d = data[()]
        if name == "scenario_config":
            d = {}
            # change the data into a numpy array
            array = np.array(list(data), dtype=str)
            for row in array:
                key, value = row
                try:
                    # if value can be converted into integer, do so
                    d[key] = int(value)
                except Exception:
                    # otherwise let it remain a string
                    d[key] = value
        config_dict.update({name: d})
    return config_dict
