"""
Logging helper with custom format and functions to set logger.

| Author: elenia@TUBS
| Copyright 2024 elenia
| This file is part of eELib, which is free software under the terms of the GNU GPL Version 3.
"""

import logging
import sys

LOGGING_FORMAT = "%(asctime)s | %(levelname)-9s| %(threadName)s | %(name)s:%(lineno)d - %(message)s"


class CustomFormatter(logging.Formatter):
    """CustomFormatter with different colors for logging levels.
    Specialization of logging.Formatter.
    """

    grey = "\x1b[38;20m"
    yellow = "\x1b[33;20m"
    red = "\x1b[31;20m"
    bold_red = "\x1b[31;1m"
    reset = "\x1b[0m"
    format_str = LOGGING_FORMAT

    FORMATS = {
        logging.DEBUG: grey + format_str + reset,
        logging.INFO: grey + format_str + reset,
        logging.WARNING: yellow + format_str + reset,
        logging.ERROR: red + format_str + reset,
        logging.CRITICAL: bold_red + format_str + reset,
    }

    def format(self, record):
        """Overrides format method.

        Args:
            record (_type_): record to log.

        Returns:
            str: Formatted log string.
        """
        log_fmt = self.FORMATS.get(record.levelno)
        formatter = logging.Formatter(log_fmt)
        return formatter.format(record)


def clear_logger():
    """Function to remove existing loggers."""
    while logging.root.handlers:
        logging.root.removeHandler(logging.root.handlers[-1])


def set_console_logger(level: int = logging.DEBUG):
    """Set console output logger.
    All messages will be printed to StdOut.

    Args:
        level (int): Logging level. Defaults to logging.DEBUG.
    """
    root = logging.getLogger()
    root.setLevel(logging.NOTSET)
    handler = logging.StreamHandler(sys.stdout)
    handler.setFormatter(CustomFormatter())
    handler.setLevel(level)
    root.addHandler(handler)


def set_file_logger(level: int = logging.DEBUG, filename: str = "eELib.log"):
    """Set file logger.
    All messages will be written to the file with given filename.

    Args:
        level (int): Logging level. Defaults to logging.DEBUG.
        filename (str): Logfile filename. Defaults to "eELib.log".
    """
    root = logging.getLogger()
    root.setLevel(logging.NOTSET)
    handler = logging.FileHandler(filename, mode="w")
    formatter = logging.Formatter(LOGGING_FORMAT)
    handler.setFormatter(formatter)
    handler.setLevel(level)
    root.addHandler(handler)
