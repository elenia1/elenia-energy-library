"""
Sends notifications for the status of simulations.

| Author: elenia@TUBS
| Copyright 2024 elenia
| This file is part of eELib, which is free software under the terms of the GNU GPL Version 3.
"""

import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from dotenv import load_dotenv
import logging
import os
import requests
import traceback
from abc import ABC, abstractmethod

_logger = logging.getLogger(__name__)


class Notifier(ABC):
    """Abstract parent class for notifier objects."""

    @abstractmethod
    def send_message(
        self, sim_identifier: str, success: bool, message: str = None, recipient_email: str = None
    ):
        """Abstract method for sending a message.

        Args:
            sim_identifier (str): an identifier for the simulation.
            success (bool): whether or not the simulation was successful.
            message (str): An additional message. Defaults to None.
            recipient_email (str): the recipient email. Defaults to None.
        """
        pass

    def construct_message(self, sim_identifier: str, success: bool, message: str = None):
        """Method to construct a message depending on success or failure of a simulation.

        Args:
            sim_identifier (str): an identifier for the simulation.
            success (bool): whether or not the simulation was successful.
            message (str): An additional message. Defaults to None.

        Returns:
            str, str: the subject and content of the constructed message.
        """
        if success:
            subject = f"Simulation {sim_identifier} successful"
            if message:
                message = (
                    f"Simulation {sim_identifier} is successful with the following"
                    f" message:\n{message}"
                )
            else:
                message = f"Simulation {sim_identifier} is successful."

        if not success:
            subject = f"Simulation {sim_identifier} failed"
            if message:
                message = (
                    f"Simulation {sim_identifier} has failed with the following message:\n{message}"
                )
            else:
                message = f"Simulation {sim_identifier} has failed."
        return subject, message


class EmailNotifier(Notifier):
    """Class for sending notifications through email. Extends Notifier."""

    def __init__(
        self,
        smtp_server: str = None,
        smtp_port: int = None,
        sender_email: str = None,
        sender_password: str = None,
    ):
        """Initializes an EmailNotifier Object.

        Args:
            smtp_server (str): Address for the SMTP server.
            smtp_port (int): Port for the SMTP server.
            sender_email (str): the email from which the message is sent.
            sender_password (str): the password for the sneder email.

        Raises:
            FileNotFoundError: in case .env file is not found.
        """
        root_folder = os.getcwd()
        for root, dirs, files in os.walk(root_folder):
            # Check if .env file is present in the current directory
            if ".env" in files:
                dotenv_path = os.path.join(root, ".env")
                break
        else:
            raise FileNotFoundError(
                ".env file not found in any subfolder under the root folder " + root_folder
            )
        load_dotenv(dotenv_path)
        # store server data. If data not provided in arguments use .env file.
        self.smtp_server = smtp_server if smtp_server else os.getenv("SMTP_SERVER")
        self.smtp_port = smtp_port if smtp_port else os.getenv("SMTP_PORT")
        self.sender_email = sender_email if sender_email else os.getenv("SENDER_EMAIL")
        self.sender_password = sender_password if sender_password else os.getenv("SENDER_PASSWORD")

    def send_message(
        self, sim_identifier: str, success: bool, message: str = None, recipient_email: str = None
    ):
        """Sends email as a status update for a simulation.

        Args:
            sim_identifier (str): an identifier for the simulation.
            success (bool): whether or not the simulation was successful.
            message (str): An additional message. Defaults to None.
            recipient_email (str): the recipient email. Defaults to None.
        """

        # if not given in arguments read recipient email from .env file.
        recipient_email = recipient_email if recipient_email else os.getenv("SMTP_RECIPIENT_EMAIL")

        # Create a MIME multipart message
        msg = MIMEMultipart()
        msg["From"] = self.sender_email
        msg["To"] = recipient_email

        subject, message = self.construct_message(sim_identifier, success, message)
        msg["Subject"] = subject
        # Attach the message to the MIME message
        msg.attach(MIMEText(message, "plain"))

        # Connect to the SMTP server
        with smtplib.SMTP(self.smtp_server, self.smtp_port) as server:
            try:
                server.starttls()  # Start TLS encryption
            except Exception as e:
                _logger.warning(e)
            server.login(self.sender_email, self.sender_password)
            server.send_message(msg)


class WebexNotifier(Notifier):
    """Class for sending notifications through Webex App. Extends Notifier."""

    def __init__(self, api_url: str = None, access_token: str = None):
        """Initializes a WebexNotifier Object.

        Args:
            api_url (str): URL for Webex API.
            access_token (str): Access Token for Webex bot.

        Raises:
            FileNotFoundError: in case .env file is not found.
        """

        root_folder = os.getcwd()
        for root, dirs, files in os.walk(root_folder):
            # Check if .env file is present in the current directory
            if ".env" in files:
                dotenv_path = os.path.join(root, ".env")
                break
        else:
            raise FileNotFoundError(
                ".env file not found in any subfolder under the root folder " + root_folder
            )

        load_dotenv(dotenv_path)
        # store access token and url. If data not provided in arguments use .env file.
        self.access_token = access_token if access_token else os.getenv("WEBEX_ACCESS_TOKEN")
        self.url = api_url if api_url else os.getenv("WEBEX_API_URL")

    def send_message(
        self, sim_identifier: str, success: bool, message: str = None, recipient_email: str = None
    ):
        """Sends webex message as a status update for a simulation.

        Args:
            sim_identifier (str): an identifier for the simulation.
            success (bool): whether or not the simulation was successful.
            message (str): An additional message. Defaults to None.
            recipient_email (str): the recipient's email. Defaults to None.

        Returns:
            Any: json-encoded content of response or None.
        """

        # if not given in arguments read recipient email from .env file.
        recipient_email = recipient_email if recipient_email else os.getenv("WEBEX_RECIPIENT_EMAIL")

        subject, message = self.construct_message(sim_identifier, success, message)

        headers = {
            "Authorization": f"Bearer {self.access_token}",
            "Content-Type": "application/json",
        }

        responses = []

        # in case there are multiple emails we send a message for each separately
        emails = recipient_email.split(",")
        for email in emails:
            email = email.strip()
            data = {"text": message, "toPersonEmail": email}
            resp_msg = requests.post(self.url, headers=headers, json=data)
            responses.append(resp_msg.json())

        return responses


class StatusMonitor(ABC):
    """Abstract parent class for a group of context manager classes.

    Monitors the status of simulation and informs the user about success or failure.
    Use in the following format:

    `with StatusMonitor(...):`
        `execute_simulation()`
    """

    def __init__(self, notifier, sim_identifier, recipient_email=None):
        """Initializes a StatusMonitor object.

        Args:
            notifier (Notifier): the notifier object used to pass messages.
            sim_identifier (str): an identifier for the simulation
            recipient_email (str): the recipient email. Defaults to None.
        """
        self.notifier = notifier
        self.sim_identifier = sim_identifier
        self.recipient_email = recipient_email

    def __enter__(self):
        """Called at the beginning of a with statement. Performs setup actions."""
        pass

    def __exit__(self, exc_type, exc_val, exc_tb):
        """Called at the end of a with statement. Performs teardown actions and handles exceptions.

        Args:
            exc_type (Any): The type of the exception if any.
            exc_val (Any): The value of the exception (e.g., the exception instance) if any.
            exc_tb (Any): A traceback object encapsulating the call stack at the point where the
                exception occurred.

        Returns:
            bool: returns False to propogate the exception.
        """
        if exc_type is not None:
            tb = "".join(traceback.format_tb(exc_tb))
            message = f"{exc_val} at:\n{tb}"
            self.notifier.send_message(
                self.sim_identifier, False, message=message, recipient_email=self.recipient_email
            )
        else:
            self.notifier.send_message(
                self.sim_identifier, True, recipient_email=self.recipient_email
            )

        return False


class StatusMonitorEmail(StatusMonitor):
    """context manager class used to notify user of a simulation's status through email.

    Extends StatusMonitor.
    Use in the following format:

    `with StatusMonitor(...):`
        `execute_simulation()`
    """

    def __init__(self, sim_identifier, recipient_email=None, **kwargs):
        """Initializes a StatusMonitorEmail object.

        Args:
            sim_identifier (str): an identifier for the simulation.
            recipient_email (str): the recipient's email. Defaults to None.
            **kwargs:
                smtp_server (str): Address for the SMTP server.
                smtp_port (int): Port for the SMTP server.
                sender_email (str): the email from which the message is sent.
                sender_password (str): the password for the sneder email.
        """
        self.smtp_server = kwargs.get("smtp_server", None)
        self.smtp_port = kwargs.get("smtp_port", None)
        self.sender_email = kwargs.get("sender_email", None)
        self.sender_password = kwargs.get("sender_password", None)
        self.email_sender = EmailNotifier(
            smtp_server=self.smtp_server,
            smtp_port=self.smtp_port,
            sender_email=self.sender_email,
            sender_password=self.sender_password,
        )
        super().__init__(self.email_sender, sim_identifier, recipient_email)


class StatusMonitorWebex(StatusMonitor):
    """context manager class used to notify user of a simulation's status through Webex.

    Extends StatusMonitor.
    Use in the following format:

    `with StatusMonitor(...):`
        `execute_simulation()`
    """

    def __init__(self, sim_identifier, recipient_email=None, **kwargs):
        """Initializes a StatusMonitorWebex object.

        Args:
            sim_identifier (str): an identifier for the simulation.
            recipient_email (str): the recipient's email. Defaults to None.
            **kwargs:
                api_url (str): URL for Webex API.
                access_token (str): Access Token for Webex bot.
        """
        self.api_url = kwargs.get("api_url", None)
        self.access_token = kwargs.get("access_token", None)
        self.webex_sender = WebexNotifier(api_url=self.api_url, access_token=self.access_token)
        super().__init__(self.webex_sender, sim_identifier, recipient_email)
