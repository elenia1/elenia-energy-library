"""
Reads .pickle files.

| Author: elenia@TUBS
| Copyright 2024 elenia
| This file is part of eELib, which is free software under the terms of the GNU GPL Version 3.
"""

import pickle
import gzip
import os


def read_pickle(path: str):
    """Read the whole data from a stored pickle file.

    Args:
        path (str): path to pickle file

    Raises:
        FileNotFoundError: Error if given pickle file path is not existent

    Returns:
        data: stored data from pickle file
    """
    data = None
    if os.path.exists(path):
        pickle_off = gzip.open(path, "rb")
        data = pickle.load(pickle_off)
        pickle_off.close()
    else:
        raise FileNotFoundError("Path to pickle file is not existent and could not be loaded!")

    return data
