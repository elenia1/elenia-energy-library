"""
Methods for inter- or extrapolating timeseries data (in pandas Dataframe format).

| Author: elenia@TUBS
| Copyright 2024 elenia
| This file is part of eELib, which is free software under the terms of the GNU GPL Version 3.
"""

import pandas as pd
import numpy as np
import logging

_logger = logging.getLogger(__name__)


def get_resolution_pandas_timeseries(timeseries: pd.DataFrame | pd.Series) -> int:
    """Computes the time resolution of a given timeseries.

    Args:
        timeseries (pd.DataFrame | pd.Series): pandas Timeseries to determine resolution

    Raises:
        TypeError: unexpected input format

    Returns:
        int: time resolution in seconds
    """

    # first we need to extract a list of timestamps from whatever data structure we have

    # we deal with lists and arrays first
    # if input is a list convert it to numpy array
    if isinstance(timeseries, list):
        timeseries = np.array(timeseries)

    # if input is numpy array (or list converted to ndarray), try to extract a list of timestamps
    if isinstance(timeseries, np.ndarray):
        # for 1d ndarray, assume it is a list of timestamps
        if timeseries.ndim == 1:
            index_array = timeseries

        # for 2d ndarray, assume the first column is the list of timestamps
        elif timeseries.ndim == 2:
            index_array = timeseries[:, 0]

        # raise error if array dimension is neither 1 or 2
        else:
            raise TypeError(f"Undefined behavior for array of {timeseries.ndim} dimensions.")

    # if input is dataframe, take the index as list of timestamps
    elif isinstance(timeseries, pd.DataFrame):
        index_array = timeseries.index.values

    # if input is series, it is a list of timestamps
    elif isinstance(timeseries, pd.Series):
        index_array = timeseries

    else:
        raise TypeError(f"Timeseries is of type {type(timeseries)}, which cannot be resampled.")

    # Now that we have our list of timestamps, we deal with the formatting of that data
    # if the type of time data is string convert to proper format
    if isinstance(index_array[0], str):
        try:
            # convert to format np.datetime64 or Timestamp
            index_array = pd.to_datetime(index_array)
        except Exception as e:  # if an unexpected format is given raise error
            raise TypeError(f"Cannot process time index of type {type(index_array[0])}.") from e

    # Then we calculate the difference between timestamps in seconds
    diff_array = np.diff(index_array[:2])
    diff = diff_array.min()

    # if data is of type Timestamp, the value here would be of type Timedelta
    if isinstance(diff, pd.Timedelta):
        resolution = diff.seconds
    else:
        resolution = int(diff_array.min() / 1e9)
    return resolution


def resample_pandas_timeseries_agg(
    df: pd.DataFrame,
    target_resolution: int,
    interpolation_method: str = "linear",
    interpolation_params: dict = None,
    ffill_columns: list = [],
    distribute_columns: list = [],
    key: str = "",
) -> pd.DataFrame:
    """Resample a given DataFrame to a target time resolution.

    Columns that must be uniformly distributed or just filled forward

    Args:
        df (pd.DataFrame): The df to be resampled.
        target_resolution (int): The resolution to be sampled in (seconds).
        interpolation_method (str): The method used for interpolation.
        interpolation_params (dict): Additional parameters for interpoation, if needed.
        ffill_columns (list): Columns to be filled forward. Defaults to [].
        distribute_columns (list): Columns to be distributed. Defaults to [].
        key (str): Key to describe the series (just for logging). Defaults to "".

    Returns:
        pd.DataFrame: the resampled DataFrame
    """
    ts_resolution = get_resolution_pandas_timeseries(df)
    if ts_resolution > target_resolution:
        # upsampling is desired
        _logger.warning(
            f"Timeseries {key} with {ts_resolution}s resolution. Upsampling to {target_resolution}s"
            " and agg."
        )
        new_index = pd.date_range(
            start=df.index[0],
            end=df.index[-1] + pd.Timedelta(ts_resolution - 1, unit="S"),
            freq=f"{target_resolution}S",
        )
        df = df.resample(f"{target_resolution}S").first().reindex(new_index)
        for col in ffill_columns:
            df[col] = df[col].ffill()
        for col in distribute_columns:
            df[col] = df[col].ffill()
            df[col] *= target_resolution / ts_resolution
        for col in df.columns:
            if col not in ffill_columns + distribute_columns:
                if interpolation_params:
                    df[col] = df[col].interpolate(
                        method=interpolation_method, **interpolation_params
                    )
                else:
                    df[col] = df[col].interpolate(method=interpolation_method)

        return df

    elif ts_resolution < target_resolution:
        # downsampling is desired
        _logger.warning(
            f"Timeseries {key} with {ts_resolution}s resolution. "
            f"Downsampling to {target_resolution}s may lead to unexpected results. "
        )
        mean_columns = [x for x in df.columns if x not in ffill_columns + distribute_columns]
        df_sampler = df.resample(f"{target_resolution}s")
        new_df = pd.DataFrame()
        for col in mean_columns:
            new_df[col] = df_sampler[col].mean()
        for col in ffill_columns:
            new_df[col] = df_sampler[col].first()
        for col in distribute_columns:
            new_df[col] = df_sampler[col].sum()
        return new_df
    else:
        return df
