"""
Helper methods for retrieving simulation objects from lists or listing available entitities.

| Author: elenia@TUBS
| Copyright 2024 elenia
| This file is part of eELib, which is free software under the terms of the GNU GPL Version 3.
"""

from mosaik import util as mosaik_util
from mosaik import World
import logging
import os
from datetime import datetime
from eelib.model_connections.connections import get_connection_directions_config
from copy import deepcopy

_logger = logging.getLogger(__name__)


def get_entity_by_id(e_list: list, e_id: str):
    """Provides an entity with regard to a given id from the entity list.

    Args:
        e_list (list): list of entities to be searched in
        e_id (str): id of the entity that we search for

    Returns:
        entity, dict:
        (1) model entity if found, otherwise None
        (2) dict for the model entity (eid, full_id, type) if found, otherwise None
    """

    # goes by each entity in the list separately and checks whether the name corresponds
    for entity in e_list:
        if entity.eid == e_id:
            # return the entity, if found
            return entity, {"eid": entity.eid, "full_id": entity.full_id, "type": entity.type}

    # entity was not found: return None
    return None, None


def get_entity_by_id_from_dict_list(e_dict_list: dict, e_id: str):
    """Provides an entity with regard to a given id from the entity list.

    Args:
        e_dict_list (dict): dict with lists of entities (sorted by type) to be searched in
        e_id (str): id of the entity that we search for

    Returns:
        entity, dict:
        (1) model entity if found, otherwise None
        (2) dict for the model entity (eid, full_id, type) if found, otherwise None
    """

    # go by each list stored in the dict and try to find the entity in the lists
    for e_list in e_dict_list.values():
        entity, ent_dict = get_entity_by_id(e_list=e_list, e_id=e_id)

        # return the entity, if found
        if entity is not None:
            return entity, ent_dict

    # entity was not found: return None
    return None, None


def create_plots(world: World, dir_graphs: str, bool_plot: bool, slices_execution: list = [0, 4]):
    """Create some plots after the simulation.
    Especially the execution_graph take some time to create and should not be created for every
    simulation.

    Args:
        world (World): mosaik world object to orchestrate the simulation process
        dir_graphs (str): direction where to save the create plots
        bool_plot (bool): whether to create the plots or not
        slices_execution (list): list of length 2 to assign start and end index of the execution
            graph to plot. Defaults to [0, 4].
    """
    if bool_plot:
        mosaik_util.plot_dataflow_graph(world, folder=dir_graphs)
        mosaik_util.plot_execution_graph(world, folder=dir_graphs, slice=slices_execution)
        mosaik_util.plot_execution_time(world, folder=dir_graphs)
        mosaik_util.plot_execution_time_per_simulator(world, folder=dir_graphs)


def start_simulators(sim_config: dict, world: World, scenario_config: dict) -> dict:
    """Start all model factories for the simulators listed in SIM_CONFIG and one for each model.

    Args:
        sim_config (dict): Information about the used simulators and their models
        world (World): mosaik world object to orchestrate the simulation process
        scenario_config (dict): parameters for the simulation scenario

    Returns:
        dict: Contains all ModelFactorys sorted by the name of the corresponding model
    """

    dict_simulators = {}

    for sim_name, sim_dict in sim_config.items():
        for model_name in sim_dict["models"].keys():
            dict_simulators[model_name] = world.start(sim_name, scenario_config=scenario_config)
            _logger.info(f"ModelFactory for {model_name} of simulator {sim_name} is started.")

    return dict_simulators


def create_entities(
    sim_config: dict, model_data: dict, dict_simulators: dict, grid_model_config: dict = {}
) -> dict:
    """Create all models based on simulators and their models dict given in sim_config.

    Args:
        sim_config (dict): Information about the used simulators and their models
        model_data (dict): dict containing all information about the to-be-created model entities
        dict_simulators (dict): dict of all used simulators with their ModelFactory-objects
        grid_model_config (dict): contains assigning the devices (by its type) to loads in the grid

    Returns:
        dict: contains all created entities sorted by their model type
    """

    dict_entities = {}

    # go by each simulator and its models given in sim_config
    for sim_name, sim_dict in sim_config.items():
        for model_name in sim_dict["models"].keys():
            # adaption for grid ems models: add grid model config
            if model_name == "GridEMS":
                for eid in model_data[model_name]:
                    model_data[model_name][eid]["grid_model_config"] = grid_model_config

            # simply create the entities with the given simulator, the name and the model data
            dict_entities[model_name] = create_entities_of_model(
                model_factory=dict_simulators[model_name],
                model_name=model_name,
                init_vals=model_data[model_name],
            )
            _logger.info(
                f"Models created for modeltype {model_name} with ModelFactory of {sim_name}."
            )

    return dict_entities


def create_entities_of_model(model_factory: object, model_name: str, init_vals: dict) -> list:
    """Create model entities for a given ModelFactory object with regard to model name and input.

    Args:
        model_factory (object): ModelFactory for a model
        model_name (str): name of the model to create entities for
        init_vals (dict): initial values of the model (attributes to set)

    Raises:
        KeyError: If the ModelFactory has no such model type
        TypeError: If the initial values were given in the wrong format

    Returns:
        list: all created model entities in one list (empty if no entity to create)
    """

    # check if model class is existing
    if not hasattr(model_factory, model_name):
        raise KeyError(f"The ModelFactory {model_factory} has no model type {model_name}")

    # check if initial model values are given correctly
    if not isinstance(init_vals, dict):
        raise TypeError(f"Init values for {model_name} were given in wrong format, should be dict")

    # if no entities should be created, return empty list
    if len(init_vals) == 0:
        return []

    # create the entities
    entity_list = getattr(model_factory, model_name).create(num=len(init_vals), init_vals=init_vals)

    return entity_list


def get_grid_components(grid_comps_list: list) -> dict:
    """Collect all components of a specific grid.

    Args:
        grid_comps_list (list): list of all grid components to collect from

    Returns:
        dict: containing all grid components separated by type
    """
    grid_comps = {}
    grid_comps["bus"] = [b for b in grid_comps_list if b.type in "bus"]
    grid_comps["load"] = [
        c for c in grid_comps_list if (c.type in "load") and ("ext_load" not in c.eid)
    ]
    grid_comps["trafo"] = [t for t in grid_comps_list if t.type in "trafo"]
    grid_comps["line"] = [t for t in grid_comps_list if t.type in "line"]
    grid_comps["ext_grid"] = [e for e in grid_comps_list if e.type in "ext_grid"]

    return grid_comps


def check_model_connect_config(model_connect_config: list):
    """Check the layout and input of a connection configuration for models.

    Args:
        model_connect_config (list): list of the model properties to connect

    Raises:
        TypeError: If the connection config is no list
        TypeError: If an element of the connection config is neither a string nor a tuple
        TypeError: If an connection config element is a tuple but not consisting of two strings
    """

    # check if connection configuration is a list of string values or tuples
    if not isinstance(model_connect_config, list):
        raise TypeError(
            f"Given connection configuration is not a list but a {type(model_connect_config)}"
        )
    else:
        for elem_connect in model_connect_config:
            if not isinstance(elem_connect, (str, tuple)):
                raise TypeError(
                    "Given connection config has an inaccessible element with the type"
                    f" {type(elem_connect)} that is not string or tuple"
                )
            elif isinstance(elem_connect, tuple) and (
                not isinstance(elem_connect[0], str) or not isinstance(elem_connect[1], str)
            ):
                raise TypeError(
                    "Given connection config has an inaccessible tuple element where one of the two"
                    " is not a string"
                )


def connect_entities(
    world: World,
    dict_entities: dict,
    model_connect_config: dict,
    dict_simulators: dict = {},
    cs_ev_connection: dict = {},
):
    """Connect all model entities to each other based on the created models and their connections.

    Args:
        world (World): mosaik world object to orchestrate the simulation process
        dict_entities (dict): dict of all used model entity objects as lists, sorted by their type
        model_connect_config (dict): dict of all connected attributes between each model type
        dict_simulators (dict): dict of all used simulators with their ModelFactory-objects.
            Defaults to {}.
        cs_ev_connection (dict): ev connections to charging stations. Defaults to {}.

    Raises:
        ValueError: If cs-ev connection given, but the entities cannot be found
    """

    # connect all models to each other (so check every combination of the entities)
    for idx_model_outer, loop_name_from in enumerate(dict_entities.keys()):
        for idx_model_inner, loop_name_to in enumerate(dict_entities.keys()):
            # only connect into one direction and also leave out connections to itself
            if idx_model_inner <= idx_model_outer:
                continue

            # check if connection attributes are existent (for any direction)
            dir_from_to_nothing = (  # nothing in direction FROM -> TO ?
                loop_name_from not in model_connect_config.keys()
                or loop_name_to not in model_connect_config[loop_name_from].keys()
                or model_connect_config[loop_name_from][loop_name_to] == []
            )
            dir_to_from_nothing = (  # nothing in direction TO -> FROM ?
                loop_name_to not in model_connect_config.keys()
                or loop_name_from not in model_connect_config[loop_name_to].keys()
                or model_connect_config[loop_name_to][loop_name_from] == []
            )

            # in neither direction there is a connection attribute, then SKIP
            if dir_from_to_nothing and dir_to_from_nothing:
                continue

            # nothing in direction TO -> FROM
            elif dir_to_from_nothing:
                name_from = loop_name_from
                name_to = loop_name_to
                model_connect_conf_to_from = []
                model_connect_conf_from_to = model_connect_config[name_from][name_to]

            # nothing in direction FROM -> TO
            elif dir_from_to_nothing:  # then change the directions (otherwise no strong connection)
                name_from = loop_name_to
                name_to = loop_name_from
                model_connect_conf_from_to = model_connect_config[name_from][name_to]
                model_connect_conf_to_from = []

            # something in both directions (FROM -> TO and TO -> FROM)
            else:
                # Check and set direction destination
                name_from, name_to = set_connection_direction(
                    name_from_init=loop_name_from, name_to_init=loop_name_to
                )
                model_connect_conf_to_from = model_connect_config[name_to][name_from]
                model_connect_conf_from_to = model_connect_config[name_from][name_to]

            # Set dicts for entities
            list_ent_from = dict_entities[name_from]
            list_ent_to = dict_entities[name_to]

            # check if connections between charging stations and ev have to be considered
            list_conn_e_id_ev = [ev for cs_conn in cs_ev_connection.values() for ev in cs_conn]
            list_conn_e_id_cs = [cs for cs in cs_ev_connection.keys()]
            list_entities = list_ent_from + list_ent_to
            # check if there is at least one entity of cs & ev list in the entities dict
            if any(
                e_id for ent in list_entities for e_id in list_conn_e_id_ev if ent.eid == e_id
            ) and any(
                e_id for ent in list_entities for e_id in list_conn_e_id_cs if ent.eid == e_id
            ):
                # go by each desired cs-ev pairing
                for cs_id, ev_id_list in cs_ev_connection.items():
                    # sort to sub-lists for connections that are given in the connection dict entry
                    list_cs = [ent for ent in list_ent_from if ent.eid == cs_id]
                    if list_cs == []:  # wrong direction
                        list_cs = [ent for ent in list_ent_to if ent.eid == cs_id]
                        list_ev = [
                            ent for ent in list_ent_from for ev_id in ev_id_list if ent.eid == ev_id
                        ]
                        list_ent_from = list_ev
                        list_ent_to = list_cs
                    else:
                        list_ev = [
                            ent for ent in list_ent_to for ev_id in ev_id_list if ent.eid == ev_id
                        ]
                        list_ent_from = list_cs
                        list_ent_to = list_ev
                    # check whether no entities were found
                    if list_cs == [] or list_ev == []:
                        raise ValueError(f"No entities found for cs-ev-conn: {cs_ev_connection}")

                    # create the connection for this cs-ev pairing
                    connect_entities_of_two_model_types(
                        world,
                        entity_list_strong=list_ent_from,
                        entity_list_weak=list_ent_to,
                        model_connect_config_from_strong=model_connect_conf_from_to,
                        model_connect_config_from_weak=model_connect_conf_to_from,
                    )

                continue  # cs-ev-connections are handled, go to next model pairing

            # connect the entities of the two models with each other
            sim_from = dict_simulators[name_from] if name_from in dict_simulators else None
            sim_to = dict_simulators[name_to] if name_to in dict_simulators else None
            connect_entities_of_two_model_types(
                world,
                entity_list_strong=list_ent_from,
                entity_list_weak=list_ent_to,
                model_connect_config_from_strong=model_connect_conf_from_to,
                model_connect_config_from_weak=model_connect_conf_to_from,
                sim_ent_strong=sim_from,
                sim_ent_weak=sim_to,
            )

    # for the ems: add all the assigned evs to the ems control
    for model_type, model_factory in dict_simulators.items():
        # check if it is EMS Factory and this EMS type is existent in entities
        if hasattr(model_factory, "add_controlled_entity") and model_type in dict_entities:
            for ems_ent in dict_entities[model_type]:  # for all ems entities to be connected
                if "EV" in dict_entities.keys():
                    for ev_ent in dict_entities["EV"]:  # for all ev entities to be connected
                        ev_entity, ev_entity_dict = get_entity_by_id(
                            e_list=dict_entities["EV"], e_id=ev_ent.eid
                        )
                        model_factory.add_controlled_entity(
                            ems_ent.eid, {ev_entity.eid: ev_entity_dict}
                        )
                        _logger.info(f"Device {ev_ent.eid} added to {ems_ent.eid}'s ctrl list.")


def set_connection_direction(name_from_init: str, name_to_init: str) -> tuple[str, str]:
    """Orders the weak-strong direction of two models by its model names to begin with.

    Args:
        name_from_init (str): name of model in initial from-position
        name_to_init (str): name of model in initial to-position

    Raises:
        ValueError: if no decision was found for connection of these two models

    Returns:
        tuple[str, str]: names of models, first FROM- and second TO-position
    """

    # load configuration for directions
    con_dir_dict = get_connection_directions_config()

    # Check if one of the devices always needs to be weak
    for connection_dict in con_dir_dict["ALWAYS_WEAK"].values():
        incl_name = connection_dict["includes"]
        full_name = connection_dict["full_name"]
        if (
            incl_name != "" and incl_name in name_from_init.lower()
        ) or full_name == name_from_init.lower():  # swith direction
            return name_to_init, name_from_init
        elif (
            incl_name != ""
            and incl_name in name_to_init.lower()
            or full_name == name_to_init.lower()
        ):  # direction works
            return name_from_init, name_to_init

    # Check if one of the devices always needs to be strong
    for connection_dict in con_dir_dict["ALWAYS_STRONG"].values():
        incl_name = connection_dict["includes"]
        full_name = connection_dict["full_name"]
        if (
            incl_name != "" and incl_name in name_to_init.lower()
        ) or full_name == name_to_init.lower():  # swith direction
            return name_to_init, name_from_init
        elif (
            incl_name != "" and incl_name in name_from_init.lower()
        ) or full_name == name_from_init.lower():  # direction works
            return name_from_init, name_to_init

    # Check if direction for these two devices is given
    for connection_dict in con_dir_dict["STRONG_DIR"].values():
        if (
            connection_dict["from"] == name_to_init.lower()
            and connection_dict["to"] == name_from_init.lower()
        ):  # swith direction
            return name_to_init, name_from_init
        elif (
            connection_dict["from"] == name_from_init.lower()
            and connection_dict["to"] == name_to_init.lower()
        ):  # direction works
            return name_from_init, name_to_init

    # no fitting entry found
    raise ValueError(
        "No strong/weak decision found in connection_directions_config for connection of models "
        f"'{name_from_init}' and '{name_to_init}', although both directions are set!"
    )


def connect_entities_in_grid(
    grid_model_config: dict,
    grid: object,
    grid_loads: list,
    world: World,
    model_connect_conf: dict,
    dict_entities: dict,
    dict_simulators: dict,
    architecture_models: list = ["RetailElectricityProvider", "GridEMS"],
):
    """Connect all entities for a simulation containing a power grid.
    Includes connection between the devices in the grid and between HEMS and grid loads.

    Args:
        grid_model_config (dict): contains assigning the devices (by its type) to loads in the grid
        grid (object): power grid entity
        grid_loads (list): list of all load objects in the power grid
        world (World): mosaik world object to orchestrate the simulation process
        model_connect_conf (dict): dict of all connected attributes between each model type
        dict_entities (dict): dict of all used model entity objects as lists, sorted by their type
        dict_simulators (dict): dict of all used simulators with their ModelFactory-objects
        architecture_models (list): all superordinate models to connect to each other and every HEMS

    Raises:
        ValueError: If more than one entity of any architecture model type is created.
        KeyError: No entity was created for a desired connection in the grid.
        KeyError: Given grid load name from model_connect_conf not found in grid loads list.
        ValueError: There are electric vehicles but no charging stations at a connection point.
        KeyError: A grid load is planned to be connected to two different entities.
    """

    # go by each architecture model
    bool_architecture_model_present = {}  # initialize dict for boolean values (if models exist)
    arch_model_ent = {}  # initialize dict for architecture model entities
    for model_name in architecture_models:
        # check whether this model exists
        if model_name in dict_entities.keys():
            bool_architecture_model_present[model_name] = True

            # retrieve model entity if present
            if len(dict_entities[model_name]) > 1:
                raise ValueError(f"More than one '{model_name}' is not possible!")
            arch_model_ent[model_name] = dict_entities[model_name][0]
        else:
            bool_architecture_model_present[model_name] = False
            _logger.warning(f"Architecture model '{model_name}': No entity existent!")

    # connect the grid to the grid ems
    if "GridEMS" in dict_entities.keys():
        connect_entities_of_two_model_types(
            world=world,
            entity_list_strong=[dict_entities["GridEMS"][0]],
            entity_list_weak=[grid],
            model_connect_config_from_strong=model_connect_conf["GridEMS"]["Grid"],
            model_connect_config_from_weak=model_connect_conf["Grid"]["GridEMS"],
            sim_ent_strong=None,
            sim_ent_weak=None,
        )
    else:
        _logger.warning(f"No GridEMS existent despite calculation of grid '{grid.eid}'.")

    # connect all architecture models with each other
    connect_entities(
        world=world,
        dict_entities={name: [ent] for name, ent in arch_model_ent.items()},
        model_connect_config=model_connect_conf,
        dict_simulators=dict_simulators,
    )

    # initialize counter for loads with no ems (where GCP aggregator HEMS is created)
    count_gcp_aggr = 0

    # iterate over all grid connection points (gcp) which are connected to the entitites
    # NOTE: gcps are modeled as loads
    for gcp_id, gcp_dict in grid_model_config.items():
        # save the name of the energy management system and connection between cs and ev
        ems_gcp_id = gcp_dict["ems"]
        cs_ev_connection = gcp_dict["cs_ev_connection"]

        # initialize empty dict of entity types (containing list of entities) at this gcp
        dict_ent_gcp = {}
        # add connected devices at this gcp to the entity dict
        for ent_id in gcp_dict["entities"]:
            ent, ent_dict = get_entity_by_id_from_dict_list(e_dict_list=dict_entities, e_id=ent_id)

            # check if no entity was found
            if ent is None:
                raise KeyError(
                    f"Entity {ent_id} given in grid_model_config at load {gcp_id} but"
                    " not created for simulation!"
                )

            # check if type already existent in entity dict, otherwise create key
            if ent_dict["type"] not in dict_ent_gcp.keys():
                dict_ent_gcp[ent_dict["type"]] = []

            dict_ent_gcp[ent_dict["type"]].append(ent)

        # find adjacent gcp in grid load
        for gcp in grid_loads:
            if gcp.eid == gcp_id:  # gcp is found, break the for loop
                break
        # go to the next load if this one was not found
        if not gcp.eid == gcp_id:
            raise KeyError(f"Load '{gcp_id}' from model_connect_config not found in grid loads.")

        # if no ems intended: create synthetic one (which just aggregates at the grid conn. point)
        if ems_gcp_id is None or ems_gcp_id == "":
            # find ems simulator (ModelFactory)
            mod_fac_ems = None
            for mod_fac in dict_simulators.values():
                if hasattr(mod_fac, "GCP_Aggregator_HEMS"):
                    mod_fac_ems = mod_fac
                    break
            if mod_fac_ems is None:
                raise ValueError(
                    "Cannot create synthetic GCP_Aggregator_HEMS at loads with no EMS ModelFactory!"
                )
            # create field for entities if needed
            if "GCP_Aggregator_HEMS" not in dict_entities.keys():
                dict_entities["GCP_Aggregator_HEMS"] = []
            # create field for simulator if needed
            if "GCP_Aggregator_HEMS" not in dict_simulators.keys():
                dict_simulators["GCP_Aggregator_HEMS"] = mod_fac_ems

            # create the entity without init values and append it to list of entities
            gcp_aggr_entity = create_entities_of_model(
                model_factory=mod_fac_ems,
                model_name="GCP_Aggregator_HEMS",
                init_vals={"GCP_Aggregator_HEMS_" + str(count_gcp_aggr): {}},
            )[0]
            count_gcp_aggr += 1
            dict_entities["GCP_Aggregator_HEMS"].append(gcp_aggr_entity)
            ems_gcp_id = gcp_aggr_entity.eid
            _logger.warning(f"Synthetic GCP_Aggregator_HEMS created at load '{gcp_id}'.")

        # save the corresponding ems
        ems_ent, ems_ent_dict = get_entity_by_id_from_dict_list(
            e_dict_list=dict_entities, e_id=ems_gcp_id
        )
        ems_type = ems_ent_dict["type"]

        # connect the ems power values to the gcp
        world.connect(ems_ent, gcp, *model_connect_conf[ems_type]["grid_load"])
        _logger.info(f"EMS '{ems_ent.eid}' connected to grid at {gcp.eid}.")

        # connect the ems to all architecture models
        for model_name, model_ent in arch_model_ent.items():
            connect_entities(
                world=world,
                dict_entities={ems_type: [ems_ent], model_name: [model_ent]},
                model_connect_config=model_connect_conf,
                dict_simulators={},  # empty dict to prevent adding to ems control list
            )

        # add ems at this gcp to the entity dict
        dict_ent_gcp[ems_ent.type] = [ems_ent]

        # connect all devices at (behind) this gcp
        connect_entities(
            world=world,
            dict_entities=dict_ent_gcp,
            model_connect_config=model_connect_conf,
            dict_simulators=dict_simulators,
            cs_ev_connection=cs_ev_connection,
        )


def connect_entities_to_db(sim_config: dict, world: World, database: object, dict_entities: dict):
    """Connects all models (model entities) to the database.

    Args:
        sim_config (dict): Information about the used simulators and their models
        world (World): mosaik world object to orchestrate the simulation process
        database (object): model object of the database for the simulation
        dict_entities (dict): dict of all used model entity objects
    """

    # connect all models given in sim_config simulator lists
    for sim_dict in sim_config.values():
        for model_name, model_output_properties in sim_dict["models"].items():
            mosaik_util.connect_many_to_one(
                world, dict_entities[model_name], database, *model_output_properties
            )
            _logger.info(f"Models of {model_name} connected to database.")


def connect_grid_to_db(sim_config_grid: dict, world: World, database: object, dict_comps: dict):
    """Connects all grid components to the database.

    Args:
        sim_config_grid (dict): Information about the used grid simulator and the components with a
            list of their output attributes
        world (World): mosaik world object to orchestrate the simulation process
        database (object): model object of the database for the simulation
        dict_comps (dict): dict of all used grid component entity objects, sorted by their type
    """

    for comp_name, comp_attr_list in sim_config_grid["components"].items():
        mosaik_util.connect_many_to_one(world, dict_comps[comp_name], database, *comp_attr_list)
        _logger.info(f"Grid components of type '{comp_name}' connected to database.")


def remove_cs_ev_conn_from_connect_config(connect_config: dict) -> dict:
    """In copied dict, remove information on connections between cs and ev.

    Args:
        connect_config (dict): info on connections between devices (and grid)

    Returns:
        dict: cleared dict with info on connections (without cs_ev_connection in 2nd order)
    """

    # create copy of connection dict
    db_structure = deepcopy(connect_config)

    # remove information on connections between charging stations and electric vehicles
    for load_dict in db_structure.values():
        if "cs_ev_connection" in load_dict.keys():
            del load_dict["cs_ev_connection"]

    return db_structure


def connect_to_forecast(
    world: World,
    dict_entities: dict,
    dict_simulators: dict,
    forecast: object,
    forecast_sim: object,
    connect_models_names: list,
):
    """Create connections for the forecasts to work.
    Includes mosaik connections to ems model and adding of the model entities to the forecasts list.

    Args:
        world (World): mosaik world object to orchestrate the simulation process
        dict_entities (dict): dict of all used model entity objects
        dict_simulators (dict): dict of all used simulators with their ModelFactory-objects
        forecast (object): forecast model entity
        forecast_sim (object): simulator for the forecast model
        connect_models_names (list): model names to directly connect to forecast entity
    """

    # create connections for each entity of each model type
    for model_name, ent_list in dict_entities.items():
        for entity in ent_list:
            # first check whether model should directly be connected to forecast entity
            bool_ent_connect_to_forecast = False
            for model_conn_name in connect_models_names:
                if model_conn_name in model_name.lower():
                    bool_ent_connect_to_forecast = True

            # create connections to forecast entity if desired
            if bool_ent_connect_to_forecast:
                if (
                    "forecast_request" in entity.meta["attrs"]
                    and "forecast" in entity.meta["attrs"]
                ):
                    world.connect(entity, forecast, "forecast_request")
                    world.connect(
                        forecast,
                        entity,
                        "forecast",
                        weak=True,
                        initial_data={"forecast": {forecast.full_id: {}}},
                    )
                else:
                    _logger.warning(
                        f"Model '{entity.eid}' to be connected to forecast, but has no "
                        "attributes 'forecast' and 'forecast_request'!"
                    )
            # for other models (devices) add those entities to the forecast entity list
            else:
                forecast_sim.add_forecasted_entity(
                    forecast.eid,
                    {entity.full_id: dict_simulators[model_name].get_entity_by_id(entity.eid)},
                )


def connect_entities_of_two_model_types(
    world: World,
    entity_list_strong: list,
    entity_list_weak: list,
    model_connect_config_from_strong: list,
    model_connect_config_from_weak: list = [],
    sim_ent_strong: object = None,
    sim_ent_weak: object = None,
):
    """Connect the elements of two lists in both directions (if needed) via mosaik.
    Also add elements to list of controlled entities if the "weak" entity is an ems.

    Args:
        world (World): mosaik World object
        entity_list_strong (list): entities that have "strong" outgoing connections
        entity_list_weak (list): entities that have "weak" outgoing connections
        model_connect_config_from_strong (list): list of properties (str or tuple) to connect the
            two models types - from strong to weak
        model_connect_config_from_weak (list): list of properties (str or tuple) to connect the
            two models types - from weak to strong. Defaults to [].
        sim_ent_strong (object): Simulator of the "strong" entities for the adding of entities
            to the ems controlled entities. Defaults to None.
        sim_ent_weak (object): Simulator of the "weak" entities for the adding of entities to
            the ems controlled entities. Defaults to None.

    Raises:
        TypeError: if given mosaik world is not correct (does not have connect method)
        ValueError: if no strong connection properties are given but there are weak connections
    """
    # check if world is correct mosaik:World object
    if not hasattr(world, "connect"):
        raise TypeError("world is not a correct mosaik:World object")

    # check connection configurations for both directions
    check_model_connect_config(model_connect_config_from_strong)
    check_model_connect_config(model_connect_config_from_weak)
    if model_connect_config_from_strong == []:  # check if given list is empty
        if model_connect_config_from_weak != []:
            raise ValueError("Not possible to give weak but no strong connections!")
        else:
            return  # no connections to be made for these models

    # go by each combination of entities in both directions
    for ent_strong in entity_list_strong:
        for ent_weak in entity_list_weak:
            # execute mosaik connect for strong connections
            world.connect(ent_strong, ent_weak, *model_connect_config_from_strong, weak=False)
            _logger.info(f"Added connection from {ent_strong.type} to {ent_weak.type}.")

            # execute mosaik connect for weak connections if needed
            if model_connect_config_from_weak is not None and model_connect_config_from_weak != []:
                # set initial data for weak connections
                dict_init_data = {}
                for conn_elem in model_connect_config_from_weak:
                    # set output values to None for all outgoing weak connections to all entities
                    if isinstance(conn_elem, str):
                        dict_init_data[conn_elem] = None
                    elif isinstance(conn_elem, tuple):
                        dict_init_data[conn_elem[0]] = None

                # execute connect command for weak connection via mosaik
                world.connect(
                    ent_weak,
                    ent_strong,
                    *model_connect_config_from_weak,
                    weak=True,
                    initial_data=dict_init_data,
                )
                _logger.info(f"Added connection from {ent_weak.type} to {ent_strong.type}.")

            # if one entity is an HEMS: add other entity to its control list
            if sim_ent_weak is not None and hasattr(sim_ent_weak, "add_controlled_entity"):
                entity, entity_dict = get_entity_by_id(e_list=[ent_strong], e_id=ent_strong.eid)
                sim_ent_weak.add_controlled_entity(ent_weak.eid, {entity.eid: entity_dict})
                _logger.info(f"Entity {ent_strong.eid} added to {ent_weak.eid}'s control list.")
            if sim_ent_strong is not None and hasattr(sim_ent_strong, "add_controlled_entity"):
                entity, entity_dict = get_entity_by_id(e_list=[ent_weak], e_id=ent_weak.eid)
                sim_ent_strong.add_controlled_entity(ent_strong.eid, {entity.eid: entity_dict})
                _logger.info(f"Entity {ent_weak.eid} added to {ent_strong.eid}'s control list.")

            # if one entity is an REP and counterpart is a market: add market entity to REPs list
            if (
                "market" in ent_strong.type.lower()
                and sim_ent_weak is not None
                and hasattr(sim_ent_weak, "add_market_entity")
            ):
                entity, entity_dict = get_entity_by_id(e_list=[ent_strong], e_id=ent_strong.eid)
                sim_ent_weak.add_market_entity(ent_weak.eid, {entity.eid: entity_dict})
                _logger.info(f"Market {ent_strong.eid} added to {ent_weak.eid}'s market list.")
            if (
                "market" in ent_weak.type.lower()
                and sim_ent_strong is not None
                and hasattr(sim_ent_strong, "add_market_entity")
            ):
                entity, entity_dict = get_entity_by_id(e_list=[ent_weak], e_id=ent_weak.eid)
                sim_ent_strong.add_market_entity(ent_strong.eid, {entity.eid: entity_dict})
                _logger.info(f"Market {ent_weak.eid} added to {ent_strong.eid}'s market list.")


def check_entities_connections(world: World, dict_entities: dict):
    """Check that there are adjacent connections for each entity of the simulation.

    Args:
        world (World): mosaik World object
        dict_entities (dict): dict of all used model entity objects
    """
    # create dictionary representing whether or not each entity is connected to anything
    entitiy_connection_status = dict()
    for ent_list in dict_entities.values():
        for entity in ent_list:
            entitiy_connection_status[entity.full_id] = False

    # go by each connecting edge in the mosaik connection graph
    for edge in world.entity_graph.edges:
        entitiy_connection_status[edge[0]] = True
        entitiy_connection_status[edge[1]] = True

    # output warnings for isolated entities
    for e_id, connection_status in entitiy_connection_status.items():
        if not connection_status:
            _logger.error(f"Entity '{e_id}' not connected to any other entity.")

    return


def get_default_dirs(
    dir_base,
    scenario: str = "building",
    grid: str = "example_grid_kerber.json",
    format_db: str = "hdf5",
) -> dict:
    """Gathers all needed directories resp. its paths based on eELib's default structure.

    Args:
        dir_base (str): string with path to root director
        scenario (str): Type of used scenario (like building, grid etc.). Defaults to "building".
        grid (str): name of the grid file to use. Defaults to "example_grid_kerber.json".
        format_db (str): format of the database. Defaults to None.

    Raises:
        FileNotFoundError: If model data for current simulation scenario not found

    Returns:
        dict: all relevant paths for a simulation
    """

    directory = {}

    directory["DATA"] = os.path.join(dir_base, "data")

    directory["RESULTS"] = os.path.join(directory["DATA"], "results")
    if not os.path.exists(directory["RESULTS"]):  # create result directory if necessary
        os.makedirs(directory["RESULTS"])

    directory["GRAPHS"] = os.path.join(directory["RESULTS"], "sim_graphs")
    if not os.path.exists(directory["GRAPHS"]):  # create graph directory if necessary
        os.makedirs(directory["GRAPHS"])

    directory["MODEL_DATA"] = os.path.join(
        directory["DATA"], "model_data_scenario", "model_data_" + scenario + ".json"
    )
    if not os.path.isfile(directory["MODEL_DATA"]):  # check if MODEL_DATA is valid
        raise FileNotFoundError("Model data for scenario not found!")

    if grid is not None:
        directory["GRID_DATA"] = os.path.join(directory["DATA"], "grid", grid)
        if not os.path.isfile(directory["GRID_DATA"]):  # check if GRID_DATA is valid
            raise FileNotFoundError("Grid data for grid scenario not found!")
        directory["MODEL_GRID_DATA"] = os.path.join(
            directory["DATA"], "grid", "grid_model_config.json"
        )
        if not os.path.isfile(
            directory["MODEL_GRID_DATA"]
        ):  # check if DIR_MODEL_GRID_DATA is valid
            raise FileNotFoundError("Data to grid-model-connection for grid scenario not found!")

    db_format = datetime.now().strftime("%Y-%m-%d-%H-%M-%S")
    directory["DATABASE_FILENAME"] = os.path.join(
        directory["RESULTS"], ("results_{}." + format_db).format(db_format)
    )

    return directory
