"""
Helper functions for the generation of new simulations.

| Author: elenia@TUBS
| Copyright 2024 elenia
| This file is part of eELib, which is free software under the terms of the GNU GPL Version 3.
"""

import pandas as pd
import pandapower as pp
import pandapower.networks as nw
from pandapower.plotting import simple_plot
from openpyxl import Workbook

# possible fields for all grid components
bus_fields = ["index", "vn_kv", "name", "in_service", "geodata"]
load_fields = ["bus", "name", "scaling"]
ext_grid_fields = ["bus", "name", "in_service"]
trafo_fields = ["hv_bus", "lv_bus", "std_type", "name", "in_service", "parallel"]
line_fields = ["from_bus", "to_bus", "name", "in_service", "length_km", "std_type", "parallel"]
grid = {
    "bus": bus_fields,
    "load": load_fields,
    "ext_grid": ext_grid_fields,
    "trafo": trafo_fields,
    "line": line_fields,
}


def create_excel_for_grid():
    """Create an empty excel fill to fill with information on grid components."""
    # initialize empty excel workbook
    excel = Workbook()

    # create sheet for each component (bus seperate because of already active sheet)
    bus_sheet = excel.active
    bus_sheet.title = "bus"
    bus_sheet.append(bus_fields)
    for comp in ["load", "ext_grid", "trafo", "line"]:
        sheet_add = excel.create_sheet(comp)
        sheet_add.append(grid[comp])

    # save file
    excel.save("input_grid.xlsx")


def read_excel_for_grid() -> dict:
    """Read information on grid components from excel file and save into dict.

    Returns:
        dict: Information on all grid components
    """
    grid_info = {}

    # go by each component and read table from corresponding sheet, save into dict
    for comp in grid.keys():
        grid_info[comp] = pd.read_excel("input_grid.xlsx", sheet_name=comp, index_col=None)

    return grid_info


def create_grid_file_with_name(
    pp_grid_name: str = "create_kerber_landnetz_kabel_1",
    grid_path: str = "grid.json",
    adjust_comp_names: bool = True,
    plot_grid: bool = False,
):
    """Create pandapower grid including saved json file from information about pandapower template.

    Args:
        pp_grid_name (str): PP method for grid. Defaults to "create_kerber_landnetz_kabel_1".
        grid_path (str): path to save json file to. Defaults to "grid.json".
        adjust_comp_names (bool): whether to adjust component names if None. Defaults to True.
        plot_grid (bool): whether to create simple plot of grid. Defaults to False.

    Returns:
        _type_: _description_
    """
    # get grid from pandapower networks
    net = getattr(nw, pp_grid_name)()

    # check all component fiels for "None"-Names and change them
    if adjust_comp_names:
        for comp in grid.keys():
            comp_field = getattr(net, comp)
            for i_ent in range(len(comp_field)):
                if comp_field["name"][i_ent] is None:
                    comp_field.at[i_ent, "name"] = comp + "_" + str(i_ent)

    # save grid into .json file
    pp.to_json(net, grid_path)

    # create simple plot of the created grid using pandapower
    if plot_grid:
        simple_plot(net, show_plot=True)

    return net


def create_grid_file(
    grid_data: dict, grid_path: str = "grid.json", plot_grid: bool = False
) -> object:
    """Create pandapower grid including saved json file from information about grid components.

    Args:
        grid_data (dict): information on all grid components
        grid_path (str): path to save json file to. Defaults to "grid.json".
        plot_grid (bool): whether to create simple plot of grid. Defaults to False.

    Returns:
        object: pandapower grid object
    """
    # create empty grid
    net = pp.create_empty_network()

    # create elements with a loop over each element type and all elements
    for comp, comp_vals in grid_data.items():
        for i_ent in range(len(comp_vals)):
            comp_create_method = getattr(pp, "create_" + comp)
            comp_ent_vals = comp_vals.iloc[i_ent].to_dict()
            if "geodata" in comp_ent_vals:
                comp_ent_vals["geodata"] = tuple(map(int, comp_ent_vals["geodata"].split(",")))
            comp_create_method(net, **comp_ent_vals)

    # save grid into .json file
    pp.to_json(net, grid_path)

    # create simple plot of the created grid using pandapower
    if plot_grid:
        simple_plot(net, show_plot=True)

    return net
