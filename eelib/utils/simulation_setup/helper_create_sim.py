"""
Helper functions for the generation of new simulations.

| Author: elenia@TUBS
| Copyright 2024 elenia
| This file is part of eELib, which is free software under the terms of the GNU GPL Version 3.
"""

import pandas as pd
from openpyxl import Workbook
import json
import numpy as np


def create_excel_for_models(models_dict: dict, bool_same_models: bool = False):
    """Create excel with sheets for all existing models and their parameters.

    Args:
        models_dict (dict): info about existing models and their parameters
        bool_same_models (bool): if models can have all the same entities. Defaults to False.
    """
    model_names_list = list(models_dict.keys())

    # initialize empty excel workbook
    excel = Workbook()

    # create sheet for each model
    for idx_model in range(len(model_names_list)):
        # check if model (class) is abstract -> no sheet for that because no instantiation possible
        if models_dict[model_names_list[idx_model]]["is_abstract"]:
            continue

        if idx_model == 0:  # first seperate because of active first sheet
            sheet = excel.active
            sheet.title = model_names_list[0]
        else:
            sheet = excel.create_sheet(model_names_list[idx_model])
        # get list of parameters for this model
        list_params = list(models_dict[model_names_list[idx_model]]["valid_params"].keys())
        # add loadbus to parameter list for configuration, if each model added seperately
        if bool_same_models:
            sheet.append(list_params)
        else:
            sheet.append(["loadbus", *list_params])

    # save file
    excel.save("input_models.xlsx")


def read_excel_for_models(models_dict: dict) -> dict:
    """Create dict of model data from information in excel on all model types.

    Args:
        models_dict (dict): info about existing models and their parameters

    Returns:
        dict: info on all models/entities for the simulation
    """
    # initialize empty dict for information of filled excel sheets
    model_info = {}

    # go by each component and read table from corresponding sheet, save into dict
    for model_name in models_dict.keys():
        # no sheet for abstract model (class)
        if models_dict[model_name]["is_abstract"]:
            continue

        model_excel_input = pd.read_excel(
            "input_models.xlsx", sheet_name=model_name, index_col=None, dtype={"start_time": "str"}
        )
        if not model_excel_input.empty:  # only save if info available
            model_info[model_name] = model_excel_input

    return model_info


def create_excel_for_same_models(model_data: dict):
    """Create excel with sheets for all named models with 1 entity to possibly multiply them.

    Args:
        model_data (dict): info on models for the simulation
    """
    # initialize empty excel workbook
    excel = Workbook()
    counter_sheets = 0

    # go by each model type
    for model_name, model_info in model_data.items():
        # check if model data is empty -> no entities to create -> no sheet
        if not model_info.empty:
            # check that there is just one line of parameters given (s.t. these can be used)
            if len(model_info) > 1:
                print(f"Model Type {model_name} recieved >1 list of parameters - cannot be used.")
                continue

            if counter_sheets == 0:  # first seperate because of active first sheet
                sheet = excel.active
                sheet.title = model_name
            else:
                sheet = excel.create_sheet(model_name)

            # add sheet for this model just with loadbus column
            sheet.append(["loadbus_list"])

    # save file
    excel.save("models_list_loadbus.xlsx")


def read_excel_for_same_models(model_data: dict) -> dict:
    """Create dict of model data from existent data and information on same models at loadbuses.

    Args:
        model_data (dict): info on models for the simulation

    Returns:
        dict: info on all entities for the simulation
    """
    # go by each component and read table from corresponding sheet, save into dict
    for model_name, model_df in model_data.items():
        try:  # try to read loadbus list for this model type - sheet may not exist for all types
            loadbus_list = pd.read_excel(
                "models_list_loadbus.xlsx", sheet_name=model_name, index_col=None
            )
        except ValueError:
            continue  # if sheet does not exist, continue with next model

        # go by list and copy the first element to the end
        for _ in range(len(loadbus_list) - 1):  # one element already existing
            model_df = pd.concat(
                [model_df, pd.DataFrame([model_data[model_name].iloc[0]])], ignore_index=True
            )

        # append column with loadbus for the entities
        model_df["loadbus"] = loadbus_list

        model_data[model_name] = model_df

    return model_data


def create_model_data_file(model_data: dict, model_data_path="model_data.json"):
    """Create model data file with information on all model parameterizations.

    Args:
        model_data (dict): info on all entities for the simulation
        model_data_path (str): path to store model data file. Defaults to "model_data.json".
    """
    # initialize empty dict for information to all models
    write_models_dict = dict()

    # go by each model type
    for model_name, model_info in model_data.items():
        # check if excel sheet was empty -> no models to create
        if not model_info.empty:
            # create empty dict for all entities of this model
            dict_models = {}

            # go by full list of given models (from excel file)
            for idx_model in range(len(model_info)):
                # collect each given parameter value in dict format
                params_dict = {}
                for param in model_info.columns:
                    value = model_info.at[idx_model, param]
                    if param == "loadbus":  # given loadbus for config should be ignored
                        continue
                    elif pd.isna(value):  # filter out non-given values
                        continue
                    elif isinstance(value, np.int64):
                        params_dict[param] = int(value)
                    elif isinstance(value, np.float64):
                        params_dict[param] = float(value)
                    elif isinstance(value, np.ndarray):
                        params_dict[param] = value.tolist()
                    else:
                        params_dict[param] = value

                # append dict for this entity to list for this model type
                dict_models[model_name + "_" + str(idx_model)] = params_dict

            # write list for this model type into dict for all models
            write_models_dict[model_name] = dict_models

    # dump full dict into json file
    with open(model_data_path, "w") as json_file:
        json.dump(write_models_dict, json_file, indent=4)


def create_grid_model_config_file(model_data: dict, config_path: str = "grid_model_config.json"):
    """Create connection file for grid simulations with entities at specific load buses.

    Args:
        model_data (dict): info on all entities for the simulation and their loadbuses
        config_path (str): path to store config file. Defaults to "grid_model_config.json".
    """
    # initialize empty dict for information to all connections
    config_dict = dict()

    # go by each model type and all entities of that type
    for model_name, model_info in model_data.items():
        for idx_entity in range(len(model_info)):
            # retrieve the loadbus name and create dict fields (if not present)
            loadbus = model_info.at[idx_entity, "loadbus"]
            if loadbus not in config_dict.keys():
                config_dict[loadbus] = {"ems": "", "entities": [], "cs_ev_connection": {}}

            # append this entity to the corresponding dict field
            entity_name = model_name + "_" + str(idx_entity)
            if "ems" in model_name.lower():
                config_dict[loadbus]["ems"] = entity_name
            else:
                config_dict[loadbus]["entities"].append(entity_name)

    # dump full dict into json file
    with open(config_path, "w") as json_file:
        json.dump(config_dict, json_file, indent=4)
