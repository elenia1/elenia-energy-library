"""
Helper functions for the generation of model overview for the eELib.

| Author: elenia@TUBS
| Copyright 2024 elenia
| This file is part of eELib, which is free software under the terms of the GNU GPL Version 3.
"""

import os
import fnmatch
import importlib
import pandas as pd
from datetime import datetime
from abc import ABCMeta


def gen_model_overview(path_rst: str = "model_overview.rst", path_txt: str = None) -> dict:
    r"""Create overview of models in the eELib with search for all simulators.

    Args:
        path_rst (str): Path to save model info in rst-format, if needed. Defaults to
            "model_overview.rst".
        path_txt (str): Path to save model info in txt-format, if needed. Defaults to None.

    Returns:
        dict: information about all eELib model, including class and valid parameters
    """
    # save eELib directory - up two directories from current working directory
    eelib_dir = os.path.dirname(os.path.dirname(os.getcwd()))

    # Load simulator files
    simulator_files_list = find_specific_files(directory=eelib_dir, pattern="*_simulator.py")

    # initialize dict for all models including their parameters
    models_dict = {}

    # load information for each simulator
    for simulator_file in simulator_files_list:
        sim_name, sim_dict = get_sim_info(full_file_path=simulator_file)

        for model_name in sim_dict["models_list"]:
            try:
                models_dict[model_name] = get_model_info(
                    model_class=model_name, sim_dict=sim_dict, eelib_dir=eelib_dir
                )
            except AttributeError as err:
                print(err)

    # create dataframe of valid parameters for each model (in case on needs to print that)
    for model_name in models_dict.keys():
        models_dict[model_name]["valid_params_df"] = model_info_to_valid_params_df(
            model_info=models_dict[model_name]
        )

    # create text string from model info and store that, if wanted
    rst_text = model_info_to_string(model_info=models_dict, title="Model Overview with Parameters")
    if path_rst is not None:
        string_to_file(input=rst_text, file_path=path_rst)
    if path_txt is not None:
        string_to_file(input=rst_text, file_path=path_txt)

    return models_dict


def find_specific_files(directory: str, pattern: str) -> list:
    r"""Recursively search for files matching the pattern (in the filename) in the given directory.

    Args:
        directory (str): The root directory to start searching from.
        pattern (str): The pattern to match files (e.g., '\*.txt' for all text files).

    Returns:
        list: paths to the matching files
    """
    matching_files = []

    # go by each file in the directory
    for root, _, files in os.walk(directory):
        # check the filename for the given pattern
        for filename in fnmatch.filter(files, pattern):
            # if pattern inside the filename, add filename to the matching list
            matching_files.append(os.path.join(root, filename))

    # return matching list
    return matching_files


def get_sim_info(full_file_path: str) -> dict:
    """Load simulator and model module to retrieve data for both into a dict.

    Args:
        full_file_path (str): path to the simulator file

    Returns:
        dict: information about simulator and list of corresponding models
    """
    # initialize dict for sim
    simulator_dict = {"full_path": full_file_path}

    # retrieve only the name of the module (\\ and -1) and remove the ".py" part (0:-3)
    simulator_dict["path_to_model"] = os.path.dirname(full_file_path)  # .split("\\")[0:-1]
    module_name = os.path.basename(full_file_path).replace(".py", "")  # .split("\\")[-1][0:-3]
    simulator_dict["model_filename"] = module_name.replace("_simulator", "_model")

    simulator_dict["model_full_file_path"] = os.path.join(
        simulator_dict["path_to_model"], simulator_dict["model_filename"] + ".py"
    )

    # load specifications of the file, get the module and import it + save it
    spec = importlib.util.spec_from_file_location(module_name, full_file_path)
    module_sim = importlib.util.module_from_spec(spec)
    spec.loader.exec_module(module_sim)
    simulator_dict["sim_module"] = module_sim

    # get module for the model file
    spec = importlib.util.spec_from_file_location(
        simulator_dict["model_filename"], simulator_dict["model_full_file_path"]
    )
    module_model = importlib.util.module_from_spec(spec)
    spec.loader.exec_module(module_model)
    simulator_dict["model_module"] = module_model

    # store the META
    simulator_dict["META"] = module_sim.META

    # retrieve the model names
    simulator_dict["models_list"] = list(module_sim.META["models"].keys())

    # return name of the module and the info dict
    return module_name, simulator_dict


def get_model_info(model_class: str, sim_dict: dict, eelib_dir: str) -> dict:
    """Retrieve data for a model class including valid input parameters.

    Args:
        model_class (str): name of the class for the model type
        sim_dict (dict): information collected from the simulator (file paths etc.)
        eelib_dir (str): path to eELib directory

    Returns:
        dict: information about the model, including class and valid parameters
    """
    # initialize model dict
    model_dict = {}

    # for model class extract the class and get the valid parameters
    model_dict["class"] = getattr(sim_dict["model_module"], model_class)
    model_dict["path_to_model"] = os.path.join(
        "eelib", os.path.relpath(sim_dict["path_to_model"], start=os.path.abspath(eelib_dir))
    )
    model_dict["valid_params"] = model_dict["class"].get_valid_parameters()

    # check if the model (the class) is abstract
    model_dict["is_abstract"] = isinstance(model_dict["class"], ABCMeta) and bool(
        getattr(model_dict["class"], "__abstractmethods__", False)
    )

    # coverty allowed types from class to string for each parameter
    for param, val_dict in model_dict["valid_params"].items():
        try:
            types_list = []
            for allow_type in val_dict["types"]:
                if isinstance(allow_type, str):
                    types_list.append(allow_type)
                elif isinstance(allow_type, type):
                    types_list.append(allow_type.__name__)
            val_dict["types"] = types_list
        except Exception as e:
            print(f"Model {model_class} - Attribute {param}: {e}")

    # return info dict for this model
    return model_dict


def model_info_to_valid_params_df(model_info: dict) -> pd.DataFrame:
    """From model information print valid input parameters to the console.

    Args:
        model_info (dict): information about the model, including class and valid parameters

    Returns:
        pd.DataFrame: Dataframe of valid parameters
    """
    valid_params = {"Input Parameter": [], "Allowed Types": [], "Possible Values": []}
    for param, val in model_info["valid_params"].items():
        valid_params["Input Parameter"].append(param)
        valid_params["Allowed Types"].append(val["types"])
        valid_params["Possible Values"].append(val["values"])
    valid_params = pd.DataFrame(data=valid_params)

    return valid_params


def model_info_to_string(model_info: dict, title: str = "Model Information") -> str:
    """Convert the collected information about models into a single string.

    Args:
        model_info (dict): information about models with class, path, and valid parameters
        title (str): Title for the string. Defaults to "Model Information".

    Returns:
        str: the full string with all model information
    """
    # initialize empty string
    text = []

    # add title for overview and empty lines
    text.append(title)
    text.append("=" * len(title))
    text.append(f"(State: {datetime.now().strftime('%Y-%m-%d')})")
    text.append("")
    text.append("")

    # initialize headers for table with parameters to each model
    str_tbl_titles_par = "INPUT PARAMETER"
    empty_par = 13
    len_par = len(str_tbl_titles_par) + empty_par
    str_tbl_titles_typ = "ALLOWED TYPES"
    empty_typ = 8
    len_typ = len(str_tbl_titles_typ) + empty_typ
    str_tbl_titles_val = "POSSIBLE VALUES"
    empty_val = 50
    len_val = len(str_tbl_titles_val) + empty_val
    str_tbl_titles = (
        str_tbl_titles_par
        + " " * (empty_par + 1)
        + str_tbl_titles_typ
        + " " * (empty_typ + 1)
        + str_tbl_titles_val
        + " " * (empty_val + 1)
    )
    str_hor_lines = "=" * len_par + " " + "=" * len_typ + " " + "=" * len_val

    # go by each model and save all relevant info into the string (formatted)
    for model_name, model_data_dict in model_info.items():
        # model name and the path inside eELib
        text.append(f"{model_name}")
        text.append("-" * len(model_name))
        model_path = model_data_dict["path_to_model"].replace("\\", ".").replace("/", ".")
        text.append("")
        text.append(f"path: {model_path}")
        text.append("")

        # print header for the parameter table with titles and horizontal lines
        text.append(str_hor_lines)
        text.append(str_tbl_titles)
        text.append(str_hor_lines)
        for param, val in model_data_dict["valid_params"].items():
            # collect and print info for each parameter within single line
            str_par = param + " " * (len_par - len(param))
            str_typ = str(val["types"]) + " " * (len_typ - len(str(val["types"])))
            str_val = str(val["values"]) + " " * (len_val - len(str(val["values"])))
            text.append(str_par + " " + str_typ + " " + str_val)

        text.append(str_hor_lines)  # horizontal lines to finish table
        text.append("")  # empty lines after each model
        text.append("")

    return "\n".join(text)


def string_to_file(input: str, file_path: str = "model_overview.rst"):
    """Saves a string in specified format to a file.

    Args:
        input (str): The text content to save.
        file_path (str): The full path of the file to save the content in.
    """
    with open(file_path, "w") as file:
        file.write(input)
