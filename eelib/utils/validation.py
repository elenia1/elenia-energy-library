"""
Provide a dictionary of type and value validation rules and
validate given parameters or print a readable string of the given set of rules.

| Author: elenia@TUBS
| Copyright 2024 elenia
| This file is part of eELib, which is free software under the terms of the GNU GPL Version 3.
"""


def validate_init_parameters(model_class, input_param_dict: dict):
    """Validate the type and value of a list of parameters according to a provided dictionary.

    Args:
        model_class (class): a class representing a component. This should contain a method
            get_valid_parameters() that returns a dictionary.
        input_param_dict (dict): a dictionary of parameter name value pairs to be assessed and
            validated.

    Raises:
        TypeError: Invalid parameter input types
        ValueError: Invalid parameter input values
    """
    """
    NOTE: Example for the dictionary that should be returned:
        {
            "name_of_variable_one": {
                "types": [int],
                "values": None # Allow any integer value
            },
            "name_of_variable_two": {
                "types": [str],
                "values": ["abc", "def"]  # Allow only these choices
            },
            "name_of_variable_three": {
                "types": [float],
                "values": (0, 1)  # Allow only values in this range
            }
        }
    """

    # dictionary of valid parameters and their types and values
    valid_params = model_class.get_valid_parameters()

    invalid_prop_params = []  # invalid properties
    invalid_prop_types = []  # properties with invalid given types
    invalid_prop_values = []  # properties with invalid given values
    types_received = []  # list of invalid types recieved in order of properties recieved
    values_received = []  # list of invalid values recieved in order of properties recieved

    # iterate over parameters recieved and their values recieved
    for name, param_value in input_param_dict.items():
        # if any property is invalid (not present in our VALID_PARAMETERS list)
        if name not in valid_params:
            invalid_prop_params.append(name)
            continue

        param_info_valid = valid_params[name]  # information (types, values) of parameter if valid

        valid_types = param_info_valid["types"]  # valid types for parameter if valid

        # validate types
        if not isinstance(param_value, tuple(valid_types)):
            invalid_prop_types.append(name)
            types_received.append(type(param_value))
            continue

        valid_values = param_info_valid["values"]  # valid values for parameter if valid

        # validate values if given
        if valid_values is None:
            # if no limitation on valid values exist
            continue
        elif isinstance(valid_values, tuple):
            # if we have a range
            if param_value < valid_values[0] or param_value > valid_values[-1]:
                # if the given value is not inside that range
                invalid_prop_values.append(name)
                values_received.append(param_value)
        elif param_value not in valid_values:
            # if we have specific valid values and the given value is not any of them
            invalid_prop_values.append(name)
            values_received.append(param_value)

    # raise error if invalid parameters are found
    if invalid_prop_params:
        # display a list of invalid parameters recieved
        error_message = (
            f"Invalid parameter(s): {', '.join(str(p) for p in invalid_prop_params)}\nAcceptable"
            f" parameters for model '{model_class.__name__}':"
            f" \n{', '.join(str(p) for p in valid_params)}"
        )
        raise TypeError(error_message)

    # if no invalid parameters, raise error if invalid types are found
    elif invalid_prop_types:
        # display a list of parameters which recieved invalid types.
        # also display the invalid type recieved and display acceptable types.
        error_message = (
            "Invalid type for parameter(s): "
            + ", ".join(str(p) for p in invalid_prop_types)
            + f" for model '{model_class.__name__}'"
            + "\nTypes received:\n"
            + "\n".join(str(p) + ": " + str(t) for p, t in zip(invalid_prop_types, types_received))
            + "\nAcceptable types:\n"
            + "\n".join(str(p) + ": " + str(valid_params[p]["types"]) for p in invalid_prop_types)
        )
        raise TypeError(error_message)

    # if no invalid types, raise error if invalid values are found
    elif invalid_prop_values:
        # display a list of parameters which recieved invalid values.
        # also display the invalid type recieved and display acceptable values.
        error_message = (
            "Invalid value for parameter(s): "
            + ", ".join(str(p) for p in invalid_prop_values)
            + f" for model '{model_class.__name__}'"
            + "\nValues received:\n"
            + "\n".join(
                str(p) + ": " + str(t) for p, t in zip(invalid_prop_values, values_received)
            )
            + "\nAcceptable values:\n"
            + "\n".join(str(p) + ": " + str(valid_params[p]["values"]) for p in invalid_prop_values)
        )

        raise ValueError(error_message)


def format_valid_parameters_dictionary(valid_parameters: dict):
    """Take the valid parameters dict and transform it into a readable string format to display it.

    Args:
        valid_parameters (dict): a dictionary representing valid parameter types and values

    Returns:
        str: Message string with the collected errors from input validation
    """

    """
    NOTE: Example for the definition of the dictionary:
        {
            "name_of_variable_one": {
                "types": [int],
                "values": None # Allow any integer value
            },
            "name_of_variable_two": {
                "types": [str],
                "values": ["abc", "def"]  # Allow only these choices
            },
            "name_of_variable_three": {
                "types": [float],
                "values": (0, 1)  # Allow only values in this range (inclusive)
            }
        }
    """
    message = ""
    for key in valid_parameters:
        types = valid_parameters[key]["types"]
        values = valid_parameters[key]["values"]
        message += (
            key
            + ": acceptable types: "
            + ", ".join(str(p) for p in types)
            + ", acceptable values: "
            + ("any" if values is None else ", ".join(str(p) for p in values))
            + "\n"
        )
    return message
