__Author: elenia@TUBS__

Copyright 2024 elenia

The eELib is free software under the terms of the GNU GPL Version 3.

## Introduction to exemplary data sets

The provided data just presents examples that one can easily start simulations and test new
implementations. The data is not fully validated and should not be used for analyses etc.

The sources of that data are provided in the following.


### Car and charging station

Profiles generated with the tool emobpy:

Gaete-Morales, C. et al. An open tool for creating battery-electric vehicle time series from
empirical data, emobpy. Sci Data 8, 152 (2021)

DOI: https://doi.org/10.1038/s41597-021-00932-9

### Grid

Kerber Landnetz Kabel 1

From: Kerber, Georg - Aufnahmefähigkeit von Niederspannungsverteilnetzen fuer die Einspeisung aus
Photovoltaikkleinanlagen, PhD thesis, 2010

For visualization and information,
[see here](https://pandapower.readthedocs.io/en/v2.1.0/networks/kerber.html)

### Heating

Simple synthetic heat demand profiles for room and warm water with daily/seasonal values.

1400 (during night) resp. 900 (during day) Watt room heating demand during the winter,
700 (during night) resp. 500 (during day) Watt room heating demand during the summer.

2,000 Watt warm water heating demand: 1am, 3:30am, 5:45-6:30am, 7:00-7:45am, 11:00am, 1:45-2:00pm,
6:30-7:45pm, 8:15-9:00pm, 11:00-11:30pm

### Heatpump

Schlemminger, Marlon et al. Dataset on electrical single-family house and heat pump load profiles in
Germany (2022), selected SFH9

DOI: 10.1038/s41597-022-01156-1

### Load

Tjaden, T. et al. Representative electrical load profiles of residential buildings in germany with a
temporal resolution of one second, Hochschule fuer Technik und Wirtschaft (HTW), HTW 31&12, Berlin,
license: CC-BY-NC-4.0, DOI: 10.13140/RG.2.1.5112.0080/1;
[go to dataset](https://solar.htw-berlin.de/elektrische-lastprofile-fuer-wohngebaeude/)

NOTE: The files in the sub-folder are randomly drawn profiles from the above mentioned source, which
are adjusted resp. scaled to statistical average annual electricity demand of households with 1 to 4
persons:

- 1_person_profile: 1900 kWh/a
- 2_person_profile: 2890 kWh/a
- 3_person_profile: 3720 kWh/a
- 4_person_profile: 4085 kWh/a

### Market

Day-ahead market prices from year 2023, from german grid agency (Bundesnetzagentur);
[go to dataset download platform](https://www.smard.de/home/downloadcenter/download-marktdaten/)

### PV

Measured data from a pv rooftop system at Technical University Braunschweig, elenia institute
