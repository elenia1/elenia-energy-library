"""
Scenario file for the basic scenario 'grid' with a small standardized low voltage grid (Kerber).
This contains eight residential household with a few prosumer components like PV or battery,
controlled by energy management systems.

Within this test scenario mosaik is used as the co-simulation orchestrator.
Copyright (c) LGPL

| Author: elenia@TUBS
| Copyright 2024 elenia
| This file is part of eELib, which is free software under the terms of the GNU GPL Version 3.
"""

import os
import json
import mosaik
import mosaik.util
import eelib.utils.simulation_helper as sim_help
from eelib.model_connections.connections import get_default_connections
from eelib.utils.logging_helpers import set_console_logger
import arrow
import logging

set_console_logger(logging.INFO)

_logger = logging.getLogger(__name__)


""" ############################################################################# """
""" ################################### SETUP ################################### """
""" ############################################################################# """

# define paths and filenames
DIR = sim_help.get_default_dirs(
    os.path.realpath(os.path.dirname(__file__)),
    scenario="grid",
    grid="example_grid_kerber.json",
    format_db="hdf5",
)

# log paths
_logger.info(f"Selected model data: {DIR['MODEL_DATA']}")
_logger.info(f"Selected grid data: {DIR['GRID_DATA']}")
_logger.info(f"Results will be stored in {DIR['DATABASE_FILENAME']}.")


# Sim config.: Simulators and their used model types with the properties to store into DB
SIM_CONFIG = {
    # used database, will be left out for model creation and connections
    "DBSim": {"python": "eelib.data.database.hdf5:Hdf5Database"},
    # used grid model, will be left out for model creation and connections
    "GridSim": {
        "python": "eelib.core.grid.pandapower.pandapower_simulator:Sim",
        "model": "Pandapower",
        "components": {
            "bus": ["vm_pu", "va_degree", "p_w", "q_var"],
            "line": ["loading_percent"],
            "trafo": ["loading_percent", "tap_pos"],
            "ext_grid": ["p_w", "q_var"],
        },
    },
    # forecast, will be left out for model creation and connections
    "ForecastSim": {
        "python": "eelib.core.forecast.forecast_simulator:Sim",
        "models": {"Forecast": []},
    },
    # all the used simulators and their models for this simulation
    "REPSim": {
        "python": "eelib.core.market.retail_electricity_provider.rep_simulator:Sim",
        "models": {"RetailElectricityProvider": ["elec_price", "feedin_tariff"]},
    },
    "GridEMSSim": {
        "python": "eelib.core.control.grid.grid_ems_simulator:Sim",
        "models": {"GridEMS": ["congested"]},
    },
    "HEMSSim": {
        "python": "eelib.core.control.hems.hems_simulator:Sim",
        "models": {
            "HEMS_default": [
                "p_balance",
                "p_demand",
                "p_generation",
                "q_balance",
                "p_th_balance",
                "p_th_dem",
                "e_th",
                "profit",
            ],
            "HEMS_forecast_default": ["p_balance", "q_balance", "p_th_balance", "e_th", "profit"],
            "HEMS_forecast_opt": ["p_balance", "p_th_balance", "e_th", "profit"],
        },
    },
    "CSVSim": {
        "python": "eelib.data.csv_reader.csv_reader_simulator:Sim",
        "models": {
            "HouseholdCSV": ["p", "q"],
            "PvCSV": ["p", "q"],
            "ChargingStationCSV": ["p", "q"],
            "HeatpumpCSV": ["p_el", "q_el"],
            "HouseholdThermalCSV": ["p_th_room", "p_th_water"],
            "MarketDayAheadCSV": ["price"],
        },
    },
    "CSSim": {
        "python": "eelib.core.devices.charging_station.charging_station_simulator:Sim",
        "models": {"ChargingStation": ["p"]},
    },
    "EVSim": {
        "python": "eelib.core.devices.ev.ev_simulator:Sim",
        "models": {"EV": ["soc", "consumption"]},
    },
    "BSSSim": {
        "python": "eelib.core.devices.bss.bss_simulator:Sim",
        "models": {"BSS": ["soc", "p"]},
    },
    "HPSim": {
        "python": "eelib.core.devices.heatpump.heatpump_simulator:Sim",
        "models": {"Heatpump": ["p_th", "p"]},
    },
    "PVLibSim": {
        "python": "eelib.core.devices.pv.pv_lib_simulator:Sim",
        "models": {
            "PVLib": ["p", "q"],
            "PVLibExact": ["p", "q"],
        },
    },
}

# state for which models forecasts can be calculated (those are connected to the forecast simulator)
FORECAST_MODELS_NAMES = ["ems", "retailelectricityprovider"]


# Configuration of scenario: time and granularity
START = "2023-01-01 00:00:00"
END = "2023-01-04 00:00:00"
STEP_SIZE_IN_SECONDS = 900  # 1=sec-steps, 3600=hour-steps, 900=15min-steps, 600=10min-steps
USE_FORECAST = True
N_SECONDS = int(
    (
        arrow.get(END, "YYYY-MM-DD HH:mm:ss") - arrow.get(START, "YYYY-MM-DD HH:mm:ss")
    ).total_seconds()
)
N_STEPS = int(N_SECONDS / STEP_SIZE_IN_SECONDS)
scenario_config = {
    "start": START,  # time of beginning for simulation
    "end": END,  # time of ending
    "step_size": STEP_SIZE_IN_SECONDS,
    "n_steps": N_STEPS,
    "use_forecast": USE_FORECAST,
    "bool_plot": False,
}

# Read Scenario files with data for model entities
with open(DIR["MODEL_DATA"]) as f:
    model_data = json.load(f)

# Read files with data for connection of prosumer components to grid busses
with open(DIR["MODEL_GRID_DATA"]) as f:
    grid_model_config = json.load(f)

# Data representing groupings additional to grid_model_config
additional_groups = {"Grid": ["GridSim", "GridEMSSim"]}

# Read configuration file with data for connections between prosumer devices
model_connect_config = get_default_connections()

# Create world
world = mosaik.World(SIM_CONFIG, debug=True)


""" ############################################################################# """
""" ############################# START SIMULATORS ############################## """
""" ############################################################################# """

# start database and delete it from SIM_CONFIG
database_sim = world.start(
    "DBSim",
    scenario_config=scenario_config,
    db_structure=sim_help.remove_cs_ev_conn_from_connect_config(grid_model_config),
    additional_groups=additional_groups,
)
_logger.info("ModelFactory for Database is started.")
del SIM_CONFIG["DBSim"]

# start grid and delete it from SIM_CONFIG
grid_sim = world.start("GridSim", scenario_config=scenario_config, trigger=True)
sim_config_grid = SIM_CONFIG["GridSim"]
_logger.info(f"ModelFactory for Grid {sim_config_grid['model']} is started.")
del SIM_CONFIG["GridSim"]

# start database and delete it from SIM_CONFIG
if scenario_config["use_forecast"]:
    forecast_sim = world.start("ForecastSim", scenario_config=scenario_config)
    _logger.info("ModelFactory for Forecast is started.")
if "ForecastSim" in SIM_CONFIG.keys():
    del SIM_CONFIG["ForecastSim"]

# start all simulators/model factories with mosaik for data given in SIM_CONFIG
dict_simulators = sim_help.start_simulators(
    sim_config=SIM_CONFIG, world=world, scenario_config=scenario_config
)


""" ############################################################################# """
""" ############################ INSTANTIATE MODELS ############################# """
""" ############################################################################# """

# create database
database = database_sim.Database(filename=DIR["DATABASE_FILENAME"])
_logger.info(f"Database {database.eid} created.")

# create grid
grid = getattr(grid_sim, sim_config_grid["model"])(gridfile=DIR["GRID_DATA"])
_logger.info(f"Grid {grid.eid} created.")
# get grid elements
grid_comps = sim_help.get_grid_components(grid_comps_list=grid.children)

# create forecast model
if scenario_config["use_forecast"]:
    forecast = forecast_sim.Forecast.create(1)[0]
    _logger.info(f"Forecast entity {forecast.eid} created.")

# create all models based on given SIM_CONFIG
dict_entities = sim_help.create_entities(
    sim_config=SIM_CONFIG,
    model_data=model_data,
    dict_simulators=dict_simulators,
    grid_model_config=grid_model_config,
)


""" ############################################################################# """
""" ############################# CONNECT ENTITIES ############################## """
""" ############################################################################# """

# connect created models by means of their connection to the grid and each other
sim_help.connect_entities_in_grid(
    grid_model_config=grid_model_config,
    grid=grid,
    grid_loads=grid_comps["load"],
    world=world,
    model_connect_conf=model_connect_config,
    dict_entities=dict_entities,
    dict_simulators=dict_simulators,
    architecture_models=["RetailElectricityProvider", "GridEMS", "MarketDayAheadCSV"],
)

# add connections to forecast
if scenario_config["use_forecast"]:
    sim_help.connect_to_forecast(
        world=world,
        dict_entities=dict_entities,
        dict_simulators=dict_simulators,
        forecast=forecast,
        forecast_sim=forecast_sim,
        connect_models_names=FORECAST_MODELS_NAMES,
    )

# check whether all simulated entities are connected to at least one other entity
sim_help.check_entities_connections(world=world, dict_entities=dict_entities)

# connect all models to database based on SIM_CONFIG and created models
sim_help.connect_entities_to_db(
    sim_config=SIM_CONFIG, world=world, database=database, dict_entities=dict_entities
)

# connect grid components to database
sim_help.connect_grid_to_db(
    sim_config_grid=sim_config_grid, world=world, database=database, dict_comps=grid_comps
)


""" ############################################################################# """
""" ############################## RUN SIMULATION ############################### """
""" ############################################################################# """

world.run(until=scenario_config["n_steps"], print_progress=True)


""" ############################################################################# """
""" ########################## PLOT SIMULATION GRAPHS ########################### """
""" ############################################################################# """

sim_help.create_plots(world=world, dir_graphs=DIR["GRAPHS"], bool_plot=scenario_config["bool_plot"])
