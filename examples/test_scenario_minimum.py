"""
Scenario file for the 'minimum example scenario' with a residential household including pv-system,
battery storage and household baseload with an energy management system.

Within this test scenario mosaik is used as the co-simulation orchestrator.
Copyright (c) LGPL

| Author: elenia@TUBS
| Copyright 2024 elenia
| This file is part of eELib, which is free software under the terms of the GNU GPL Version 3.
"""

import os
import json
import mosaik
import mosaik.util
from datetime import datetime
import eelib.utils.simulation_helper as sim_help
from eelib.model_connections.connections import get_default_connections
from eelib.utils.logging_helpers import set_console_logger
import arrow
import logging

set_console_logger(logging.INFO)

_logger = logging.getLogger(__name__)


""" ############################################################################# """
""" ################################### SETUP ################################### """
""" ############################################################################# """

# define paths and filenames
DIR = dict()
DIR["DATA"] = os.path.join(os.path.realpath(os.path.dirname(__file__)), "data")
DIR["RESULTS"] = os.path.join(DIR["DATA"], "results")
if not os.path.exists(DIR["RESULTS"]):  # create result directory if necessary
    os.makedirs(DIR["RESULTS"])
DIR["GRAPHS"] = os.path.join(DIR["RESULTS"], "sim_graphs")
if not os.path.exists(DIR["GRAPHS"]):  # create graph directory if necessary
    os.makedirs(DIR["GRAPHS"])
DIR["MODEL_DATA"] = os.path.join(DIR["DATA"], "model_data_scenario", "model_data_minimum.json")
if not os.path.isfile(DIR["MODEL_DATA"]):  # check if MODEL_DATA is valid
    raise FileNotFoundError("Model data for scenario not found!")
DIR["DATABASE_FILENAME"] = os.path.join(
    DIR["RESULTS"], f"results_{datetime.now().strftime('%Y-%m-%d-%H-%M-%S')}.hdf5"
)
# log paths
_logger.info(f"Selected model data: {DIR['MODEL_DATA']}")
_logger.info(f"Results will be stored in {DIR['DATABASE_FILENAME']}")


# Sim config.: Simulators and their used model types with the properties to store into DB
SIM_CONFIG = {
    # used database, will be left out for model creation and connections
    "DBSim": {"python": "eelib.data.database.hdf5:Hdf5Database"},
    # all the used simulators and their models for this simulation
    "HEMSSim": {
        "python": "eelib.core.control.hems.hems_simulator:Sim",
        "models": {"HEMS_default": ["p_balance"]},
    },
    "CSVSim": {
        "python": "eelib.data.csv_reader.csv_reader_simulator:Sim",
        "models": {
            "HouseholdCSV": ["p"],
            "PvCSV": ["p", "q"],
        },
    },
    "BSSSim": {
        "python": "eelib.core.devices.bss.bss_simulator:Sim",
        "models": {"BSS": ["soc", "e_bat", "p"]},
    },
}


# Configuration of scenario: time and granularity
START = "2023-05-01 00:00:00"
END = "2023-05-15 00:00:00"
STEP_SIZE_IN_SECONDS = 900  # 1=sec-steps, 3600=hour-steps, 900=15min-steps, 600=10min-steps
N_SECONDS = int(
    (
        arrow.get(END, "YYYY-MM-DD HH:mm:ss") - arrow.get(START, "YYYY-MM-DD HH:mm:ss")
    ).total_seconds()
)
N_STEPS = int(N_SECONDS / STEP_SIZE_IN_SECONDS)
scenario_config = {
    "start": START,  # time of beginning for simulation
    "end": END,  # time of ending
    "step_size": STEP_SIZE_IN_SECONDS,
    "n_steps": N_STEPS,
    "use_forecast": False,
    "bool_plot": True,
}

# Read Scenario file with data for model entities
with open(DIR["MODEL_DATA"]) as f:
    model_data = json.load(f)  # TODO: new file with only 4 models

# Read configuration file with data for connections between prosumer devices
model_connect_config = get_default_connections()

# Create world (debugging needs to be on, otherwise plotting after simulation not possible)
world = mosaik.World(SIM_CONFIG, debug=True)


""" ############################################################################# """
""" ############################# START SIMULATORS ############################## """
""" ############################################################################# """

# start database
simulator_db = world.start("DBSim", scenario_config=scenario_config)
_logger.info("ModelFactory for Database is started.")

# create all simulators (model factories) with mosaik
simulator_hems = world.start(sim_name="HEMSSim", scenario_config=scenario_config)
simulator_csv_house = world.start(sim_name="CSVSim", scenario_config=scenario_config)
simulator_csv_pv = world.start(sim_name="CSVSim", scenario_config=scenario_config)
simulator_bss = world.start(sim_name="BSSSim", scenario_config=scenario_config)
_logger.info("ModelFactories for all simulators are started.")

""" ############################################################################# """
""" ############################ INSTANTIATE MODELS ############################# """
""" ############################################################################# """

# create database
database = simulator_db.Database(filename=DIR["DATABASE_FILENAME"])
_logger.info(f"Database {database.eid} created.")

# create all model entities
entity_hems = simulator_hems.HEMS_default.create(num=1, init_vals=model_data["HEMS_default"])[0]
entity_csv_house = simulator_csv_house.HouseholdCSV.create(
    num=1, init_vals=model_data["HouseholdCSV"]
)[0]
entity_csv_pv = simulator_csv_pv.PvCSV.create(num=1, init_vals=model_data["PvCSV"])[0]
entity_bss = simulator_bss.BSS.create(num=1, init_vals=model_data["BSS"])[0]
_logger.info("All Model Entities are created.")


""" ############################################################################# """
""" ############################# CONNECT ENTITIES ############################## """
""" ############################################################################# """

# connect all model entities to the HEMS (first entity is source, second is destination)
world.connect(entity_csv_house, entity_hems, "p", weak=False)
world.connect(entity_csv_pv, entity_hems, *[("p", "p")], weak=False)
world.connect(entity_bss, entity_hems, *[("bss_data", "bss_data")], weak=False)
# connect HEMS to BSS to send set values (weak connection, s.t. the other direction comes first)
world.connect(entity_hems, entity_bss, *[("p_set_storage", "p_set")], weak=True)
simulator_hems.add_controlled_entity(
    entity_hems.eid,
    {
        entity_bss.eid: {
            "eid": entity_bss.eid,
            "full_id": entity_bss.full_id,
            "type": entity_bss.type,
        }
    },
)
_logger.info("Model Entities are connected to each other.")

# connect all models to database
world.connect(entity_csv_house, database, *["p"])
world.connect(entity_csv_pv, database, *["p"])
world.connect(entity_bss, database, *["p", "soc", "e_bat"])
world.connect(entity_hems, database, *["p_balance", "p_demand", "p_generation"])
_logger.info("Model Entities are connected to the database.")


""" ############################################################################# """
""" ############################## RUN SIMULATION ############################### """
""" ############################################################################# """

world.run(until=scenario_config["n_steps"], print_progress=True)


""" ############################################################################# """
""" ########################## PLOT SIMULATION GRAPHS ########################### """
""" ############################################################################# """

sim_help.create_plots(world=world, dir_graphs=DIR["GRAPHS"], bool_plot=scenario_config["bool_plot"])
