"""
Copyright 2024 elenia
This file is part of eELib, which is free software under the terms of the GNU GPL Version 3.
"""
from setuptools import setup, find_packages

setup(
    name='eELib',
    version='0.0',
    packages=find_packages(exclude=['tests*']),
    install_requires=[
        'mosaik>=3.1',
        'pandas>=1.5.3',
        'arrow>=1.2.3',
    ]
)
